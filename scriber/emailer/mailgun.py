import json, logging, requests, datetime
from operator import itemgetter
from django.conf import settings
from django.template.loader import render_to_string
logger = logging.getLogger(__name__)

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.db import connection
from emailer.snowball import daily_snowball_data
from premailer import transform
from django.template.loader import render_to_string
from dashboard.utils import graph_followed_apps_data_for_user_id, graph_followed_apps_estimates_for_user_id
from customers.models import Customer, CustomerAppFollow
from appinfo import fetcher
from collections import defaultdict

''' mailgun constants '''
_EMAIL_FROM_ADDRESS = 'App Theta <stats@apptheta.com>'
_EMAIL_SUBJECT = 'App Theta - Daily Sales Report'
_MAILGUN_API_KEY = 'key-9185b23dbedb9a9df5c02800c71990bf'
_MAILGUN_DOMAIN = 'mg.apptheta.com'
_MAILGUN_SANDBOX_DOMAIN = 'sandbox704c05add86a4083b91657e94f60b8bc.mailgun.org'


def send_daily_email(recipientVariablesDict):
  if settings.ENABLE_MAILGUN:
    return _send_daily_email(recipientVariablesDict)
  else:
    return _skip_send_daily_email(recipientVariablesDict)


def _send_daily_email(recipientVariablesDict):
  customer_data = recipientVariablesDict
  # don't send the email if they toggled the option not to
  if not customer_data['customer']['send_daily_report']:
    return

  email_for_report = customer_data['email']
  customer_data['customer'] = sort_apps(customer_data['customer'])
  customer_data = update_customer_info_with_percents(customer_data)
  customer_data = update_customer_info_with_follows(email_for_report, customer_data)
  html = transform(render_to_string('daily-report-email.html', {'info':customer_data['customer'], 'email':email_for_report}))
  
  plain_text = render_to_string('daily-report-email-plaintext.html', 
    {'info':customer_data} )

  response = requests.post(
      "https://api.mailgun.net/v2/%s/messages" % _MAILGUN_DOMAIN,
      auth=("api", _MAILGUN_API_KEY),
      data={"from": _EMAIL_FROM_ADDRESS,
            "to": [recipientVariablesDict['email']],
            "subject": _EMAIL_SUBJECT,
            "text": plain_text,
            "html": html
           })

  return response


def _skip_send_daily_email(recipientVariablesDict):
  logger.info("Skipping daily email distribution since Mailgun is disabled")
  return requests.Response()


def send_batch_daily_email(recipientVariablesDict):
  if settings.ENABLE_MAILGUN:
    return _send_batch_daily_email(recipientVariablesDict)
  else:
    return _skip_send_batch_daily_email(recipientVariablesDict)


# TODO(d-felix): Implement batch sending.
def _send_batch_daily_email(recipientVariablesDict):
  raise NotImplementedError('Batch email sending not yet available.')


def _skip_send_batch_daily_email(recipientVariablesDict):
  logger.info("Skipping daily email distribution since Mailgun is disabled")
  return requests.Response()


def update_record_with_percents(series_type, record):
  type_28_str = series_type + '_28d'
  type_7_str = series_type + '_7d'
  type_1_str = series_type + '_1d'

  # add percent change values
  type_28_str_percent = series_type + '_28d_percent'
  type_7_str_percent = series_type + '_7d_percent'
  type_1_str_percent = series_type + '_1d_percent'
  if record[series_type] is None:
    value = 0
  else:
    value = float(record[series_type])

  # add classes to show red/green
  type_28_class_str = type_28_str + '_class'
  type_7_class_str = type_7_str + '_class'
  type_1_class_str = type_1_str + '_class'
   
  # 28-day value
  if not record[series_type]:
    return record

  if record[type_28_str] > 0:
    record[type_28_str_percent] = (value-record[type_28_str])/record[type_28_str] * 100       
    if record[type_28_str_percent] < 0:
      record[type_28_class_str] = "negative-percent"       
    elif record[type_28_str_percent] > 0:
      record[type_28_class_str] = "positive-percent"       
  # 7-day value
  if record[type_7_str] > 0:
    record[type_7_str_percent] =  (value-record[type_7_str])/record[type_7_str] * 100      
    if record[type_7_str_percent] < 0:
      record[type_7_class_str] = "negative-percent"       
    elif record[type_7_str_percent] > 0:
      record[type_7_class_str] = "positive-percent"       
  # 1-day value
  if record[type_1_str] > 0:
    record[type_1_str_percent] =  (value-record[type_1_str])/record[type_1_str] * 100      
    if record[type_1_str_percent] < 0:
      record[type_1_class_str] = "negative-percent"       
    elif record[type_1_str_percent] > 0:
      record[type_1_class_str] = "positive-percent"       

  return record


def test_daily_email(request, email_for_report):
  # must be superuser or user for given email_for_report to see page

  if email_for_report != "team@gaiagps.com":
    if request.user.is_authenticated() and not request.user.is_superuser and request.user.email != email_for_report:
      return render(request, 'daily-report-email.html', {'info':{}})
    if not request.user.is_authenticated():
      return render(request, 'daily-report-email.html', {'info':{}})

  # fetch up whole snowball, then select one user from it
  customer_data = customer_data_for_email(email_for_report)

  customer_data['customer'] = sort_apps(customer_data['customer'])
  customer_data = update_customer_info_with_percents(customer_data)
  customer_data = update_customer_info_with_follows(email_for_report, customer_data)
  html = transform(render_to_string('daily-report-email.html', {'info':customer_data['customer'], 'email':email_for_report}))  
  
  return render(request, "daily-report-email-shell.html", {'html':html})


# grab the right customer record from the snowball dataset
def customer_data_for_email(email_for_report):
  customer_data = None
  for data in daily_snowball_data():
    # Fow now, send one email per Mailgun API call.
    for email in data:
      # skip empty emails that should only occur from test data
      if not len(email):
        continue
      if email == email_for_report:
        customer_data = {'customer': data[email]['customers'][0], 'email': email}
        break
  if not customer_data:
    customer_data = {'customer': {}, 'email': email_for_report}
  return customer_data


def sort_apps(customer_data):
  if 'apps' in customer_data:
    apps = customer_data['apps']
    logging.info(apps)
    apps = sorted(apps,key=itemgetter('downloads'), reverse=True)
    apps = sorted(apps,key=itemgetter('revenue'), reverse=True)
    for app in apps:
      app = update_record_with_percents('revenue', app) 
      app = update_record_with_percents('downloads', app) 
      if 'updates' in app:
        app = update_record_with_percents('updates', app) 
    customer_data['apps'] = apps
  else:
    customer_data['apps'] = []
  return customer_data


# grab the right customer record from the snowball dataset
def update_customer_info_with_percents(customer_data):
  if 'revenue' in customer_data['customer']:
    customer_data['customer'] = update_record_with_percents('revenue', customer_data['customer']) 
  if 'downloads' in customer_data['customer']:
    customer_data['customer'] = update_record_with_percents('downloads', customer_data['customer']) 
  if 'updates' in customer_data['customer']:
    customer_data['customer'] = update_record_with_percents('updates', customer_data['customer']) 
  return customer_data



# serialize followed_app queryset
def dict_for_followed_app_info(customer_object):
  now = datetime.datetime.now()
  yesterday = now - datetime.timedelta(days=29)
  now_seconds = (now-datetime.datetime(1970,1,1)).total_seconds()
  yesterday_seconds = (yesterday-datetime.datetime(1970,1,1)).total_seconds() 

  follow_app_info = graph_followed_apps_data_for_user_id(customer_object.auth_user.id, "US", yesterday_seconds, now_seconds)

  followed_app_dict = {}
  for app in follow_app_info:
    followed_app_dict[app['app_id']] = {}
    app_dict = followed_app_dict[app['app_id']]
    app_dict['app_id'] = app['app_id']
    app_dict['platform'] = app['platform']
    app_dict['platform_id'] = app['platform_id'];
    app_dict['series'] = [];
    for series in app['series_data']:

      series_dict = {}
      series_dict['rank'] = series['current'][len(series['current'])-1];

      series_dict['rank_1d'] = series['current'][len(series['current'])-2];
      if series_dict['rank_1d'] > series_dict['rank']:
        series_dict['rank_1d_class'] = "positive-percent"
      elif series_dict['rank_1d'] < series_dict['rank']:
        series_dict['rank_1d_class'] = "negative-percent"
      
      if len(series['current']) >= 8:
        series_dict['rank_7d'] = series['current'][len(series['current'])-8];
        if series_dict['rank_7d'] > series_dict['rank']:
          series_dict['rank_7d_class'] = "positive-percent"
        elif series_dict['rank_7d'] < series_dict['rank']:
          series_dict['rank_7d_class'] = "negative-percent"
      
      if len(series['current']) >= 29:
        series_dict['rank_28d'] = series['current'][len(series['current'])-29];
        if series_dict['rank_28d'] > series_dict['rank']:
          series_dict['rank_28d_class'] = "positive-percent"
        elif series_dict['rank_28d'] < series_dict['rank']:
          series_dict['rank_28d_class'] = "negative-percent"
        
      series_dict['timeForLastRank'] = series['times'][len(series['times'])-1];
      series_dict['device_type'] = series['device_type'];
      series_dict['category'] = series['category'];
      series_dict['category_type'] = series['category_type'];
      app_dict['series'].append(series_dict);

    app_dict['series'] = sorted(app_dict['series'],key=itemgetter('rank'), reverse=False)

    # add product metadata, ios only now
    info  = fetcher.fetch(app['app_id'], "App Store");
    app_dict['display_name'] = info.name
    app_dict['icon_url'] = info.icon_url
    app_dict['developer'] = info.developer
  
  # inflate with Nones so sorted works
  for app in follow_app_info:
    app_dict = {}
    app_dict['revenue'] = None
    app_dict['revenue_1d'] = None
    app_dict['revenue_7d'] = None
    app_dict['revenue_28d'] = None

    app_dict['downloads'] = None
    app_dict['downloads_1d'] = None
    app_dict['downloads_7d'] = None
    app_dict['downloads_28d'] = None

    followed_app_dict[app['app_id']]['estimates'] = app_dict

  follow_app_revenue_info = graph_followed_apps_estimates_for_user_id(
    customer_object.auth_user.id, 
    "US", 
    yesterday_seconds, 
    now_seconds, 
    'revenue')
  for app in follow_app_revenue_info:
    followed_app_dict[app['app_id']]['estimates'] = app
    current = app['series_data'][0]['current']
    app['revenue'] = current[len(current)-1] 
    app['revenue_1d'] = current[len(current)-2]
    app['revenue_7d'] = current[len(current)-8]
    app['revenue_28d'] = current[len(current)-29]
    update_record_with_percents('revenue', app)


  follow_app_downloads_info = graph_followed_apps_estimates_for_user_id(
    customer_object.auth_user.id, 
    "US", 
    yesterday_seconds, 
    now_seconds, 
    'downloads')
  logging.info(follow_app_downloads_info)
  for app in follow_app_downloads_info:
    old_app = followed_app_dict[app['app_id']]['estimates']
    current = app['series_data'][0]['current']
    old_app['downloads'] = current[len(current)-1] 
    old_app['downloads_1d'] = current[len(current)-2]
    old_app['downloads_7d'] = current[len(current)-8]
    old_app['downloads_28d'] = current[len(current)-29]
    update_record_with_percents('downloads', old_app)

  return followed_app_dict

def update_customer_info_with_follows(email_for_report, customer_data):
  customer_object = Customer.objects.get(auth_user__email=email_for_report)
  followed_app_dict = dict_for_followed_app_info(customer_object)    

  app_list = []
  # sort followed apps by revenue
  for key, app in followed_app_dict.iteritems():
    app_list.append(app)
  app_list = sorted(app_list,key=lambda x: x['estimates']['revenue'], reverse=True)
  # add the followed apps data to the customer info
  customer_data['customer']['followed_apps'] = app_list
  return customer_data

