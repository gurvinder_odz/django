from __future__ import absolute_import

import json

from celery import group, task
from celery.utils.log import get_task_logger
from core.celeryutils import exponential_backoff
from core.utils import unpack
from customers.models import CustomerExternalLoginInfo
from dashboard.utils import graph_followed_apps_data_for_user_id
from datetime import date, datetime, time, timedelta
from dateutil import parser
from emailer.mailgun import send_daily_email, send_batch_daily_email
from emailer.snowball import daily_snowball_data
from metrics.models import DailyReportAvailabilityTime
from pytz import timezone, utc


logger = get_task_logger(__name__)

MAX_DAILY_EMAIL_WEST_COAST_SEND_TIME = time(16, 30, 0)

_THIRTY_MINUTES_IN_SECS = 60 * 30

_MAX_RETRIES = 5

@task(max_retries=30, ignore_result=True, bind=True)
def daily_email_kickoff(self, *args, **kwargs):
  # TODO(d-felix): Call this function hourly and only create tasks for customers with appropriate
  # time zone settings.
  # TODO(d-felix): Exclude customers who have opted out of daily email, instead of in mailgun.py
  try:
    (dateStr,) = unpack(kwargs, 'date_str')
    execDate = parser.parse(dateStr).date() if dateStr else datetime.utcnow().date()
    reportDate = execDate - timedelta(days=1)
    itcReadyTime = DailyReportAvailabilityTime.objects.filter(
        external_service=CustomerExternalLoginInfo.ITUNES_CONNECT, report_date=reportDate).first()
    gcReadyTime = DailyReportAvailabilityTime.objects.filter(
        external_service=CustomerExternalLoginInfo.GOOGLE_CLOUD, report_date=reportDate).first()
    maxWestCoastSendTime = datetime.combine(execDate,
        MAX_DAILY_EMAIL_WEST_COAST_SEND_TIME).replace(tzinfo=timezone('America/Los_Angeles'))
    westCoastTime = datetime.utcnow().replace(tzinfo=utc).astimezone(
        timezone('America/Los_Angeles'))

    if (itcReadyTime is None or gcReadyTime is None) and (
        westCoastTime < maxWestCoastSendTime):
      raise Exception('Daily reports have not yet been retrieved')

    emailData = daily_snowball_data()

    taskList = []
    for data in emailData:
      # Fow now, send one email per Mailgun API call.
      for email in data:
        # skip empty emails that should only occur from test data
        if not len(email):
          continue
        for customer in data[email]['customers']:
          # skip anyone who doesn't have data yet
          if not 'apps' in customer or len(customer['apps']) == 0:
            cid = customer['customer_id']
            customer_obj = Customer.objects.get(customer_id=cid)
            now = datetime.now()
            yesterday = now - timedelta(days=29)
            now_seconds = (now-datetime(1970,1,1)).total_seconds()
            yesterday_seconds = (yesterday-datetime(1970,1,1)).total_seconds() 
            follow_app_info = graph_followed_apps_data_for_user_id(customer_obj.auth_user.id, "US", yesterday_seconds, now_seconds)
            if len(follow_app_info) == 0:
              continue
          customerData = {'customer': customer, 'email': email}
          taskList.append(daily_email.s(customerData))

    emailTaskGroup = group(taskList)
    emailTaskGroup.apply_async()

  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=_THIRTY_MINUTES_IN_SECS)

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def daily_email(self, data):
  try:
    response = send_daily_email(data)
    if not response.ok:
      raise Exception("Attempt to send daily email batch failed with response text: %s"
          % response.json()['message'])
  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def batch_daily_email(self, data):
  try:
    response = send_batch_daily_email(data)
    if not response.ok:
      raise Exception("Attempt to send daily email batch failed with response text: %s"
          % response.json()['message'])
  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))
