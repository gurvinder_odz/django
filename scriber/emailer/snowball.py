import logging

from appinfo.constants import ANDROID_DEFAULT_ICON_URL, ANDROID_PLATFORM_STRING, APPLE_DEFAULT_ICON_URL, IOS_PLATFORM_STRING
from core.timeutils import DATE_FMT
from datetime import timedelta
from django.db import connection
from django.shortcuts import render
from ingestor.models import PlatformType
from operator import itemgetter


logger = logging.getLogger(__name__)

# The maximum number of recipients for a single mailgun API call.
_MAX_EMAIL_RECIPIENTS = 1000

SINGLE_CUSTOMER_DAILY_EMAIL_QUERY = """
  SELECT
    s.customer_id customer_id,
    s.organization organization,
    s.email email,
    s.send_daily_report send_daily_report,
    s.date date,
    s.today_for_customer today_for_customer,
    cm.dau customer_dau,
    cm.mau customer_mau,
    s.platform_app_id platform_app_id,
    s.platform_app_str platform_app_str,
    s.platform_type_str,
    (CASE WHEN (s.blended_iap_revenue IS NULL) THEN 0 ELSE s.blended_iap_revenue END) +
      (CASE WHEN (sm2.developer_revenue_usd_micros IS NULL) THEN
      (CASE WHEN (pm2.developer_revenue_usd_micros IS NULL) THEN 0
      ELSE pm2.developer_revenue_usd_micros END)
      ELSE sm2.developer_revenue_usd_micros END) app_revenue,
    pam.dau app_dau,
    pam.mau app_mau,
    sm2.downloads downloads,
    sm2.refunds refunds,
    sm2.updates updates
  FROM (
    SELECT
      c.customer_id customer_id,
      c.organization organization,
      c.email email,
      c.date date,
      pa.platform_app_id platform_app_id,
      pa.platform_app_str platform_app_str,
      pt.platform_type_str platform_type_str,
      CASE WHEN(subquery.apple_identifier IS NOT NULL) THEN true ELSE false END active_ios_app,
      SUM(pm.developer_revenue_usd_micros) realtime_iap_revenue,
      SUM(sm.developer_revenue_usd_micros) report_iap_revenue,
      SUM(CASE WHEN (sm.developer_revenue_usd_micros IS NOT NULL) THEN
        sm.developer_revenue_usd_micros ELSE pm.developer_revenue_usd_micros END)
        blended_iap_revenue,
      MAX(CASE WHEN(c.send_daily_report) THEN 1 ELSE 0 END) send_daily_report,
      MAX(c.today_for_customer) today_for_customer
    FROM (
      SELECT
        c1.customer_id customer_id,
        c1.name organization,
        c1.send_daily_report send_daily_report,
        au.email email,
        (NOW() AT TIME ZONE timezone)::date AS today_for_customer,
        ((NOW() AT TIME ZONE timezone) - dates.delta)::date
      FROM
        customers c1 JOIN auth_user au ON c1.auth_user = au.id
        CROSS JOIN (
          SELECT
            CASE WHEN generate_series=0 THEN INTERVAL '1 days'
            WHEN generate_series=1 THEN INTERVAL '2 days'
            WHEN generate_series=2 THEN INTERVAL '8 days'
            ELSE INTERVAL '29 days' END AS delta
          FROM
            GENERATE_SERIES(0, 3)
        ) dates
    ) c
    LEFT JOIN platform_apps pa ON c.customer_id = pa.customer_id
    LEFT JOIN channel_products cp0 on pa.platform_app_id = cp0.platform_app_id
    LEFT JOIN (
      SELECT DISTINCT
        li.customer_id customer_id,
        li.apple_vendor_id apple_vendor_id,
        arr.apple_identifier apple_identifier
      FROM customer_external_login_info li
        JOIN apple_reports ar ON (li.apple_vendor_id = ar.vendor_id)
        JOIN apple_report_records arr ON (ar.report_id = arr.report_id)
      WHERE
        li.is_active = true
    ) subquery ON (c.customer_id = subquery.customer_id AND cp0.apple_identifier = subquery.apple_identifier)
    LEFT JOIN platform_types pt ON pa.platform_type_id = pt.platform_type_id
    LEFT JOIN channels ch ON pa.platform_app_id = ch.platform_app_id
    LEFT JOIN channel_products cp ON ch.channel_id = cp.channel_id
    LEFT JOIN daily_product_metrics pm
    ON (cp.channel_product_id = pm.channel_product_id and c.date = pm.date)
    LEFT JOIN daily_sales_report_metrics sm
    ON (cp.channel_product_id = sm.channel_product_id and c.date = sm.date)
    GROUP BY 1, 2, 3, 4, 5, 6, 7, 8
  ) s
  LEFT JOIN channel_products cp2 ON s.platform_app_id = cp2.platform_app_id
  LEFT JOIN daily_product_metrics pm2
  ON (cp2.channel_product_id = pm2.channel_product_id and s.date = pm2.date)
  LEFT JOIN daily_sales_report_metrics sm2
  ON (cp2.channel_product_id = sm2.channel_product_id and s.date = sm2.date)
  LEFT JOIN daily_platform_app_metrics pam
  ON (s.platform_app_id = pam.platform_app_id and s.date = pam.date)
  LEFT JOIN daily_customer_metrics cm
  ON (s.customer_id = cm.customer_id and s.date = cm.date)
  WHERE
    (s.platform_type_str != 'iOS' OR s.active_ios_app = true)
  ;
  """

CUSTOMER_NONAPP_DAILY_EMAIL_QUERY = """
  SELECT
    s.customer_id,
    s.organization organization,
    s.email email,
    s.date date,
    cp.channel_product_id channel_product_id,
    cp.channel_product_str channel_product_str,
    pt.platform_type_str,
    SUM(CASE WHEN (sm.developer_revenue_usd_micros IS NOT NULL) THEN sm.developer_revenue_usd_micros
      ELSE pm.developer_revenue_usd_micros END) blended_revenue,
    SUM(CASE WHEN(sm.downloads IS NOT NULL) THEN sm.downloads ELSE 0 END) downloads,
    MAX(CASE WHEN(c.send_daily_report) THEN 1 ELSE 0 END) send_daily_report,
    MAX(s.today_for_customer) today_for_customer
  FROM (
    SELECT
      c1.customer_id customer_id,
      c1.name organization,
      c1.send_daily_report send_daily_report,
      au.email email,
      (NOW() AT TIME ZONE timezone)::date AS today_for_customer,
      ((NOW() AT TIME ZONE timezone) - dates.delta)::date
    FROM
      customers c1 JOIN auth_user au ON c1.auth_user = au.id
      CROSS JOIN (
        SELECT
          CASE WHEN generate_series=0 THEN INTERVAL '1 days'
          WHEN generate_series=1 THEN INTERVAL '2 days'
          WHEN generate_series=2 THEN INTERVAL '8 days'
          ELSE INTERVAL '29 days' END AS delta
        FROM
          GENERATE_SERIES(0, 3)
      ) dates
  ) s
  JOIN customers c on s.customer_id = c.customer_id
  JOIN channel_products cp on c.customer_id = cp.customer_id
  JOIN channels ch ON cp.channel_id = ch.channel_id
  JOIN platform_types pt on ch.platform_type_id = pt.platform_type_id
  LEFT JOIN daily_product_metrics pm
  ON (cp.channel_product_id = pm.channel_product_id and s.date = pm.date)
  LEFT JOIN daily_sales_report_metrics sm
  ON (cp.channel_product_id = sm.channel_product_id and s.date = sm.date)
  WHERE
    cp.platform_app_id IS NULL
    AND ch.platform_app_id IS NULL
  GROUP BY 1, 2, 3, 4, 5, 6, 7
  ;
"""

BULK_APP_INFO_QUERY = """
  SELECT
    r.app,
    r.platform,
    r.name,
    r.icon_url
  FROM (
    SELECT
      app,
      platform,
      max(fetch_time) latest_fetch_time
    FROM
      app_info
    GROUP BY 1, 2
  ) l
  JOIN app_info r ON (l.app = r.app AND l.platform = r.platform AND l.latest_fetch_time = r.fetch_time)
  ;
  """

# TODO(d-felix): Modify the queries so that this function can accept a date argument.
def daily_snowball_data():
  cursor = connection.cursor()
  cursor.execute(SINGLE_CUSTOMER_DAILY_EMAIL_QUERY)
  appQueryResults = [r for r in cursor.fetchall()]
  cursor.execute(CUSTOMER_NONAPP_DAILY_EMAIL_QUERY)
  nonAppQueryResults = [r for r in cursor.fetchall()]
  cursor.close()

  snowballData = extract_email_data(appQueryResults, nonAppQueryResults)
  return snowballData


def extract_email_data(appData, nonAppData):
  emailData = [{}]
  for row in appData:
    process_record(row, emailData, True)

  for row in nonAppData:
    process_record(row, emailData, False)

  # Augment the data with app name and icon URL.
  # TODO(d-felix): Offer optimized bulk querying from the appinfo app.
  appInfo = {}
  cursor = connection.cursor()
  cursor.execute(BULK_APP_INFO_QUERY)
  for row in cursor.fetchall():
    app, platform, name, url = row
    key = (app, platform)
    appInfo[key] = {'name': name, 'url': url}

  for data in emailData:
    for email in data:
      for customer in data[email]['customers']:
        for app in customer['apps']:
          key = None
          if app['platform'] == PlatformType.IOS:
            key = (app['name'], IOS_PLATFORM_STRING)
          elif app['platform'] == PlatformType.ANDROID:
            key = (app['name'], ANDROID_PLATFORM_STRING)
          if key in appInfo:
            app['display_name'] = appInfo[key]['name']
            app['icon_url'] = appInfo[key]['url']
  return emailData


def process_record(row, email_data, is_app_data=True):
  customerId = row[0]
  emailAddress = row[2]
  if not emailAddress:
    logger.info("Daily email data for customer %s missing valid email address" % customerId)
    return

  # Select the appropriate emailData entry to update.
  # Appends a new dictionary to email_data and returns -1 if no such index is found.
  indexToUpdate = index_to_update(email_data, emailAddress)

  update_email_data(email_data[indexToUpdate], email_data_dict_from_row(row, is_app_data))


# Returns the appropriate emailData entry to update
def index_to_update(emailData, emailAddress):
  for index in range(len(emailData)):
    if emailAddress in emailData[index] or len(emailData[index]) < _MAX_EMAIL_RECIPIENTS:
      return index
  emailData.append({})
  return -1


def email_data_dict_from_row(row, is_app_data=True):
  downloads = 0
  updates = 0
  refunds = 0
  aRev = 0
  if is_app_data:
    (customerId, organization, email, sendReport, date, todayForCustomer, cDau, cMau, appId,
        appName, appPlatform, aRev, aDau, aMau, downloads, refunds, updates) = row
    dateTodayStr = todayForCustomer.strftime(DATE_FMT)
    dateYesterdayStr = (todayForCustomer - timedelta(days=1)).strftime(DATE_FMT)
    cDau = cDau if cDau is not None else 0
    cMau = cMau if cMau is not None else 0
    aDau = aDau if aDau is not None else 0
    aMau = aMau if aMau is not None else 0
    aRev = (float(aRev) / 1000000.0) if aRev is not None else 0.0
    downloads = downloads if downloads is not None else 0
    refunds = refunds if refunds is not None else 0
    updates = updates if updates is not None else 0
  else:
    (customerId, organization, email, date, productId, appName, appPlatform, aRev, downloads,
        sendReport, todayForCustomer) = row
    appId = None
    dateTodayStr = todayForCustomer.strftime(DATE_FMT)
    dateYesterdayStr = (todayForCustomer - timedelta(days=1)).strftime(DATE_FMT)
    cDau = 0
    cMau = 0
    aDau = 0
    aMau = 0
    aRev = (float(aRev) / 1000000.0) if aRev is not None else 0.0
    downloads = downloads if downloads is not None else 0
    refunds = refunds if refunds is not None else 0
    updates = updates if updates is not None else 0

  iconUrl = None
  if appPlatform == 'iOS':
    iconUrl = APPLE_DEFAULT_ICON_URL
  elif appPlatform == 'Android':
    iconUrl = ANDROID_DEFAULT_ICON_URL

  revenue_0d = revenue_1d = revenue_7d = revenue_28d = 0.0
  downloads_0d = downloads_1d = downloads_7d = downloads_28d = 0
  updates_0d = updates_1d = updates_7d = updates_28d = 0

  daysAgo = (todayForCustomer - date).days - 1
  if daysAgo == 0:
    revenue_0d = aRev
    downloads_0d = downloads
    updates_0d = updates
  elif daysAgo == 1:
    revenue_1d = aRev
    downloads_1d = downloads
    updates_1d = updates
  elif daysAgo == 7:
    revenue_7d = aRev
    downloads_7d = downloads
    updates_7d = updates
  elif daysAgo == 28:
    revenue_28d = aRev
    downloads_28d = downloads
    updates_28d = updates

  app = {
    "app_id": appId,
    "name": appName,
    "platform": appPlatform,
    "dau": aDau,
    "mau": aMau,
    "revenue": revenue_0d,
    "revenue_1d": revenue_1d,
    "revenue_7d": revenue_7d,
    "revenue_28d": revenue_28d,
    "display_name": appName,
    "icon_url": iconUrl,
    "downloads": downloads_0d,
    "downloads_1d": downloads_1d,
    "downloads_7d": downloads_7d,
    "downloads_28d": downloads_28d,
    "refunds": refunds,
  }

  if appPlatform == 'iOS':
    app['updates'] = updates_0d
    app['updates_1d'] = updates_1d
    app['updates_7d'] = updates_7d
    app['updates_28d'] = updates_28d

  if not organization:
    organization = ""

  customer = {
    "customer_id": customerId,
    "organization": organization,
    "send_daily_report": sendReport,
    "date_yesterday": dateYesterdayStr,
    "date_today": dateTodayStr,
    "dau": cDau,
    "mau": cMau,
    # Prepopulate revenue, download, and update sums with the current app's values.
    "revenue": revenue_0d,
    "revenue_1d": revenue_1d,
    "revenue_7d": revenue_7d,
    "revenue_28d": revenue_28d,
    "downloads": downloads_0d,
    "downloads_1d": downloads_1d,
    "downloads_7d": downloads_7d,
    "downloads_28d": downloads_28d,
    "updates": updates_0d,
    "updates_1d": updates_1d,
    "updates_7d": updates_7d,
    "updates_28d": updates_28d,
    "apps": [app],
  }

  ret = {
    "email": email,
    "customers": [customer]
  }
  return ret

def update_email_data(data, result):
  emailAddress = result['email']
  partialResults = data.get(emailAddress, None)
  if partialResults is None:
    data[emailAddress] = result
    return
  resultCustomer = result['customers'][0]
  resultCustomerApp = resultCustomer['apps'][0]
  for idx in range(len(partialResults['customers'])):
    customer = partialResults['customers'][idx]
    if customer['customer_id'] == resultCustomer['customer_id']:
      customer['revenue'] += resultCustomer['revenue']
      customer['revenue_1d'] += resultCustomer['revenue_1d']
      customer['revenue_7d'] += resultCustomer['revenue_7d']
      customer['revenue_28d'] += resultCustomer['revenue_28d']
      customer['downloads'] += resultCustomer['downloads']
      customer['downloads_1d'] += resultCustomer['downloads_1d']
      customer['downloads_7d'] += resultCustomer['downloads_7d']
      customer['downloads_28d'] += resultCustomer['downloads_28d']
      customer['updates'] += resultCustomer['updates']
      customer['updates_1d'] += resultCustomer['updates_1d']
      customer['updates_7d'] += resultCustomer['updates_7d']
      customer['updates_28d'] += resultCustomer['updates_28d']

      for idx in range(len(customer['apps'])):
        app = customer['apps'][idx]
        if app['app_id'] == resultCustomerApp['app_id']:
          app['revenue'] += resultCustomerApp['revenue']
          app['revenue_1d'] += resultCustomerApp['revenue_1d']
          app['revenue_7d'] += resultCustomerApp['revenue_7d']
          app['revenue_28d'] += resultCustomerApp['revenue_28d']
          app['downloads'] += resultCustomerApp['downloads']
          app['downloads_1d'] += resultCustomerApp['downloads_1d']
          app['downloads_7d'] += resultCustomerApp['downloads_7d']
          app['downloads_28d'] += resultCustomerApp['downloads_28d']
          if 'updates' in resultCustomerApp:
            app['updates'] += resultCustomerApp['updates']
            app['updates_1d'] += resultCustomerApp['updates_1d']
            app['updates_7d'] += resultCustomerApp['updates_7d']
            app['updates_28d'] += resultCustomerApp['updates_28d']
          break
        elif idx == len(customer['apps']) - 1:
          customer['apps'].append(resultCustomerApp)

      break

    elif idx == len(partialResults['customers']) - 1:
      partialResults['customers'].append(resultCustomer)


def snowball(request):
  emailData = daily_snowball_data()

  total_downloads = 0
  total_revenue = 0
  total_mau = 0
  total_dau = 0
  total_sdk_users = 0
  total_sdk_apps = 0
  users_with_apps = {}
  apps = []
  real_apps = []

  app_filter = {}

  for data in emailData:
    for email in data:
      # skip empty emails that should only occur from test data
      if not len(email) or email == "dan@scriber.io":
        continue

      customer = data[email]['customers'][0]

      # skip andrew's duplicate accounts
      must_continue = False
      for app in customer['apps']:
        if app['app_id'] in app_filter:
          must_continue = True
      if must_continue:
        continue

      total_downloads += customer['downloads']
      total_revenue += customer['revenue']
      # tag each app with an email address
      mau_dau = 0
      for app in customer['apps']:
        if app['app_id']:
          # skip andrew's duplicate accounts
          if app['display_name'] == "scriber.io":
            continue
          if app['display_name'] == "gaiacloud":
            continue
          if not app['display_name'] in app_filter:
            total_mau += app['mau']
            total_dau += app['dau']
            mau_dau += app['mau'] + app['dau']
          if mau_dau > 0:
            total_sdk_apps += 1
          app_filter[app['app_id']] = True
          app['email'] = email
          app['organization'] = customer['organization']
          real_apps.append(app)
          if not email in users_with_apps:
            users_with_apps[email] = 1
          else:
            users_with_apps[email] += 1
      if mau_dau > 0:
        total_sdk_users += 1

  real_apps = sorted(real_apps, key=itemgetter('revenue'), reverse=True)
  
  superuser = False;
  if request.user.is_superuser:
    superuser = True
  info = {
    'mau':total_mau,
    'dau':total_dau,
    'superuser':superuser,
    'sdk_apps':total_sdk_apps,
    'sdk_users':total_sdk_users,
    'revenue':total_revenue,
    'downloads':total_downloads,
    'users_with_apps':users_with_apps,
    'apps':real_apps,
    'organization':"App Theta",
  }

  return render(request, 'daily-snowball.html', {'info':info})
