# Generates DAU metrics for a tuple of platform_apps using the customer-provided customer_user_id
# value to identify end users.
MULTIPLE_PLATFORM_APP_USER_ID_DAU_QUERY = """
  SELECT
    subquery.date date,
    COUNT(*) dau
  FROM (
    SELECT DISTINCT
      (e.time AT TIME ZONE %s)::date date,
      (CASE WHEN (u.customer_user_id = '') THEN CAST(u.scriber_id AS varchar) ELSE u.customer_user_id END) user_id
    FROM
      user_events e JOIN users u ON e.scriber_id = u.scriber_id
    WHERE
      e.platform_app_id IN %s
      AND e.time >= %s
      AND e.time < %s
      AND e.event_type IN (2, 3, 5, 9)
  ) subquery
  GROUP BY 1;
  """

# Calculates the number of active users over the specified time period for a tuple of platform_apps
# using the customer-provided customer_user_id value to identify end users.
MULTIPLE_PLATFORM_APP_USER_ID_ACTIVES_QUERY = """
  SELECT
    COUNT(*) actives
  FROM (
    SELECT DISTINCT
      (CASE WHEN (u.customer_user_id = '') THEN CAST(u.scriber_id AS varchar) ELSE u.customer_user_id END) user_id
    FROM
      user_events e JOIN users u ON e.scriber_id = u.scriber_id
    WHERE
      e.platform_app_id IN %s
      AND e.time >= %s
      AND e.time < %s
      AND e.event_type IN (2, 3, 5, 9)
  ) subquery;
  """

# Generates DAU metrics for a tuple of channel_products using the customer-provided
# customer_user_id value to identify end users. Only channel_products corresponding to
# platform_apps generate result records.
MULTIPLE_CHANNEL_PRODUCT_USER_ID_DAU_QUERY = """
  SELECT
    subquery.date date,
    COUNT(*) dau
  FROM (
    SELECT DISTINCT
      (e.time AT TIME ZONE %s)::date date,
      (CASE WHEN (u.customer_user_id = '') THEN CAST(u.scriber_id AS varchar) ELSE u.customer_user_id END) user_id
    FROM
      channel_products cp JOIN user_events e ON cp.platform_app_id = e.platform_app_id
      JOIN users u ON e.scriber_id = u.scriber_id
    WHERE
      cp.channel_product_id IN %s
      AND e.time >= %s
      AND e.time < %s
      AND e.event_type IN (2, 3, 5, 9)
  ) subquery
  GROUP BY 1;
  """

# Calculates the number of active users over the specified time period for a tuple of
# channel_products using the customer-provided customer_user_id value to identify end users.
# Only channel_products corresponding to platform_apps generate result records.
MULTIPLE_PLATFORM_APP_USER_ID_ACTIVES_QUERY = """
  SELECT
    COUNT(*) actives
  FROM (
    SELECT DISTINCT
      (CASE WHEN (u.customer_user_id = '') THEN CAST(u.scriber_id AS varchar) ELSE u.customer_user_id END) user_id
    FROM
      channel_products cp JOIN user_events e ON cp.platform_app_id = e.platform_app_id
      JOIN users u ON e.scriber_id = u.scriber_id
    WHERE
      cp.channel_product_id IN %s
      AND e.time >= %s
      AND e.time < %s
      AND e.event_type IN (2, 3, 5, 9)
  ) subquery;
  """
