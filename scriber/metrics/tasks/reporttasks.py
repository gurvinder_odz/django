from __future__ import absolute_import

import traceback

from appinfo import fetcher
from celery import chain, chord, group, task
from celery.utils.log import get_task_logger
from core.celeryutils import exponential_backoff
from core.db.constants import TEN_PLACES
from core.timeutils import from_timestamp, utc_epoch
from core.utils import unpack
from customers.models import Customer, CustomerExternalLoginInfo
from datetime import date, datetime, time, timedelta
from dateutil import parser
from decimal import Decimal
from django.core.cache import cache
from django.db import connection, IntegrityError, transaction
from django.db.models import Q
from ingestor.models import App, Channel, ChannelProduct, PlatformApp, PlatformType, Product
from metrics.constants import APPLE_REPORT_TERRITORY_TIMEZONES
from metrics.googlesalesreports import fetch_google_daily_sales_report, fetch_google_report
from metrics.models import DailyReportAvailabilityTime, DailySalesReportMetrics
from metrics.salesreports import fetch_apple_daily_sales_report, ReportUnavailableException
from ml.modelregistry import DAILY_REVENUE
from ml.tasks import train_model
from pytz import timezone, utc
from re import match

GOOGLE_CLOUD = CustomerExternalLoginInfo.GOOGLE_CLOUD
ITUNES_CONNECT = CustomerExternalLoginInfo.ITUNES_CONNECT

# The maximum number of times a task from this module may be retried.
_MAX_RETRIES = 7

# Probe daily reports status using login information under only our control.
_POLLING_BUCKET_ID = '00292849710855110990'
_POLLING_USERNAME = 'dan@scriber.io'

# Time constants used for report polling.
_4_AM = time(4, 0)
_6_AM = time(6, 0)
_10_PM = time(22, 0)
_10_MINUTES_SECS = 600
_30_MINUTES_SECS = 1800

logger = get_task_logger(__name__)

class ReportFetchExhaustionException(Exception):
  pass

def kickoff_retry_countdown(timezone):
  currentTime = from_timestamp(utc_epoch(), timezone).time()
  if currentTime <= _6_AM:
    return _10_MINUTES_SECS
  elif currentTime <= _10_PM:
    return _30_MINUTES_SECS
  else:
    return None

# TODO(d-felix): Merge this task and the territory task since we now plan to use different
# celery beat entries for different territories.
@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def daily_apple_metrics_kickoff(self, *args, **kwargs):
  try:
    (tzStr, ) = unpack(kwargs, 'timezone_str')
    tz = timezone(tzStr)
    todayLocal = from_timestamp(utc_epoch(), tz).date()
    reportDate = todayLocal - timedelta(days=1)
    etaDate = todayLocal
    etaDatetime = datetime.combine(etaDate, _4_AM)
    etaUtc = etaDatetime.replace(tzinfo=tz).astimezone(utc)

    kwargs = {'report_date_str': str(reportDate), 'timezone_str': tzStr}
    daily_apple_metrics_territory_kickoff.apply_async(None, kwargs, eta=etaUtc)

  except Exception as e:
    raise self.retry(exc=e, countdown=_10_MINUTES_SECS)

@task(max_retries=None, ignore_result=True, bind=True)
def daily_apple_metrics_territory_kickoff(self, *args, **kwargs):
  try:
    reportDateStr, timezoneStr = unpack(kwargs, 'report_date_str', 'timezone_str')

    # For now, silently succeed for non-US territories.
    if timezoneStr != 'America/Los_Angeles':
      return

    # parser.parse() returns a datetime.datetime object.
    reportDate = parser.parse(reportDateStr).date()

    # TODO(d-felix): When necessary, establish canonical logins for non-US territories.
    pollingLogin = CustomerExternalLoginInfo.objects.filter(
        customer_id__auth_user__username=_POLLING_USERNAME,
        external_service=CustomerExternalLoginInfo.ITUNES_CONNECT, is_active=True).first()
    pollingTimezone = timezone(pollingLogin.customer_id.timezone)

    # Raises a ReportUnavailableException if reports are not ready.
    report = fetch_apple_daily_sales_report(pollingLogin.username, pollingLogin.password,
        pollingLogin.apple_vendor_id, reportDate)

    # TODO(d-felix): Filter by relevent territory.
    logins = CustomerExternalLoginInfo.objects.filter(external_service=ITUNES_CONNECT,
        is_active=True)
    taskList = []
    for login in logins:
      taskList.append(daily_apple_metrics.s(login_id=login.login_info_id,
          report_date_str=reportDateStr))
    reportTaskGroup = group(taskList)
    reportTaskGroup.apply_async()

    # Record the availability time.
    utcNow = from_timestamp(utc_epoch(), utc)
    availTime = DailyReportAvailabilityTime(external_service=ITUNES_CONNECT, report_type='Sales',
        report_region=timezoneStr, report_date=reportDate, time=utcNow)
    try:
      availTime.save()
    except IntegrityError as e:
      logger.warning('Could not record apple sales report availability due to IntegrityError: %s'
          % e.message)

  except Exception as e:
    if 'pollingTimezone' in locals():
      tz = pollingTimezone
    else:
      logger.info('Could not identify time zone corresponding to auth user %s' %
          _POLLING_USERNAME)
      tz = timezone('America/Los_Angeles')
    retryCountdown = kickoff_retry_countdown(tz)
    if retryCountdown is None:
      raise ReportFetchExhaustionException(
          'Abandoning daily apple report fetching due to too many failures')
    else:
      raise self.retry(exc=e, countdown=retryCountdown)

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def batch_daily_apple_metrics(self, *args, **kwargs):
  try:
    loginId, startDateStr, endDateStr, doneToken = unpack(kwargs, 'login_id', 'start_date_str',
        'end_date_str', 'done_token')
    # parser.parse() returns a datetime.datetime object.
    startDate = parser.parse(startDateStr).date()
    endDate = parser.parse(endDateStr).date()
    taskList = []
    for delta in range((endDate - startDate).days + 1):
      reportDate = startDate + timedelta(days=delta)
      if doneToken:
        taskList.append(daily_apple_metrics_store_result.s(login_id=loginId,
            report_date_str=str(reportDate)))
      else:
        taskList.append(daily_apple_metrics.s(login_id=loginId, report_date_str=str(reportDate)))
    if doneToken:
      callback = all_done.s(done_token=doneToken, login_id=loginId)
      batchTasks = chord(taskList, callback)
    else:
      batchTasks = group(taskList)
    batchTasks.apply_async()
  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def daily_apple_metrics(self, *args, **kwargs):
  try:
    return daily_apple_metrics_inner_function(*args, **kwargs)
  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))

@task(max_retries=_MAX_RETRIES, ignore_result=False, bind=True)
def daily_apple_metrics_store_result(self, *args, **kwargs):
  try:
    return daily_apple_metrics_inner_function(*args, **kwargs)
  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))

def daily_apple_metrics_inner_function(*args, **kwargs):
  loginId, reportDateStr = unpack(kwargs, 'login_id', 'report_date_str')
  login = CustomerExternalLoginInfo.objects.get(pk=loginId)
  customer = login.customer_id
  channelProducts = [cp for cp in ChannelProduct.objects.filter(
      Q(platform_app_id__platform_type_id__platform_type_str=PlatformType.IOS) |
      Q(channel_id__platform_app_id__platform_type_id__platform_type_str=PlatformType.IOS) |
      Q(channel_id__platform_type_id__platform_type_str=PlatformType.IOS),
      customer_id=customer)]
  platformApps = [pa for pa in PlatformApp.objects.filter(customer_id=customer,
      platform_type_id__platform_type_str=PlatformType.IOS)]
  tz = timezone(customer.timezone)

  if reportDateStr is None:
    reportDate = from_timestamp(utc_epoch(), tz).date() - timedelta(days=1)
  else:
    # parser.parse() returns a datetime.datetime object.
    reportDate = parser.parse(reportDateStr).date()

  records = fetch_apple_daily_sales_report(login.username, login.password,
      login.apple_vendor_id, reportDate)

  # Produce dicts of known apple identifiers and bundle IDs.
  # These dicts are updated as new database entities are saved.
  identifierToProduct = {cp.apple_identifier: cp for cp in channelProducts if cp.apple_identifier}
  skuToProduct = {cp.apple_sku: cp for cp in channelProducts if cp.apple_sku}
  bundleIdToApp = {pa.platform_app_str: pa for pa in platformApps}

  # Create any unrecognized apps and their corresponding products.
  for record in records:
    if not record.is_app():
      continue
    identifier = record.apple_identifier
    sku = record.sku
    if identifier not in identifierToProduct:
      appInfo = fetcher.IOS_ID_APP_INFO_FETCHER.fetch(identifier)
      if appInfo is None:
        logger.warning('Could not retrieve app info for apple identifier %s' % identifier)
        continue
      if appInfo.app not in bundleIdToApp:
        # The app does not exist
        with transaction.atomic():
          app = App(customer_id=customer, name=appInfo.app)
          app.save()
          platformType = PlatformType.objects.filter(platform_type_str='iOS').first()
          platformApp = PlatformApp(platform_app_str=appInfo.app, platform_type_id=platformType,
            customer_id=customer, latest_sdk_version='', description=record.title, app_id=app)
          platformApp.save()
          bundleIdToApp[appInfo.app] = platformApp
      else:
        platformApp = bundleIdToApp[appInfo.app]

      # We now know that the app exists, but either:
      # 1) the product does not exist, or
      # 2) the product's apple_identifier field isn't populated.
      # We first examine channelProducts for any entries corresponding to the required app.
      # Note that the contents of channelProducts remain fixed as we iterate over the report
      # records, though newly created channelProducts generate new identifierToProduct entries.
      # Thus subsequent records for a new product will steer clear of this code branch, avoiding
      # IntegrityErrors.
      matchingProducts = [cp for cp in channelProducts if cp.platform_app_id == platformApp]
      if matchingProducts:
        # The product exists but doesn't have an apple_identifier.
        channelProduct = matchingProducts[0]
        channelProduct.apple_identifier = identifier
        channelProduct.apple_sku = record.sku
        channelProduct.amt_local = appInfo.amt_local
        channelProduct.amt_local_micros = appInfo.amt_local_micros
        channelProduct.amt_usd_micros = appInfo.amt_usd_micros
        channelProduct.currency_str = 'USD'
        channelProduct.save()
        identifierToProduct[identifier] = channelProduct
        skuToProduct[record.sku] = channelProduct
      else:
        # The product does not exist.
        with transaction.atomic():
          product = Product(customer_id=customer, description=appInfo.app)
          product.save()
          channel = Channel.objects.get(platform_type_id=platformApp.platform_type_id)
          channelProduct = ChannelProduct(channel_product_str=appInfo.app, channel_id=channel,
              platform_app_id=platformApp, description=record.title, product_id=product,
              customer_id=customer, apple_identifier=identifier, apple_sku=record.sku,
              amt_local=appInfo.amt_local, amt_local_micros=appInfo.amt_local_micros,
              amt_usd_micros=appInfo.amt_usd_micros, currency_str='USD')
          channelProduct.save()
          identifierToProduct[identifier] = channelProduct
          skuToProduct[record.sku] = channelProduct
    elif sku and sku not in skuToProduct:
      # We have an apple identifier, but not a sku. Update the product record.
      channelProduct = identifierToProduct[identifier]
      channelProduct.apple_sku = sku
      channelProduct.save()
      skuToProduct[sku] = channelProduct

  # Now that we've identified as many apps as possible, create any unrecognized
  # in-app products and app bundles.
  for record in records:
    if record.is_app():
      continue
    identifier = record.apple_identifier
    if record.is_in_app_purchase():
      if identifier not in identifierToProduct:
        # Either the product does not exist or its apple identifier field isn't populated.
        # Start by identifying the parent product.
        pSku = record.parent_identifier
        if pSku in skuToProduct:
          parentProduct = skuToProduct[pSku]
          # The parent product exists, but it may not have a payment channel entry yet.
          channel, created = Channel.objects.get_or_create(
              platform_app_id=parentProduct.platform_app_id)
          # Try to identify the IAP product using its parent's sku.
          channelProduct = ChannelProduct.objects.filter(channel_product_str=record.sku,
              channel_id=channel).first()
          if channelProduct is None:
            # The IAP product does not exist, so create one.
            with transaction.atomic():
              product = Product(customer_id=customer, description=record.title)
              product.save()
              amtLocal = None if record.units == Decimal('0') else (
                  record.developer_proceeds_usd / record.units).quantize(TEN_PLACES)
              amtLocalMicros = None if amtLocal is None else long(amtLocal * 1000000L)
              channelProduct = ChannelProduct(channel_product_str=record.sku, channel_id=channel,
                  description=record.title, product_id=product, customer_id=customer,
                  apple_identifier=identifier, apple_sku=record.sku, amt_local=amtLocal,
                  amt_local_micros=amtLocalMicros, amt_usd_micros=amtLocalMicros,
                  currency_str='USD')
              channelProduct.save()
              identifierToProduct[identifier] = channelProduct
              skuToProduct[record.sku] = channelProduct
          else:
            # The product exists after all, so we need to update its apple identifier
            channelProduct.apple_identifier = identifier
            channelProduct.apple_sku = record.sku
            channelProduct.save()
            identifierToProduct[identifier] = channelProduct
            skuToProduct[record.sku] = channelProduct
        else:
          # The parent product does not exist, or its apple_sku field isn't populated.
          # We cannot create a parent product since we don't have the bundle ID.
          logger.warning('Could not identify parent product by SKU: %s' % pSku)
          continue
    elif record.is_app_bundle():
      if identifier not in identifierToProduct:
        # Apple app bundle products always have apple_identifier populated.
        # The product does not exist, so we create it.
        with transaction.atomic():
          channel = Channel.objects.filter(platform_type_id__platform_type_str='iOS').first()
          product = Product(customer_id=customer, description=record.title)
          product.save()
          amtLocal = None if record.units == Decimal('0') else (
              record.developer_proceeds_usd / record.units).quantize(TEN_PLACES)
          amtLocalMicros = None if amtLocal is None else long(amtLocal * 1000000L)
          channelProduct = ChannelProduct(channel_product_str=record.sku, channel_id=channel,
              description=record.title, product_id=product, customer_id=customer,
              apple_identifier=identifier, apple_sku=record.sku, amt_local=amtLocal,
              amt_local_micros=amtLocalMicros, amt_usd_micros=amtLocalMicros, currency_str='USD')
          channelProduct.save()
          identifierToProduct[identifier] = channelProduct
          skuToProduct[record.sku] = channelProduct

  # Product identification is complete; process the report data.
  # Build a dict of instances to save.
  metricsDict = {}

  # Fetch any existing metrics records that would need to be updated instead of inserted.
  # Only fetch records for iOS platform.
  existingAppMetrics = DailySalesReportMetrics.objects.filter(
      customer_id=customer, date=reportDate,
      channel_product_id__channel_id__platform_type_id__platform_type_str='iOS')
  existingProductMetrics = DailySalesReportMetrics.objects.filter(
      customer_id=customer, date=reportDate,
      channel_product_id__channel_id__platform_app_id__platform_type_id__platform_type_str='iOS')

  # Update the dictionary with existing metrics entries, but zero out the metrics that
  # we intend to overwrite.
  for metric in existingAppMetrics:
    key = (metric.channel_product_id.channel_product_id, metric.date)
    metric.developer_revenue_usd_micros = 0L
    metric.recurring_revenue_usd_micros = 0L
    metric.quantity = 0L
    metric.downloads = 0
    metric.updates = 0
    metric.refunds = 0
    metricsDict[key] = metric
  for metric in existingProductMetrics:
    key = (metric.channel_product_id.channel_product_id, metric.date)
    metric.developer_revenue_usd_micros = 0L
    metric.recurring_revenue_usd_micros = 0L
    metric.quantity = 0L
    metric.downloads = 0
    metric.updates = 0
    metric.refunds = 0
    metricsDict[key] = metric

  # Process the records and update the dictionary.
  for record in records:
    identifier = record.apple_identifier
    if identifier not in identifierToProduct:
      continue
    channelProduct = identifierToProduct[identifier]
    channelProductId = channelProduct.channel_product_id
    key = (channelProductId, reportDate)
    if key not in metricsDict:
      metricsDict[key] = DailySalesReportMetrics(customer_id=customer,
          channel_product_id=channelProduct, date=reportDate,
          developer_revenue_usd_micros=0L, recurring_revenue_usd_micros=0L,
          quantity=0L, downloads=0, updates=0, refunds=0)
    developerProceedsUsdMicros = long(record.developer_proceeds_usd * 1000000)
    metric = metricsDict[key]
    metric.developer_revenue_usd_micros += developerProceedsUsdMicros
    metric.quantity += long(record.units)
    if record.is_recurrence():
      metric.recurring_revenue_usd_micros += developerProceedsUsdMicros
    if record.is_download():
      metric.downloads += int(record.units)
    if record.is_update():
      metric.updates += int(record.units)
    if record.is_refund():
      metric.refunds += int(abs(record.units))

  # Write results to the database
  with transaction.atomic():
    for key in metricsDict:
      metricsDict[key].save()


# TODO(d-felix) Improve the retry schedule once we have sufficient data.
@task(max_retries=48, ignore_result=True, bind=True)
def daily_google_metrics_kickoff(self, *args, **kwargs):
  try:
    todayUtc = date.today()
    reportDate = todayUtc - timedelta(days=1)
    reportDateStr = str(reportDate)
    tbLogin = CustomerExternalLoginInfo.objects.filter(gc_bucket_id=_POLLING_BUCKET_ID,
        is_active=True).first()

    # fetch_google_daily_sales_report may succeed without returning any records.
    # This can happen if the monthly report zip file is available for download,
    # but no records matching the provided reportDate are found.
    records = fetch_google_daily_sales_report(tbLogin.refresh_token, tbLogin.gc_bucket_id,
        reportDate)
    if not records:
      raise ReportUnavailableException(
          'Google report fetch succeeded but no records were returned.')

    logins = CustomerExternalLoginInfo.objects.filter(external_service=GOOGLE_CLOUD,
        refresh_token__isnull=False, is_active=True)
    taskList = []
    for login in logins:
      taskList.append(batch_daily_google_metrics.s(login_info_id=login.login_info_id,
          start_date_str=reportDateStr, end_date_str=reportDateStr))
    reportTaskGroup = group(taskList)
    reportTaskGroup.apply_async()

    # Record the availability time.
    utcNow = from_timestamp(utc_epoch(), utc)
    availTime = DailyReportAvailabilityTime(external_service=GOOGLE_CLOUD, report_type='sales',
        report_date=reportDate, time=utcNow)
    try:
      availTime.save()
    except IntegrityError as e:
      logger.warning('Could not record google sales report availability due to IntegrityError: %s'
          % e.message)

  except Exception as e:
    raise self.retry(exc=e, countdown=_30_MINUTES_SECS)


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def batch_daily_google_metrics(self, *args, **kwargs):
  try:
    batch_tasks = chain(
        batch_daily_google_sales_metrics.s(**kwargs),
        batch_daily_google_installation_metrics.s(**kwargs))
    batch_tasks.apply_async()
  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def batch_daily_google_sales_metrics(self, *args, **kwargs):
  try:
    loginInfoId, startDateStr, endDateStr, doneToken = unpack(kwargs, 'login_info_id',
        'start_date_str', 'end_date_str', 'done_token')
    login = CustomerExternalLoginInfo.objects.get(pk=loginInfoId)
    customer = login.customer_id
    channelProducts = [cp for cp in ChannelProduct.objects.filter(
        Q(platform_app_id__platform_type_id__platform_type_str=PlatformType.ANDROID) |
        Q(channel_id__platform_app_id__platform_type_id__platform_type_str=PlatformType.ANDROID) |
        Q(channel_id__platform_type_id__platform_type_str=PlatformType.ANDROID),
        customer_id=customer)]
    platformApps = [pa for pa in PlatformApp.objects.filter(customer_id=customer,
       platform_type_id__platform_type_str=PlatformType.ANDROID)]

    if not (startDateStr and endDateStr):
      raise ValueError('Missing or invalid arguments start_date_str: %s and end_date_str: %s' %
          (startDateStr, endDateStr))
    startDate = parser.parse(startDateStr).date()
    endDate = parser.parse(endDateStr).date()

    records = fetch_google_report(login.refresh_token, login.gc_bucket_id, 'sales',
        startDate, endDate)

    # fetch_google_daily_sales_report may succeed without returning any records.
    # This can happen if the monthly report zip file is available for download,
    # but no records matching the provided reportDate are found.
    if not records:
      raise ReportUnavailableException(
          'Google report fetch succeeded but no records were returned.')

    # Produce dicts of known packages and product IDs.
    # These dicts are updated as new database entities are saved.
    idToApp = {pa.platform_app_str: pa for pa in platformApps}
    idToProduct = {cp.channel_product_str: cp for cp in channelProducts}

    # Create any unrecognized apps and their corresponding products.
    for record in records:
      productId = record.product_id
      if productId not in idToProduct:
        appInfo = fetcher.ANDROID_APP_INFO_FETCHER.fetch(productId)
        if appInfo is None:
          logger.warning('Could not retrieve app info for google package %s' % productId)
          continue
        if appInfo.app not in idToApp:
          # The app does not exist
          with transaction.atomic():
            app = App(customer_id=customer, name=appInfo.app)
            app.save()
            platformType = PlatformType.objects.filter(platform_type_str='Android').first()
            platformApp = PlatformApp(platform_app_str=appInfo.app, platform_type_id=platformType,
              customer_id=customer, latest_sdk_version='', description=appInfo.name, app_id=app)
            platformApp.save()
            idToApp[appInfo.app] = platformApp
        else:
          platformApp = idToApp[appInfo.app]

        # We now know that the app exists but the product does not.
        with transaction.atomic():
          product = Product(customer_id=customer, description=appInfo.app)
          product.save()
          channel = Channel.objects.get(platform_type_id=platformApp.platform_type_id)
          channelProduct = ChannelProduct(channel_product_str=appInfo.app, channel_id=channel,
              platform_app_id=platformApp, description=appInfo.name, product_id=product,
              customer_id=customer, amt_local=appInfo.amt_local,
              amt_local_micros=appInfo.amt_local_micros, amt_usd_micros=appInfo.amt_usd_micros,
              currency_str='USD')
          channelProduct.save()
          idToProduct[productId] = channelProduct

    # Now that we've identified as many apps as possible, create any unrecognized
    # in-app products.
    for record in records:
      if record.is_app():
        continue
      productId = record.product_id
      skuId = record.sku_id
      if skuId not in idToProduct:
        # The product does not exist, but we expect the parent product to exist at this point.
        if productId not in idToProduct:
          logger.info('Encountered unrecognized product %s' % productId)
          continue
        parentProduct = idToProduct[productId]
        channel, created = Channel.objects.get_or_create(
            platform_app_id=parentProduct.platform_app_id)
        with transaction.atomic():
          product = Product(customer_id=customer, description=record.sku_id)
          product.save()
          amtLocal = record.developer_proceeds_usd
          amtLocalMicros = long(amtLocal * 1000000L)
          channelProduct = ChannelProduct(channel_product_str=record.sku_id, channel_id=channel,
              description=record.product_title, product_id=product, customer_id=customer,
              amt_local=amtLocal, amt_local_micros=amtLocalMicros, amt_usd_micros=amtLocalMicros,
              currency_str='USD')
          channelProduct.save()
          idToProduct[skuId] = channelProduct

    # Product identification is complete; process the report data.
    # Build a dict of instances to save.
    metricsDict = {}

    # Fetch any existing metrics records that would need to be updated instead of inserted.
    # Only fetch records for Android platform.
    existingAppMetrics = DailySalesReportMetrics.objects.filter(
        customer_id=customer, date__gte=startDate, date__lte=endDate,
        channel_product_id__channel_id__platform_type_id__platform_type_str='Android')
    existingProductMetrics = DailySalesReportMetrics.objects.filter(
        customer_id=customer, date__gte=startDate, date__lte=endDate,
        channel_product_id__channel_id__platform_app_id__platform_type_id__platform_type_str=
        'Android')

    # Update the dictionary with existing metrics entries, but zero out the metrics that
    # we intend to overwrite.
    for metric in existingAppMetrics:
      key = (metric.channel_product_id.channel_product_id, metric.date)
      metric.developer_revenue_usd_micros = 0L
      metric.recurring_revenue_usd_micros = 0L
      metric.quantity = 0L
      metric.downloads = 0
      metric.refunds = 0
      metricsDict[key] = metric
    for metric in existingProductMetrics:
      key = (metric.channel_product_id.channel_product_id, metric.date)
      metric.developer_revenue_usd_micros = 0L
      metric.recurring_revenue_usd_micros = 0L
      metric.quantity = 0L
      metric.downloads = 0
      metric.refunds = 0
      metricsDict[key] = metric

    # Process the records and update the dictionary.
    for record in records:
      productId = record.product_id
      skuId = record.sku_id
      if productId not in idToProduct:
        # This shouldn't happen. Log and continue.
        logger.info('Encountered unrecognized product %s' % productId)
        continue
      if skuId and skuId not in idToProduct:
        # This shouldn't happen. Log and continue.
        logger.info('Encountered unrecognized product %s' % skuId)
        continue
      channelProduct = idToProduct[skuId] if skuId else idToProduct[productId]
      channelProductId = channelProduct.channel_product_id
      key = (channelProductId, record.charged_date)
      if key not in metricsDict:
        metricsDict[key] = DailySalesReportMetrics(customer_id=customer,
            channel_product_id=channelProduct, date=record.charged_date,
            developer_revenue_usd_micros=0L, recurring_revenue_usd_micros=0L,
            quantity=0L, downloads=0, updates=0, refunds=0)
      developerProceedsUsdMicros = long(record.developer_proceeds_usd * 1000000)
      metric = metricsDict[key]
      metric.developer_revenue_usd_micros += developerProceedsUsdMicros
      metric.quantity += record.payment_direction()
      if record.is_recurrence():
        metric.recurring_revenue_usd_micros += developerProceedsUsdMicros
      if record.is_download():
        metric.downloads += 1
      if record.is_refund():
        metric.refunds += 1

    # Write results to the database
    with transaction.atomic():
      for key in metricsDict:
        metricsDict[key].save()

    if doneToken:
      cache.set(doneToken, True, 60*60)

  except Exception as e:
    logger.info(traceback.format_exc())
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def batch_daily_google_installation_metrics(self, *args, **kwargs):
  try:
    loginInfoId, startDateStr, endDateStr = unpack(kwargs, 'login_info_id',
        'start_date_str', 'end_date_str')
    login = CustomerExternalLoginInfo.objects.get(pk=loginInfoId)
    customer = login.customer_id
    channelProducts = ChannelProduct.objects.filter(customer_id=customer)
    nameToProduct = {}
    for cp in channelProducts:
      if cp.platform_app_id:
        nameToProduct[cp.platform_app_id.platform_app_str] = cp

    if not (startDateStr and endDateStr):
      raise ValueError('Missing or invalid arguments start_date_str: %s and end_date_str: %s' %
          (startDateStr, endDateStr))
    startDate = parser.parse(startDateStr).date()
    endDate = parser.parse(endDateStr).date()

    records = fetch_google_report(login.refresh_token, login.gc_bucket_id, 'installs',
        startDate, endDate)

    # fetch_google_daily_sales_report may succeed without returning any records.
    # This can happen if the monthly report zip file is available for download,
    # but no records matching the provided reportDate are found.
    if not records:
      raise ReportUnavailableException(
          'Google report fetch succeeded but no records were returned.')

    # Build a dict of instances to save.
    metricsDict = {}

    # Fetch any existing metrics records that would need to be updated instead of inserted.
    # Only fetch records for Android platform.
    existingAppMetrics = DailySalesReportMetrics.objects.filter(
        customer_id=customer, date__gte=startDate, date__lte=endDate,
        channel_product_id__channel_id__platform_type_id__platform_type_str='Android')

    # Update the dictionary with existing metrics entries, but zero out the metrics that
    # we intend to overwrite.
    for metric in existingAppMetrics:
      key = (metric.channel_product_id.channel_product_id, metric.date)
      metric.updates = 0
      metricsDict[key] = metric

    # Process the records and update the dictionary.
    for record in records:
      packageName = record.package_name
      if packageName not in nameToProduct:
        logger.info('Encountered unrecognized package name %s' % packageName)
        continue
      channelProduct = nameToProduct[packageName]
      channelProductId = channelProduct.channel_product_id
      key = (channelProductId, record.date)
      if key not in metricsDict:
        metricsDict[key] = DailySalesReportMetrics(customer_id=customer,
            channel_product_id=channelProduct, date=record.date,
            developer_revenue_usd_micros=0L, recurring_revenue_usd_micros=0L,
            quantity=0L, downloads=0, updates=0, refunds=0)
      metric = metricsDict[key]
      metric.updates += record.daily_device_upgrades

    # Write results to the database
    with transaction.atomic():
      for key in metricsDict:
        metricsDict[key].save()

  except Exception as e:
    # Treat all exceptions as transient errors and retry.
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def backfill_daily_sales_report_metrics(self, *args, **kwargs):
  try:
    loginInfoId, doneToken = unpack(kwargs, 'login_info_id', 'done_token')
    loginInfo = CustomerExternalLoginInfo.objects.get(pk=loginInfoId)
    if loginInfo.external_service == CustomerExternalLoginInfo.GOOGLE_CLOUD:
      backfill_daily_google_metrics.delay(login_info_id=loginInfoId, done_token=doneToken)
    elif loginInfo.external_service == CustomerExternalLoginInfo.ITUNES_CONNECT:
      backfill_daily_apple_metrics.delay(login_info_id=loginInfoId, done_token=doneToken)
    else:
      logger.info('Not backfilling daily sales report metrics for unrecognized service %s' %
          loginInfo.external_service)
      return
  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def backfill_daily_apple_metrics(self, *args, **kwargs):
  try:
    loginInfoId, doneToken = unpack(kwargs, 'login_info_id', 'done_token')

    # For now assume all new customers belong to Apple's North American territory.
    tz = timezone('America/Los_Angeles')
    todayLocal = from_timestamp(utc_epoch(), tz).date()
    yesterdayLocal = todayLocal - timedelta(days=1)
    startDate = todayLocal - timedelta(days=30)

    # Yesterday's report may not be available yet. This can be a problem when
    # we're waiting eagerly for notification that the backfill task has
    # completed. To avoid delays, we schedule the task that pulls yesterday's
    # data separately.
    if doneToken:
      endDate = yesterdayLocal - timedelta(days=1)
      batch_daily_apple_metrics.delay(login_id=loginInfoId, start_date_str=str(startDate),
          end_date_str=str(endDate), done_token=doneToken)
      daily_apple_metrics.delay(login_id=loginInfoId, report_date_str=str(yesterdayLocal))
    else:
      endDate = yesterdayLocal
      batch_daily_apple_metrics.delay(login_id=loginInfoId, start_date_str=str(startDate),
          end_date_str=str(endDate))

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def backfill_daily_google_metrics(self, *args, **kwargs):
  try:
    loginInfoId, doneToken = unpack(kwargs, 'login_info_id', 'done_token')
    loginInfo = CustomerExternalLoginInfo.objects.get(pk=loginInfoId)

    # Google reports use UTC day boundaries.
    todayUtc = from_timestamp(utc_epoch(), utc).date()
    yesterdayUtc = todayUtc - timedelta(days=1)
    startDate = todayUtc - timedelta(days=30)

    # Yesterday's report may not be available yet. This can be a problem when
    # we're waiting eagerly for notification that the backfill task has
    # completed. To avoid delays, we schedule the task that pulls yesterday's
    # data separately.
    if doneToken:
      endDate = yesterdayUtc - timedelta(days=1)
      batch_daily_google_metrics.delay(login_info_id=loginInfoId, start_date_str=str(startDate),
          end_date_str=str(endDate), done_token=doneToken)
      batch_daily_google_metrics.delay(login_info_id=loginInfoId, start_date_str=str(yesterdayUtc),
          end_date_str=str(yesterdayUtc))
    else:
      endDate = yesterdayUtc
      batch_daily_google_metrics.delay(login_info_id=loginInfoId, start_date_str=str(startDate),
          end_date_str=str(endDate))

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


TRAIN_MODEL_QUERY = """
  SELECT DISTINCT
    ap.id platform_id,
    c.alpha2 country_alpha2,
    cg.id category_id,
    cu.customer_id customer_id,
    cu.training_privacy training_privacy
  FROM
    customer_external_login_info li
  JOIN customers cu ON (li.customer_id = cu.customer_id)
  JOIN channel_products cp ON (li.customer_id = cp.customer_id)
  JOIN app_store_apps a ON (cp.apple_identifier = a.track_id)
  JOIN app_store_ranks k ON (k.app = a.id)
  JOIN app_store_rank_requests r ON (k.rank_request = r.id)
  JOIN app_store_categories cg ON (r.category = cg.id)
  JOIN app_store_regions rg ON (r.region = rg.id)
  JOIN countries c ON (rg.country = c.id)
  JOIN app_store_rank_types t ON (r.rank_type = t.id)
  JOIN app_store_platforms ap ON (a.platform = ap.id)
  WHERE
    li.login_info_id = %s AND
    cp.apple_identifier IS NOT NULL AND
    ap.name = 'iOS'
  ;
  """

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def all_done(self, *args, **kwargs):
  try:
    doneToken, loginId = unpack(kwargs, 'done_token', 'login_id')
    cache.set(doneToken, True, 60*60)

    taskList = []
    taskList.append(fetch_app_info.s(login_info_id=loginId))

    cursor = connection.cursor()
    cursor.execute(TRAIN_MODEL_QUERY, [loginId])
    for row in cursor.fetchall():
      platform_id, country_alpha2, category_id, customer_id, training_privacy = row[:5]
      ownerId = customer_id if training_privacy == Customer.PRIVATE else None
      taskList.append(train_model.s(model_name=DAILY_REVENUE, owner_id=ownerId, request_data=
        [{
          'platform_id': platform_id,
          'country_alpha2': country_alpha2,
          'category_id': category_id,
        }]))

    trainModelTaskGroup = group(taskList)
    trainModelTaskGroup.apply_async()

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def fetch_app_info(self, *args, **kwargs):
  try:
    (loginInfoId, ) = unpack(kwargs, 'login_info_id')

    taskList = []
    channelProducts = ChannelProduct.objects.filter(customer_id__auth_user__id=loginInfoId,
        platform_app_id__isnull=False)
    for cp in channelProducts:
      platformStr = cp.platform_app_id.platform_type_id.platform_type_str
      if platformStr == 'Android':
        fetcherPlatformStr = fetcher.ANDROID_PLATFORM_STRING
      elif platformStr == 'iOS':
        fetcherPlatformStr = fetcher.IOS_PLATFORM_STRING
      else:
        continue
      taskList.append(fetch_single_app_info.s(app=cp.channel_product_str,
          platform=fetcherPlatformStr))

    fetchSingleAppInfoTaskGroup = group(taskList)
    fetchSingleAppInfoTaskGroup.apply_async()

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def fetch_single_app_info(self, *args, **kwargs):
  try:
    app, platform = unpack(kwargs, 'app', 'platform')
    fetcher.fetch(app, platform, direct=True)

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))
