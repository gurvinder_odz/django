from core.customfields import BigAutoField, BigForeignKey
from core.db.constants import DEFAULT_DECIMAL_PRECISION
from core.models import TimestampedModel
from customers.models import Customer, CustomerExternalLoginInfo
from django.db import models
from ingestor.models import App, ChannelProduct, CustomerLabel, PlatformApp
from metrics.constants import SESSION_START_EVENT_IDS

SESSION_START_EVENT_IDS_TUPLE = tuple(SESSION_START_EVENT_IDS)

# Apple product type identifier groupings
APP_BUNDLE_RECORDS = ['1-B']
FREE_OR_PAID_APP_RECORDS = ['1', '1F', '1T', 'F1']
PAID_APP_RECORDS = ['1E', '1EP', '1EU']
UPDATE_RECORDS = ['7', '7F', '7T', 'F7']
IN_APP_PURCHASE_RECORDS = ['IA1', 'IA9', 'IAC', 'IAY', 'FI1']
RECURRENCE_RECORDS = ['IA9', 'IAC', 'IAY']

class DailyPlatformAppMetrics(TimestampedModel):
  metrics_id = BigAutoField(primary_key=True, db_column="id")
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id")
  date = models.DateField(db_index=True)
  dau = models.IntegerField()

  # MAU is a misnomer since these values will be 28-day actives.
  mau = models.IntegerField()
  sessions = models.IntegerField()

  class Meta:
    db_table = 'daily_platform_app_metrics'
    unique_together = ('customer_id', 'platform_app_id', 'date')


class DailyAppMetrics(TimestampedModel):
  metrics_id = BigAutoField(primary_key=True, db_column="id")
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  app_id = models.ForeignKey(App, db_column="app_id")
  date = models.DateField(db_index=True)
  dau = models.IntegerField()

  # MAU is a misnomer since these values will be 28-day actives.
  mau = models.IntegerField()
  sessions = models.IntegerField()

  class Meta:
    db_table = 'daily_app_metrics'
    unique_together = ('customer_id', 'app_id', 'date')


class DailyCustomerMetrics(TimestampedModel):
  metrics_id = BigAutoField(primary_key=True, db_column="id")
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  date = models.DateField(db_index=True)
  dau = models.IntegerField()

  # MAU is a misnomer since these values will be 28-day actives.
  mau = models.IntegerField()

  class Meta:
    db_table = 'daily_customer_metrics'
    unique_together = ('customer_id', 'date')


class DailyLabelMetrics(TimestampedModel):
  metrics_id = BigAutoField(primary_key=True, db_column="id")
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id")
  customer_label_id = models.ForeignKey(CustomerLabel, db_column="customer_label_id")
  date = models.DateField(db_index=True)
  count = models.IntegerField()

  class Meta:
    db_table = 'daily_label_metrics'
    unique_together = ('platform_app_id', 'customer_label_id', 'date')


class DailyProductMetrics(TimestampedModel):
  metrics_id = BigAutoField(primary_key=True, db_column="id")
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  channel_product_id = BigForeignKey(ChannelProduct, db_column="channel_product_id")
  date = models.DateField(db_index=True)
  revenue = models.BigIntegerField()
  recurring_revenue = models.BigIntegerField()
  developer_revenue_usd_micros = models.BigIntegerField(null=True)
  developer_recurring_revenue_usd_micros = models.BigIntegerField(null=True)

  class Meta:
    db_table = 'daily_product_metrics'
    unique_together = ('customer_id', 'channel_product_id', 'date')


class DailySalesReportMetrics(TimestampedModel):
  metrics_id = BigAutoField(primary_key=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  channel_product_id = BigForeignKey(ChannelProduct, db_column="channel_product_id")
  date = models.DateField(db_index=True)
  developer_revenue_usd_micros = models.BigIntegerField()
  recurring_revenue_usd_micros = models.BigIntegerField()
  quantity = models.BigIntegerField()

  downloads = models.IntegerField(default=0)
  updates = models.IntegerField(default=0)
  refunds = models.IntegerField(default=0)

  class Meta:
    db_table = 'daily_sales_report_metrics'
    unique_together = ('channel_product_id', 'date')


class AppleReport(TimestampedModel):
  report_id = BigAutoField(primary_key=True)
  report_type = models.CharField(max_length=100)
  report_subtype = models.CharField(max_length=100)
  report_date_type = models.CharField(max_length=100)
  report_date = models.DateField(db_index=True)
  vendor_id = models.BigIntegerField(db_index=True)
  request_nonce = models.CharField(max_length=256)
  request_time = models.DateTimeField(db_index=True)

  class Meta:
    db_table = 'apple_reports'


class AppleReportRecord(TimestampedModel):
  record_id = BigAutoField(primary_key=True)
  report_id = BigForeignKey(AppleReport, db_column="report_id")
  apple_identifier = models.BigIntegerField(db_index=True)
  provider = models.CharField(max_length=10)
  provider_country = models.CharField(max_length=10)
  sku = models.CharField(max_length=100)
  developer = models.CharField(max_length=4000, null=True)
  title = models.CharField(max_length=600)
  version = models.CharField(max_length=100, null=True)
  product_type_identifier = models.CharField(max_length=20)
  units = models.DecimalField(max_digits=20, decimal_places=2)
  developer_proceeds = models.DecimalField(max_digits=20, decimal_places=2)
  developer_proceeds_usd = models.DecimalField(max_digits=30,
      decimal_places=DEFAULT_DECIMAL_PRECISION)
  begin_date = models.DateField()
  end_date = models.DateField()
  customer_currency = models.CharField(max_length=10)
  country_code = models.CharField(max_length=10)
  currency_of_proceeds = models.CharField(max_length=10)
  customer_price = models.DecimalField(max_digits=20, decimal_places=2)
  promo_code = models.CharField(max_length=10, null=True)
  parent_identifier = models.CharField(max_length=100, null=True)
  subscription = models.CharField(max_length=10, null=True)
  period = models.CharField(max_length=30, null=True)
  category = models.CharField(max_length=50)
  cmb = models.CharField(max_length=5, null=True)

  def is_app(self):
    return self.product_type_identifier in FREE_OR_PAID_APP_RECORDS + PAID_APP_RECORDS + \
        UPDATE_RECORDS

  def is_app_bundle(self):
    return self.product_type_identifier in APP_BUNDLE_RECORDS

  def is_in_app_purchase(self):
    return self.product_type_identifier in IN_APP_PURCHASE_RECORDS

  def is_recurrence(self):
    return self.product_type_identifier in RECURRENCE_RECORDS

  def is_update(self):
    return self.product_type_identifier in UPDATE_RECORDS

  def is_download(self):
    return (self.product_type_identifier in FREE_OR_PAID_APP_RECORDS + PAID_APP_RECORDS +
        APP_BUNDLE_RECORDS) and self.units >= 0

  def is_refund(self):
    return self.units < 0

  class Meta:
    db_table = 'apple_report_records'


class GoogleReport(TimestampedModel):
  report_id = BigAutoField(primary_key=True)
  report_type = models.CharField(max_length=100)
  report_start_date = models.DateField()
  report_end_date = models.DateField()
  gc_bucket_id = models.CharField(max_length=256, null=True)
  request_nonce = models.CharField(max_length=256)
  request_time = models.DateTimeField(db_index=True)

  class Meta:
    db_table = 'google_reports'


class GoogleReportRecord(TimestampedModel):
  record_id = BigAutoField(primary_key=True)
  report_id = BigForeignKey(GoogleReport, db_column='report_id')
  order_number = models.CharField(max_length=100)
  charged_date = models.DateField()
  charged_time = models.DateTimeField()
  financial_status = models.CharField(max_length=100)
  device_model = models.CharField(max_length=100, null=True)
  product_title = models.CharField(max_length=600)
  product_id = models.CharField(max_length=256, db_index=True)
  product_type = models.CharField(max_length=100)
  sku_id = models.CharField(max_length=256, null=True)
  sale_currency = models.CharField(max_length=10)
  item_price = models.DecimalField(max_digits=30, decimal_places=DEFAULT_DECIMAL_PRECISION)
  taxes = models.DecimalField(max_digits=30, decimal_places=DEFAULT_DECIMAL_PRECISION)
  charged_amount = models.DecimalField(max_digits=30, decimal_places=DEFAULT_DECIMAL_PRECISION)
  developer_proceeds = models.DecimalField(max_digits=30,
      decimal_places=DEFAULT_DECIMAL_PRECISION)
  developer_proceeds_usd = models.DecimalField(max_digits=30,
      decimal_places=DEFAULT_DECIMAL_PRECISION)
  buyer_city = models.CharField(max_length=256, null=True)
  buyer_state = models.CharField(max_length=256, null=True)
  buyer_postal_code = models.CharField(max_length=256, null=True)
  buyer_country = models.CharField(max_length=256, null=True)

  def is_app(self):
    return False if self.sku_id else True

  def is_recurrence(self):
    return self.product_type == 'subscription'

  def payment_direction(self):
    return -1 if (self.financial_status == 'Refund' or self.charged_amount < 0) else 1

  def is_download(self):
    return self.is_app() and self.financial_status == 'Charged'

  def is_refund(self):
    return self.financial_status == 'Refund'

  class Meta:
    db_table = 'google_report_records'


class GoogleInstallationReportRecord(TimestampedModel):
  record_id = BigAutoField(primary_key=True)
  report_id = BigForeignKey(GoogleReport, db_column='report_id')
  date = models.DateField()
  package_name = models.CharField(max_length=256, db_index=True)
  current_device_installs = models.IntegerField()
  daily_device_installs = models.IntegerField()
  daily_device_uninstalls = models.IntegerField()
  daily_device_upgrades = models.IntegerField()
  current_user_installs = models.IntegerField()
  total_user_installs = models.IntegerField()
  daily_user_installs = models.IntegerField()
  daily_user_uninstalls = models.IntegerField()

  class Meta:
    db_table = 'google_installation_report_records'


class DailyReportAvailabilityTime(TimestampedModel):
  id = models.AutoField(primary_key=True)
  external_service = models.CharField(choices=CustomerExternalLoginInfo.EXTERNAL_SERVICE_CHOICES,
      max_length=256)
  report_type = models.CharField(max_length=256)
  report_region = models.CharField(max_length=256, default='')
  report_date = models.DateField()
  time = models.DateTimeField()

  class Meta:
    db_table = 'daily_report_availability_times'
    unique_together = ('external_service', 'report_type', 'report_region', 'report_date')
