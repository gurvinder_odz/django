# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0009_googlereport_googlereportrecord'),
    ]

    operations = [
        migrations.AlterField(
            model_name='googlereportrecord',
            name='report_id',
            field=core.customfields.BigForeignKey(to='metrics.GoogleReport', db_column=b'report_id'),
            preserve_default=True,
        ),
    ]
