# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0016_auto_20150220_1632'),
        ('metrics', '0004_dailyproductmetrics_recurring_revenue'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyLabelMetrics',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('metrics_id', core.customfields.BigAutoField(serialize=False, primary_key=True, db_column=b'id')),
                ('date', models.DateField(db_index=True)),
                ('count', models.IntegerField()),
                ('customer_label_id', models.ForeignKey(to='ingestor.CustomerLabel', db_column=b'customer_label_id')),
                ('platform_app_id', models.ForeignKey(to='ingestor.PlatformApp', db_column=b'platform_app_id')),
            ],
            options={
                'db_table': 'daily_label_metrics',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='dailylabelmetrics',
            unique_together=set([('platform_app_id', 'customer_label_id', 'date')]),
        ),
        migrations.AlterField(
            model_name='dailyappmetrics',
            name='date',
            field=models.DateField(db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailycustomermetrics',
            name='date',
            field=models.DateField(db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailyplatformappmetrics',
            name='date',
            field=models.DateField(db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='dailyproductmetrics',
            name='date',
            field=models.DateField(db_index=True),
            preserve_default=True,
        ),
    ]
