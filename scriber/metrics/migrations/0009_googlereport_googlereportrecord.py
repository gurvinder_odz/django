# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0008_auto_20150310_0034'),
    ]

    operations = [
        migrations.CreateModel(
            name='GoogleReport',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('report_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('report_type', models.CharField(max_length=100)),
                ('report_date', models.DateField(db_index=True)),
                ('gc_bucket_id', models.CharField(max_length=256, null=True)),
                ('request_nonce', models.CharField(max_length=256)),
                ('request_time', models.DateTimeField(db_index=True)),
            ],
            options={
                'db_table': 'google_reports',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GoogleReportRecord',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('record_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('order_number', models.CharField(max_length=100)),
                ('charged_date', models.DateField()),
                ('charged_time', models.DateTimeField()),
                ('financial_status', models.CharField(max_length=100)),
                ('device_model', models.CharField(max_length=100, null=True)),
                ('product_title', models.CharField(max_length=600)),
                ('product_id', models.CharField(max_length=256, db_index=True)),
                ('product_type', models.CharField(max_length=100)),
                ('sku_id', models.CharField(max_length=256, null=True)),
                ('sale_currency', models.CharField(max_length=10)),
                ('item_price', models.DecimalField(max_digits=30, decimal_places=10)),
                ('taxes', models.DecimalField(max_digits=30, decimal_places=10)),
                ('charged_amount', models.DecimalField(max_digits=30, decimal_places=10)),
                ('developer_proceeds', models.DecimalField(max_digits=30, decimal_places=10)),
                ('developer_proceeds_usd', models.DecimalField(max_digits=30, decimal_places=10)),
                ('buyer_city', models.CharField(max_length=256, null=True)),
                ('buyer_state', models.CharField(max_length=256, null=True)),
                ('buyer_postal_code', models.CharField(max_length=256, null=True)),
                ('buyer_country', models.CharField(max_length=256, null=True)),
                ('report_id', core.customfields.BigForeignKey(to='metrics.AppleReport', db_column=b'report_id')),
            ],
            options={
                'db_table': 'google_report_records',
            },
            bases=(models.Model,),
        ),
    ]
