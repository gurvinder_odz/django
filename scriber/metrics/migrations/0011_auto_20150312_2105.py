# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0010_auto_20150312_1920'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='googlereport',
            name='report_date',
        ),
        migrations.AddField(
            model_name='googlereport',
            name='report_end_date',
            field=models.DateField(default=datetime.date(2015, 3, 12)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='googlereport',
            name='report_start_date',
            field=models.DateField(default=datetime.date(2015, 3, 12)),
            preserve_default=False,
        ),
    ]
