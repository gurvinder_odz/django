# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0021_auto_20150303_2057'),
        ('customers', '0003_auto_20150130_2045'),
        ('metrics', '0005_auto_20150220_1854'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppleReport',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('report_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('report_type', models.CharField(max_length=100)),
                ('report_subtype', models.CharField(max_length=100)),
                ('report_date_type', models.CharField(max_length=100)),
                ('report_date', models.DateField(db_index=True)),
                ('vendor_id', models.BigIntegerField(db_index=True)),
                ('request_nonce', models.CharField(max_length=256)),
                ('request_time', models.DateTimeField(db_index=True)),
            ],
            options={
                'db_table': 'apple_reports',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AppleReportRecord',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('record_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('apple_identifier', models.BigIntegerField(db_index=True)),
                ('provider', models.CharField(max_length=10)),
                ('provider_country', models.CharField(max_length=10)),
                ('sku', models.CharField(max_length=100)),
                ('developer', models.CharField(max_length=4000, null=True)),
                ('title', models.CharField(max_length=600)),
                ('version', models.CharField(max_length=100, null=True)),
                ('product_type_identifier', models.CharField(max_length=20)),
                ('units', models.DecimalField(max_digits=20, decimal_places=2)),
                ('developer_proceeds', models.DecimalField(max_digits=20, decimal_places=2)),
                ('developer_proceeds_usd', models.DecimalField(max_digits=30, decimal_places=10)),
                ('begin_date', models.DateField()),
                ('end_date', models.DateField()),
                ('customer_currency', models.CharField(max_length=10)),
                ('country_code', models.CharField(max_length=10)),
                ('currency_of_proceeds', models.CharField(max_length=10)),
                ('customer_price', models.DecimalField(max_digits=20, decimal_places=2)),
                ('promo_code', models.CharField(max_length=10, null=True)),
                ('parent_identifier', models.CharField(max_length=100, null=True)),
                ('subscription', models.CharField(max_length=10, null=True)),
                ('period', models.CharField(max_length=30, null=True)),
                ('category', models.CharField(max_length=50)),
                ('cmb', models.CharField(max_length=5, null=True)),
                ('report_id', core.customfields.BigForeignKey(to='metrics.AppleReport', db_column=b'report_id')),
            ],
            options={
                'db_table': 'apple_report_records',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DailySalesReportMetrics',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('metrics_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('date', models.DateField(db_index=True)),
                ('developer_revenue_usd_micros', models.BigIntegerField()),
                ('recurring_revenue_usd_micros', models.BigIntegerField()),
                ('quantity', models.BigIntegerField()),
                ('channel_product_id', core.customfields.BigForeignKey(to='ingestor.ChannelProduct', db_column=b'channel_product_id')),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'daily_sales_report_metrics',
            },
            bases=(models.Model,),
        ),
    ]
