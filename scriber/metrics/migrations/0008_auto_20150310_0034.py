# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0007_auto_20150306_0144'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailyappmetrics',
            name='sessions',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='dailyplatformappmetrics',
            name='sessions',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
