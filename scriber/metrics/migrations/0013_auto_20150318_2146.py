# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0012_auto_20150318_0103'),
    ]

    operations = [
        migrations.CreateModel(
            name='GoogleInstallationReportRecord',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('record_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('date', models.DateField()),
                ('package_name', models.CharField(max_length=256, db_index=True)),
                ('current_device_installs', models.IntegerField()),
                ('daily_device_installs', models.IntegerField()),
                ('daily_device_uninstalls', models.IntegerField()),
                ('daily_device_upgrades', models.IntegerField()),
                ('current_user_installs', models.IntegerField()),
                ('total_user_installs', models.IntegerField()),
                ('daily_user_installs', models.IntegerField()),
                ('daily_user_uninstalls', models.IntegerField()),
                ('report_id', core.customfields.BigForeignKey(to='metrics.GoogleReport', db_column=b'report_id')),
            ],
            options={
                'db_table': 'google_installation_report_records',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='dailysalesreportmetrics',
            name='downloads',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysalesreportmetrics',
            name='refunds',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailysalesreportmetrics',
            name='updates',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
