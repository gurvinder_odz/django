# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0011_auto_20150312_2105'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailyproductmetrics',
            name='developer_recurring_revenue_usd_micros',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dailyproductmetrics',
            name='developer_revenue_usd_micros',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
        migrations.RunSQL(
            sql="""
                UPDATE daily_product_metrics
                SET developer_revenue_usd_micros = CAST(revenue * 0.7 AS bigint);
                """,
        ),
        migrations.RunSQL(
            sql="""
                UPDATE daily_product_metrics
                SET developer_recurring_revenue_usd_micros = CAST(recurring_revenue * 0.7 AS bigint);
                """,
        ),
    ]
