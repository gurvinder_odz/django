# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0003_remove_dailyproductmetrics_platform_app_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailyproductmetrics',
            name='recurring_revenue',
            field=models.BigIntegerField(default=0L),
            preserve_default=False,
        ),
    ]
