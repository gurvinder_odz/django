# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0006_applereport_applereportrecord_dailysalesreportmetrics'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='dailysalesreportmetrics',
            unique_together=set([('channel_product_id', 'date')]),
        ),
    ]
