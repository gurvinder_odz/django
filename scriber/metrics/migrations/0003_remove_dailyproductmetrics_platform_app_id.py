# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0002_auto_20150115_2226'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dailyproductmetrics',
            name='platform_app_id',
        ),
    ]
