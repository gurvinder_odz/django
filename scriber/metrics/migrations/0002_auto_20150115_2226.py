# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_customer_timezone'),
        ('ingestor', '0003_auto_20150115_2226'),
        ('metrics', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyCustomerMetrics',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('metrics_id', core.customfields.BigAutoField(serialize=False, primary_key=True, db_column=b'id')),
                ('date', models.DateField()),
                ('dau', models.IntegerField()),
                ('mau', models.IntegerField()),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'daily_customer_metrics',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DailyProductMetrics',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('metrics_id', core.customfields.BigAutoField(serialize=False, primary_key=True, db_column=b'id')),
                ('date', models.DateField()),
                ('revenue', models.BigIntegerField()),
                ('channel_product_id', core.customfields.BigForeignKey(to='ingestor.ChannelProduct', db_column=b'channel_product_id')),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
                ('platform_app_id', models.ForeignKey(db_column=b'platform_app_id', blank=True, to='ingestor.PlatformApp', null=True)),
            ],
            options={
                'db_table': 'daily_product_metrics',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='dailyproductmetrics',
            unique_together=set([('customer_id', 'channel_product_id', 'date')]),
        ),
        migrations.AlterUniqueTogether(
            name='dailycustomermetrics',
            unique_together=set([('customer_id', 'date')]),
        ),
    ]
