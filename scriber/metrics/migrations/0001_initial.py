# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_customer_timezone'),
        ('ingestor', '0002_auto_20150113_0022'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyAppMetrics',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('metrics_id', core.customfields.BigAutoField(serialize=False, primary_key=True, db_column=b'id')),
                ('date', models.DateField()),
                ('dau', models.IntegerField()),
                ('mau', models.IntegerField()),
                ('app_id', models.ForeignKey(to='ingestor.App', db_column=b'app_id')),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'daily_app_metrics',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DailyPlatformAppMetrics',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('metrics_id', core.customfields.BigAutoField(serialize=False, primary_key=True, db_column=b'id')),
                ('date', models.DateField()),
                ('dau', models.IntegerField()),
                ('mau', models.IntegerField()),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
                ('platform_app_id', models.ForeignKey(to='ingestor.PlatformApp', db_column=b'platform_app_id')),
            ],
            options={
                'db_table': 'daily_platform_app_metrics',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='dailyplatformappmetrics',
            unique_together=set([('customer_id', 'platform_app_id', 'date')]),
        ),
        migrations.AlterUniqueTogether(
            name='dailyappmetrics',
            unique_together=set([('customer_id', 'app_id', 'date')]),
        ),
    ]
