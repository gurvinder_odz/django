# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metrics', '0013_auto_20150318_2146'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyReportAvailabilityTime',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('external_service', models.CharField(max_length=256, choices=[(b'Google Cloud', b'Google Cloud'), (b'iTunes Connect', b'iTunes Connect')])),
                ('report_type', models.CharField(max_length=256)),
                ('report_region', models.CharField(default=b'', max_length=256)),
                ('report_date', models.DateField()),
                ('time', models.DateTimeField()),
            ],
            options={
                'db_table': 'daily_report_availability_times',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='dailyreportavailabilitytime',
            unique_together=set([('external_service', 'report_type', 'report_region', 'report_date')]),
        ),
    ]
