import csv
import logging
import requests
import zlib

from base64 import b64encode
from core.currency import convert_decimal_to_usd
from core.db.constants import TEN_PLACES, TWO_PLACES
from core.timeutils import from_timestamp, utc_epoch
from datetime import date, timedelta
from dateutil import parser
from decimal import Decimal
from django.db import transaction
from metrics.models import AppleReport, AppleReportRecord
from os import urandom
from pytz import utc

_APPLE_REPORT_URL_BASE = "https://reportingitc.apple.com/autoingestion.tft?"
_APPLE_REPORT_HEADERS = {"Content-Type" : "application/x-www-form-urlencoded"}  
_APPLE_REPORT_EXPECTED_FIELDS = [
  'Provider',                #  0
  'Provider Country',        #  1
  'SKU',                     #  2
  'Developer',               #  3
  'Title',                   #  4
  'Version',                 #  5
  'Product Type Identifier', #  6
  'Units',                   #  7
  'Developer Proceeds',      #  8
  'Begin Date',              #  9
  'End Date',                # 10
  'Customer Currency',       # 11
  'Country Code',            # 12
  'Currency of Proceeds',    # 13
  'Apple Identifier',        # 14
  'Customer Price',          # 15
  'Promo Code',              # 16
  'Parent Identifier',       # 17
  'Subscription',            # 18
  'Period',                  # 19
  'Category',                # 20
  'CMB',                     # 21
]

logger = logging.getLogger(__name__)

# Indicates a generic failure when trying to pull a report.
class ReportFetchException(Exception):
  pass

# An exception indicating that a report is not yet available.
class ReportUnavailableException(ReportFetchException):
  pass

def fetch_apple_daily_sales_report(username, password, vendor_id, report_date=None):
  # Warning: this defaults to UTC date.
  if not report_date:
    report_date = date.today() - timedelta(days=1)
  return fetch_apple_report(username, password, vendor_id, report_type='Sales',
      date_type='Daily', report_subtype='Summary', report_date=report_date)

def fetch_apple_report(username, password, vendor_id, report_type, date_type,
    report_subtype, report_date):
  report_date_str = report_date.strftime('%Y%m%d')
  params = ({
    "USERNAME" : username,
    "PASSWORD" : password,
    "VNDNUMBER" : vendor_id,
    "TYPEOFREPORT" : report_type,
    "DATETYPE" : date_type,
    "REPORTTYPE" : report_subtype,
    "REPORTDATE" : report_date_str
  })

  request_time = from_timestamp(utc_epoch(), utc)
  # The request_nonce is only used to disambiguate requests made within short times of one another.
  # Uniqueness is not required.
  request_nonce = b64encode(urandom(10)).rstrip('==')
  response = requests.post(_APPLE_REPORT_URL_BASE, headers=_APPLE_REPORT_HEADERS, params=params)

  if 'errormsg' in response.headers and response.headers['errormsg']:
    # For now, assume all 2XX status codes represent report unavailable exceptions.
    if response.ok:
      logger.warning('fetch_apple_report failed with remote error message %s' %
          response.headers['errormsg'])
      raise ReportUnavailableException('Report not yet available')
    else:
      raise ReportFetchException(response.headers['errormsg'])
  elif 'filename' in response.headers and response.headers['filename']:
    decompressed_data=zlib.decompress(response.content, 16+zlib.MAX_WBITS)
    reader = csv.reader(decompressed_data.splitlines(), delimiter='\t')
    
    records = []
    for i, row in enumerate(reader):
      if i == 0:
        if _APPLE_REPORT_EXPECTED_FIELDS != row[:len(_APPLE_REPORT_EXPECTED_FIELDS)]:
          logger.warning('Encountered unexpected apple report fields: %s' % row)
          break
      else:
        provider = row[0]
        provider_country = row[1]
        sku = row[2]
        developer = row[3] if row[3] != '' else None
        title = row[4]
        version = row[5] if row[5] != '' else None
        product_type_identifier = row[6]
        units = Decimal(row[7]).quantize(TWO_PLACES)
        developer_proceeds = Decimal(row[8]).quantize(TWO_PLACES)
        begin_date = parser.parse(row[9]).date()
        end_date = parser.parse(row[10]).date()
        customer_currency = row[11]
        country_code = row[12]
        currency_of_proceeds = row[13]
        apple_identifier = long(row[14])
        customer_price = Decimal(row[15]).quantize(TWO_PLACES)
        promo_code = row[16] if row[16] != '' else None
        parent_identifier = row[17] if row[17] != '' else None
        subscription = row[18] if row[18] != '' else None
        period = row[19] if row[19] != '' else None
        category = row[20]
        cmb = row[21] if row[21] != '' else None

        total_developer_proceeds = units * developer_proceeds
        developer_proceeds_usd = convert_decimal_to_usd(total_developer_proceeds,
            currency_of_proceeds).quantize(TEN_PLACES)
        records.append(AppleReportRecord(
          provider=provider, provider_country=provider_country, sku=sku, developer=developer,
          title=title, version=version, product_type_identifier=product_type_identifier,
          units=units, developer_proceeds=developer_proceeds, begin_date=begin_date,
          end_date=end_date, customer_currency=customer_currency, country_code=country_code,
          currency_of_proceeds=currency_of_proceeds, apple_identifier=apple_identifier,
          customer_price=customer_price, promo_code=promo_code, parent_identifier=parent_identifier,
          subscription=subscription, period=period, category=category, cmb=cmb,
          developer_proceeds_usd=developer_proceeds_usd))

    appleReport = AppleReport(report_type=report_type, report_subtype=report_subtype,
        report_date_type=date_type, report_date=report_date, vendor_id=vendor_id,
        request_nonce=request_nonce, request_time=request_time)
    with transaction.atomic():
      appleReport.save()
      for record in records:
        record.report_id = appleReport
        record.save()

    return records

  else:
    raise ReportFetchException('An unexpected error occurred. Received response headers: %s' %
        response.headers)
