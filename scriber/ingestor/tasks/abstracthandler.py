from core.timeutils import from_timestamp
from core.utils import unpack
from ingestor.models import RawUserEvent, UserEvent
from ingestor.requestutils import event_time
from ingestor.tasks.constants import PCONN
from json import dumps
from pytz import utc


class AbstractMessageHandler():
  """
  An abstract base class for ingestor message handlers.
  Subclasses are expected to override the inner_handle method.
  """
  def pre_handle(self, *args, **kwargs):
    eventType, logger, message, platformApp, receivedEpoch, user = \
        unpack(kwargs, 'eventType', 'logger', 'message', 'platformApp', 'receivedEpoch', 'user')
    eventTime = event_time(message)
    receivedTime = from_timestamp(receivedEpoch, utc) if receivedEpoch else None
    userEvent = UserEvent(event_type=eventType, platform_app_id=platformApp,
        scriber_id=user, time=eventTime, received_time=receivedTime)
    userEvent.save(using=PCONN)

    # Allow dumps() to fail, though this should never happen due to the API view's JSON handling.
    rawUserEventJsonStr = dumps(message)
    rawUserEvent = RawUserEvent(user_event_id=userEvent, raw_user_event=rawUserEventJsonStr)
    rawUserEvent.save(using=PCONN)
    kwargs['userEvent'] = userEvent
    kwargs['rawUserEvent'] = rawUserEvent
    return args, kwargs

  def inner_handle(self, *args, **kwargs):
    raise NotImplementedError('Implementation not defined for abstract base class.')

  def post_handle(self, *args, **kwargs):
    # No implementation for now.
    pass

  def handle(self, *args, **kwargs):
    args, kwargs = self.pre_handle(*args, **kwargs)
    args, kwargs = self.inner_handle(*args, **kwargs)
    self.post_handle(*args, **kwargs)
