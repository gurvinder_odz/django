import re

from base64 import b64decode
from core.apple.apple import APPLE_ROOT_CA_CERT, ReceiptVerifier
from core.apple.receiptfields import ORIGINAL_PURCHASE_DATE_UNDOCUMENTED
from core.currency import convert_decimal_to_usd, parse
from core.db.constants import TEN_PLACES
from core.timeutils import from_timestamp, timestamp
from decimal import Decimal
from django.db import connections
from ingestor.models import Channel, ChannelProduct, CustomerLabel, Product, User, UserEvent, UserPurchase, RawUserEvent
from ingestor.requestutils import event_info_key, event_time, platform_order_id, purchase_time, purchased_product, unpack
from ingestor.tasks.abstracthandler import AbstractMessageHandler
from ingestor.tasks.constants import PCONN
from json import dumps
from pytz import utc


_RECEIPT_VERIFIER = ReceiptVerifier(ca_cert_string=APPLE_ROOT_CA_CERT,
    parse_undocumented_fields=True)

_MERGE_SINGLE_USER_PURCHASE_HISTORY_QUERY = """
  SELECT
    up1.purchase_id,
    subquery.purchase_id master_purchase_id
  FROM (
    SELECT
      up.purchase_id purchase_id,
      up.channel_product_id channel_product_id,
      cp.platform_app_id platform_app_id
    FROM
      user_purchases up JOIN channel_products cp ON up.channel_product_id = cp.channel_product_id
      JOIN users u ON up.scriber_id = u.scriber_id
    WHERE
      cp.platform_app_id IS NOT NULL
      AND u.customer_user_id = %s
      AND up.master_purchase_id IS NULL
  ) subquery JOIN user_purchases up1 ON subquery.channel_product_id = up1.channel_product_id
  WHERE
    up1.scriber_id = %s
  ;
  """

_MERGE_MULTIPLE_USERS_PURCHASE_HISTORY_QUERY = """
  SELECT
    up1.purchase_id,
    subquery.purchase_id master_purchase_id
  FROM (
    SELECT
      up.purchase_id purchase_id,
      up.channel_product_id channel_product_id,
      cp.platform_app_id platform_app_id
    FROM
      user_purchases up JOIN channel_products cp ON up.channel_product_id = cp.channel_product_id
      JOIN users u ON up.scriber_id = u.scriber_id
    WHERE
      cp.platform_app_id IS NOT NULL
      AND u.customer_user_id = %s
      AND up.master_purchase_id IS NULL
  ) subquery JOIN user_purchases up1 ON subquery.channel_product_id = up1.channel_product_id
  JOIN users u1 ON up1.scriber_id = u1.scriber_id
  WHERE
    u1.customer_user_id = %s
  ;
  """


class GenericMessageHandler(AbstractMessageHandler):
  """
  A generic message handler for use with event types without any specific handling requirements.
  """
  def inner_handle(self, *args, **kwargs):
    return args, kwargs


class OriginalReceiptHandler(AbstractMessageHandler):
  """
  Handles record_original_purchase_receipt messages.
  """
  def inner_handle(self, *args, **kwargs):
    customer, message, logger, platformApp = unpack(kwargs, 'customer', 'message', 'logger',
        'platformApp')

    # TODO(d-felix): Handle the Web request case.
    if platformApp is None:
      return args, kwargs
    key = event_info_key(message)

    if 'receipt_info_b64' in message.get(key, {}):
      return self.inner_handle_b64(*args, **kwargs)

    purchases = message.get(key, {}).get('receipt_info', {}).get('receipt', {}).get('in_app', [])
    if len(purchases) == 0:
      return args, kwargs

    # TODO(d-felix): In the long term, cache this channel lookup as well as the channel product ones.
    channel, channelCreated = Channel.objects.using(PCONN).get_or_create(
        platform_app_id=platformApp)

    for purchase in purchases:
      if 'product_id' in purchase:
        channelProduct = ChannelProduct.objects.using(PCONN).filter(customer_id=customer,
            channel_product_str=purchase['product_id'], channel_id=channel).first()
        if channelProduct is None:
          product = Product(customer_id=customer, description='')
          product.save(using=PCONN)
          channelProduct = ChannelProduct(channel_product_str=purchase['product_id'],
              channel_id=channel, customer_id=customer, product_id=product, description='')
          channelProduct.save(using=PCONN)
      else:
        logger.warning("No product ID found for in-app purchase having platform_app_id %s" %
            platformApp.platform_app_id)
        pass

    return args, kwargs

  def inner_handle_b64(self, *args, **kwargs):
    customer, message, logger, platformApp, user, userEvent = unpack(kwargs, 'customer', 'message',
        'logger', 'platformApp', 'user', 'userEvent')
    if platformApp is None:
      return args, kwargs
    key = event_info_key(message)
    rawReceipt = message[key]['receipt_info_b64']
    pkcs7_der = b64decode(rawReceipt)

    # TODO(d-felix): Perform verification and not just parsing
    receiptObj = _RECEIPT_VERIFIER.verify_and_parse(pkcs7_der)
    appReceipt = receiptObj.receipt

    # Begin the process of recording the app purchase if necessary.
    appProduct = ChannelProduct.objects.using(PCONN).filter(platform_app_id=platformApp).first()

    # Check for an existing app purchase using the customer_user_id if one exists,
    # otherwise just use scriber_id.
    appPurchase = UserPurchase.objects.using(PCONN).filter(
        scriber_id__customer_user_id=user.customer_user_id, channel_product_id=appProduct).first() \
        if user.customer_user_id else \
        UserPurchase.objects.using(PCONN).filter(scriber_id=user,
        channel_product_id=appProduct).first()
    if appPurchase is None:
      appPurchaseTime = appReceipt[ORIGINAL_PURCHASE_DATE_UNDOCUMENTED.name()]
      appPurchaseEpoch = timestamp(appPurchaseTime)
      appPurchase = UserPurchase(
          scriber_id=user,
          channel_product_id=appProduct,
          event_id=userEvent,
          fingerprint=str(appPurchaseEpoch),
          fingerprint_type=UserPurchase.TIMESTAMP_SEC,
          platform_app_id=platformApp,
          time=appPurchaseTime,
      )
      appPurchase.save(using=PCONN)

    # Process in-app purchases.
    purchases = appReceipt.get('in_app', [])
    if len(purchases) == 0:
      return args, kwargs

    # TODO(d-felix): Override nonsensical django ORM caching behavior here.
    # Do this for every usage of get_or_create
    channel, channelCreated = Channel.objects.using(PCONN).get_or_create(
        platform_app_id=platformApp) 
    for purchase in purchases:
      if 'product_id' in purchase:
        productId = purchase['product_id']
        channelProduct = ChannelProduct.objects.using(PCONN).filter(customer_id=customer,
            channel_product_str=productId, channel_id=channel).first()
        if channelProduct is None:
          product = Product(customer_id=customer, description='')
          product.save(using=PCONN)
          channelProduct = ChannelProduct(channel_product_str=productId, channel_id=channel,
              customer_id=customer, product_id=product, description='')
          channelProduct.save(using=PCONN)

        # Extract proper variables
        cancellationTime = purchase.get('cancellation_date', None)
        expiresTime = purchase.get('expires_date', None)
        platformOrderId = purchase.get('transaction_id', None)
        originalPlatformOrderId = purchase.get('original_transaction_id', None)
        purchaseTime = purchase.get('purchase_date', None)
        originalPurchaseTime = purchase.get('original_purchase_date', None)
        quantity = purchase.get('quantity', None)
        webOrderLineItemId = purchase.get('web_order_line_item_id', None)

        # TODO(d-felix): Revisit this exclusion logic.
        # Does this work for refunds and cancellations?.
        userPurchase = UserPurchase.objects.using(PCONN).filter(platform_app_id=platformApp,
            platform_order_id=platformOrderId).first()
        if userPurchase is not None:
          # Duplicate purchase.
          return args, kwargs

        userPurchase = UserPurchase(
            cancellation_time=cancellationTime,
            channel_product_id=channelProduct,
            expires_time=expiresTime,
            event_id=userEvent,
            ios_web_order_line_item_id=webOrderLineItemId,
            original_platform_order_id=originalPlatformOrderId,
            original_purchase_time=originalPurchaseTime,
            platform_app_id=platformApp,
            platform_order_id=platformOrderId,
            quantity=quantity,
            scriber_id=user,
            time=purchaseTime)
        userPurchase.save(using=PCONN)

      else:
        logger.warning("No product ID found for in-app purchase having platform_app_id %s" %
            platformApp.platform_app_id)
        pass

    return args, kwargs


class PackageInfoHandler(AbstractMessageHandler):
  """
  Handles package_info messages.
  """
  def inner_handle(self, *args, **kwargs):
    customer, message, logger, platformApp, user, userEvent = unpack(kwargs, 'customer', 'message',
        'logger', 'platformApp', 'user', 'userEvent')
    if platformApp is None:
      logger.info("Could not identify platform_app for package_info message for customer %s" %
          customer.customer_id)
      return args, kwargs

    key = event_info_key(message)
    firstInstallTimestamp = message[key].get('firstInstallTime', None)

    if not firstInstallTimestamp:
      logger.info("Missing first install time for package_info message for customer %s" %
          customer.customer_id)
      return args, kwargs

    firstInstallTime = from_timestamp(long(firstInstallTimestamp), utc)
    fingerprint = str(firstInstallTimestamp)
    digits = len(str(fingerprint))
    if digits == 10:
      fingerprintType = UserPurchase.TIMESTAMP_SEC
    elif digits == 13:
      fingerprintType = UserPurchase.TIMESTAMP_MSEC
    elif digits == 16:
      fingerprintType = UserPurchase.TIMESTAMP_USEC
    else:
      fingerprintType = UserPurchase.UNKNOWN

    # Check for an existing app purchase using the customer_user_id if one exists,
    # otherwise just use scriber_id.
    # We expect the channel product to exist.
    appProduct = ChannelProduct.objects.using(PCONN).filter(platform_app_id=platformApp).first()
    appPurchase = UserPurchase.objects.using(PCONN).filter(
        scriber_id__customer_user_id=user.customer_user_id, channel_product_id=appProduct).first() \
        if user.customer_user_id else \
        UserPurchase.objects.using(PCONN).filter(scriber_id=user,
        channel_product_id=appProduct).first()
    if appPurchase is None:
      # Use first install time as a proxy for the purchase time.
      appPurchaseTime = firstInstallTime
      appPurchase = UserPurchase(
          scriber_id=user,
          channel_product_id=appProduct,
          event_id=userEvent,
          fingerprint=fingerprint,
          fingerprint_type=fingerprintType,
          platform_app_id=platformApp,
          time=appPurchaseTime,
      )
      appPurchase.save(using=PCONN)

    return args, kwargs


class ProductPricesHandler(AbstractMessageHandler):
  """
  Handles product_prices messages.
  """
  def inner_handle(self, *args, **kwargs):
    customer, message, logger, platformApp = unpack(kwargs, 'customer', 'message', 'logger',
        'platformApp')
    if platformApp is None:
      logger.info("Could not identify platform_app for product_prices message for customer %s" %
          customer.customer_id)
      return args, kwargs
    key = event_info_key(message)

    # TODO(d-felix): Remove this special handling once event_info is always a dict.
    prices = message[key] if isinstance(message[key], list) else message[key].get('prices', [])
    for price in prices:
      channelProductStr = price.get('product_id', price.get('productIdentifier', None))
      if channelProductStr is None:
        logger.info("Received product_prices message with no product_id field for platform_app_id %s"
            % platformApp.platform_app_id)
        continue

      # Note that product_id values in product_prices messages always correspond to in-app purchases,
      # so the channel_product's purchase channel should contain a non-null platform_app_id
      # corresponding to the app in which the product was purchased.
      channelProducts = ChannelProduct.objects.using(PCONN).filter(customer_id=customer,
          channel_id__platform_app_id=platformApp, channel_product_str=channelProductStr)[:1]
      if not channelProducts:
        logger.info("Could not identify channel_product named %s for platform_app_id %s" %
            (channelProductStr, platformApp.platform_app_id))
        continue
      channelProduct = channelProducts[0]

      priceLocale = price.get('priceLocale', None)
      currency = _currency_from_locale(priceLocale)
      amtLocalStr = price.get('price', None)
      amtLocalRaw = Decimal(str(amtLocalStr)) if amtLocalStr is not None else None
      if currency is None or amtLocalRaw is None:
        logger.info("Could not identify currency or price from product_prices message for " +
            "channel_product_id %s" % channelProduct.channel_product_id)
        continue

      amtLocal = amtLocalRaw.quantize(TEN_PLACES)
      amtLocalMicros = long(amtLocalRaw * 1000000)
      amtUsd = convert_decimal_to_usd(amtLocalRaw, currency)
      if amtUsd is None:
        logger.info("Currency conversion failed due to unrecognized currency: %s" % currency)
        continue
      amtUsdMicros = long(amtUsd * 1000000)

      channelProduct.currency_str = currency
      channelProduct.amt_local = amtLocal
      channelProduct.amt_local_micros = amtLocalMicros
      channelProduct.amt_usd_micros = amtUsdMicros

      # Update the display_name
      displayName = price.get('localizedTitle', None)
      if displayName and priceLocale.startswith('en_US'):
        channelProduct.display_name = displayName

      channelProduct.save(using=PCONN)

    return args, kwargs


class RecordEventHandler(AbstractMessageHandler):
  """
  Handles record_event messages.
  """
  # We override pre_handle since the UserEvent record maintains a reference to the
  # appropriate CustomerLabel.
  def pre_handle(self, *args, **kwargs):
    customer, eventType, logger, message, platformApp, receivedEpoch, user = unpack(kwargs,
        'customer', 'eventType', 'logger', 'message', 'platformApp', 'receivedEpoch', 'user')
    eventTime = event_time(message)
    receivedTime = from_timestamp(receivedEpoch, utc) if receivedEpoch else None

    key = event_info_key(message)
    labelStr = message[key].get('label', message[key].get('event_label', None))
    label, labelCreated = CustomerLabel.objects.using(PCONN).get_or_create(
        customer_id=customer, label=labelStr) if labelStr else (None, False)
    userEvent = UserEvent(event_type=eventType, platform_app_id=platformApp, scriber_id=user,
        time=eventTime, received_time=receivedTime, customer_label_id=label)
    userEvent.save(using=PCONN)

    # Allow dumps() to fail, though this should never happen due to the API view's JSON handling.
    rawUserEventJsonStr = dumps(message)
    rawUserEvent = RawUserEvent(user_event_id=userEvent, raw_user_event=rawUserEventJsonStr)
    rawUserEvent.save(using=PCONN)
    kwargs['userEvent'] = userEvent
    kwargs['rawUserEvent'] = rawUserEvent
    return args, kwargs

  def inner_handle(self, *args, **kwargs):
    return args, kwargs


class RecordPurchaseHandler(AbstractMessageHandler):
  """
  Handles record_purchase messages.
  """
  def inner_handle(self, *args, **kwargs):
    customer, message, logger, platformApp, user, userEvent = unpack(kwargs, 'customer', 'message',
        'logger', 'platformApp', 'user', 'userEvent')
    # TODO(d-felix): Handle iOS, Web better.
    purchaseTime = purchase_time(message)
    if platformApp is None:
      logger.info('No platformApp found for record_purchase message for customer %s: %s' %
          (customer.customer_id, message))
      return args, kwargs
    platformOrderId = platform_order_id(message)
    if platformOrderId is None:
      logger.info('No platform order ID found for record_purchase message for customer %s: %s' %
          (customer.customer_id, message))
      return args, kwargs

    # TODO(d-felix): Revisit this exclusion logic (does this work for refunds and cancellations?).
    userPurchase = UserPurchase.objects.using(PCONN).filter(platform_app_id=platformApp,
        platform_order_id=platformOrderId).first()
    if userPurchase is not None:
      # Duplicate purchase.
      return args, kwargs
    channelProductStr = purchased_product(message)
    if channelProductStr is None:
      logger.info('No purchased product found for record_purchase message for customer %s: %s' %
          (customer.customer_id, message))
      return args, kwargs

    channel, channelCreated = Channel.objects.using(PCONN).get_or_create(
        platform_app_id=platformApp)
    channelProduct = None if channelCreated else ChannelProduct.objects.using(PCONN).filter(
        customer_id=customer, channel_product_str=channelProductStr, channel_id=channel).first()
    if channelProduct is None:
      product = Product(customer_id=customer, description='')
      product.save(using=PCONN)
      # TODO(d-felix): Add description updating in SkuDetailsHandler
      channelProduct = ChannelProduct(channel_product_str=channelProductStr, channel_id=channel,
          customer_id=customer, product_id=product, description='')
      channelProduct.save(using=PCONN)

    userPurchase = UserPurchase(channel_product_id=channelProduct, scriber_id=user,
        event_id=userEvent, platform_app_id=platformApp, platform_order_id=platformOrderId,
        time=purchaseTime)
    userPurchase.currency_str = channelProduct.currency_str
    userPurchase.amt_local = channelProduct.amt_local
    userPurchase.amt_local_micros = channelProduct.amt_local_micros
    userPurchase.amt_usd_micros = channelProduct.amt_usd_micros
    userPurchase.save(using=PCONN)

    kwargs['userPurchase'] = userPurchase
    return args, kwargs


class SetUserInfoHandler(AbstractMessageHandler):
  """
  Handles set_user_info messages.
  """
  def inner_handle(self, *args, **kwargs):
    message, logger, user = unpack(kwargs, 'message', 'logger', 'user')
    eventInfo = message[event_info_key(message)]

    # Use empty strings instead of nulls for fallback values since these User
    # fields are non-nullable.
    oldUserId = user.customer_user_id
    oldEmail = ''
    oldUsername = ''
    newUserId = eventInfo.get('user_id', '')
    newEmail = eventInfo.get('email', '')
    newUsername = eventInfo.get('username', '')

    peers = [user]
    # Retrieve all users who have the same customer_user_id as the current user.
    if user.customer_user_id:
      for peer in User.objects.using(PCONN).filter(customer_user_id=user.customer_user_id):
        if peer.scriber_id != user.scriber_id:
          peers.append(peer)
          # Keep email and username in sync across related users.
          # TODO(d-felix): Find a better solution for this.
          oldEmail = peer.email if not newEmail and peer.email else oldEmail
          oldUsername = peer.username if not newUsername and peer.username else oldUsername
    # Retrieve all users who have the customer_user_id provided in the message.
    if newUserId:
      for peer in User.objects.using(PCONN).filter(customer_user_id=newUserId):
        if peer.scriber_id != user.scriber_id:
          peers.append(peer)
          # Keep email and username in sync across related users.
          oldEmail = peer.email if not newEmail and peer.email else oldEmail
          oldUsername = peer.username if not newUsername and peer.username else oldUsername
    newEmail = newEmail if newEmail else oldEmail
    newUsername = newUsername if newUsername else oldUsername

    # Merge purchase histories if necessary.
    # Apply these merges before overwriting any peers' customer_user_id values.
    if newUserId and (newUserId != oldUserId) and len(peers) > 1:
      merges = {}
      cursor = connections[PCONN].cursor()

      # If an old customer_user_id exists, we must merge purchases from all matching users.
      if oldUserId:
        cursor.execute(_MERGE_MULTIPLE_USERS_PURCHASE_HISTORY_QUERY, [newUserId, oldUserId])
      # Otherwise, only merge purchases made by the current user.
      else:
        cursor.execute(_MERGE_SINGLE_USER_PURCHASE_HISTORY_QUERY, [newUserId, user.scriber_id])
      for row in cursor.fetchall():
        purchaseId, masterPurchaseId = row[0], row[1]
        if purchaseId not in merges:
          merges[purchaseId] = masterPurchaseId
        else:
          # Log if we find multiple master purchases
          logger.warning('Multiple master purchases found when merging purchase_id %s' %
              purchaseId)
      # Assign a master purchase for each collision.
      for purchaseId in merges:
        UserPurchase.objects.using(PCONN).filter(purchase_id=purchaseId).update(
            master_purchase_id=merges[purchaseId])

    # Now apply changes to peers
    for peer in peers:
      peer.customer_user_id = newUserId if newUserId else peer.customer_user_id
      peer.email = newEmail
      peer.username = newUsername
      peer.save(using=PCONN)

    kwargs['user'] = user
    return args, kwargs


class SkuDetailsHandler(AbstractMessageHandler):
  """
  Handles sku_details messages.
  """
  def inner_handle(self, *args, **kwargs):
    customer, message, logger, platformApp = unpack(kwargs, 'customer', 'message',
        'logger', 'platformApp')
    if platformApp is None:
      logger.info('No platformApp found for SKU details message for customer %s: %s' %
          (customer.customer_id, message))
      return args, kwargs
    eventInfo = message[event_info_key(message)]
    isRecurrence = eventInfo.get('type', None) == 'subs'
    sku = eventInfo.get('sku', None)
    if sku is None:
      logger.info('Received SKU details message for customer %s with no sku field: %s' %
          (customer.customer_id, message))
      return args, kwargs

    channel, channelCreated = Channel.objects.using(PCONN).get_or_create(
        platform_app_id=platformApp)
    channelProduct = None if channelCreated else ChannelProduct.objects.using(PCONN).filter(
        customer_id=customer, channel_product_str=sku, channel_id=channel).first()
    if _skip_sku_details_update(channelProduct):
      return args, kwargs

    # At this point the price information must be updated, though the channelProduct itself
    # may already exist.
    if channelProduct is None:
      # product and channelProduct must be created
      product = Product(customer_id=customer, description='')
      product.save(using=PCONN)
      channelProduct = ChannelProduct(channel_product_str=sku, channel_id=channel,
          customer_id=customer, product_id=product, description=eventInfo.get('title', ''))

    title = eventInfo.get('title', '')
    m = re.match(r'([^\(]+)(\(.*\).*)*', title)
    displayName = m.group(1).rstrip() if m else None

    priceString = eventInfo.get('price', None)
    currency, amtLocalRaw = parse(priceString) if priceString else (None, None)
    if currency is None or amtLocalRaw is None:
      logger.info("Could not identify currency or price from sku_details message for %s" % message)
      return args, kwargs
    amtLocal = amtLocalRaw.quantize(TEN_PLACES)
    amtLocalMicros = long(amtLocalRaw * 1000000)
    amtUsd = convert_decimal_to_usd(amtLocalRaw, currency)
    if amtUsd is None:
      logger.info("Currency conversion failed due to unrecognized currency: %s" % currency)
      return args, kwargs
    amtUsdMicros = long(amtUsd * 1000000)

    channelProduct.currency_str = currency
    channelProduct.amt_local = amtLocal
    channelProduct.amt_local_micros = amtLocalMicros
    channelProduct.amt_usd_micros = amtUsdMicros
    channelProduct.is_recurrence = isRecurrence
    channelProduct.display_name = displayName
    channelProduct.save(using=PCONN)

    kwargs['channelProduct'] = channelProduct
    return args, kwargs


def _skip_sku_details_update(channelProduct):
  if not channelProduct:
    return False
  elif channelProduct.amt_local is None or channelProduct.display_name is None:
    return False
  return True


def _currency_from_locale(locale):
  if locale is None:
    return None
  localeParts = locale.split('=')
  if len(localeParts) == 0:
    logger.info("Could not identify currency from localeIdentifier %s" % locale)
    return None
  return localeParts[1]
