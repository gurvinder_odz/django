from ingestor.tasks import handlers

GENERIC_MESSAGE_HANDLER = handlers.GenericMessageHandler()
ORIGINAL_RECEIPT_HANDLER = handlers.OriginalReceiptHandler()
PACKAGE_INFO_HANDLER = handlers.PackageInfoHandler()
PRODUCT_PRICES_HANDLER = handlers.ProductPricesHandler()
RECORD_EVENT_HANDLER = handlers.RecordEventHandler()
RECORD_PURCHASE_HANDLER = handlers.RecordPurchaseHandler()
SET_USER_INFO_HANDLER = handlers.SetUserInfoHandler()
SKU_DETAILS_HANDLER = handlers.SkuDetailsHandler()

EVENT_TYPE_TO_HANDLER_MAP = {
  'app_background': GENERIC_MESSAGE_HANDLER,
  'app_foreground': GENERIC_MESSAGE_HANDLER,
  'app_start': GENERIC_MESSAGE_HANDLER,
  'app_terminate': GENERIC_MESSAGE_HANDLER,
  'device_info': GENERIC_MESSAGE_HANDLER,
  'logout': GENERIC_MESSAGE_HANDLER,
  'package_info': PACKAGE_INFO_HANDLER,
  'product_prices': PRODUCT_PRICES_HANDLER,
  'record_event': RECORD_EVENT_HANDLER,
  'record_original_purchase_receipt': ORIGINAL_RECEIPT_HANDLER,
  'record_purchase': RECORD_PURCHASE_HANDLER,
  'set_user_info': SET_USER_INFO_HANDLER,
  'sku_details': SKU_DETAILS_HANDLER,
}

def handler_for_event_type(eventType):
  """
  Returns the proper message handler instance for the provided event type.
  """
  return handler_for_event_type_str(eventType.event_type_str)

def handler_for_event_type_str(eventTypeStr):
  """
  Returns the proper message handler instance for the provided event type string descriptor.
  No validation of the argument is performed.
  """
  return EVENT_TYPE_TO_HANDLER_MAP[eventTypeStr]
