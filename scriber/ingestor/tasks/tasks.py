from __future__ import absolute_import

import requests

from appinfo import fetcher
from billiard import current_process
from celery import task
from celery.signals import worker_process_init
from celery.utils.log import get_task_logger
from core import scriberid
from core.cache import cache_results
from core.celeryutils import exponential_backoff
from core.monitor import CloudWatchCounter
from core.timeutils import from_timestamp
from customers.models import Customer, CustomerApiKey
from django.conf import settings
from django.db import transaction
from ingestor.models import App, Channel, ChannelProduct, PlatformApp, PlatformType, Product, User, UserEvent, UserEventType
from ingestor.requestutils import event_info_key, unpack
from ingestor.tasks import handlerfactory
from ingestor.tasks.constants import PCONN
from json import loads
from pytz import utc

_MAX_RETRIES = 10

# TODO(d-felix): Reimplement this by adding appropriate User methods.
_SET_USER_INFO_COLUMNS = {'email': 'email', 'username': 'username', 'user_id': 'customer_user_id'}
_SET_USER_INFO_EVENT_TYPE = 'set_user_info'

_IOS_APP_INFO_URL_PREFIX = 'https://itunes.apple.com/lookup?bundleId='

_ANDROID_APP_INFO_API_KEY = 'cd1937dc829cad5dc281f6ccac67c7bc'
_ANDROID_APP_INFO_URL = 'http://api.playstoreapi.com/v1.1/apps/%s?key=%s'
_IOS_APP_INFO_URL = 'https://itunes.apple.com/lookup?bundleId=%s'

handledCtr = CloudWatchCounter(name='HandledMessages')

logger = get_task_logger(__name__)

@worker_process_init.connect
def init_worker_process_cloudwatch(*args, **kwargs):
  if settings.ENABLE_CLOUDWATCH and 'DefaultWorker' in current_process().initargs[1]:
    logger.info('Received worker_process_init signal, initializing CloudWatch connection')
    handledCtr.connect()

# TODO(d-felix): If newScriberId=False then we should be able to use externalScriberId
# as a proxy for deviceId and platformAppId if those values can't be found.
# TODO(d-felix): Improve idempotency.
# TODO(d-felix): Convert all task parameter passing to *args and **kwargs to avoid failures after
# in-place signature modifications.
@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def process_message_list(self, requestData, externalScriberId, newScriberId, receivedEpoch):
  try:
    # TODO(d-felix): Remove this functionality once we've processed all old malformed requests.
    _unpack_messages(requestData)
    # TODO(d-felix): Remove this functionality once we've implemented proper request validation.
    if 'platform' not in requestData:
      logger.info('Received request without a platform value: %s' % requestData)
      return

    customerApiKey = CustomerApiKey.objects.using(PCONN).get(api_key=requestData['api_key'])
    customer = customerApiKey.customer_id
    user, newScriberId = _extract_user(requestData, externalScriberId, newScriberId, customer)

    # TODO(d-felix): Implement SDK version updating.
    # Create a new PlatformApp if necessary.
    platformType = _platform_type(requestData)
    platformAppStr = _platform_app_str(requestData)
    platformApp, createNewPlatformApp = _create_new_platform_app(customer, platformType,
        platformAppStr)
    channelProduct = None

    with transaction.atomic(using=PCONN):
      if newScriberId:
        user.save(using=PCONN)
      if createNewPlatformApp:
        app = App(customer_id=customer, name=platformAppStr, is_active=True)
        app.save(using=PCONN)
        platformApp = PlatformApp(platform_app_str=platformAppStr, platform_type_id=platformType,
            customer_id=customer, latest_sdk_version="", description=platformAppStr, app_id=app)
        platformApp.save(using=PCONN)
      # Create a new ChannelProduct for the app itself if necessary
      if platformApp is not None and (createNewPlatformApp or
         not _channel_product_exists(platformApp)):
        channel = Channel.objects.using(PCONN).get(platform_type_id=platformApp.platform_type_id)
        product = Product(customer_id=customer, description=platformApp.platform_app_str)
        product.save(using=PCONN)
        channelProduct = ChannelProduct(channel_product_str=platformApp.platform_app_str,
            channel_id=channel, customer_id=customer, platform_app_id=platformApp,
            product_id=product, description='')
        channelProduct.save(using=PCONN)
      if platformApp is not None:
        channelProduct = ChannelProduct.objects.using(PCONN).filter(
            platform_app_id=platformApp).first() if channelProduct is None else channelProduct
        if platformType.platform_type_str in ('iOS', 'Android'):
          if (channelProduct.amt_local is None) or (platformType.platform_type_str == 'iOS'
              and not channelProduct.apple_identifier):
            fetch_app_info.delay(
                channel_product_id=channelProduct.channel_product_id,
                platform_app_str=platformApp.platform_app_str,
                platform_type_str=platformType.platform_type_str)

      # Try to infer the platformApp if we haven't found one yet.
      platformApp = _platform_app(externalScriberId) if platformApp is None else platformApp

      platformAppId = None if platformApp is None else platformApp.platform_app_id
      for message in requestData['messages']:
        process_message.delay(message=message, platformAppId=platformAppId,
            receivedEpoch=receivedEpoch, userId=user.scriber_id)

  # TODO(d-felix): Consider special handling for these and other cases:
  # CustomerApiKey.DoesNotExist, CustomerApiKey.MultipleObjectsReturned,
  # User.DoesNotExist, User.MultipleObjectsReturned
  except Exception as e:
    logger.info("Process message list failed for request %s" % requestData)
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def process_message(self, *args, **kwargs):
  try:
    message, platformAppId, receivedEpoch, userId = unpack(kwargs, 'message', 'platformAppId',
        'receivedEpoch', 'userId')
    user = User.objects.using(PCONN).get(pk=userId)
    platformApp = PlatformApp.objects.using(PCONN).get(pk=platformAppId) if platformAppId else None

    # TODO(d-felix): Remove this functionality once we've implemented proper request validation.
    if message.get('event_type', None) == 'record_purchase_reciept':
      message['event_type'] = 'record_purchase'

    eventType = UserEventType.objects.using(PCONN).get(event_type_str=message['event_type'])
    with transaction.atomic(using=PCONN):
      handler = handlerfactory.handler_for_event_type(eventType)
      handler.handle(customer=user.customer_id, eventType=eventType, logger=logger,
          message=message, platformApp=platformApp, receivedEpoch=receivedEpoch, user=user)

    # Redis does not yet support fanout exchange types, so we can't rely on more precise
    # periodic boadcasts to dispatch buffered metrics.
    handledCtr.increment()
    if handledCtr.is_ready():
      handledCtr.dispatch_results()

  # For now, treat all exceptions as transient errors and retry.
  except (Exception, UserEventType.DoesNotExist) as e:
    # Add message contents to the log entry.
    e.message = 'Ingestor process_message failed with error %s for message %s' % \
        (e.message, message)
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def fetch_app_info(self, *args, **kwargs):
  try:
    channelProductId, platformAppStr, platformTypeStr = unpack(kwargs, 'channel_product_id',
        'platform_app_str', 'platform_type_str')
    appInfo = fetcher.fetch(platformAppStr, platformTypeStr)
    if appInfo:
      channelProduct = ChannelProduct.objects.using(PCONN).get(pk=channelProductId)
      channelProduct.currency_str = appInfo.currency
      channelProduct.amt_local = appInfo.amt_local
      channelProduct.amt_local_micros = appInfo.amt_local_micros
      channelProduct.amt_usd_micros = appInfo.amt_usd_micros
      if appInfo.identifier and platformTypeStr == 'iOS':
        channelProduct.apple_identifier = long(appInfo.identifier)
      channelProduct.save(using=PCONN)
    else:
      logger.info('Could not fetch app info for app %s on platform %s' %
          (platformAppStr, platformTypeStr))

  # For now, treat all exceptions as transient errors and retry.
  except Exception as e:
    # Add message contents to the log entry.
    e.message = 'fetch_app_info task failed with error %s for channel_product_id %s' % \
        (e.message, channelProductId)
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


def _extract_user(requestData, externalScriberId, newScriberId, customer):
  # End user identity is handled differently for Web requests with user_id populated.
  webRequest = requestData.get('platform', None) == 'Web'
  customerManagedIdentity = webRequest and requestData.get('user_id', None) is not None

  # To avoid permanent task failures, always re-check if the user currently exists.
  if customerManagedIdentity:
    users = User.objects.using(PCONN).filter(customer_user_id=requestData['user_id'],
        customer_id=customer)
  else:
    users = User.objects.using(PCONN).filter(external_scriber_id=externalScriberId,
        customer_id=customer)

  if not users:
    user = User(customer_id=customer)
    if customerManagedIdentity:
      # Treat as a new user. Ignore any provided externalScriberId, and generate a new one to
      # prevent bad customer-generated data from being persisted.
      user.customer_user_id = requestData['user_id']
      user.external_scriber_id = scriberid.new_scriber_id()
      newScriberId = True
    elif not newScriberId:
      # An unrecognized scriber_id value was provided. Abort processing to prevent bad
      # customer-generated data from being persisted.
      raise ValueError('Client passed unrecognized scriber_id %s for customer %s' %
          (externalScriberId, customer.customer_id))
    elif webRequest:
      # This shouldn't happen since we require a user_id or a scriber_id for all web requests.
      # Abort processing.
      raise ValueError(('Encountered Web request for customer %s without valid user_id or ' +
          'scriber_id. Aborting processing.') % customer.customer_id)
    else:
      # Treat as a new user. This is the normal new user flow.
      user.external_scriber_id = externalScriberId
  else:
    # Note that each user is uniquely identified by its external_scriber_id together with its
    # customer_id
    newScriberId = False
    user = users[0]
  return user, newScriberId

def _platform_type(requestData):
  if 'platform' in requestData and requestData['platform'] is not None:
    return _platform_type_backend(requestData['platform'])
  return None

@cache_results(timeout=60*60)
def _platform_type_backend(platform):
  return PlatformType.objects.using(PCONN).get(platform_type_str=platform)

def _platform_app(externalScriberId):
  events = UserEvent.objects.using(PCONN).filter(scriber_id__external_scriber_id=externalScriberId)\
      .exclude(platform_app_id__isnull=True)[:1]
  if len(events) == 0:
    return None
  return events[0].platform_app_id

def _platform_app_str(requestData):
  if 'app_id' in requestData and requestData['app_id'] is not None:
    return requestData['app_id']
  # Fall back to bundle_id values in purchase receipts.
  else:
    for message in requestData['messages']:
      if message['event_type'] == 'record_original_purchase_receipt':
        key = event_info_key(message)
        bundleId = message[key].get('receipt_info', {}).get('receipt', {}).get('bundle_id', None)
        if bundleId is not None:
          return bundleId
  return None

def _result_is_false(result, *args, **kwargs):
  if result is False:
    return True
  return False

def _second_result_entry_is_false(result, *args, **kwargs):
  if result[1] is False:
    return True
  return False

# @cache_results(timeout=60*60, update_cache=_second_result_entry_is_false)
def _create_new_platform_app(customer, platformType, platformAppStr):
  if not (customer and platformType and platformAppStr):
    return (None, False)
  platformApps = PlatformApp.objects.using(PCONN).filter(customer_id=customer,
      platform_app_str=platformAppStr, platform_type_id=platformType)
  if len(platformApps) == 0:
    return (None, True)
  return (platformApps[0], False)

# @cache_results(timeout=60*60, update_cache=_result_is_false)
def _channel_product_exists(platformApp):
  return ChannelProduct.objects.using(PCONN).filter(platform_app_id=platformApp).exists()

# TODO(d-felix): Remove this functionality once we've processed all old malformed requests.
def _unpack_messages(requestData):
  messages = requestData.get('messages', [])
  if isinstance(messages, str) or isinstance(messages, unicode):
    requestData['messages'] = loads(messages)
