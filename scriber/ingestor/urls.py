from django.conf.urls import patterns, url
from ingestor import views

urlpatterns = patterns('',
  url(r'^$', views.handle_api),
)
