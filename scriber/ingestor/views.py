import json
import logging
import time
import requests

from core import apikey, scriberid
from core.timeutils import utc_epoch
from django.core.exceptions import MultipleObjectsReturned
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from ingestor.requestutils import products_without_prices
from ingestor.tasks.tasks import process_message_list

logger = logging.getLogger(__name__)

# TODO(d-felix): Improve responses for error cases
# TODO(d-felix): Add logging
@csrf_exempt
def handle_api(request):
  """
  Processes API calls.  If no scriber_id is provided, then a new user is created and its
  scriber_id is returned.
  """
  if request.method == 'POST':
    try:
      data = json.loads(request.body)

      # Check for and validate the api key
      if 'api_key' not in data or not apikey.is_valid(data['api_key']):
        return _http_response(message='Invalid or missing API key', status=400)

      newScriber = 'scriber_id' not in data or not scriberid.is_valid_format(data['scriber_id'])
      external_scriber_id = scriberid.new_scriber_id() if newScriber else data['scriber_id']
      receivedEpoch = utc_epoch()

      if newScriber and 'user_id' not in data and 'platform' in data and data['platform'] == 'Web':
        return _http_response(message="No valid scriber_id or user_id value found in request " +\
            "having a 'Web' platform value", status=400)

      process_message_list.delay(data, external_scriber_id, newScriber, receivedEpoch)

      # Construct and return the response
      isWebUser = 'user_id' in data and data['user_id'] is not None and \
          'platform' in data and data['platform'] == 'Web'
      external_scriber_id_to_return = external_scriber_id if (newScriber and not isWebUser) \
          else None
      productsWithoutPrices = products_without_prices(data)
      productIdsToLookUp = productsWithoutPrices if productsWithoutPrices else None
      return _http_response(product_ids_to_look_up=productIdsToLookUp,
          scriber_id=external_scriber_id_to_return)

    except ValueError:
      logger.info("Could not decode request body in handle_api function")
      return _http_response(message='Invalid JSON in request body', status=400)

  logger.info("Received a request without a POST method: %s" % request.method)
  return _http_response(message='Only POST method allowed', status=405)

def _http_response(message='OK', product_ids_to_look_up=None, scriber_id=None, status=200):
  contentDict = {'message': message}
  if scriber_id is not None:
    contentDict['scriber_id'] = scriber_id
  if product_ids_to_look_up is not None:
    contentDict['product_ids_to_look_up'] = product_ids_to_look_up
  content = json.dumps(contentDict)
  return HttpResponse(content, status=status, content_type='application/json')
