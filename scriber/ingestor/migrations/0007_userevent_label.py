# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0006_channelproduct_platform_app_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='userevent',
            name='label',
            field=models.CharField(max_length=256, null=True, blank=True),
            preserve_default=True,
        ),
    ]
