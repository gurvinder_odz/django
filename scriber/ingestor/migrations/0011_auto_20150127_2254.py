# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0010_channel_platform_type_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='channelproduct',
            name='amt_local',
            field=models.DecimalField(null=True, max_digits=30, decimal_places=10),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='amt_local_micros',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='amt_usd_micros',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='currency_str',
            field=models.CharField(default=b'', max_length=32, blank=True),
            preserve_default=True,
        ),
    ]
