# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0021_auto_20150303_2057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channelproduct',
            name='customer_id',
            field=models.ForeignKey(db_column=b'customer_id', default=1, to='customers.Customer'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='platformapp',
            name='customer_id',
            field=models.ForeignKey(db_column=b'customer_id', default=1, to='customers.Customer'),
            preserve_default=False,
        ),
    ]
