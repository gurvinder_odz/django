# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0008_auto_20150122_2334'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='channel',
            name='channel_type_id',
        ),
        migrations.DeleteModel(
            name='ChannelType',
        ),
        migrations.AddField(
            model_name='channel',
            name='description',
            field=models.CharField(default=b'', max_length=256, blank=True),
            preserve_default=True,
        ),
    ]
