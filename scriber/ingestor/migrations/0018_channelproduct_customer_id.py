# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20150130_2045'),
        ('ingestor', '0017_auto_20150220_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='channelproduct',
            name='customer_id',
            field=models.ForeignKey(db_column=b'customer_id', to='customers.Customer', null=True),
            preserve_default=True,
        ),
        migrations.RunSQL(
            sql="""
                UPDATE channel_products cp
                SET customer_id = p.customer_id
                FROM products p
                WHERE cp.product_id = p.product_id;
                """,
        ),
    ]