# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0011_auto_20150127_2254'),
    ]

    operations = [
        migrations.AddField(
            model_name='channelproduct',
            name='is_recurrence',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
