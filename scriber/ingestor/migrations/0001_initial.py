# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='App',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('app_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'apps',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChannelProduct',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('channel_product_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('channel_product_str', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
            ],
            options={
                'db_table': 'channel_products',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ChannelType',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('channel_type_id', models.AutoField(serialize=False, primary_key=True)),
                ('channel_type_str', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
            ],
            options={
                'db_table': 'channel_types',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(max_length=256, serialize=False, primary_key=True)),
                ('numeric', models.IntegerField(unique=True)),
                ('description', models.CharField(max_length=512)),
            ],
            options={
                'db_table': 'currencies',
                'verbose_name': 'currency',
                'verbose_name_plural': 'currencies',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlatformApp',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('platform_app_id', models.AutoField(serialize=False, primary_key=True)),
                ('platform_app_str', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
                ('app_id', models.ForeignKey(to='ingestor.App', db_column=b'app_id')),
            ],
            options={
                'db_table': 'platform_apps',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlatformType',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('platform_type_id', models.AutoField(serialize=False, primary_key=True)),
                ('platform_type_str', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
            ],
            options={
                'db_table': 'platform_types',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('product_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('description', models.CharField(max_length=512)),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'products',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawUserDevice',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('raw_user_device_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('raw_user_device', models.CharField(max_length=65536)),
            ],
            options={
                'db_table': 'raw_user_devices',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawUserEvent',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('raw_user_event_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('raw_user_event', models.CharField(max_length=65536)),
            ],
            options={
                'db_table': 'raw_user_events',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawUserPurchase',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('raw_user_purchase_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('raw_user_purchase', models.CharField(max_length=16384)),
            ],
            options={
                'db_table': 'raw_user_purchases',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RawUserReceipt',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('raw_user_receipt_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('raw_user_receipt', models.CharField(max_length=16384)),
            ],
            options={
                'db_table': 'raw_user_receipts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('scriber_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('external_scriber_id', models.CharField(max_length=256, db_index=True)),
                ('customer_user_id', models.CharField(db_index=True, max_length=256, blank=True)),
                ('email', models.CharField(max_length=256, blank=True)),
                ('username', models.CharField(max_length=256, blank=True)),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'users',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserDevice',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('device_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'user_devices',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserEvent',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('event_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('time', models.DateTimeField(null=True, blank=True)),
                ('received_time', models.DateTimeField()),
            ],
            options={
                'db_table': 'user_events',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserEventType',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('event_type_id', models.AutoField(serialize=False, primary_key=True)),
                ('event_type_str', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
            ],
            options={
                'db_table': 'user_event_types',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserPurchase',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('purchase_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('amt_local', models.DecimalField(null=True, max_digits=30, decimal_places=10, blank=True)),
                ('amt_local_micros', models.BigIntegerField()),
                ('amt_usd_micros', models.BigIntegerField(null=True, blank=True)),
                ('time', models.DateTimeField(null=True, blank=True)),
                ('channel_type_id', models.ForeignKey(to='ingestor.ChannelType', db_column=b'channel_type_id')),
                ('currency', models.ForeignKey(to='ingestor.Currency', db_column=b'currency')),
                ('event_id', core.customfields.BigForeignKey(to='ingestor.UserEvent', db_column=b'event_id')),
                ('platform_app_id', models.ForeignKey(to='ingestor.PlatformApp', db_column=b'platform_app_id')),
                ('product_id', core.customfields.BigForeignKey(to='ingestor.Product', db_column=b'product_id')),
                ('scriber_id', core.customfields.BigForeignKey(to='ingestor.User', db_column=b'scriber_id')),
            ],
            options={
                'db_table': 'user_purchases',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserReceipt',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('receipt_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('original_purchase_time', models.DateTimeField(null=True, blank=True)),
                ('original_transaction_id', models.CharField(max_length=256, blank=True)),
                ('channel_product_id', core.customfields.BigForeignKey(db_column=b'channel_product_id', blank=True, to='ingestor.ChannelProduct', null=True)),
                ('event_id', core.customfields.BigForeignKey(to='ingestor.UserEvent', db_column=b'event_id')),
                ('platform_app_id', models.ForeignKey(db_column=b'platform_app_id', blank=True, to='ingestor.PlatformApp', null=True)),
                ('scriber_id', core.customfields.BigForeignKey(to='ingestor.User', db_column=b'scriber_id')),
            ],
            options={
                'db_table': 'user_receipts',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='userevent',
            name='event_type',
            field=models.ForeignKey(to='ingestor.UserEventType', db_column=b'event_type'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userevent',
            name='platform_app_id',
            field=models.ForeignKey(db_column=b'platform_app_id', blank=True, to='ingestor.PlatformApp', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userevent',
            name='scriber_id',
            field=core.customfields.BigForeignKey(to='ingestor.User', db_column=b'scriber_id'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userdevice',
            name='event_id',
            field=core.customfields.BigForeignKey(to='ingestor.UserEvent', db_column=b'event_id'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rawuserreceipt',
            name='user_receipt_id',
            field=core.customfields.BigForeignKey(db_column=b'user_receipt_id', to='ingestor.UserReceipt', unique=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rawuserpurchase',
            name='user_purchase_id',
            field=core.customfields.BigForeignKey(db_column=b'user_purchase_id', to='ingestor.UserPurchase', unique=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rawuserevent',
            name='user_event_id',
            field=core.customfields.BigForeignKey(db_column=b'user_event_id', to='ingestor.UserEvent', unique=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='rawuserdevice',
            name='user_device_id',
            field=core.customfields.BigForeignKey(db_column=b'user_device_id', to='ingestor.UserDevice', unique=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='platformapp',
            name='platform_type_id',
            field=models.ForeignKey(to='ingestor.PlatformType', db_column=b'platform_type_id'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='platformapp',
            unique_together=set([('platform_app_str', 'platform_type_id')]),
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='channel_type_id',
            field=models.ForeignKey(to='ingestor.ChannelType', db_column=b'channel_type_id'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='product_id',
            field=core.customfields.BigForeignKey(to='ingestor.Product', db_column=b'product_id'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='channelproduct',
            unique_together=set([('channel_product_str', 'channel_type_id')]),
        ),
    ]
