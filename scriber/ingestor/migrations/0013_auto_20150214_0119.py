# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0012_channelproduct_is_recurrence'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userpurchase',
            name='currency',
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='currency_str',
            field=models.CharField(default=b'', max_length=32, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='platform_order_id',
            field=models.CharField(default=None, max_length=256, null=True, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userpurchase',
            name='amt_local_micros',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='userpurchase',
            name='amt_usd_micros',
            field=models.BigIntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='userpurchase',
            unique_together=set([('platform_app_id', 'platform_order_id')]),
        ),
    ]
