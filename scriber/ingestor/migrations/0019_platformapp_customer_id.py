# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20150130_2045'),
        ('ingestor', '0018_channelproduct_customer_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='platformapp',
            name='customer_id',
            field=models.ForeignKey(db_column=b'customer_id', to='customers.Customer', null=True),
            preserve_default=True,
        ),
        migrations.RunSQL(
            sql="""
                UPDATE platform_apps pa
                SET customer_id = a.customer_id
                FROM apps a
                WHERE pa.app_id = a.app_id;
                """,
        ),
    ]
