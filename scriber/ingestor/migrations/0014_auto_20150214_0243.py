# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0013_auto_20150214_0119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userpurchase',
            name='channel_product_id',
            field=core.customfields.BigForeignKey(to='ingestor.ChannelProduct', db_column=b'channel_product_id'),
            preserve_default=True,
        ),
    ]
