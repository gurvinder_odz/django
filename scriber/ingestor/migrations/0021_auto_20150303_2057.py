# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0020_auto_20150224_1901'),
    ]

    operations = [
        migrations.AddField(
            model_name='channelproduct',
            name='apple_identifier',
            field=models.BigIntegerField(unique=True, null=True, db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='apple_sku',
            field=models.CharField(max_length=100, null=True),
            preserve_default=True,
        ),
    ]
