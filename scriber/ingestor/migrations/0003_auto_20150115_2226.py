# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0002_auto_20150113_0022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userpurchase',
            name='amt_usd_micros',
            field=models.BigIntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='userpurchase',
            name='platform_app_id',
            field=models.ForeignKey(db_column=b'platform_app_id', blank=True, to='ingestor.PlatformApp', null=True),
            preserve_default=True,
        ),
    ]
