# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0022_auto_20150317_0021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channelproduct',
            name='apple_identifier',
            field=models.BigIntegerField(null=True, db_index=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='channelproduct',
            unique_together=set([('customer_id', 'apple_identifier')]),
        ),
        migrations.AlterUniqueTogether(
            name='platformapp',
            unique_together=set([('customer_id', 'platform_app_str', 'platform_type_id')]),
        ),
    ]
