# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0024_auto_20150319_1928'),
    ]

    operations = [
        migrations.AddField(
            model_name='channelproduct',
            name='display_name',
            field=models.CharField(max_length=256, null=True),
            preserve_default=True,
        ),
    ]
