# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0014_auto_20150214_0243'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpurchase',
            name='cancellation_time',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='expires_time',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='ios_web_order_line_item_id',
            field=models.BigIntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='original_platform_order_id',
            field=models.CharField(default=None, max_length=256, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='original_purchase_time',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='quantity',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
