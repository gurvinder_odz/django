# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0025_channelproduct_display_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='platformtype',
            name='platform_type_str',
            field=models.CharField(unique=True, max_length=256, choices=[(b'Android', b'Android'), (b'iOS', b'iOS'), (b'Web', b'Web')]),
            preserve_default=True,
        ),
    ]
