# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0009_auto_20150127_1957'),
    ]

    operations = [
        migrations.AddField(
            model_name='channel',
            name='platform_type_id',
            field=models.ForeignKey(db_column=b'platform_type_id', to='ingestor.PlatformType', null=True),
            preserve_default=True,
        ),
    ]
