# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0019_platformapp_customer_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpurchase',
            name='fingerprint',
            field=models.CharField(max_length=256, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='fingerprint_type',
            field=models.CharField(max_length=256, null=True, choices=[(b'timestamp_sec', b'Timestamp in seconds')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='master_purchase_id',
            field=core.customfields.BigForeignKey(db_column=b'master_purchase_id', to='ingestor.UserPurchase', null=True),
            preserve_default=True,
        ),
    ]
