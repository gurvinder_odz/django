# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userpurchase',
            name='channel_type_id',
        ),
        migrations.RemoveField(
            model_name='userpurchase',
            name='product_id',
        ),
        migrations.AddField(
            model_name='app',
            name='is_active',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userpurchase',
            name='channel_product_id',
            field=core.customfields.BigForeignKey(db_column=b'channel_product_id', default=1, to='ingestor.Product'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='usereventtype',
            name='event_type_str',
            field=models.CharField(unique=True, max_length=256),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='user',
            unique_together=set([('external_scriber_id', 'customer_id')]),
        ),
    ]
