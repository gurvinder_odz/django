# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0005_remove_channelproduct_channel_type_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='channelproduct',
            name='platform_app_id',
            field=models.ForeignKey(null=True, db_column=b'platform_app_id', to='ingestor.PlatformApp', unique=True),
            preserve_default=True,
        ),
    ]
