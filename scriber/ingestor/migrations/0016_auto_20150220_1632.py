# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20150130_2045'),
        ('ingestor', '0015_auto_20150218_1916'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerLabel',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('customer_label_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('label', models.CharField(max_length=256)),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'customer_labels',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='customerlabel',
            unique_together=set([('customer_id', 'label')]),
        ),
        migrations.RemoveField(
            model_name='userevent',
            name='label',
        ),
        migrations.AddField(
            model_name='userevent',
            name='customer_label_id',
            field=core.customfields.BigForeignKey(db_column=b'customer_label_id', to='ingestor.CustomerLabel', null=True),
            preserve_default=True,
        ),
    ]
