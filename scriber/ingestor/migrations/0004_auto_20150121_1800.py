# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0003_auto_20150115_2226'),
    ]

    operations = [
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('channel_id', models.AutoField(serialize=False, primary_key=True)),
                ('channel_type_id', models.ForeignKey(to='ingestor.ChannelType', db_column=b'channel_type_id')),
                ('platform_app_id', models.ForeignKey(null=True, db_column=b'platform_app_id', to='ingestor.PlatformApp', unique=True)),
            ],
            options={
                'db_table': 'channels',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='channelproduct',
            name='channel_id',
            field=models.ForeignKey(db_column=b'channel_id', default=0, to='ingestor.Channel'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='platformapp',
            name='latest_sdk_version',
            field=models.CharField(default='', max_length=32, blank=True),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='channelproduct',
            unique_together=set([('channel_product_str', 'channel_id')]),
        ),
    ]
