# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0004_auto_20150121_1800'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='channelproduct',
            name='channel_type_id',
        ),
    ]
