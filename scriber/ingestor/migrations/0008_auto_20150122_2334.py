# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0007_userevent_label'),
    ]

    operations = [
        migrations.AlterField(
            model_name='platformtype',
            name='platform_type_str',
            field=models.CharField(unique=True, max_length=256),
            preserve_default=True,
        ),
    ]
