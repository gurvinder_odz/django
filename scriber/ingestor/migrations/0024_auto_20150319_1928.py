# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ingestor', '0023_auto_20150317_0032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userpurchase',
            name='fingerprint_type',
            field=models.CharField(max_length=256, null=True, choices=[(b'timestamp_sec', b'Timestamp in seconds'), (b'timestamp_msec', b'Timestamp in milliseconds'), (b'timestamp_usec', b'Timestamp in microseconds'), (b'unknown', b'Unknown')]),
            preserve_default=True,
        ),
    ]
