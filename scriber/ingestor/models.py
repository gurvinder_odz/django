from core.customfields import BigAutoField, BigForeignKey
from core.models import TimestampedModel
from customers.models import Customer, CustomerApiKey
from django.db import models


class User(TimestampedModel):
  scriber_id = BigAutoField(primary_key=True)
  external_scriber_id = models.CharField(max_length=256, db_index=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  customer_user_id = models.CharField(max_length=256, db_index=True, blank=True)
  email = models.CharField(max_length=256, blank=True)
  username = models.CharField(max_length=256, blank=True)

  class Meta:
    db_table = 'users'
    unique_together = ('external_scriber_id', 'customer_id')


class App(TimestampedModel):
  app_id = models.AutoField(primary_key=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  name = models.CharField(max_length=256)

  # This field indicates whether the current grouping of one or more PlatformApp records into an app
  # is active for metrics computation and display purposes.  No PlatformApp records should have
  # associated apps that are inactive.
  is_active = models.BooleanField(default=True)

  class Meta:
    db_table = 'apps'


class UserEventType(TimestampedModel):
  event_type_id = models.AutoField(primary_key=True)
  event_type_str = models.CharField(max_length=256, unique=True)
  description = models.CharField(max_length=512)

  class Meta:
    db_table = 'user_event_types'


class PlatformType(TimestampedModel):
  platform_type_id = models.AutoField(primary_key=True)

  ANDROID = 'Android'
  IOS = 'iOS'
  WEB = 'Web'
  PLATFORM_TYPE_STR_CHOICES = (
    (ANDROID, 'Android'),
    (IOS, 'iOS'),
    (WEB, 'Web'),
  )
  platform_type_str = models.CharField(choices=PLATFORM_TYPE_STR_CHOICES,
      max_length=256, unique=True)
  description = models.CharField(max_length=512)

  class Meta:
    db_table = 'platform_types'


class PlatformApp(TimestampedModel):
  platform_app_id = models.AutoField(primary_key=True)
  platform_app_str = models.CharField(max_length=256)
  platform_type_id = models.ForeignKey(PlatformType, db_column="platform_type_id")
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  latest_sdk_version = models.CharField(max_length=32, blank=True)
  description = models.CharField(max_length=512)
  app_id = models.ForeignKey(App, db_column="app_id")

  class Meta:
    db_table = 'platform_apps'
    unique_together = ('customer_id', 'platform_app_str', 'platform_type_id')


class CustomerLabel(TimestampedModel):
  customer_label_id = BigAutoField(primary_key=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  label = models.CharField(max_length=256)

  class Meta:
    db_table = 'customer_labels'
    unique_together = ('customer_id', 'label')


class UserEvent(TimestampedModel):
  event_id = BigAutoField(primary_key=True)
  event_type = models.ForeignKey(UserEventType, db_column="event_type")
  scriber_id = BigForeignKey(User, db_column="scriber_id")

  # There may not be an app associated with a user event, so allow platform_app_id to be null
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id",
      null=True, blank=True)

  # The time of the event as recorded by the user's device.  Allow blank values so that otherwise
  # valid events with malformed timestamps can still be persisted.
  time = models.DateTimeField(db_index=True, null=True, blank=True)

  # The time Scriber received the corresponding API call.
  received_time = models.DateTimeField()

  # A customer-assigned label for custom events.
  customer_label_id = BigForeignKey(CustomerLabel, null=True, db_column="customer_label_id")

  class Meta:
    db_table = 'user_events'


# Use a unique BigForeignKey instead of relying on the unstable one-to-one support for
# BigAutoFields.
class RawUserEvent(TimestampedModel):
  raw_user_event_id = BigAutoField(primary_key=True)
  user_event_id = BigForeignKey(UserEvent, unique=True, db_column="user_event_id")

  # AWS RDS currently does not support PostgreSQL 9.4, so there is no opportunity to write a custom
  # model field for the jsonb data type.
  raw_user_event = models.CharField(max_length=65536)

  class Meta:
    db_table = 'raw_user_events'


# Begin models that will ultimately be moved out of the ingestor application.

class Currency(TimestampedModel):
  code = models.CharField(primary_key=True, max_length=256)
  numeric = models.IntegerField(unique=True)
  description = models.CharField(max_length=512)

  class Meta:
    db_table = 'currencies'
    verbose_name = 'currency'
    verbose_name_plural = 'currencies'


class Product(TimestampedModel):
  product_id = BigAutoField(primary_key=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id")
  description = models.CharField(max_length=512)

  class Meta:
    db_table = 'products'


# Purchase channels through which users purchase products.
# TODO(d-felix): Add unique nullable foreign keys to additional tables (e.g. CustomerWebStore)
# once these tables exist.
class Channel(TimestampedModel):
  channel_id = models.AutoField(primary_key=True)
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id",
      unique=True, null=True)
  description = models.CharField(max_length=256, blank=True, default='')

  # Foreign keys to PlatformType values represent generic platform stores, e.g. the iTunes store
  # and the Google Play Store.
  platform_type_id = models.ForeignKey(PlatformType, db_column="platform_type_id", null=True)

  class Meta:
    db_table = 'channels'


class ChannelProduct(TimestampedModel):
  channel_product_id = BigAutoField(primary_key=True)
  channel_product_str = models.CharField(max_length=256)
  channel_id = models.ForeignKey(Channel, db_column="channel_id")
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id",
      unique=True, null=True)
  description = models.CharField(max_length=512)
  display_name = models.CharField(max_length=256, null=True)
  product_id = BigForeignKey(Product, db_column="product_id")
  customer_id = models.ForeignKey(Customer, db_column="customer_id")

  apple_identifier = models.BigIntegerField(null=True, db_index=True)
  apple_sku = models.CharField(max_length=100, null=True)

  # Price information
  amt_local = models.DecimalField(max_digits=30, decimal_places=10, null=True)
  amt_local_micros = models.BigIntegerField(null=True)
  amt_usd_micros = models.BigIntegerField(null=True)

  # For now, avoid using a foreign key to the currencies table.
  currency_str = models.CharField(max_length=32, blank=True, default='')
  is_recurrence = models.BooleanField(default=False)

  class Meta:
    db_table = 'channel_products'
    unique_together = ('customer_id', 'channel_product_str', 'channel_id')
    unique_together = ('customer_id', 'apple_identifier')


class UserPurchase(TimestampedModel):
  purchase_id = BigAutoField(primary_key=True)
  master_purchase_id = BigForeignKey('self', null=True, db_column="master_purchase_id")
  channel_product_id = BigForeignKey(ChannelProduct, db_column="channel_product_id")
  scriber_id = BigForeignKey(User, db_column="scriber_id")
  event_id = BigForeignKey(UserEvent, db_column="event_id")

  # There may not be an app associated with a user purchase, so allow platform_app_id to be null
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id",
      null=True, blank=True)
  platform_order_id = models.CharField(max_length=256, db_index=True, null=True, default=None)
  original_platform_order_id = models.CharField(max_length=256, null=True, default=None)

  # Allow amounts to be null so that user_purchase processing can succeed without product price
  # information. Product price information can be retrieved irregularly, and price information
  # in the channel_products table can be used as a fallback.
  amt_local = models.DecimalField(max_digits=30, decimal_places=10, null=True, blank=True)
  amt_local_micros = models.BigIntegerField(null=True)
  amt_usd_micros = models.BigIntegerField(null=True)

  # For now, avoid using a foreign key to the currencies table.
  currency_str = models.CharField(max_length=32, blank=True, default='')

  # The time of the purchase
  time = models.DateTimeField(null=True, blank=True)
  original_purchase_time = models.DateTimeField(null=True, blank=True)
  cancellation_time = models.DateTimeField(null=True, blank=True)
  expires_time = models.DateTimeField(null=True, blank=True)

  quantity = models.IntegerField(default=1)
  ios_web_order_line_item_id = models.BigIntegerField(null=True, blank=True)

  # Fingerprinting fields used for deduplication.
  TIMESTAMP_SEC = 'timestamp_sec'
  TIMESTAMP_MSEC = 'timestamp_msec'
  TIMESTAMP_USEC = 'timestamp_usec'
  UNKNOWN = 'unknown'
  FINGERPRINT_TYPE_CHOICES = (
    (TIMESTAMP_SEC, 'Timestamp in seconds'),
    (TIMESTAMP_MSEC, 'Timestamp in milliseconds'),
    (TIMESTAMP_USEC, 'Timestamp in microseconds'),
    (UNKNOWN, 'Unknown'),
  )
  fingerprint = models.CharField(max_length=256, null=True)
  fingerprint_type = models.CharField(choices=FINGERPRINT_TYPE_CHOICES, max_length=256, null=True)

  class Meta:
    db_table = 'user_purchases'
    unique_together = ('platform_app_id', 'platform_order_id')


# Use a unique BigForeignKey instead of relying on unstable one-to-one support for BigAutoFields.
class RawUserPurchase(TimestampedModel):
  raw_user_purchase_id = BigAutoField(primary_key=True)
  user_purchase_id = BigForeignKey(UserPurchase, unique=True, db_column="user_purchase_id")
  raw_user_purchase = models.CharField(max_length=16384)

  class Meta:
    db_table = 'raw_user_purchases'


class UserReceipt(TimestampedModel):
  receipt_id = BigAutoField(primary_key=True)
  event_id = BigForeignKey(UserEvent, db_column="event_id")
  scriber_id = BigForeignKey(User, db_column="scriber_id")

  # Allow null channel_product_id and platform_app_id values so that unrecognized receipt
  # information can be processed.
  channel_product_id = BigForeignKey(ChannelProduct, db_column="channel_product_id",
      null=True, blank=True)
  platform_app_id = models.ForeignKey(PlatformApp, db_column="platform_app_id",
      null=True, blank=True)
  original_purchase_time = models.DateTimeField(null=True, blank=True)
  original_transaction_id = models.CharField(max_length=256, blank=True)

  class Meta:
    db_table = 'user_receipts'


# Use a unique BigForeignKey instead of relying on unstable one-to-one support for BigAutoFields.
class RawUserReceipt(TimestampedModel):
  raw_user_receipt_id = BigAutoField(primary_key=True)
  user_receipt_id = BigForeignKey(UserReceipt, unique=True, db_column="user_receipt_id")
  raw_user_receipt = models.CharField(max_length=16384)

  class Meta:
    db_table = 'raw_user_receipts'


class UserDevice(TimestampedModel):
  device_id = BigAutoField(primary_key=True)
  event_id = BigForeignKey(UserEvent, db_column="event_id")

  class Meta:
    db_table = 'user_devices'


# Use a unique BigForeignKey instead of relying on unstable one-to-one support for BigAutoFields.
class RawUserDevice(TimestampedModel):
  raw_user_device_id = BigAutoField(primary_key=True)
  user_device_id = BigForeignKey(UserDevice, unique=True, db_column="user_device_id")
  raw_user_device = models.CharField(max_length=65536)

  class Meta:
    db_table = 'raw_user_devices'
