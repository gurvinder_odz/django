from django.db import connection
from ingestor.models import PlatformApp

# select exists(select event_id from user_events where platform_app_id = 1 limit 1);
SDK_QUERY = 'SELECT %s;'
SDK_QUERYARG = 'EXISTS(SELECT 1 FROM user_events where platform_app_id = %s LIMIT 1)'

def sdk_versions_for_app_list(app_list):
  """
  Returns the latest SDK version seen for every app in the provided list.
  These values are returned as a dict of dicts. The outer dict is indexed by platform type strings,
  while the inner dicts are indexed by the PlatformApp.platform_app_str identifiers.
  """
  platformApps = app_list
  resultsDict = {}
  queryArgs = [SDK_QUERYARG % p.platform_app_id for p in platformApps]
  if queryArgs:
    query = SDK_QUERY % ', '.join(queryArgs)
    cursor = connection.cursor()
    cursor.execute(query)
    row = cursor.fetchone()
    for idx, eventExists in enumerate(row):
      if eventExists:
        platformApp = platformApps[idx]
        platformType = platformApp.platform_type_id.platform_type_str
        latestSdkVersion = platformApp.latest_sdk_version if (
            platformApp.latest_sdk_version != "") else None
        if platformType not in resultsDict:
          resultsDict[platformType] = {}
        resultsDict[platformType][platformApp.platform_app_str] = latestSdkVersion
  return resultsDict

def sdk_versions(customer):
  """
  Returns the latest SDK version seen for every app belonging to the provided customer.
  """
  platformApps = [p for p in PlatformApp.objects.filter(customer_id=customer)]
  return sdk_versions_for_app_list(platformApps)

def sdk_versions_for_auth_user(user):
  """
  Returns the latest SDK version seen for every app belonging to a customer associated with the
  provided auth user.
  """
  platformApps = [p for p in PlatformApp.objects.filter(customer_id__auth_user=user)]
  return sdk_versions_for_app_list(platformApps)
