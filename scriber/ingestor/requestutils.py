import logging

from core.timeutils import from_timestamp
from dateutil import parser
from django.db import connection
from numbers import Real
from pytz import utc

logger = logging.getLogger(__name__)

_PRODUCT_PRICES_QUERY = """
  SELECT
    cp.channel_product_str channel_product_str
  FROM
    customers c JOIN customer_api_keys k ON c.customer_id = k.customer_id
    JOIN platform_apps pa ON pa.customer_id = c.customer_id
    JOIN platform_types pt ON pa.platform_type_id = pt.platform_type_id
    JOIN channels ch ON pa.platform_app_id = ch.platform_app_id
    JOIN channel_products cp ON ch.channel_id = cp.channel_id
  WHERE
    k.api_key = %s
    AND pa.platform_app_str = %s
    AND pt.platform_type_str = %s
    AND (cp.amt_local IS NULL OR ((NOW() - INTERVAL '7 day') < cp.last_modified))
  ;
  """

def unpack(kwargs, *args):
  """
  Utility function for retrieving desired values from a kwargs dict.
  """
  ret = []
  for arg in args:
    ret.append(kwargs.get(arg, None))
  return tuple(ret)

def api_key(requestData):
  return requestData.get('api_key', None)

def event_info_key(message):
  key = 'event_info' if 'event_info' in message else 'info'
  return key

def event_time(message):
  """
  Returns a datetime object for the event_time value.
  """
  rawEventTime = message.get('event_time', None)
  if isinstance(rawEventTime, Real):
    return from_timestamp(rawEventTime, utc)
  elif isinstance(rawEventTime, str):
    return parser.parse(rawEventTime).astimezone(utc)
  else:
    return None

def platform_app_str(requestData):
  return requestData.get('app_id', None)

def platform_order_id(message):
  if message.get('event_type', None) != 'record_purchase':
    logger.info('Cannot retrieve platform order id from message %s' % message)
    return None
  key = event_info_key(message)
  ret = message[key].get('orderId', None)
  # TODO(d-felix): Handle iOS
  return ret

def platform_type_str(requestData):
  return requestData.get('platform', None)

# TODO(d-felix): Caching
def products_without_prices(requestData):
  """
  Returns a list of string product IDs present in the requestData argument for which we have no
  price information in the database.
  """
  apiKey = api_key(requestData)
  platformAppStr = platform_app_str(requestData)
  platformTypeStr = platform_type_str(requestData)

  if platformAppStr is None or platformTypeStr != 'iOS':
    return []

  # Gate the inclusion of product IDs by the appearance of a
  # record_original_purchase_receipt message.
  if 'record_original_purchase_receipt' not in [m.get('event_type', None)
      for m in requestData.get('messages', [])]:
    return []

  productIdList = []
  cursor = connection.cursor()
  cursor.execute(_PRODUCT_PRICES_QUERY, [apiKey, platformAppStr, platformTypeStr])
  for row in cursor.fetchall():
    productIdList.append(row[0])

  return productIdList

def purchase_time(message):
  eventTime = from_timestamp(message['event_time'], utc) if 'event_time' in message else None
  if message.get('event_type', None) != 'record_purchase':
    logger.info('Cannot retrieve purchased product from message %s' % message)
    return None
  key = event_info_key(message)
  if 'purchaseTime' in message[key]:
    return from_timestamp(message[key]['purchaseTime'], utc)
  return eventTime

def purchased_product(message):
  if message.get('event_type', None) != 'record_purchase':
    logger.info('Cannot retrieve purchased product from message %s' % message)
    return None
  # TODO(d-felix): Handle iOS and Web
  key = event_info_key(message)
  return message[key].get('productId', None)
