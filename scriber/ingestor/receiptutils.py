from core.timeutils import timestamp
from datetime import datetime

_EXCLUDED_KEYS = ['opaque_value', 'raw_bundle_id', 'sha1_hash']

def normalize_receipt(arg):
  """
  Normalizes receipt objects for persistence.
  The arguments is modified during execution; apply to a deep copy of a receipt
  for non-destructive use.
  """
  if isinstance(arg, dict):
    for key in _EXCLUDED_KEYS:
      if arg.get(key, None):
        arg.pop(key, None)
  for entry in arg:
    value = arg[entry] if isinstance(arg, dict) else entry
    if hasattr(value, '__iter__'):
      normalize_receipt(value)
    elif isinstance(arg, dict) and isinstance(value, datetime):
      arg[entry] = timestamp(arg[entry])
