<div>
  <h3>
    <a id="user-content-api-for-sdk" class="anchor" href="#api-for-sdk" aria-hidden="true"><span class="octicon octicon-link"></span></a>API for SDK</h3>

    <h3>
      <a id="user-content-post-scriberioapi" class="anchor" href="#post-scriberioapi" aria-hidden="true"><span class="octicon octicon-link"></span></a>POST apptheta.com/api/</h3>

      <p>This is the main API endpoint that consumes all end user request data.</p>

      <p>Please note that:</p>

      <ul class="task-list">
        <li>GET requests to this endpoint will receive a 405 'Method Not Allowed' response.</li>
        <li>The trailing slash is required. POST requests to <code>http://apptheta.com/api</code> will receive a 301 'Moved Permanently' response with the <code>Location</code> header set to <code>http://apptheta.com/api/</code>. This response will be returned with text/html content type.</li>
      </ul>

      <h5>
        <a id="user-content-request-format" class="anchor" href="#request-format" aria-hidden="true"><span class="octicon octicon-link"></span></a>Request format</h5>

        <p>The request body must be valid JSON with the following format.</p>

        <pre><code>{
          scriber_id:  STRING,
          user_id:     STRING
          api_key:     STRING,
          app_id:      STRING,
          platform:    STRING,
          sdk_version: STRING,
          messages: [
            {
              event_type: STRING,
              event_time: NUMBER,
              event_info: { }
            },
          ]
}</code></pre>
    <p>Unrecognized fields are allowed. Recognized top-level fields are described in the following table.</p>

    <p><strong>Note that only the required top-level fields are used for initial request validation and response generation. Other required fields are inspected at message processing time, which occurs after the API server dispatches a response.</strong></p>

    <table class="table table-striped code-table">
      <thead>
        <tr>
          <th>Field</th>
          <th>Allowed values</th>
          <th>Required</th>
          <th>Comments</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><code>scriber_id</code></td>
          <td>An alphanumeric string</td>
          <td>No</td>
          <td>Omit only for new users</td>
        </tr>
        <tr>
          <td><code>user_id</code></td>
          <td>String (256 character limit)</td>
          <td>No, but one of <code>user_id</code> and <code>scriber_id</code> is required for all requests with a <code>platform</code> value of 'Web'</td>
          <td>This is a customer-defined ID space</td>
        </tr>
        <tr>
          <td><code>api_key</code></td>
          <td>Your alphanumeric API key string</td>
          <td>Yes</td>
          <td></td>
        </tr>
        <tr>
          <td><code>app_id</code></td>
          <td>Bundle ID for iOS apps, or application package for Android apps</td>
          <td>No</td>
          <td>Omit if the request does not originate from an iOS or Android app</td>
        </tr>
        <tr>
          <td><code>platform</code></td>
          <td>'iOS', 'Android', or 'Web'</td>
          <td>No</td>
          <td>Will likely become required in the future</td>
        </tr>
        <tr>
          <td><code>sdk_version</code></td>
          <td>The SDK version used, if applicable</td>
          <td>No</td>
          <td></td>
        </tr>
        <tr>
          <td><code>messages</code></td>
          <td>A list of message objects formatted as described below</td>
          <td>Yes</td>
          <td></td>
        </tr>
      </tbody>
    </table>

    <h4>
      <a id="user-content-message-format" class="anchor" href="#message-format" aria-hidden="true"><span class="octicon octicon-link"></span></a>Message format</h4>

      <p>Each entry of the required <code>messages</code> list should adhere to the following format.</p>

      <pre><code>{
        event_type: STRING,
        event_time: NUMBER,
        event_info: { }
      }
    </code></pre>

    <p>Unrecognized fields are allowed. Recognized fields are described in the following table:</p>

    <table class="table table-striped code-table">
      <thead>
        <tr>
          <th>Field</th>
          <th>Allowed values</th>
          <th>Required</th>
          <th>Comments</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><code>event_type</code></td>
          <td>One of the allowed values shown below</td>
          <td>Yes</td>
          <td></td>
        </tr>
        <tr>
          <td><code>event_time</code></td>
          <td>Seconds, milliseconds, or microsenconds since the UNIX epoch time</td>
          <td>No</td>
          <td>Scale is detected automatically</td>
        </tr>
        <tr>
          <td><code>event_info</code></td>
          <td>A JSON object with fields described below</td>
          <td>Yes</td>
          <td>May be empty for some <code>event_type</code> values (see below)</td>
        </tr>
        <tr>
          <td><code>info</code></td>
          <td>Synonym for <code>event_info</code>
          </td>
          <td>No (one of <code>event_info</code> or <code>info</code> is required)</td>
          <td>DEPRECATED</td>
        </tr>
      </tbody>
    </table>

    <p>The <code>event_type</code> string must take on one of the following values.</p>

    <ul class="task-list">
      <li><code>app_start</code></li>
      <li><code>app_background</code></li>
      <li><code>app_foreground</code></li>
      <li><code>app_terminate</code></li>
      <li><code>record_event</code></li>
      <li><code>record_money_event</code></li>
      <li><code>record_purchase</code></li>
      <li><code>record_original_purchase_receipt</code></li>
      <li><code>set_user_info</code></li>
      <li><code>logout</code></li>
    </ul>

    <h4>
      <a id="user-content-event_info-format" class="anchor" href="#event_info-format" aria-hidden="true"><span class="octicon octicon-link"></span></a><code>event_info</code> format</h4>

      <p>Recognized <code>event_info</code> fields are shown below for each allowed <code>event_type</code> value. Unrecognized fields are allowed.</p>

      <h5>
        <a id="user-content-app_start-app_background-app_foreground-app_terminate-and-logout" class="anchor" href="#app_start-app_background-app_foreground-app_terminate-and-logout" aria-hidden="true"><span class="octicon octicon-link"></span></a><code>app_start</code>, <code>app_background</code>, <code>app_foreground</code>, <code>app_terminate</code>, and <code>logout</code>
      </h5>

      <p>No recognized fields.</p>

      <h5>
        <a id="user-content-record_event" class="anchor" href="#record_event" aria-hidden="true"><span class="octicon octicon-link"></span></a><code>record_event</code>
      </h5>

      <p>This event type is used for recording customer-defined events. A <code>label</code> field is used to distinguish between different kinds of customer-defined events.</p>

      <table class="table table-striped code-table">
        <thead>
          <tr>
            <th>Field</th>
            <th>Allowed values</th>
            <th>Required</th>
            <th>Comments</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><code>label</code></td>
            <td>String (256 character maximum)</td>
            <td>No, but not very useful if omitted</td>
            <td></td>
          </tr>
        </tbody>
      </table>

      <h4>
        <a id="user-content-record_money_event" class="anchor" href="#record_money_event" aria-hidden="true"><span class="octicon octicon-link"></span></a><code>record_money_event</code>
      </h4>

      <h4>
        <a id="user-content-record_purchase-record_original_purchase_receipt" class="anchor" href="#record_purchase-record_original_purchase_receipt" aria-hidden="true"><span class="octicon octicon-link"></span></a><code>record_purchase</code>, <code>record_original_purchase_receipt</code>
      </h4>

      <p>TODO(d-felix): Add documentation.</p>

      <h4>
        <a id="user-content-set_user_info" class="anchor" href="#set_user_info" aria-hidden="true"><span class="octicon octicon-link"></span></a><code>set_user_info</code>
      </h4>

      <p>This event type is used for associating various pieces of information with an end user.</p>

      <table class="table table-striped code-table">
        <thead>
          <tr>
            <th>Field</th>
            <th>Allowed values</th>
            <th>Required</th>
            <th>Comments</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><code>email</code></td>
            <td>String (256 character maximum)</td>
            <td>No</td>
            <td></td>
          </tr>
          <tr>
            <td><code>user_id</code></td>
            <td>String (256 character maximum)</td>
            <td>No</td>
            <td></td>
          </tr>
          <tr>
            <td><code>username</code></td>
            <td>String (256 character maximum)</td>
            <td>No</td>
            <td></td>
          </tr>
        </tbody>
      </table>

      <h4>
        <a id="user-content-response-format" class="anchor" href="#response-format" aria-hidden="true"><span class="octicon octicon-link"></span></a>Response format</h4>

        <p>Response content will be JSON formatted and will have the following generic format.</p>

        <pre><code>{
          message:    STRING,
          scriber_id: STRING
        }
      </code></pre>

      <p>Field details are as follows.</p>

      <table class="table table-striped code-table">
        <thead>
          <tr>
            <th>Field</th>
            <th>Allowed values</th>
            <th>Required</th>
            <th>Comments</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><code>message</code></td>
            <td>String</td>
            <td>Yes</td>
            <td>Typically 'OK', but contains additional information for some status codes</td>
          </tr>
          <tr>
            <td><code>scriber_id</code></td>
            <td>Alphanumeric string</td>
            <td>No</td>
            <td>Present only if no scriber_id was provided in the original request</td>
          </tr>
        </tbody>
      </table>

      <h2>
        <a id="user-content-using-the-api" class="anchor" href="#using-the-api" aria-hidden="true"><span class="octicon octicon-link"></span></a>Using the API</h2>



        <h3>
          <a id="user-content-example-request" class="anchor" href="#example-request" aria-hidden="true"><span class="octicon octicon-link"></span></a>Example request</h3>

          <p>Here is a pretty-printed example body of a request from an iOS app with a single <code>record_event</code> message.</p>

          <pre><code>{
            "api_key": "ntLFil6CkVmhKpU7kue26pQuC12HkJAwNnuPJb1HERs",
            "app_id": "fake.bundleId.forTesting",
            "messages": [
            {
            "event_info": {
            "label": "custom test event"
          },
          "event_time": 1422057665,
          "event_type": "record_event"
        }
        ],
        "platform": "iOS",
        "scriber_id": "lJVSoRD6PryVdg87XPhjxOIqBVCq6InA85zNkf9HsJ9WiK54yZNQHrlwmzb6LU4k"
      }
    </code></pre>


  </div>
