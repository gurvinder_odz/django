from __future__ import absolute_import

import os
from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'scriber_app.settings.prod')

app = Celery(include=[
    'appstore.deeprank.deeprank',
    'appstore.tasks',
    'dashboard.tasks',
    'emailer.tasks',
    'ingestor.tasks.tasks',
    'metrics.tasks.reporttasks',
    'metrics.tasks.tasks',
    'ml.tasks',
    'realtime.batchtasks',
    'realtime.onlinetasks',
])

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object(os.environ['DJANGO_SETTINGS_MODULE'])
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
