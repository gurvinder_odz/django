/*global thetaUtil:true */
angular.module('theta')

// controller for dasdhboard and drilldowns
.controller('AccountController', function($location, $scope, $http, $modal) {

  var url = "/customers/get_user_settings/?z";
  $http.get(url, {}).then(function(response) {
    console.log(response.data)
    $scope.sendNewsletter = response.data.send_marketing_email;
    $scope.sendDailySalesReport = response.data.send_daily_report;
  });     

  $scope.toggleDailyReport = function () {
    console.log("toggle daily");
    var url = "/customers/toggle_daily_report/?z";
    $http.get(url, {}).then(function(response) {
      console.log(response.data.current_value)
      $scope.sendDailySalesReport = response.data.current_value;
    });     
  }

  $scope.toggleNewsletter = function () {
    console.log("toggle newsletter");
    var url = "/customers/toggle_newsletter_subscription/?z";
    $http.get(url, {}).then(function(response) {
      console.log(response.data.current_value)
      $scope.sendNewsletter = response.data.current_value;
    });     
  }
 
  $scope.openCancelPopover = function () {
    mixpanel.track("open cancel confirmation");
    $modal.open({
     templateUrl: 'cancel_popover.html',
     controller: 'CancelInstanceController',
     size: 'sm',
     resolve: {
     }
   });

  };

  $scope.openAddAccountPopover = function (platform, username) {
    mixpanel.track("open add account popover - " + platform);
    ga('send', 'event', 'Open Add Account Form');
    $modal.open({
     templateUrl: 'add_account_popover.html',
     controller: 'AddAccountInstanceController',
     size: 'sm',
     resolve: {
       'platform': function() { return platform; },
       'username': function() { return username; },
     }
   });
  };

  $scope.openDeleteAccountConfirmPopover = function (platform, username) {
    mixpanel.track("open delete account popover - " + platform);
    $modal.open({
     templateUrl: 'confirm_delete_account_popover.html',
     controller: 'ConfirmDeleteAccountInstanceController',
     size: 'sm',
     resolve: {
       'platform': function() { return platform; },
       'username': function() { return username; },
     }
   });
  };

})


.controller('CancelInstanceController', function ($scope, $http, $modalInstance) {

  $scope.ok = function () {
  	var url = "/customers/cancel_ajax/";
    var req = {
      method: 'POST',
      url: url,
      headers: {
        'Content-Type': null,
        "X-CSRFToken": thetaUtil.getCookie('csrftoken')
      },
      data: {}
    };
    $http(req).then(function(response) {
      $modalInstance.dismiss('cancel');
      alert(response.data.message);
    });
  };

  $scope.cancel = function () {
   $modalInstance.dismiss('cancel');
 };


})


.controller('AddAccountInstanceController', function ($window, $http, $scope, $modalInstance, platform, username) {
  $scope.loadingAccount = false;
  $scope.username = username;
  if (username) {
    $scope.isUpdate = true;
  }
  $scope.salesPlatform = platform;
  if (platform == "iTunes Connect") {
    $scope.platformIconURL = "/static/img/apple-bite.png"
  } else {
    $scope.platformIconURL = "/static/img/android.png"
  }

  $scope.addAccount = function () {
    $scope.loadingAccount = true;
    var url = "/customers/update_sales_reports_account/";
    var req = {
      method: 'POST',
      url: url,
      headers: {
        'Content-Type': null,
        "X-CSRFToken": thetaUtil.getCookie('csrftoken')
      },
      data: { "platform":$scope.salesPlatform,
              "username":$scope.username,
              "password":$scope.password,
      }
    };
    $http(req).then(function(response) {
      if (!response.data.error) {
        if (response.data.message) {
          mixpanel.track("Update Account", {"success":true, "platform":platform});
          ga('send', 'event', 'Update Account');
          $scope.loadingAccount = false;
          $scope.accountSyncMessage = response.data.message;
        } else {
          mixpanel.track("Add Account", {"success":true, "platform":platform});
          ga('send', 'event', 'Add Account');
          setTimeout(function() {
            $window.location.reload();
          }, 3000);
        }
      } else {
        mixpanel.track("Add Account Error", {"success":false, "platform":platform});
        ga('send', 'event', 'Add Account', 'Error', response.data.error);
        $scope.loadingAccount = false;
        $scope.accountSyncError = response.data.error;
      }
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
    mixpanel.track("Cancel Add Account");
  };

})

.controller('ConfirmDeleteAccountInstanceController', function ($http, $scope, $modalInstance, platform, username) {
  $scope.username = username;
  $scope.salesPlatform = platform;

  $scope.deleteAccount = function () {
    var url = "/customers/delete_sales_reports_account/";
    var req = {
      method: 'POST',
      url: url,
      headers: {
        'Content-Type': null,
        "X-CSRFToken": thetaUtil.getCookie('csrftoken')
      },
      data: { "platform":$scope.salesPlatform,
              "username":$scope.username,
      }
    };
    $http(req).then(function(response) {
      $modalInstance.dismiss('cancel');
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

});





