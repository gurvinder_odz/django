/*global moment:true, developer_mode:true, sdk_versions:true, django_user:true */

angular.module('theta')

// controller for dasdhboard and drilldowns, itunes_accounts:true
.controller('GraphController', function(FollowAppsGraphDataService, PlatformInfoService, ProductService, DatePickerService, $location, $timeout, $scope, $http, $window, $rootScope, $modal, $log, GraphDataService, CustomEventsGraphDataService) {

  $scope.initSuperClass = function () {
    // used when loading sales data initially 
    $scope.pollEnded = false;
    $scope.accountJustSynced = false;
    $scope.pollForUpdateGoing = false;

    // control visibility of search field
    $scope.searchShowing = false;

    $scope.datePickerService = new DatePickerService();
    $scope.datePickerService.initDate();

    // fetch customers apps
    ProductService.fetchApps();

    // fetch category/country/rank-type info
    PlatformInfoService.fetchPlatformInfo();

    $rootScope.$on('productsUpdated', function(){
      $scope.fetchDataIfProducts($scope.graphData);
      if (!$scope.pollEnded && $scope.user) {
          $scope.pollForNewAccountData();
      }
    });
  };


  $scope.endDateString = function () {
   var endDate = $scope.datePickerService.date.endDate;

    var today = moment();
    var daysPassed = moment(today).diff(endDate, 'days');
    if(daysPassed === 0) {
      return "Today";      
    }
    if(daysPassed == 1) {
      return "Yesterday";      
    }
    return moment(new Date(endDate)).fromNow();
  };


  $scope.fetchDataIfProducts = function (graphData) {
    // as soon as products arrive, fetch some graph data
    if (ProductService.productIDString) {
      graphData.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate);
    } else {
      // show controls, user doesn't have products yet
      graphData.dataLoaded = true;
    }    
  };

  $scope.pollForNewAccountData = function () {
    var url = '/customers/check_report_sync_status';
    $http.get(url).then(function(response) {
      if (response.data.status["Google Cloud"] == "pending" ||
        response.data.status["iTunes Connect"] == "pending") {
        $scope.pollForUpdateGoing = true;
        setTimeout(function() {
          ProductService.fetchApps();
        }, 1500);
      } else if (response.data.status["Google Cloud"] == "finished" ||
                 response.data.status["iTunes Connect"] == "finished") {
        $scope.pollEnded = true;
        if ($scope.pollForUpdateGoing) {
          $scope.accountJustSynced = true;
          $scope.pollForUpdateGoing = false;
          ProductService.fetchApps();
        }
      }
    });   
  };


  // refresh graphs after a product is selected or deselected
  $rootScope.refreshGraphsAfterProductSelect = function(app_id) {
    if ($scope.graphData.products[app_id]) {
      $scope.graphData.products[app_id].checked = $scope.graphData.products[app_id].checkedOverall;
      for (var i=0;i<$scope.graphData.products[app_id].in_app_products.length;i++) {
        $scope.graphData.products[app_id].in_app_products[i].checked = $scope.graphData.products[app_id].checkedOverall;
      }
    }
    this.saveAndRefreshUI();    
  };


  $rootScope.saveAndRefreshUI = function() {
    $scope.graphData.updateProductHash();
    $scope.graphData.saveProductSettings();
    if ($scope.active.sales) {
      $scope.graphData.refreshGraphs();
      $rootScope.$emit('graphDataUpdated');
    } else if ($scope.active.usage) {
      $scope.graphDataCustomEvents.refreshGraphs();
      $rootScope.$emit('customGraphDataUpdated');
    }
  };

  
  $scope.setGraphWidth = function (graphDataController) {
    var width;
    if ($window.innerWidth >= 768) {
      width = $scope.getWindowDimensions().w/3-30;
    } else {
      width = $scope.getWindowDimensions().w-80;
    }
    graphDataController.resizeGraphs(0, width);
  }


  // open a drilldown when a graph is clicked
  $scope.openGraph = function(graph) {
     var loc = '/dashboard/drilldown/' + graph.name + '/' + $window.location.hash;
     mixpanel.track("click graph");
     $window.location.href = loc;
  };


  $scope.openAlertsPopover = function (size) {
    mixpanel.track("open alerts");
    $modal.open({
      templateUrl: 'alerts_popover.html',
      controller: 'AlertsModalInstanceCtrl',
      windowClass: 'app-modal-window',
      size: size,
      resolve: {
      }
    });
  };


  // open the product selection modal
  $scope.openProductPopover = function (size) {
    mixpanel.track("open change products");
    $modal.open({
      templateUrl: 'product_popover.html',
      controller: 'ProductsModalInstanceCtrl',
      windowClass: 'app-modal-window',
      size: size,
      resolve: {
        graphData: function () {
          return $scope.graphData;
        },
        products: function () {
          return $scope.products;
        }
      }
    });
  };


  // open the product selection modal
  $scope.openCategoryPopover = function (size) {
    if (!$scope.user) {
      mixpanel.track("Competitors Menu", {"logged in": false});
    } else {
      mixpanel.track("Competitors Menu", {"logged in": true});      
    }

    $modal.open({
      templateUrl: 'category_popover.html',
      controller: 'ProductsModalInstanceCtrl',
      windowClass: 'app-modal-window',
      size: size,
      resolve: {
        graphData: function () {
          return $scope.graphDataRanks;
        }
      }
    });
  };


  $rootScope.graph_rendered = function(graph) {
   if (graph.realtime) {
     $rootScope.realtime_graph = graph;
     return;
   }

    if (!$rootScope.littleGraphs) {
      $rootScope.littleGraphs = [];
    }
    if (!graph.customEventGraph) {
      $rootScope.littleGraphs.push(graph);
    }
    if ($scope.graphData.graphs && !$rootScope.graphDataCustomEvents && !series_name) {
      $rootScope.graphDataCustomEvents = new CustomEventsGraphDataService();
      $scope.graphDataCustomEvents.dataLoaded = false;
      $rootScope.graphDataCustomEvents.products = $scope.graphData.products;
      $rootScope.graphDataCustomEvents.parentGraphData = $scope.graphData;
      $rootScope.graphDataCustomEvents.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate);
    }
  };


  $scope.getCustomers = function(query) {
    var url = '/dashboard/autocomplete/' + query + '/';
    return $http.get(url, {}).then(function(response){
      return response.data.slice(0,10);
    });
  };


  $scope.onSelect = function (customer) {
    $scope.openCustomerPopover(customer);
  };


  // open the product selection modal
  $scope.openCustomerPopover = function (customer) {
    mixpanel.track("open customer profile");
    $scope.selectedCustomer = customer;
    var url = '/dashboard/customer_info/' + $scope.selectedCustomer.id + '/';
    return $http.get(url, {}).then(function(response){
      $scope.selectedCustomer = response.data.customer;
      $modal.open({
        templateUrl: 'customer_popover.html',
        controller: 'CustomerLookupModalCtrl',
        size: 'lg',
        resolve: {
        graphData: function () {
          return $scope.graphData;
        },
          customer: function () {
            return $scope.selectedCustomer;
          }
        }
      });
    });
  };


  $scope.selectActiveTab = function (tab) {
    for (var key in $scope.active) {
      if (key == tab) {
        $scope.active[key] = true;
        var search = $location.path('/').search();
        search.tab = key;
        $location.path('/').search(search);
      } else {
        $scope.active[key] = false;          
      }
    }
    if (tab == "sales") {
      $scope.activeGraphData = $scope.graphData;
    } else if (tab == "usage") {
      $scope.activeGraphData = $scope.graphDataCustomEvents;
    }
    /*if (tab != "following" && $scope.activeGraphData && $scope.activeGraphData.graphs && $scope.activeGraphData.graphs[0]) {
      $scope.viewRealtime($scope.activeGraphData.graphs[0].name);      
    }*/
  };


  $scope.activateTab = function (tab) {
    mixpanel.track('Dashboard Tab', {'type': tab});
    $scope.selectActiveTab(tab);
    if (tab == "sales") {
      $scope.accountJustSynced = false;
      $scope.$emit('graphDataUpdated');
    } else if (tab == "usage") {
      $scope.$emit('selectActiveTab');
    } else if (tab == "following") {  
    }
  };
   

  $scope.iconForPlatform = function(platform) {
    if (platform == "App Store") {
      return "/static/img/apple-bite.png";      
    } else {
      return "/static/img/android.png";      
    }
  };


  $scope.iconForApp = function(key) {
    if (ProductService.appInfo.fetchedInfo[key]) {
      if (ProductService.appInfo.fetchedInfo[key].artworkUrl60) {
        return ProductService.appInfo.fetchedInfo[key].artworkUrl60;              
      }
      return ProductService.appInfo.fetchedInfo[key].thumbnails[0];              
    }
    return "/static/img/blank.png";      
  };


  $scope.nameForApp = function(key) {
    if ($scope.graphData.appInfo.fetchedInfo[key]) {
      if ($scope.graphData.appInfo.fetchedInfo[key].trackName) {
        return $scope.graphData.appInfo.fetchedInfo[key].trackName;      
      }     
      if ($scope.graphData.appInfo.fetchedInfo[key].appName) {
        return $scope.graphData.appInfo.fetchedInfo[key].appName;      
      }     
    }
    return "Unknown Name";      
  };


  $scope.viewRealtime = function(seriesName) {
    /*if ($scope.realtimeGraphData) {
      $scope.realtimeGraphData.realtimeGraphType = seriesName;
      if (ProductService.appInfo.productIDString && 
          ProductService.appInfo.productIDString.length && 
          seriesName) {
        $scope.realtimeGraphData.fetchAndLoadRealtimeData();
      }      
    }*/
  };


  $scope.hasRealtimeGraph = function(graph) {
    if (graph.name == "Sessions" ||
        graph.name == "Daily Revenue" ||
        graph.options.customEventGraph) {
      return true;
    }
    return false;
  };


  $scope.timeDescriptionForRealtimeGraph = function(graph) {
    if(!graph) {
      return;
    }
    if (graph.name == "sessions" || graph.name == "Sessions") {
      return "Now";
    }
    // revenue bucket width is 60 minutes
    return "in the last hour";
  };

});


angular.module('theta')

.controller('MainGraphController',  function (FollowAppsGraphDataService, $http, PlatformInfoService, RealtimeGraphDataService, ProductService, RanksGraphDataService, uiGridConstants, DatePickerService, $controller, $scope, $rootScope, $location, $window, GraphDataService) {
  $controller('GraphController', {$scope: $scope});

  //resize the graphs and redisplay the slider after new data is fetched
  $rootScope.$on('graphDataUpdated', function(){
    $rootScope.littleGraphs = [];                  
    $scope.setGraphWidth($scope.graphData);
  });


  $rootScope.hideSetup = function() {
    var url = '/customers/hide_setup';
    $http.get(url).then(function(response) {
      if (response.data.status == "success") {
        $scope.activateTab("sales");
        $scope.hideSetup = true;
      }
    });   
  }


  $rootScope.$on('customGraphDataUpdated', function(){
    $scope.graphDataCustomEvents.dataLoaded = true;
    if($scope.graphDataCustomEvents) {
      $scope.setGraphWidth($scope.graphDataCustomEvents);
    }
    if ($scope.active.usage) {
      $scope.selectActiveTab('usage');        
    }
  });


  $rootScope.fetchRanksForCountry = function (country) {
    PlatformInfoService.selectedCountry = country;
    PlatformInfoService.logCompetitorsMenuForSetting("country");
    PlatformInfoService.fetchRanks(PlatformInfoService.selectedPlatform);
  }


  $rootScope.fetchRanksForSubcategory = function (subcategory) {
    PlatformInfoService.selectedSubcategory = subcategory;
    PlatformInfoService.logCompetitorsMenuForSetting("subcategory");
    PlatformInfoService.fetchRanks(PlatformInfoService.selectedPlatform);
  }

  $rootScope.fetchRanksForCategory = function (category) {
    PlatformInfoService.selectedCategory = category;
    PlatformInfoService.selectedSubcategory = thetaUtil.ALL_SUBCATEGORY;
    PlatformInfoService.logCompetitorsMenuForSetting("category");
    $scope.fetchRanks(PlatformInfoService.selectedPlatform);
  }

  $rootScope.fetchRanks = function (platform) {
    $scope.graphDataRanks.fetchData(
      $scope.competitorsDatePickerService.date.startDate, 
      $scope.competitorsDatePickerService.date.endDate, 
      PlatformInfoService.selectedPlatform.id, 
      PlatformInfoService.selectedCategoryType.id, 
      PlatformInfoService.selectedCategory.id, 
      PlatformInfoService.selectedSubcategory.id, 
      PlatformInfoService.selectedCountry.id, 
      1, 20);
  }

  $rootScope.updateCategoryTypeForDevice = function (device) {
    for (var i=0;i<PlatformInfoService.selectedPlatform.rank_types.length;i++) {
      var device_type = PlatformInfoService.selectedPlatform.rank_types[i].device_type;
      if (device_type == device &&
          PlatformInfoService.selectedPlatform.rank_types[i].name == PlatformInfoService.selectedCategoryType.name) {
          PlatformInfoService.selectedCategoryType = PlatformInfoService.selectedPlatform.rank_types[i];
          break;
      }
    }
    PlatformInfoService.selectedSubcategory = thetaUtil.ALL_SUBCATEGORY;
    PlatformInfoService.logCompetitorsMenuForSetting("device");
    $rootScope.fetchRanks($rootScope.selectedPlatform);
  }

  $rootScope.updateCategoryTypeForName = function (name) {
    for (var i=0;i<$rootScope.selectedPlatform.rank_types.length;i++) {
      var rank_name = $rootScope.selectedPlatform.rank_types[i].name;
      if (rank_name == name &&
          PlatformInfoService.selectedPlatform.rank_types[i].device_type == $rootScope.selectedCategoryType.device_type) {
          PlatformInfoService.selectedCategoryType = $rootScope.selectedPlatform.rank_types[i];
          break;
      }
    }
    PlatformInfoService.selectedSubcategory = thetaUtil.ALL_SUBCATEGORY;
    PlatformInfoService.logCompetitorsMenuForSetting("rank type");
    $rootScope.fetchRanks($rootScope.selectedPlatform);
  }

  
  $rootScope.$on('ranksGraphDataUpdated', function(){
    if (!$scope.graphDataRanks.graphs) {
      $scope.graphDataRanks.dataLoaded = true;
      return;
    }
    $scope.data = [];
    $scope.gridOptions = null;
    for (var i=0;i<$scope.graphDataRanks.graphs.length;i++) {
      var graph = $scope.graphDataRanks.graphs[i];
      if (!$scope.graphDataRanks.products[graph.app_id]) {
        continue;
      }
      var product = $scope.graphDataRanks.products[graph.app_id];
      var dict = {
                   'icon':product.icon,
                   'name':product.name,
                   'graph':graph,
                   'overall_rank':product.overall_rank,
                   'category_rank':product.category_rank,
                   'change':$scope.graphDataRanks.percentChangeForGraph(graph),
                   'value':$scope.graphDataRanks.totalForGraph(graph),
                   'value_num':$scope.graphDataRanks.totalForGraphNumber(graph),
                 };
      $scope.data.push(dict);
    }

    var width = $scope.getWindowDimensions().w;
    var nameColumnWidth = 240;
    var rankWidth = 150;
    if (width <= 500) {
      nameColumnWidth = 77
      rankWidth = width/2-77 - 15; 
    }
    var colDefs =  [
          { name: 'app', 
             width:nameColumnWidth,
            field: 'name', 
            cellTemplate: '<div style="float:left;padding-top:10px;padding-left:10px;margin-right:20px"><a href="/apps/{{grid.appScope.PlatformInfoService.selectedPlatform.id}}/{{grid.appScope.PlatformInfoService.selectedCountry.id}}/{{row.entity.graph.app_id}}"><img class="app-icon" ng-src={{row.entity.icon}} /></div><div class="ngCellText hidden-xs">{{row.entity.name}}</a></div>' 
          },
          { name: 'Overall Rank', 
             width:rankWidth,
            field: 'overall_rank', 
            cellTemplate: '<div class="ngCellText" style="text-align:center">{{row.entity.overall_rank}}</div>' 
          },
          { name: 'Category Rank', 
            sort: {
             direction: uiGridConstants.ASC,
             priority: 1
            },
             width:rankWidth,
            field: 'category_rank', 
            cellTemplate: '<div class="ngCellText" style="text-align:center">{{row.entity.category_rank}}</div>' 
          },
        ];
      var revenueColDef;
      var trendColDef;
      if ($scope.user || PlatformInfoService.selectedCategory.name == "Navigation") {
         revenueColDef = 
          { name: 'revenue', 
             width:110,
            field: 'value_num', 
            cellTemplate: '<div class="ngCellText" style="text-align:center">{{row.entity.value}}</div>' 
          };
        trendColDef = 
          { name: 'revenue trend', 
            enableSorting: false,
            field: 'graph', 
            cellTemplate: '<div style="padding:10px;"><rickshaw rickshaw-options="row.entity.graph.options" rickshaw-features="row.entity.graph.features" rickshaw-series="row.entity.graph.series"></rickshaw></div>' 
          };

      } else {
          revenueColDef = { 
            name: 'Revenue', 
             field: 'value', 
    enableSorting: false,
            width:110,
            cellTemplate: '<div class="ngCellText" style="text-align:center"><i class="mdi-action-lock"></i></div>' 
          };
          trendColDef = 
            { name: 'Revenue trend', 
              enableSorting: false,
              field: 'graph', 
              cellTemplate: '<div class="ngCellText"  style="text-align:center">Subscription required to view revenue info.</div>' 
            };
      }
      colDefs.push(revenueColDef);
      colDefs.push(trendColDef);
      $scope.gridOptions = {
        paginationPageSizes: [10, 20, 30],
        paginationPageSize: 10,
        rowHeight: 80,
        data: 'data',
        columnDefs: colDefs
      };


    if($scope.graphDataRanks) {
      var width = $scope.getWindowDimensions().w-30;
      width-= 50;
      if ($window.innerWidth >= 768) {
        width = $scope.getWindowDimensions().w -760;
      }
      $scope.graphDataRanks.resizeGraphs(0, width);
    }
    if ($scope.active.custom && 
      $rootScope.graphDataRanks != $rootScope.activeGraphData) {
      $scope.selectActiveTab('ranks');        
    }    

  });


  $rootScope.$on('realtimeGraphDataInitialized', function(){
    $scope.realtime_graph = $scope.realtimeGraphData.realtime_graph;
  });   


    // set tabs based on URL hash
  $scope.initTabs = function () {
    $scope.active = {
      gettingStarted: false,
      following: false,
      sales: false,
      usage: false,
      settings: false,
    };
    var selected = false;
    if (developer_mode == "True") {
      $scope.active.gettingStarted = true;
      selected = true;
    }
    var tab = $location.path('/').search().tab;
    if (tab && tab.length) {
      for (key in $scope.active) {
        $scope.active[key] = false;
      }
      $scope.active[tab] = true;
      selected = true;
    }
    if (!selected) {
      $scope.active.sales = true;
    }
  };



  $scope.initSuperClass();
  $scope.initTabs();    


  if (hide_setup == "False") {
    $scope.hideSetup = false;    
  } else {
    $scope.hideSetup = true;        
  }

  if (django_user) {
    $scope.user = django_user;
  }

  $scope.PlatformInfoService = PlatformInfoService;
  if (sdk_versions) {
    $scope.sdkCount = Object.keys(sdk_versions).length;
  }
  $scope.graphData = new GraphDataService();
  $scope.graphData.dataLoaded = false;

  // graph data switches on tab switch
  $scope.activeGraphData = $scope.graphData;   

  // init competitors date selector
  $scope.competitorsDatePickerService = new DatePickerService();
  $scope.competitorsDatePickerService.initCompetitorRanges(); 
  $scope.competitorsDatePickerService.initDate();

  
  if (!main_dashboard) {
    // init ranks graphs
    $scope.graphDataRanks = new RanksGraphDataService();
    $scope.graphDataRanks.dataLoaded = false;
    $scope.graphDataRanks.products = $scope.graphData.products;
    $scope.graphDataRanks.category = "Navigation";
    $scope.graphDataRanks.parentGraphData = $scope.graphData;    
  }
  $scope.$watch('datePickerService.date', function(newDate, oldDate) {
    if (typeof oldDate === 'undefined') return;
    if (ProductService.productIDString) {
      $scope.activeGraphData.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate);
    }
  });

  $scope.followAppsGraphData = new FollowAppsGraphDataService();

  $scope.$watch('competitorsDatePickerService.date', function (newDate, oldDate) {
    if (typeof oldDate === 'undefined') return;
    if ($scope.graphDataRanks && PlatformInfoService.selectedPlatform) {
      $scope.fetchRanks(PlatformInfoService.selectedPlatform);
    }
  });

  // set by GraphDataService callback that fetches menu options
  $rootScope.$on('platformInfoUpdated', function(newValue, oldValue){
    /*if (!$scope.graphDataRanks.platforms) {
      return;
    }*/
    if (!main_dashboard) {
      $rootScope.fetchRanks($rootScope.selectedPlatform);
    }
    $scope.followAppsGraphData.fetchData($scope.competitorsDatePickerService.date.startDate, $scope.competitorsDatePickerService.date.endDate); 
  });

})


.controller('RankGraphController',  function (DatePickerService, $scope, $controller) {
  $controller('MainGraphController', {$scope: $scope});
  $scope.competitorsDatePickerService.range = $scope.competitorsDatePickerService.rangeLabels[0];
})


// for the follow apps tab
.controller('FollowAppsGraphController',  function (PlatformInfoService, FollowAppsGraphDataService, $http, $modal, ProductService, $window, $rootScope, AppDetailsGraphDataService, $controller, $scope) {
  $controller('GraphController', {$scope: $scope});

  /*$rootScope.$on('followAppsGraphDataUpdated', function(event, data) { 
    console.log(data); 
  });
  */

  $scope.unfollow = function (app) {
    var url = '/unfollow/'+ app.platform_id + '/' + app.product_id + '/';
    $http.get(url).then(function(response) {
    if (response.data.success) {
      delete $scope.followAppsGraphData.products[app.product_id];
    } else {
      console.log("remove follow failed"); 
    }
    });   
  };


  $rootScope.$on('followAppsUpdated', function(event, info) { 
    if (info.app_info.success) {
      $scope.followAppsGraphData.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate); 
    } else {
      console.log("follow failed"); 
    }
  });


  $scope.openCountryPopover = function () {
    mixpanel.track("open follow country popover");
    $modal.open({
      templateUrl: 'app_country_popover.html',
      controller: 'AppCountryModalInstanceCtrl',
      windowClass: 'app-modal-window',
      resolve: {
        datePickerService: function () {
          return $scope.datePickerService;
        },
        graphData: function () {
          return $scope.followAppsGraphData;
        }
      }
    });
  };

})


// for pages detailing each public app
.controller('AppDetailsGraphController',  function (PlatformInfoService, $http, $modal, ProductService, $window, $rootScope, AppDetailsGraphDataService, $controller, $scope) {
    $controller('GraphController', {$scope: $scope});
  $scope.initSuperClass();
  var chunks = $window.location.pathname.split('/');
  $scope.graphData = new AppDetailsGraphDataService();
  $scope.graphData.app_id = chunks[chunks.length-2];
  $scope.graphData.country = chunks[chunks.length-3];
  $scope.graphData.platform = chunks[chunks.length-4];
  PlatformInfoService.fetchPlatformInfo();
  $scope.activeGraphData = $scope.graphData; 
  $scope.app = app;
  $rootScope.categories = ProductService.appInfo.categories; 
  $scope.selectedCountry = "United States";

  $scope.$watch('datePickerService.date', function(newDate, oldDate) {
   if (oldDate == newDate) return;
   $scope.activeGraphData.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate);
  });    

  $rootScope.saveAndRefreshUI = function() {
    $scope.graphData.updateProductHash();
    $scope.graphData.saveProductSettings();
    $scope.graphData.refreshProductGraphs(); 
    $rootScope.$emit('graphDataUpdated');
  };

  //resize the graphs and redisplay the slider after new data is fetched
  $rootScope.$on('appDetailsGraphDataUpdated', function(){
    $rootScope.littleGraphs = [];              
    var width = $scope.getWindowDimensions().w/3-25;
    $scope.graphData.resizeGraphs(0, width);
  });

  $rootScope.$on('publisherAppsUpdated', function(){
       //$rootScope.$apply(); 
  });


  $rootScope.graph_rendered = function(graph) {
    if (!$rootScope.littleGraphs) {
      $rootScope.littleGraphs = [];
    }
  };


  $scope.openCountryPopover = function (size) {
    mixpanel.track("open country popover");
    $modal.open({
      templateUrl: 'country_popover.html',
      controller: 'ProductsModalInstanceCtrl',
      windowClass: 'app-modal-window',
      size: size,
      resolve: {
        graphData: function () {
          return $scope.graphData;
        }
      }
    });
  };


  $rootScope.fetchDataForCountry = function (country) {
    $scope.selectedCountry = country.name;
    $scope.activeGraphData.country = country.id;
    $scope.activeGraphData.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate);
  }


  $rootScope.$on('platformInfoUpdated', function(){
    $rootScope.platforms = PlatformInfoService.platforms; 
    for (var i=0;i<$rootScope.platforms.length;i++) {
      var p = $rootScope.platforms[i];
      if (p.id == $scope.graphData.platform) {
        $rootScope.selectedPlatform = p;
        break; 
      }
    }
  });
})


// drilldown for stat to show app breakout
.controller('DrilldownGraphController',  function (ProductService, DatePickerService, $scope, $rootScope, $location, $controller, DrilldownGraphDataService) {

  $controller('GraphController', {$scope: $scope});
  $scope.initSuperClass();

  $scope.graphData = new DrilldownGraphDataService();
  $scope.graphData.series_name = series_name;
  $scope.activeGraphData = $scope.graphData; 

  $scope.$watch('datePickerService.date', function() {
    if (ProductService.productIDString) {
      $scope.activeGraphData.fetchData($scope.datePickerService.date.startDate, $scope.datePickerService.date.endDate);
    }
  });    

  $rootScope.$on('productsUpdated', function(){
    $scope.fetchDataIfProducts($scope.activeGraphData);
  });


  $rootScope.saveAndRefreshUI = function() {
    $scope.graphData.updateProductHash();
    $scope.graphData.saveProductSettings();
    $scope.graphData.refreshProductGraphs();      
    $rootScope.$emit('graphDataUpdated');
  };

  //resize the graphs and redisplay the slider after new data is fetched
  $rootScope.$on('graphDataUpdated', function(){
    $rootScope.littleGraphs = [];              
    var width = $scope.getWindowDimensions().w-45;
    $scope.graphData.resizeGraphs(0, width);
  });

  $rootScope.graph_rendered = function(graph) {
    if (!$rootScope.littleGraphs) {
      $rootScope.littleGraphs = [];
    }
    if (!$rootScope.graphDataDrilldown) {
      $rootScope.growthWeeks = [52, 12, 4, 1, 0];
      $rootScope.graphDataDrilldown = new DrilldownGraphDataService();
      $rootScope.graphDataDrilldown.series_name = series_name;
      $rootScope.graphDataDrilldown.isExpansionData = true;
      var today = new Date();
      var yesterday = new Date(new Date().setDate(today.getDate()-1));
      var before = new Date(new Date().setDate(today.getDate()-366));
      $rootScope.graphDataDrilldown.fetchData(before, yesterday);
    }
  };

})


// the product popover
.controller('AppCountryModalInstanceCtrl', function (datePickerService, $http, PlatformInfoService, DatePickerService, $rootScope, $scope, $modalInstance, graphData) {
  $scope.PlatformInfoService = PlatformInfoService;
  $scope.graphData = graphData;
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.selectCountryForAppFollow = function (country) {
    PlatformInfoService.selectedCountry = country;
    graphData.fetchData(datePickerService.date.startDate, datePickerService.date.endDate);
  }



})


// the product popover
.controller('ProductsModalInstanceCtrl', function (DatePickerService, $rootScope, $scope, $modalInstance, graphData, PlatformInfoService) {
  $scope.PlatformInfoService = PlatformInfoService;
  $scope.graphData = graphData;
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.fetchRanks = function() {
    graphData.fetchDataWithParameters($scope.datePickerService.date.startDate, 
                                          $scope.datePickerService.date.endDate,
                                          $scope.selectedMarket,
                                          $scope.selectedCategoryType,
                                          $scope.selectedCategory,
                                          $scope.selectedCountry
                                          );
  }



})


// popover for customer search
.controller('CustomerLookupModalCtrl', function ($scope, $modalInstance, customer, graphData) {

  $scope.customer = customer;
  $scope.graphData = graphData;
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

})


.controller('AlertsModalInstanceCtrl', function ($window, $scope, $modalInstance) {

  $scope.alert = function () {
    mixpanel.track("click delete alert"); 
    alert("delete this, don't show me");
  };


  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };


  $scope.viewDocs = function(platform) {
    if (platform == "Subscribe") {
      mixpanel.track("click subscribe alert"); 
      $window.location.href = '/pricing/';
      return;     
    }
    mixpanel.track("click sdk alert");
    $window.location.href = '/docs/#' + platform;
  };

});