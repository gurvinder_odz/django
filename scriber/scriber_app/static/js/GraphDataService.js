/* global thetaUtil:true */

// singleton to fetch and manage customer's products/apps
angular.module('theta').service('ProductService', function(AppInfo, $rootScope, $location, $http) {

	this.fetchApps = function() {
		if (!this.appInfo) {
		  // for fetching icons and names of apps
      this.appInfo = new AppInfo();
      this.fetchedInfo = this.appInfo.fetchedInfo;
		}
		var products_url = "/dashboard/get_products/";
		var self = this;
		$http.get(products_url, {}).then(function(response){
			var productIDString = response.data.product_string;
			self.productIDString = productIDString;
			$rootScope.$emit('productsUpdated');
	  });
	};

  // select products based on URL hash if set
  if($location.search().products && 
  	$location.search().products.length > 0) {
  	this.selectedProducts = $location.search().products.split(",");
  }

});


// singleton to fetch and manage information about platforms 
// platforms are iTunes and Google Play
// information includes categories, countries, and ranking types
// used to populate menus and make selections in the UI
angular.module('theta').service('PlatformInfoService', function($rootScope, $http) {

	this.fetchPlatformInfo = function() {
		var url = "/dashboard/competitors_menu_info/";
  	var self = this;
  	$http.get(url, {}).then(function(response){
  		self.platforms = response.data;
  		// append "All" subcategory to things with subcategories
  		for (var i=0;i<self.platforms.length;i++) {
  			var platform = self.platforms[i];
		    for (var j=0;j<platform.categories.length;j++) {
		    	var category = self.platforms[i].categories[j];
		    	if (category.subcategories) {
		    		category.subcategories.unshift(thetaUtil.ALL_SUBCATEGORY);
		    	}
		    }
  		}
  		self.selectDefaults(self.platforms[0]);
  		$rootScope.$emit('platformInfoUpdated');
  	}); 
	};

   // set variables to show category menu
  this.selectDefaults = function(platform){
    this.selectedPlatform = platform;
    this.selectedPlatformDevicesTypeList = [];
    var set = {};
    this.selectedPlatformRankingTypeList = [];
    for (var i=0;i<this.selectedPlatform.rank_types.length;i++) {
      var rank_type = this.selectedPlatform.rank_types[i];
      var device_type = rank_type.device_type;
      var name = rank_type.name;

      // enumerate the device types for menu
      if (!set[device_type]) {
        set[device_type] = true;
        this.selectedPlatformDevicesTypeList.push(device_type);
      }
      // default to iPhone Top Grossing
      if (device_type == "iPhone" && name == "Top Grossing") {
        this.selectedCategoryType = rank_type;
        this.selectedPlatformDevicesType = device_type;
        this.selectedPlatformRankingType = name;
      }
      // enumerate the iPhone-related rank types for menu
      if (device_type == "iPhone") {
        this.selectedPlatformRankingTypeList.push(name);
      }
    }

    // default to All subcategory
    this.selectedSubcategory = thetaUtil.ALL_SUBCATEGORY;
    
    // default to US
    for (var i=0;i<this.selectedPlatform.countries.length;i++) {
      var country = this.selectedPlatform.countries[i];
      if (country.name == "United States") {
        this.selectedCountry = country;
        break;
      }
    }

    // default to Navigation
    for (var i=0;i<this.selectedPlatform.categories.length;i++) {
      var category = this.selectedPlatform.categories[i];
      if (category.name == "Navigation") {
        this.selectedCategory = category;
        break;
      }
    }
  };

  this.logCompetitorsMenuForSetting = function(setting) {
    mixpanel.track("Competitors Menu", 
               { "type": setting,
                 "platform": this.selectedPlatform.name,
                 "device type": this.selectedCategoryType.device_type,
                 "rank type": this.selectedCategoryType.name,
                 "category": this.selectedCategory.name,
                 "subcategory": this.selectedSubcategory.name,
                 "country": this.selectedCountry.name,
                });
  }




});


angular.module('theta').factory('GraphDataService', function(DatePickerService, ProductService, $window, $rootScope, $location, $http) {

	var GraphDataService = function () {
    this.platformImages = { "iOS": "/static/img/apple-bite.png",
                            "Android": "/static/img/android.png" };
    this.appInfo = ProductService.appInfo;                           

	};

	GraphDataService.prototype.fetchData = function(startDate, endDate) {
		var url = this.dataURLForBase("/dashboard/graph_data", ProductService.productIDString, startDate, endDate);
		var self = this;
		$http.get(url, {}).then(function(response) {
			self.dataLoaded = true;
			self.storeDataAndRefreshGraphs(response.data);
			$rootScope.$emit('graphDataUpdated');
		});     
	};


	GraphDataService.prototype.dataURLForBase = function(base, productIDString, startDate, endDate) {
		var secondsStart = startDate.getTime() / 1000;
		var secondsEnd = endDate.getTime() / 1000;
		if (startDate == -1) {
			secondsStart = 0;
		}
		return base + '/' + productIDString + '/'  + secondsStart + '/' + secondsEnd + '/';
	};


	GraphDataService.prototype.storeDataAndRefreshGraphs = function (data) {
		this.raw_data = JSON.parse(JSON.stringify(data));
		this.refreshProducts();
		this.refreshGraphs();    
	};


  // chunk for display in bootstrap grid
  GraphDataService.prototype.chunkProductsForDisplay = function() {
  	this.productArrays = [];
  	this.removeTestAndZeroTotalProducts();    
	    // \todo magic numbers, vary by dashboard or drilldown
	    var NUMBER_OF_COLUMNS_FOR_DRILLDOWN = 2;
	    var cols = NUMBER_OF_COLUMNS_FOR_DRILLDOWN;
	    this.productArrays = thetaUtil.chunk(this.productArrays, cols); 
	  };

	// \todo remove the data from the demo and remove this code
	GraphDataService.prototype.removeTestAndZeroTotalProducts = function() {
		for (var pid in this.products) {
			var dict = this.products[pid];
			dict.product_id = pid;
      // \todo just remove the beta app in this hackish way 
      // until it's a problem for others
      if (!dict.product_id_string.startsWith('fake') && dict.product_id != 57) {
      	this.productArrays.push(dict);                    
      }
    }
    this.productArrays.sort(compare);
  };

	function compare(a,b) {
		if (a.product_id_string.toLowerCase().indexOf("gaiagps") > -1)
			  return -1;
	if (a.product_id_string == "com.trailbehind.OfflineTopoMaps") {
	  	return 1;
	}
	  if (a.product_id_string < b.product_id_string)
	     return -1;
	  if (a.product_id_string > b.product_id_string)
	    return 1;
	  return 0;
	}


  // display products if checked
  GraphDataService.prototype.enableProducts = function() {
  	for (var key in this.products) {
  		this.products[key].checked = true;
  		this.products[key].checkedOverall = true;
  		/* global indexOf:true */
  		if (this.selectedProducts && indexOf.call(this.selectedProducts, key.toString()) == -1) {
  			this.products[key].checked = false;
  			this.products[key].checkedOverall = false;
  		}
  		for (var i=0;i<this.products[key].in_app_products.length;i++) {
  			var iap = this.products[key].in_app_products[i];
  			iap.checked = true;
  			if (this.selectedProducts && indexOf.call(this.selectedProducts, iap.product_id.toString()) == -1) {
  				iap.checked = false;
  			}
  		}
  	}
  };


	// break the product info out of the raw-data for structuring the UI
	GraphDataService.prototype.refreshProducts = function() {
		var data = this.raw_data;   
		this.products = data.product_info;
		this.enableProducts();
		this.chunkProductsForDisplay();    

		var self = this;
		/*jslint loopfunc: true */
		for (var product_id in this.products) {
			(function(product_id) {
				ProductService.appInfo.fetchIcon(self.products[product_id].product_id_string, self.products[product_id].platform).then(function(url) {
					self.products[product_id].icon = url;
					var bid = self.products[product_id].product_id_string;
					if (!ProductService.appInfo.fetchedInfo[bid]) {
						self.products[product_id].name = "Unknown App";
					} else if (ProductService.appInfo.fetchedInfo[bid].trackCensoredName) {
						self.products[product_id].name = ProductService.appInfo.fetchedInfo[bid].trackCensoredName.split(/[+~!.?,;:'"-]/)[0];
					} else if (ProductService.appInfo.fetchedInfo[bid].appName) {
						self.products[product_id].name = ProductService.appInfo.fetchedInfo[bid].appName.split(/[+~!.?,;:'"-]/)[0];
					}
					if (!ProductService.appInfo.companyName) {
						ProductService.appInfo.setCompanyName();
					}
				});      
			})(product_id);
		}
	};


	// used on drilldowns
	GraphDataService.prototype.salesGraphForProduct = function(product_id) {
		var graph;
		for (var key in this.raw_data.product_series) {
			if (key == product_id) {
				graph = this.raw_data.product_series[key].current[0];
				break;
			}
		}
		return graph;
	};


	// used on drilldowns, total for series by product
	GraphDataService.prototype.salesForProduct = function(product_id) {
		var graph = this.salesGraphForProduct(product_id);
		if (!graph) {
			return '';
		}
		var value = graph.data[graph.data.length-1];
		if (graph.format == "money") {
			return '$' + thetaUtil.kFormatter(Math.round(value));
		}
		return thetaUtil.kFormatter(Math.round(value));
	};


	// used on drilldowns, total for series, offset back by week_offset
	// set format to true for strings, false for numbers
	GraphDataService.prototype.pastSalesForSeries = function(series_name, week_offset) {
		if (!this.combinedInfo) {
			return;
		}
		var ci = {}, prop;
	    // loop over all the keys in the object
	    for ( prop in this.combinedInfo ) {
	    	ci[ prop ] = this.combinedInfo [ prop ];
	    }
	    var data = ci.current[0].data;
	    var index = data.length - 1 - week_offset*7;
	    return data[index];
	  };


	// used on drilldowns, total for series, offset back by week_offset
	// set format to true for strings, false for numbers
	GraphDataService.prototype.pastSalesStringForSeries = function(series_name, week_offset, format) {
		if (!this.combinedInfo) {
			return;
		}
		var data = this.pastSalesForSeries(series_name, week_offset, format);   
		if (!format) {
			return data;
		}
		if (this.combinedInfo.current[0].format == "money") {
			return '$' + thetaUtil.kFormatter(Math.round(data));
		}
		return thetaUtil.kFormatter(Math.round(data));
	};


	// percent growth since some weeks in the past for a series
	GraphDataService.prototype.percentGrowthSince = function(series_name, week_offset) {
		return (this.pastSalesForSeries(series_name, 0)-this.pastSalesForSeries(series_name, week_offset, false))/ this.pastSalesForSeries(series_name, week_offset);
	};


	// expand series for rickshaw to have dates for each series
	GraphDataService.prototype.rickshawSeries = function(series, offset, dataToCopy) {
		var seriesData = {};
		var data = dataToCopy;   
		for (var key in series) {
			var rsSeries = [];
			for (var j=0;j<data.times.length - offset;++j) {
	        // make date/value tuples for rickshaw
	        var dict = {
	        	'x':data.times[j], 
	        	'y':series[key].data[j]
	        };
	        rsSeries.push(dict);
	      }
	      seriesData[key] = {'series':rsSeries, 'name':series[key].name, 
	      'options':series[key].options, 'features':series[key].features};
	    }
	    return seriesData;
	  };


	  GraphDataService.prototype.updateProductHash = function() {
	  	var selectedIDs = [];
	  	for (var key in this.products) {
	  		var p = this.products[key];
	  		if (p.checked) {
	  			selectedIDs.push(p.product_id);
	  		}
	  		for (var iap_key in p.in_app_products) {
	  			var iap = p.in_app_products[iap_key];
	  			if (iap.checked) {
	  				selectedIDs.push(iap.product_id);
	  			}
	  		}
	  	}
	  	var search = $location.path('/').search();
	  	search.products = selectedIDs.toString();
	  	$location.path('/').search(search);
	  	this.selectedProducts = selectedIDs; 
	  };


	// \todo this isn't complete
	GraphDataService.prototype.saveProductSettings = function() {
		var settings = {};
		for (var key in this.products) {
			settings[key] = this.products[key].checked;
		}
		var url = "/dashboard/save_settings/";
		var req = {
			method: 'POST',
			url: url,
			headers: {
				'Content-Type': null,
				"X-CSRFToken": thetaUtil.getCookie('csrftoken')
			},
			data: {'settings':settings}
		};
    
    $http(req).then(function(response) {
			this.settings = response;
		});
	};

  // feature with hover
  GraphDataService.prototype.features = function(seriesInfo) {
  	var tickLimits = this.tickLimits(seriesInfo.data);
  	return {
  		'yAxis':{
  			'tickValues':tickLimits, 
  			'tickFormat': Rickshaw.Fixtures.Number.formatKMBT,
  		},
  	};
  };

  // display all graphs
  GraphDataService.prototype.refreshGraphs = function() {
  	var data = JSON.parse(JSON.stringify(this.raw_data));
  	var ci = {"product": "Combined"};
  	for (var key in data.product_series) {      
  		if (!this.skipUnselected(key)) {
  			ci = this.combineSeries(data, key, ci); 
  		}
  	}
  	this.graphs = [];
  	this.extraGraphs = [];
  	var seriesData = this.rickshawSeries(ci.current, 0, data);
  	var previousSeriesData = this.rickshawSeries(ci.previous, 0, data);

  	for (key in ci.current) {
  		ci.current[key].options = thetaUtil.defaultGraphOptions();
  		ci.current[key].options.width = $window.innerWidth/3-60;
  		ci.current[key].features = this.features(ci.current[key]);        
  		ci.current[key].previousSeries = {
  			data: previousSeriesData[key].series,
  		};
  		ci.current[key].series = [
  		{
  			color: thetaUtil.MAIN_COLOR,
  			data: seriesData[key].series,
  			name: key,
  			renderer: 'line',
  		}
  		];
  		if (seriesData[key].name == "Sessions" ||
  			seriesData[key].name == "Daily Users" ||
  			seriesData[key].name == "28-Day Users") {
  			this.extraGraphs.push(ci.current[key]);
  		} else {
  			this.graphs.push(ci.current[key]);
  		}
  		
  	}

  	this.combinedInfo = ci;    
  };


  GraphDataService.prototype.combineSeries = function(data, key, ci) {
  	if (!ci.current) {
  		ci.current = data.product_series[key].current;
  		ci.previous = data.product_series[key].previous;
  		ci.product_info = data.product_info;
  	} else {
  		for (var currKey in data.product_series[key].current) {
  			ci.current[currKey].name = data.product_series[key].current[currKey].name;
  			for (var j=0;j<data.product_series[key].current[currKey].data.length;j++) {
  				ci.current[currKey].data[j] +=
  				data.product_series[key].current[currKey].data[j];
  			}
  			if (data.product_series[key].previous) {
  				for (var k=0;k<data.product_series[key].previous[currKey].data.length;k++) {
  					ci.previous[currKey].data[k] +=
  					data.product_series[key].previous[currKey].data[k];
  				}          
  			}
  		}
  	}
  	return ci;
  };


  GraphDataService.prototype.skipUnselected = function(key) {
  	if (this.products[key] && this.products[key].checked === false) {
  		return true;
  	}

    for (var pid in this.products) {
    	var product = this.products[pid];
      for (var i=0;i<product.in_app_products.length;i++) {
      	var iap = product.in_app_products[i];
    		if (iap.product_id == key && 
      		  false === iap.checked) {
  				return true;
      	}
      }
     }
  return false;
};


GraphDataService.prototype.productIdForSubproductId = function(subproduct_id) {
	for (var i=0;i<this.products.length;i++) {
		var p = this.products[i];
		for (var iap in p.in_app_products) {
			if (iap.product_id == subproduct_id) {
				return p.product_id;
			}
		}
	}
	return null;
};


GraphDataService.prototype.tickLimits = function(data) {
	var min = Math.min(Math.min.apply(Math, data), 
		Math.min.apply(Math, data));
	var max = Math.max(Math.max.apply(Math, data), 
		Math.max.apply(Math, data));
	var tickMax = max;                                       
	if (tickMax.toString().length > 3 ) {
		tickMax = thetaUtil.sigFigs(max, 3);                                       
	}                                        
	var tickMin = min;                                       
	if (tickMin.toString().length > 3 ) {
		tickMin = thetaUtil.sigFigs(min, 3);                                       
	}   
	return [tickMin, tickMax];
};                                     


	  // resize graphs on screen size change or first load
	  // \todo magic numbers
	  GraphDataService.prototype.resizeGraphs = function (newHeight, newWidth) {
	  	if (this.graphs) {
	  		for (var i=0;i<this.graphs.length;i++) {
	  			var g = this.graphs[i];
	  			var newOptions = JSON.parse(JSON.stringify(g.options));
	  			newOptions.width = newWidth;
	  			g.options = newOptions;
	  		}
	  	}
	  };


  // the value of the most recent time in the graph's series
  GraphDataService.prototype.valueForGraph = function(graph) {
  	if (graph.format == "money") {
  		return '$' + thetaUtil.kFormatter(Math.round(graph.series[0].data[graph.series[0].data.length-1].y));
  	}
  	return thetaUtil.kFormatter(Math.round(graph.series[0].data[graph.series[0].data.length-1].y));
  };


  // the value of the most recent time in the graph's series
  GraphDataService.prototype.totalForGraphNumber = function(graph) {
    var total = 0;
    for (var i=0;i<graph.series[0].data.length;i++) {
    	total += graph.series[0].data[i].y;
    }
    return total;
  };

    // the value of the most recent time in the graph's series
  GraphDataService.prototype.totalForGraph = function(graph) {
    var total = this.totalForGraphNumber(graph);
  	if (graph.format == "money") {
  		return '$' + thetaUtil.kFormatter(Math.round(total));
  	}
  	return thetaUtil.kFormatter(Math.round(total));
  };


  GraphDataService.prototype.percentChangeForGraph = function(graph) {
  	if (!graph.previousSeries) { 
  		return 0; 
  	}
  	var oldValue = graph.previousSeries.data[graph.previousSeries.data.length-1].y;
  	if (oldValue === 0) {
  		return 0;
  	}
  	var newValue = graph.series[0].data[graph.series[0].data.length-1].y;
  	return (newValue - oldValue)/oldValue;
  };


  GraphDataService.prototype.isInactiveSDKGraph = function(graph) {
  	if (graph.name == "Sessions" ||
  		graph.name == "Daily Users" ||
  		graph.name == "28-Day Users") {
  		if (graph.data.reduce(function(previousValue, currentValue, index, array) {
  			return previousValue + currentValue;}) === 0) {
  			return true;
  		}
  	}
  	return false;
  };


  GraphDataService.prototype.hideGraph = function(product_id) {
  	var graph = this.salesGraphForProduct(product_id);
  	if (!graph) { return true; }
  	var total = graph.data.reduce(function(a, b) {return a + b;});
  	if (total > 0) {return false;}
  	for (var iap in this.products[product_id].in_app_products) {
  		var pid = this.products[product_id].in_app_products[iap].product_id;
  		var subgraphdata = this.salesGraphForProduct(pid).data;
  		var subtotal = subgraphdata.reduce(function(a, b) {return a + b;});
  		if (subtotal > 0) { return false;}
  	}
  	return true;  
  };


  GraphDataService.prototype.activateGraph = function(graph) {
  	console.log(graph);
  };


  GraphDataService.prototype.revenueFeatures = function(series) {
  	var tickLimits = this.tickLimits(series.current);
  	var featureDict = {
  		'hover': {
  			'formatter': thetaUtil.dateHoverFormatterForName(series.name)
  		},
  		'yAxis':{
        'tickValues': tickLimits
  		},
  	};
	  return featureDict;
  };


  return GraphDataService;
});


angular.module('theta').factory('DrilldownGraphDataService', function(ProductService, GraphDataService, $rootScope, $location, $http, $window) {
  // create our new custom object that reuse the original object constructor
  var DrilldownGraphDataService = function() {
  	GraphDataService.apply(this, arguments);
  };

  // reuse the original object prototype
  DrilldownGraphDataService.prototype = new GraphDataService();

  // define a new internal private method for this object
  DrilldownGraphDataService.prototype.fetchData = function(startDate, endDate) {
  	var url = this.dataURLForBase("/dashboard/series_data", ProductService.productIDString, startDate, endDate);
  	var self = this;
		$http.get(url, {}).then(function(response){
  		if (self.isExpansionData) {
  			self.raw_data = JSON.parse(JSON.stringify(response.data));
  			self.products = response.data.product_info;
  			var ci = {"product": "Combined"};
  			for (var key in response.data.product_series) {      
  				if (!self.skipUnselected(key)) {
  					ci = self.combineSeries(response.data, key, ci); 
  				}
  			}    
  			self.combinedInfo = ci;    
  			return;
  		}
  		self.storeDataAndRefreshGraphs(response.data);
  		self.refreshProductGraphs();
  		$rootScope.$emit('graphDataUpdated');
  	});
  };


  // define a new internal private method for this object
  DrilldownGraphDataService.prototype.features = function(seriesInfo) {
  	var tickLimits = this.tickLimits(seriesInfo.data);
  	return {
  		'hover': {
  			'formatter': thetaUtil.dateHoverFormatterForName(seriesInfo.name)
  		},
  		'xAxis': {
  			'ticksTreatment': 'glow',
  			'timeUnit': thetaUtil.getDateUnit(3600*(24)*2*seriesInfo.data.length/14)
  		},
  		'yAxis': {
  			'tickValues':tickLimits, 
  			'tickFormat': Rickshaw.Fixtures.Number.formatKMBT,
  		},
  	};
  };


  // chunk for display in bootstrap grid
  DrilldownGraphDataService.prototype.chunkProductsForDisplay = function() {
  	this.productArrays = [];
  	this.removeTestAndZeroTotalProducts();    
    // \todo magic numbers, vary by dashboard or drilldown
    var NUMBER_OF_COLUMNS_FOR_MAIN_DASH = 3;
    var cols = NUMBER_OF_COLUMNS_FOR_MAIN_DASH; 
    this.productArrays = thetaUtil.chunk(this.productArrays, cols); 
  };


  // on drilldowns, graphs per product
  DrilldownGraphDataService.prototype.refreshProductGraphs = function() {
  	var data = JSON.parse(JSON.stringify(this.raw_data));
  	if (!data) {
  		return;
  	}
  	var ci = this.combineSeriesForProductGraphs(data);
  	this.product_graphs = {};
  	for (var key in ci) {
  		var product_info = ci[key];

  		for (var series_type in product_info.current) {

  			product_info.current[series_type].options = thetaUtil.defaultGraphOptions();
  			product_info.current[series_type].features = this.featuresForProductGraph(product_info.current[series_type]);        

  			var seriesData = this.rickshawSeries(product_info.current, 0, data);
  			var previousSeriesData = this.rickshawSeries(product_info.previous, 0, data);
  			product_info.current[series_type].series = [
  			{
  				color: thetaUtil.MAIN_COLOR,
  				data: seriesData[series_type].series,
  				name: product_info.current[series_type].name,
  				renderer: 'line',
  			}
  			];

  			this.product_graphs[key] = product_info.current[series_type];
  			this.product_graphs[key].previousSeries = {
  				data: previousSeriesData[series_type].series,
  			};

  		}
  	}
  };


  DrilldownGraphDataService.prototype.featuresForProductGraph = function(seriesInfo) {
  	var tickLimits = this.tickLimits(seriesInfo.data);
  	return {
  		'hover': 
  		{
  			'formatter': thetaUtil.dateHoverFormatterForName(seriesInfo.name)
  		},
  		'yAxis': 
  		{
  			'tickValues':tickLimits, 
  			'tickFormat': Rickshaw.Fixtures.Number.formatKMBT,
  		},
  	};
  };


  DrilldownGraphDataService.prototype.combineSeriesForProductGraphs = function(data) {
  	var raw_data = JSON.parse(JSON.stringify(this.raw_data));
  	var ci = {};
  	for (var key in data.product_series) {
  		var info = data.product_series[key];
  		if (this.products[key] && this.products[key].checked === false) {
  			continue;
  		}
  		if (!ci[key]) {
  			ci[key] = {
  				"current": info.current,
  				"previous": info.previous,
  				"product_info": data.product_info[key]
  			};
        // why is this null sometimes?
        if (data.product_info[key]) {
        	for (var iap in data.product_info[key].in_app_products) {
        		var pid = data.product_info[key].in_app_products[iap].product_id;
        		var product = raw_data.product_series[pid];
        		for (var j=0;j<product.current[0].data.length;j++) {
        			ci[key].current[0].data[j] +=
        			product.current[0].data[j];
        		}
        		for (var k=0;k<info.previous[0].data.length;k++) {
        			ci[key].previous[0].data[k] +=
        			product.previous[0].data[k];
        		}
        	}
        }          
      }
    }
    return ci;
  };


  // resize graphs on screen size change or first load
  // \todo magic numbers
  DrilldownGraphDataService.prototype.resizeGraphs = function (newHeight, newWidth) {
  	if (this.graphs) {
  		for (var i=0;i<this.graphs.length;i++) {
  			var g = this.graphs[i];
  			var newOptions = JSON.parse(JSON.stringify(g.options));
  			newOptions.width = newWidth;
  			g.options = newOptions;
  		}
  	}
  	if (this.product_graphs) {
  		for (var key in this.product_graphs) {
  			var graph = this.product_graphs[key];
  			var productOptions = JSON.parse(JSON.stringify(graph.options));
  			productOptions.width = newWidth;
  			if ($window.innerWidth > 768) { 
  				productOptions.width = newWidth / 3 - 30;
  			}
  			graph.options = productOptions;
  		}
  	}
  };


  DrilldownGraphDataService.prototype.shouldTotal = function(graph) {
  	if (graph.name == "Revenue" ||
  		  graph.name == "Downloads" ||
  		  graph.name == "Refunds" ||
  		  graph.name == "Sessions" ||
  		  graph.name == "Daily Users" ||
  		  graph.name == "Updates"
  		  ) {
  		return true;
  	}
  	return false;
  };


  // the total value for a product for an app, 
  // including in-app purchases
  DrilldownGraphDataService.prototype.valueStringForProduct = function(product_id) {
  	var graph = this.salesGraphForProduct(product_id);
  	var total = this.valueForProduct(product_id);
  	if (graph.format == "money") {
  		return '$' + thetaUtil.kFormatter(Math.round(total));
  	}
  	return thetaUtil.kFormatter(Math.round(total));  
  };


  // the total value for a product for an app, 
  // including in-app purchases
  DrilldownGraphDataService.prototype.valueForProduct = function(product_id) {
  	var graph = this.salesGraphForProduct(product_id);
  	var total = graph.data[graph.data.length-1];
  	for (var iap in this.products[product_id].in_app_products) {
  		var pid = this.products[product_id].in_app_products[iap].product_id;
  		total += this.salesGraphForProduct(pid).data[graph.data.length-1];
  	}
  	return total;  
  };

  DrilldownGraphDataService.prototype.dataURLForBase = function(base, productIDString, startDate, endDate) {
  	var secondsStart = startDate.getTime() / 1000 - thetaUtil.secondsInADay();
  	var secondsEnd = endDate.getTime() / 1000 - thetaUtil.secondsInADay();
  	if (startDate == -1) {
  		secondsStart = 0;
  	}
  	return base + '/' + productIDString + '/'  + this.series_name + '/'  + secondsStart + '/' + secondsEnd + '/';
  };


  DrilldownGraphDataService.prototype.nameForProduct = function(product_id) {
  	var product = ProductService.appInfo.fetchedInfo[this.products[product_id].product_id_string];
  	if (!product) {
  		return product_id;
  	}
	  return product.name;
	};

  return DrilldownGraphDataService;

});


// custom events subclass
angular.module('theta').factory('AppDetailsGraphDataService', function($window, ProductService, GraphDataService, $rootScope, $location, $http) {
  // create our new custom object that reuse the original object constructor
  var AppDetailsGraphDataService = function() {
  	GraphDataService.apply(this, arguments);
  };

  AppDetailsGraphDataService.prototype = new GraphDataService();

  AppDetailsGraphDataService.prototype.fetchOtherAppsInfo = function() {
		var self = this;
		/*jslint loopfunc: true */
		// publisher_json is set in template
		self.otherApps = {};
		var counter = publisher_json.length;
		for (var key in publisher_json) {
			var app = publisher_json[key];
			var bundle_id = app.bundle_id;
			(function(bundle_id) {
				ProductService.appInfo.fetchIcon(bundle_id, app.platform).then(function(url) {
					counter--;
   			  if (bundle_id) {
						self.otherApps[bundle_id] = {};
						self.otherApps[bundle_id].icon = url;
						self.otherApps[bundle_id].platform = "1";
						if (!ProductService.appInfo.fetchedInfo[bundle_id]) {
							self.otherApps[bundle_id].name = "Unknown App";
						} else if (ProductService.appInfo.fetchedInfo[bundle_id].name) {
							self.otherApps[bundle_id].name = ProductService.appInfo.fetchedInfo[bundle_id].name.split(/[+~!.?,;:'"-]/)[0];
						}
   			  }
   			  if (counter == 0) {
            self.didFetchProductInfo = true;
   			  }
        });      
			})(bundle_id);
		}  }


  AppDetailsGraphDataService.prototype.fetchData = function(startDate, endDate) {
    this.fetchOtherAppsInfo();
		var url = this.dataURLForBase("/dashboard/graph_data_app", this.platform + '/' + this.country + '/' + this.app_id, startDate, endDate);
		var self = this;
		$http.get(url, {}).then(function(response) {
			self.dataLoaded = true;
			self.storeDataAndRefreshGraphs(response.data);
			$rootScope.$emit('appDetailsGraphDataUpdated');
		});     
	};

	AppDetailsGraphDataService.prototype.storeDataAndRefreshGraphs = function (data) {
		this.raw_data = JSON.parse(JSON.stringify(data));
		this.refreshGraphs();    
	};


	// display all graphs
  AppDetailsGraphDataService.prototype.refreshGraphs = function() {
    var data = JSON.parse(JSON.stringify(this.raw_data))[0];
  	this.graphs = [];
  	var revIndex = 0;
    for (var i=0;i<data.series_data.length;i++) {
    	var series_dict = data.series_data[i];
    	var cont = true;
      for (var j=0;j<series_dict.current.length;j++) {
        if (series_dict.current[j]) {
      		cont = false;
      	}
        if (series_dict.category_type) {
        	if (series_dict.current[j] <= 0) {
        		series_dict.current[j] = 201;
        	}
        }      
      }
      if (cont) {
      		continue;
      }
    	var seriesData = this.rickshawSeries(series_dict, series_dict.times);
    	// var previousSeriesData = this.rickshawSeries(series_dict.previous, series_dict.times);
      series_dict.options = thetaUtil.defaultGraphOptions();
      series_dict.options.width = $window.innerWidth/3-60;
  		if (!series_dict.category_type) {
    		series_dict.name = "Revenue";   
    		series_dict.format = "money";
    		series_dict.features = this.revenueFeatures(series_dict);   
		} else {
    		series_dict.name = series_dict.category + " - " + series_dict.category_type + " - " + series_dict.device_type;   
    		series_dict.features = this.features(series_dict);     
  		}
  		series_dict.series = [
	  		{
	  			color: thetaUtil.MAIN_COLOR,
	  			data: seriesData,
	  			name: series_dict.name,
	  			renderer: 'line',
	  		}
	  		];
	  	if (series_dict.category_type) {
	  		var scale = d3.scale.linear()
        .range([0, 1])
        .domain([0, -1]);
        series_dict.series[0].scale = scale;
	  	}
  		if (!series_dict.category_type) {
    		this.graphs.splice(0,0,series_dict);
  		} else {
    		this.graphs.push(series_dict);
  		}
    }
  };


  AppDetailsGraphDataService.prototype.rickshawSeries = function(series, times) {
  	var seriesData = [];

		for (var j=0;j<series.current.length;++j) {
        var dict;
        var value = series.current[j];
        	dict = {
          	'x':times[j], 
          	'y':series.current[j]
        }
        seriesData.push(dict);
     }
    return seriesData;
  };

  AppDetailsGraphDataService.prototype.features = function(series) {
  	var tickLimits = this.tickLimits(series.current);
  	var featureDict = {
  		'hover': {
  			'formatter': thetaUtil.dateHoverFormatterForName(series.name)
  		},
  		'yAxis':{
        'tickValues': [-tickLimits[0], -tickLimits[1]],
        'tickFormat': thetaUtil.reverseFormat
  		},
  	};
	  return featureDict;
  };

	return AppDetailsGraphDataService;

});



// custom events subclass
angular.module('theta').factory('CustomEventsGraphDataService', function(ProductService, GraphDataService, $rootScope, $location, $http) {
  // create our new custom object that reuse the original object constructor
  var CustomEventsGraphDataService = function() {
  	GraphDataService.apply(this, arguments);
  };

  CustomEventsGraphDataService.prototype = new GraphDataService();

  CustomEventsGraphDataService.prototype.fetchData = function(startDate, endDate) {
  	var url = this.dataURLForBase("/dashboard/series_data/custom_events", ProductService.productIDString, startDate, endDate);
  	var self = this;
		$http.get(url, {}).then(function(response){
  		self.raw_data = JSON.parse(JSON.stringify(response.data));
  		var ci = {"product": "Combined"};
  		for (var key in self.raw_data) {      
      //if (!self.skipUnselected(key)) {
      	ci = self.combineSeries(response.data, key, ci); 
      //}
    }    
    self.combinedInfo = ci;    
    self.refreshGraphs();    
    $rootScope.$emit('customGraphDataUpdated');
  });
  };


  CustomEventsGraphDataService.prototype.refreshGraphs = function() {
  	this.graphs = [];
    for (var key in this.parentGraphData.extraGraphs) {
    	this.graphs.push(this.parentGraphData.extraGraphs[key]);
    }
  	var data = JSON.parse(JSON.stringify(this.raw_data));

  	var ci = {"product": "Combined"};
  	for (key in data) {
  		if (!this.skipUnselected(key)) {
  			ci = this.combineSeries(data, key, ci); 
  		}
  	}

  	var seriesData = this.rickshawSeries(ci.current, 0);
  	for (key in ci.current) {
  		if (seriesData[key].name == "Exported Times,Cancel") continue;
  		if (seriesData[key].name == "Exported Times,Sent") continue;
  		if (seriesData[key].name == "gaia_buyer_login") continue;
  		ci.current[key].options = thetaUtil.defaultGraphOptions();
  		ci.current[key].options.customEventGraph = true;
  		ci.current[key].features = this.features(ci.current[key]);            
  		ci.current[key].series = [
  		{
  			color: thetaUtil.MAIN_COLOR,
  			data: seriesData[key].series,
  			name: key,
  			renderer: 'line',
  		}
  		];

  		this.graphs.push(ci.current[key]);
  	}
  	this.combinedInfo = ci;    
  };


  CustomEventsGraphDataService.prototype.features = function(seriesInfo) {
  	var tickLimits = this.tickLimits(seriesInfo.data);
  	return {
  		'yAxis':
  		{
  			'tickValues':tickLimits, 
  			'tickFormat': Rickshaw.Fixtures.Number.formatKMBT,
  		},
  	};
  };


  CustomEventsGraphDataService.prototype.combineSeries = function(data, key, ci) {
  	if (!ci.current) {
  		ci.current = data[key].current;
  		ci.previous = data[key].previous;
  		ci.product_info = data.product_info;
  	} else {
  		for (var currKey in data[key].current) {
  			ci.current[currKey].name = data[key].current[currKey].name;
  			for (var j=0;j<data[key].current[currKey].data.length;j++) {
  				ci.current[currKey].data[j] +=
  				data[key].current[currKey].data[j];
  			}
  			if (data[key].previous) {
  				for (var k=0;k<data[key].previous[currKey].data.length;k++) {
  					ci.previous[currKey].data[k] +=
  					data[key].previous[currKey].data[k];
  				}          
  			}
  		}
  	}
  	return ci;
  };


  CustomEventsGraphDataService.prototype.rickshawSeries = function(series, offset) {
  	var seriesData = {};
  	for (var key in series) {
  		var rsSeries = [];
  		for (var j=0;j<this.parentGraphData.raw_data.times.length - offset;++j) {
        // make date/value tuples for rickshaw
        var dict = {
        	'x':this.parentGraphData.raw_data.times[j], 
        	'y':series[key].data[j]
        };
        rsSeries.push(dict);
      }
      seriesData[key] = {'series':rsSeries, 'name':series[key].name, 
      'options':series[key].options, 'features':series[key].features};
    }
    return seriesData;
  };

  return CustomEventsGraphDataService;

});


// competitors ranks subclass
angular.module('theta').factory('RanksGraphDataService', function(ProductService, GraphDataService, $rootScope, $location, $http) {
  // create our new custom object that reuse the original object constructor
  var RanksGraphDataService = function() {
  	GraphDataService.apply(this, arguments);
  };

  RanksGraphDataService.prototype = new GraphDataService();

  RanksGraphDataService.prototype.fetchData = function(
  	startDate, endDate, platform_id, categoryType, category, subcategory, country, page, numResults) {
		var secondsStart = startDate.getTime() / 1000;
		var secondsEnd = endDate.getTime() / 1000;
		if (startDate == -1) {
			secondsStart = 0;
		}
		var url = "/dashboard/series_data/competitors/" 
		          + platform_id + '/' + categoryType + '/' 
		          + category + '/' + subcategory + '/' + country + '/'  + page + '/'  
		          + numResults + '/'  + secondsStart + '/' + secondsEnd + '/';
  	var self = this;
  	self.products = {};
		$http.get(url, {}).then(function(response){
  		self.raw_data = JSON.parse(JSON.stringify(response.data));
    	var counter = self.raw_data.length;
  		for (var key in self.raw_data) {
  			var product_id = self.raw_data[key].app_id;
  			var platform = self.raw_data[key].platform;
				var category_ranks = self.raw_data[key].series_data[0].current;
				var overall_ranks = self.raw_data[key].series_data[1].current;
				var overall_rank = overall_ranks[overall_ranks.length-1];
				var category_rank = category_ranks[category_ranks.length-1];
    		self.products[product_id] = {};
				self.products[product_id].overall_rank = overall_rank;
				self.products[product_id].category_rank = category_rank;
				(function(product_id) {
					ProductService.appInfo.fetchIconIos(product_id).then(function(url) {
						counter--;
    				if (self.products[product_id]) {
    					self.products[product_id].icon = url;
    				}
    				if (ProductService.appInfo.fetchedInfo[product_id].name &&
    					ProductService.appInfo.fetchedInfo[product_id].name.length) {
  			    	self.products[product_id].name = ProductService.appInfo.fetchedInfo[product_id].name.split(/[+~!.?,;:'"-]/)[0];
    				}
    				if (counter == 0 ) {
              self.refreshGraphs();    
              $rootScope.$emit('ranksGraphDataUpdated');
  					}
					});      
				})(product_id);
		  }
    });
  };


  RanksGraphDataService.prototype.refreshGraphs = function() {
  	this.graphs = [];
  	var data = JSON.parse(JSON.stringify(this.raw_data));
  	var ci = {"product": "all_apps"};
  	for (var key in data) {
  	  ci = this.combineSeries(data[key], key, ci); 
  	}
  	for (key in ci) {
      if (key == "product") {
  			continue;
  		} 
  		ci[key].options = thetaUtil.defaultGraphOptions();
  		ci[key].options.height = 60;
  		ci[key].options.width = 230;
  		ci[key].options.customEventGraph = true;
      ci[key].options.padding = {top: 0.1, left: 0.02, right: 0.02, bottom: 0.1}
  		if (ci[key].series_data.length >= 3 && 
  			!ci[key].series_data[2].category_type) {
    		ci[key].name = "Revenue";   
    		ci[key].format = "money";
     		ci[key].features = this.revenueFeatures(ci[key]);     
  		} else if (ci[key].series_data.length >= 3) {
    		ci[key].name = ci[key].series_data[2].category + " - " + ci[key].series_data[2].category_type + " - " + ci[key].series_data[2].device_type;   
    		ci[key].features = this.features(ci[key]);     
  		}


      if (ci[key].series_data.length >= 3) {
	    	var earningsSeriesData = this.rickshawSeries(ci, 0, 2, false);
	    	var previousSeriesData = this.rickshawSeries(ci, 0, 2, true);
	    	if (earningsSeriesData) {
		      ci[key].previousSeries = {
		  			data: previousSeriesData[key].series,
		  		};
		  		ci[key].series = [
											  		{
											  			color: thetaUtil.MAIN_COLOR,
											  			data: earningsSeriesData[key].series,
											  			name: key,
											  			renderer: 'line',
											  		}
		  		                 ];
	    		ci[key].features = this.features(ci[key].series_data[0].current);                		
	    	}
	    }
  		this.graphs.push(ci[key]);
  	}
  	this.combinedInfo = ci; 
  };


  RanksGraphDataService.prototype.features = function(series) {
  	var tickLimits = this.tickLimits(series);
  	return {
  		'yAxis':{
  			'tickValues':tickLimits, 
  			'tickFormat': Rickshaw.Fixtures.Number.formatKMBT,
  		},
  	};
  };

  RanksGraphDataService.prototype.combineSeries = function(data, key, ci) {
  	ci[key] = data;
  	for (var i=0;i>data.series_data.length;i++) {
  		var series = data.series_data[i];
  		if (series.type == "Earnings" ||
  			  series.type == "Downloads" ) {
  			series.currentTotal = 0;
  			for (var j=0;j<series.current.length;j++) {
          series.currentTotal += series.current[j];
  			}
  			series.previousTotal = 0;
  			for (var j=0;j<series.previous.length;j++) {
          series.previousTotal += series.previous[j];
  			}
  		}
  	}
  	return ci;
  };


 
  RanksGraphDataService.prototype.rickshawSeries = function(apps, offset, series_index, is_previous) {
  	var seriesData = {};
  	for (var key in apps) {
  		if (key == "product") {
  			continue;
  		}
  		var app = apps[key];
  		var rsSeries = [];
  		var earnings_series;
  		for (var j=0;j<app.series_data.length - offset;++j) {
  			if (app.series_data[j].type == "Earnings") {
          earnings_series = app.series_data[j];
          break;
  			}
  		}


  		for (var j=0;j<earnings_series.current.length - offset;++j) {
        // make date/value tuples for rickshaw
        var dict;
        if (is_previous) {
	        dict = {
	        	'x':earnings_series.times[j], 
	        	'y':earnings_series.previous[j]
	        };        	
        } else {
        	dict = {
          	'x':earnings_series.times[j], 
          	'y':earnings_series.current[j]
          };
        }
        rsSeries.push(dict);
      }
      seriesData[key] = {'series':rsSeries, 'name':app.type, 
       'options':app.options, 'features':app.features};
    }
    return seriesData;
  };

  return RanksGraphDataService;

});


angular.module('theta').factory('FollowAppsGraphDataService', function($window, ProductService, RanksGraphDataService, $rootScope, $location, $http, PlatformInfoService) {
  // create our new custom object that reuse the original object constructor
  var FollowAppsGraphDataService = function() {
  	RanksGraphDataService.apply(this, arguments);
  };

  FollowAppsGraphDataService.prototype = new RanksGraphDataService();
  

  FollowAppsGraphDataService.prototype.addApp = function(app) {
  };

  FollowAppsGraphDataService.prototype.iconClassForDeviceType = function(deviceType) {
  	if (deviceType == "iPad") {
    	return	"mdi-hardware-tablet-mac";
  	}
  	return "mdi-hardware-phone-iphone";
  };

  FollowAppsGraphDataService.prototype.iconClassForRankingType = function(categoryType) {
  	if (categoryType == "Top Grossing") {
    	return "mdi-editor-attach-money";
  	}
  	return "mdi-file-file-download";
  };

  FollowAppsGraphDataService.prototype.stringForRankingType = function(categoryType) {
  	if (categoryType == "Top Grossing") {
    	return "";
  	}
  	if (categoryType == "Top Paid") {
    	return "Paid";
  	}
  	return "Free";
  };


  FollowAppsGraphDataService.prototype.fetchData = function(startDate, endDate) {
  	var base = "/dashboard/graph_data_followed_apps/" + PlatformInfoService.selectedCountry.id;
		var url = this.dataURLForBase(base, startDate, endDate);
  	var self = this;
  	if (!self.products) {
  		self.products = {};
  	}
  	this.searching = true;
			$http.get(url, {}).then(function(response){
			  self.searching = false;
  			self.dataLoaded = true;
  	  	self.raw_data = JSON.parse(JSON.stringify(response.data));
    	  var counter = self.raw_data.length;
  		for (var key in self.raw_data) {
  			var product_id = self.raw_data[key].app_id;
  			var platform = self.raw_data[key].platform;

  			var seriesDicts = [];
  			for (var i=0;i<self.raw_data[key].series_data.length;i++) {
  				var series = self.raw_data[key].series_data[i];
  				var dict = {};
  				dict.rank = series.current[series.current.length-1];
  				dict.timeForLastRank = series.times[series.times.length-1];
  				dict.device_type = series.device_type;
  				dict.category = series.category;
  				dict.category_type = series.category_type;
  				seriesDicts.push(dict);
  			}
  			seriesDicts.sort(function(a,b) { return parseFloat(a.rank) - parseFloat(b.rank) } );
    		self.products[product_id] = {};
    		self.products[product_id].product_id = product_id;
    		self.products[product_id].platform = platform;
    		self.products[product_id].platform_id = self.raw_data[key].platform_id;
        if (self.raw_data[key].country) {
      		self.products[product_id].selectedCountry = self.raw_data[key].country;
        } else {
      		self.products[product_id].selectedCountry = PlatformInfoService.selectedPlatform.countries[0];
        }
    		self.products[product_id].series = seriesDicts;
				(function(product_id) {
					ProductService.appInfo.fetchIconIos(product_id).then(function(url) {
						counter--;
    				if (self.products[product_id]) {
    					self.products[product_id].icon = url;
    				}
    				if (ProductService.appInfo.fetchedInfo[product_id].name &&
    					ProductService.appInfo.fetchedInfo[product_id].name.length) {
  			    	self.products[product_id].name = ProductService.appInfo.fetchedInfo[product_id].name;
    				}
    				if (counter == 0 ) {
    					self.ProductService = ProductService;
              self.refreshGraphs();    
              self.fetchEstimates(startDate, endDate, 'revenue');
              self.fetchEstimates(startDate, endDate, 'downloads');
		          //$rootScope.$emit('followAppsGraphDataUpdated');
  					}
					});      
				})(product_id);
		  }
    });
	};

	FollowAppsGraphDataService.prototype.fetchEstimates = function(startDate, endDate, estimate_type) {
  	var base = "/dashboard/graph_data_followed_apps_estimates/" + PlatformInfoService.selectedCountry.id;
		var url = this.dataURLForBase(base, startDate, endDate);
    url = url + estimate_type + '/'; 

		var self = this;
  	this.searching = true;
		$http.get(url, {}).then(function(response){
	    self.searching = false;
			for (var i=0;i<response.data.length;i++) {
	      var app = response.data[i];
	      var product = self.products[app.app_id];
	      if (estimate_type == 'downloads') {
		      var currentDownloadsTotal = 0;
		      var previousDownloadsTotal = 0;
		      for (var j=0;j<app.series_data[0].current.length;j++) {
		      	currentDownloadsTotal += app.series_data[0].current[j];
		      	previousDownloadsTotal += app.series_data[0].previous[j];
		      }
          product.currentDownloadsTotal = roundBigInteger(currentDownloadsTotal);
          product.previousDownloadsTotal = roundBigInteger(previousDownloadsTotal);
	      } else {
		      var currentRevenueTotal = 0;
		      var previousRevenueTotal = 0;
		      for (var j=0;j<app.series_data[0].current.length;j++) {
		      	currentRevenueTotal += app.series_data[0].current[j];
		      	previousRevenueTotal += app.series_data[0].previous[j];
		      }
          product.currentRevenueTotal = roundBigInteger(currentRevenueTotal);
          product.previousRevenueTotal = roundBigInteger(previousRevenueTotal);
	      }
			}
    },
    
    function(response) {
  	    self.searching = false;
    }
    );
	};

	function roundBigInteger(bigint) {
  		if (bigint > 10000) {
        return Math.floor(bigint/1000)*1000;
      } else if (bigint > 1000) {
        return Math.floor(bigint/100)*100;
      } else {
        return Math.floor(bigint/10)*10;
      }
    };


	FollowAppsGraphDataService.prototype.dataURLForBase = function(base, startDate, endDate) {
		var secondsStart = startDate.getTime() / 1000;
		var secondsEnd = endDate.getTime() / 1000;
		if (startDate == -1) {
			secondsStart = 0;
		}
		return base + '/'  + secondsStart + '/' + secondsEnd + '/';
	};


  FollowAppsGraphDataService.prototype.rickshawSeries = function(apps, offset, series_index, is_previous) {
    return null;
  };

  FollowAppsGraphDataService.prototype.timeForLastRank = function(series) {
    return null;
  };


  return FollowAppsGraphDataService;
});


// realtime subclass
angular.module('theta').factory('RealtimeGraphDataService', function(ProductService, GraphDataService, $rootScope, $location, $http, $window) {
  // create our new custom object that reuse the original object constructor
  var RealtimeGraphDataService = function() {
  	GraphDataService.apply(this, arguments);
  };

  RealtimeGraphDataService.prototype = new GraphDataService();

  RealtimeGraphDataService.prototype.realtimeGraphType = "Sessions";
  
  RealtimeGraphDataService.prototype.startFetching = function(graphData) {
  	this.parentGraphData = graphData;
  	var self = this;
		$http.get(this.urlForLast24Hours(ProductService.productIDString), {}).then(function(response){
  		self.loadRealtimeData(response);
  		setInterval(function() {
  			self.fetchAndLoadRealtimeData();
  		}, self.realtimeDelayInMilliseconds());
  	});
  };


  // store the response in memory and refresh the graphs
  RealtimeGraphDataService.prototype.loadRealtimeData = function(response) {
  	this.realtime_data = JSON.parse(JSON.stringify(response.data));
  	this.products = this.parentGraphData.products;
  	this.loadRealtimeGraph();
  	$rootScope.$emit('realtimeGraphDataUpdated');
  	$rootScope.$emit('realtimeGraphDataInitialized');
  };


  RealtimeGraphDataService.prototype.fetchAndLoadRealtimeData = function () {
  	var self = this;
		$http.get(this.urlForLast24Hours(ProductService.productIDString), {}).then(function(response){
  		self.loadRealtimeData(response);
  	});
  };

  
  RealtimeGraphDataService.prototype.realtimeDelayInMilliseconds = function() {
  	return 5000;
  };


  // display the realtime graph
  RealtimeGraphDataService.prototype.loadRealtimeGraph = function() {
  	var data = this.realtime_data;
  	var ci = {"product": "Combined"};
  	var key;
  	if (this.realtimeGraphType != "sessions" &&
  		this.realtimeGraphType != "Sessions" &&
  		this.realtimeGraphType != "Daily Revenue") {
  		for (key in data.product_series) {     
  			//if (!this.skipUnselected(key)) {
  				for (var pid in data.product_series[key].current) {
  					var dict = data.product_series[key].current[pid];
  					if (dict.name == this.realtimeGraphType) {
  						if (!ci.current) {
  							ci.current = [dict];
  						} else {
  							for (var j=0;j<dict.data.length;j++) {
  								ci.current[0].data[j] +=
  								dict.data[j];
  							}
  						}
  					}
  				//}
  			}
  		}
  	} else {
  		for (key in data.product_series) {      
  			if (!this.skipUnselected(key)) {
  				ci = this.combineSeries(data, key, ci); 
  			}
  		}      
  	}

  	ci.current[0].name = this.realtimeGraphType;
  	var seriesData = this.rickshawSeries(ci.current, 10, data);
  	ci.current[0].options = thetaUtil.defaultGraphOptions();
  	ci.current[0].options.width = $window.innerWidth - 40;
  	ci.current[0].options.realtime = true;
  	ci.current[0].features = this.features(ci.current[0]);  

  	var renderer = "line";
  	if (this.realtimeGraphType != "sessions" &&
  		this.realtimeGraphType != "Sessions") {
  		renderer = "bar";      
  }
  ci.current[0].series = [
  {
  	color: thetaUtil.MAIN_COLOR,
  	data: seriesData[0].series,
  	name: this.realtimeGraphType,
  	renderer: renderer,
  }  
  ];
  this.realtime_graph =  ci.current[0];  
};


RealtimeGraphDataService.prototype.features = function(seriesInfo) {
	var tickLimits = this.tickLimits(seriesInfo.data);
	return {
		'hover': 
		{
			'formatter': thetaUtil.hoverFormatterForName(seriesInfo.name)
		},
		'xAxis': 
		{
			'ticksTreatment': 'glow',
			'timeUnit': thetaUtil.getTimeUnit(14400)
		},
		'yAxis': 
		{
			'tickValues':tickLimits, 
			'tickFormat': Rickshaw.Fixtures.Number.formatKMBT,
		},
	};
};

  // resize graphs on screen size change or first load
  // \todo magic numbers
  RealtimeGraphDataService.prototype.resizeGraphs = function (newHeight, newWidth) {
  	var rtg = this.realtime_graph;
  	var rtOptions = JSON.parse(JSON.stringify(rtg.options));
  	rtOptions.width = $window.innerWidth-40;
  	rtg.options = rtOptions;
  };





  RealtimeGraphDataService.prototype.urlForLast24Hours = function (productIDString) {
  	var secondsToday = new Date().getTime() / 1000;
  	var secondsBefore = secondsToday - thetaUtil.secondsInADay();
  	var url = "/dashboard/graph_data_realtime/" + this.realtimeGraphType + '/' + productIDString + '/'  + secondsBefore + '/' + secondsToday + '/';
  	return url;
  };



  // total the last bucket and 10 buckets ago 
  RealtimeGraphDataService.prototype.valueForRealtimeGraph = function(graph) {
  	if (!graph) {
  		return 0;
  	}
  	var value = thetaUtil.kFormatter(Math.round(graph.series[0].data[graph.series[0].data.length-1].y));
  	if (graph.format == "money") {
  		return '$' + value;
  	}
  	return thetaUtil.kFormatter(value);
  };

  return RealtimeGraphDataService;
});
