angular.module('theta')

.controller('SearchController', function (ProductService, $scope, $window, $http) {

  $scope.getApps = function(query) {
    var url = '/search/' + query + '/';
    return $http.get(url, {}).then(function(response){
      for (var i=0;i<10;i++) {
        var item = response.data.slice(0,10)[i];
        if (item) {
          // ProductService.appInfo.fetchIconForSearch(item.id, item.platform);
        }
      }
      return response.data.slice(0,10);
    });
  };


  $scope.testImg = function() {
    return "/static/img/android.png";
  }


  $scope.onSelect = function (item) {
    var url = '/apps/'+ item.platform + '/US/' + item.id + '/';
    mixpanel.track("Select Search Result", 
                   {"url":url, "app_id":item.id, "app_platform":item.platform}
                  );
    $window.location.href = url;
  };

  $scope.iconForApp = function(item) {
    if (ProductService.appInfo.fetchedInfo[item.fields.id]) {
      return ProductService.appInfo.fetchedInfo[item.fields.id].icon_url;      
    }
    return "/static/img/blank.png";      
  };

  $scope.trackSearch = function() {
    mixpanel.track("Click Search");
  }

});


angular.module('theta')

.controller('FollowAppSearchController',  function ($controller, $scope, $http, $rootScope) {
  $controller('SearchController', {$scope: $scope});

  $scope.onSelect = function (item) {
    mixpanel.track("Follow App", 
                   {"app_id":item.id, "app_platform":item.platform}
                  );
    
    var url = '/follow/'+ item.platform + '/' + item.id + '/';
    $scope.searching = true;
    $http.get(url).then(function(response) {
      $scope.searching = false;
      if (response.data.success) {
        $rootScope.$emit('followAppsUpdated', {"app_info":response.data, "app":item});
      }
    });   
  };


});