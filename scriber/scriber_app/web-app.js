// import React from "react";

// date/time library
require('moment');

// custom scss
require("./static/scss/signup.scss");
require("./static/scss/dashboard.scss");
require("./static/scss/publisher.scss");

// boot strap material design
// require('jquery');
require('./static/bower_components/bootstrap-material-design/dist/js/material.js');
require('./static/bower_components/bootstrap-material-design/dist/js/ripples.js');
require('./static/bower_components/bootstrap-material-design/dist/css/ripples.css');

// angular
require('./static/bower_components/angular-bootstrap/ui-bootstrap-tpls.js');
require('./static/bower_components/angular-ui-grid/ui-grid.js');
require('./static/bower_components/angularjs-ordinal-filter/ordinal-browser.js');
require('./static/bower_components/angular-daterangepicker/js/angular-daterangepicker.js');
require("./node_modules/angular-cookies/angular-cookies.js");

// angular controllers
angular.module('theta', ['ordinal', 'ui.bootstrap', 'ngCookies', 'daterangepicker', 'ui.grid', 'ui.grid.pagination']).run(function($http, $cookies) {
  // csrf to make django happy
  $http.defaults.headers.post['X-CSRFToken'] = $cookies.getAll()['csrftoken'];
  
  
  mixpanel.track_links("#logo-link", "Click Nav", {"link":"Home"});
  mixpanel.track_links("#demo-link", "Click Nav", {"link":"Demo"});
  mixpanel.track_links("#rankings-link", "Click Nav", {"link":"Rankings"});
  mixpanel.track_links("#blog-link", "Click Nav", {"link":"Blog"});
  mixpanel.track_links("#user-menu-link", "Click Nav", {"link":"User Dropdown"});
  mixpanel.track_links("#user-menu-account", "Click Nav", {"link":"User - Account"});
  mixpanel.track_links("#user-menu-logout", "Click Nav", {"link":"User - Logout"});
  mixpanel.track_links("#login-link", "Click Nav", {"link":"Login - Mobile"});
  mixpanel.track_links("#home-signup-button", "Click Home Signup");
  mixpanel.track_links("#bottom-home-signup-button", "Click Home Signup", {"bottom":true});
  mixpanel.track_links("#home-dashboard-image", "Click Home Image");
  mixpanel.track_links(".developer-url", "Click Headshot");
  mixpanel.track_links(".developer-icon-url", "Click Developer Icon");
  mixpanel.track_links(".footer-link", "Click Footer Link", {"loggedIn":false});
  mixpanel.track_links(".loggedin-footer-link", "Click Footer Link", {"loggedIn":true});
  mixpanel.track_links("#dashboard-range-dropdown", "Click Range Dropdown", {"view":"Sales Dashboard"});
  mixpanel.track_links("#dashboard-date-selector", "Click Date Selector", {"view":"Sales Dashboard"});
  mixpanel.track_links("#ranks-range-dropdown", "Click Range Dropdown", {"view":"Rankings"});
  mixpanel.track_links("#ranks-date-selector", "Click Date Selector", {"view":"Rankings"});
  mixpanel.track_links(".company-name", "Click Company Name");
  mixpanel.track_links("#dashboard-signup-top", "Click Dashboard Signup");
})

.filter('secondsToDateTime', [function() {
    return function(seconds) {
    	  return moment(new Date('01-01-1970 UTC').setSeconds(seconds)).utc().fromNow(); 
    };
}]);

module.exports = angular.module('theta');

require('./static/js/signup-controller');
require('./static/js/SearchController');
require('./static/js/GraphDataService');
require('./static/js/AppInfoService');
  // init ripples effect
  $.material.init()
