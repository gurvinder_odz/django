var webpack = require('webpack');  

// https://github.com/owais/webpack-bundle-tracker
// Spits out some stats about webpack compilation process to a file
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {  
  entry: [
    './static/js/bootstrap-angular.js',
    "bootstrap-sass!./static/js/bootstrap-sass.config.js",
  ],
  plugins: [
    new webpack.NoErrorsPlugin(),
    new BundleTracker({filename: './webpack-stats.json'}),
    new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery'
        })
  ],
  output: {
    path: __dirname + '/static/build',
    filename: "bundle.js"
  },
  module: {
    loaders: [

      // es6-ify the JS
      { test: /\.js?$/,  
        loaders: ['babel'], exclude: ["/node_modules/", "/static/bower_components/"] },
      { test: /\.js$/,   
        loader: 'babel-loader', exclude:  ["/node_modules/", "/static/bower_components/"]},

      // for request library for ajax
      { test: /\.json$/, loader: 'json-loader', exclude: ["/node_modules/", "/static/bower_components/"] },

      // load CSS and SCSS
      { test: /\.scss$/, loader: "style!css!sass", exclude: ["/node_modules/", "/static/bower_components/"] },    
      { test: /\.css$/, loader: "style!css", exclude: ["/node_modules/", "/static/bower_components/"] },

      // This is needed so that each bootstrap js file required by
      // bootstrap-webpack has access to the jQuery object
      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },

      // Needed for the css-loader when [bootstrap-webpack](https://github.com/bline/bootstrap-webpack)
      // loads bootstrap's css.
      { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   
        loader: "url?limit=10000&minetype=application/font-woff" },
      { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,  
        loader: "url?limit=10000&minetype=application/font-woff" },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    
        loader: "url?limit=10000&minetype=application/octet-stream" },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    
        loader: "file" },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    
        loader: "url?limit=10000&minetype=image/svg+xml" }
    ]
  },
  node: {
    console: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  }
};

// angular
// <!-- script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0/angular.js"></script -->
//        <!-- % include partials/main-js-includes.html" %} -->

