import www.views
from dashboard.views import publisher, rankings, demo, apps, search, follow_app, unfollow_app, update_follow
import help_docs

from django.conf.urls import patterns, include, url
from django.contrib import admin
import payments
import stripe
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
  url(r'^$', www.views.homepage),
  url(r'^admin/', include(admin.site.urls)),
  url(r'^2DsCBXZKBsw7CmxyUYWDPMrQ', www.views.test_homepage),
  url(r'^docs/', www.views.docs),
  url(r'^support/', www.views.support),
  url(r'^terms-of-use/', www.views.terms_of_use),
  url(r'^faq/', www.views.faq),
  url(r'^pricing/', www.views.pricing),
  url(r'^demo/', demo),
  url(r'^account/', include('customers.urls')),
  url(r'^api/', include('ingestor.urls')),
  url(r'^accounts/login/',  'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
  url(r'^accounts/logout/', 'django.contrib.auth.views.logout',
                          {'next_page': '/'}),
  url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            'www.views.reset_confirm', name='password_reset_confirm'),
  url(r'^password_reset/$', 'www.views.reset', name='reset'),
  url(r"^customers/", include("customers.urls")),
  url(r"^payments/", include("payments.urls")),
  url(r'^stripe/', include('v_stripe.urls')),
  url(r'^help/', include('help_docs.urls')),
  url(r'^dashboard/', include('dashboard.urls')),
  url(r'^search/(?P<prefix>.+)/$', search),
  url(r'^rankings/$', rankings),
  url(r'^follow/(?P<platform>.+)/(?P<platform_app_id>.+)/$', follow_app),
  url(r'^unfollow/(?P<platform>.+)/(?P<platform_app_id>.+)/$', unfollow_app),
  url(r'^update_follow/(?P<platform>.+)/(?P<platform_app_id>.+)/(?P<country_id>.+)/$', update_follow),
  url(r'^apps/(?P<platform>.+)/(?P<country>.+)/(?P<app_id>.+)/$', apps),
  url(r'^publisher/(?P<artist_id>.+)/$', publisher),
  url(r'^hijack/', include('hijack.urls')),
)
