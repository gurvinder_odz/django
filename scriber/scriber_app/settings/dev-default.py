"""
  Scriber project Django settings for the production environment.
  """

from scriber_app.settings.common import *

# LOGGING SETTINGS
BASE_LOG_DIR = '/Users/andrewjohnson/src/Scriber/appserver/logs/'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': BASE_LOG_DIR + 'scriber_django.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # root logger
        '': {
            'handlers': ['file'],
            'level': 'INFO',
        },
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
        'ingestor': {
            'handlers': ['file'],
            'level': 'INFO',
        },
    }
}

PERSISTENT_DEFAULT_CONNECTION = 'persistent'
DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
      'NAME': 'sdbrealdata5',
        'USER': 'postgres',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
  },
  PERSISTENT_DEFAULT_CONNECTION: {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
      'NAME': 'sdbrealdata5',
        'USER': 'postgres',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
        'CONN_MAX_AGE': 60*20,
  },
}


# CELERY SETTINGS
# Since we use Redis as a cache backend, we connect to it over a socket file to avoid TCP overhead.
BROKER_URL = 'redis+socket:///tmp/redis.sock'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# CACHE SETTINGS
CACHES = {
  'default': {
    'BACKEND': 'redis_cache.RedisCache',
      'LOCATION': '/tmp/redis.sock',
    },
}

TIMEOUT=0

DEMO_AUTH_USER_ID = 2

# ENVIRONMENT-SPECIFIC STRIPE SETTINGS
PAYMENTS_PLANS = {
  "small": {
    "stripe_plan_id": "smallMonthlyTestPlan",
    "short_plan_id": "small",
      "name": "Small Monthly Test Plan",
        "description": "The small monthly subscription test plan",
        "features": ["iOS and Android stats",
                    "Subscription & IAP Analytics",
                    "Up to $7,500 per month earnings",
                    "Email Support"
                    ],
        "price": 79.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
    "medium": {
      "stripe_plan_id": "mediumMonthlyTestPlan",
      "short_plan_id": "medium",
        "name": "Medium Monthly Test Plan",
        "description": "The medium monthly subscription test plan",
        "features": ["iOS and Android stats",
                    "Subscription & IAP Analytics",
                    "Up to $25,000 per month earnings",
                    "Email Support"
                    ],
        "price": 129.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
    "large": {
      "stripe_plan_id": "largeMonthlyTestPlan",
      "short_plan_id": "large",
        "name": "Large Monthly Test Plan",
        "description": "The large monthly subscription test plan",
        "features": ["iOS and Android stats",
                    "Subscription & IAP Analytics",
                    "Up to $100,000 per month earnings",
                    "Email & Phone Support"
                    ],
        "price": 249.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
}

DEBUG = True

# CLOUDWATCH SETTINGS
ENABLE_CLOUDWATCH = False
CLOUDWATCH_NAMESPACE = 'ScriberDev'
CLOUDWATCH_REGION = 'us-west-1'
