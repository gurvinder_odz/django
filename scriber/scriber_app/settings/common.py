"""
Scriber project Django settings common to all environments (development, testing, production).

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMPLATE_DIRS = (
    BASE_DIR + '/templates/',
)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9axuzpvdvj6jw*j6s^g=ida7kucqn%vx4dt(fzyotk_$^&#c#9'
ENCRYPTED_DB_FIELD_SECRET_KEY = os.environ.get("ENCRYPTED_DB_FIELD_SECRET_KEY", SECRET_KEY)

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

# Application definition
INSTALLED_APPS = (
    'appinfo',
    'appstore',
    'customers',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.staticfiles',
    'django_forms_bootstrap',
    'emailer',
    'help_docs',
    'hijack',
    'ingestor',
    'localization',
    'metrics',
    'ml',
    'payments',
    'rest_framework',
    'v_stripe',
)

ROOT_URLCONF = 'scriber_app.urls'

WSGI_APPLICATION = 'scriber_app.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

LOGIN_REDIRECT_URL = '/'

MAILCHIMP_API_KEY = '02887fb3f4fe9f47aad7155eb533f55b-us10'

# COMMON STRIPE SETTINGS
STRIPE_PUBLIC_KEY = os.environ.get("STRIPE_PUBLIC_KEY", "pk_test_35pN0MAJJ1Z3JW8bfgMCrgiN")
STRIPE_SECRET_KEY = os.environ.get("STRIPE_SECRET_KEY", "sk_test_3PcA0eOuqyViA1PaRKFAKBmr")

HTTPS_SUPPORT = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'postmaster@mg.apptheta.com'
EMAIL_HOST_PASSWORD = '48d2e64b927c9ad475a0fb04f161d08b'
 
DEFAULT_FROM_EMAIL = "password_reset@apptheta.com"
SERVER_EMAIL = EMAIL_HOST_USER
