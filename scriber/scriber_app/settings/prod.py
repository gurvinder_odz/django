"""
Scriber project Django settings for the production environment.
"""

from .common import *
from celery.schedules import crontab
from datetime import datetime, timedelta
from kombu import Exchange, Queue

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [
    '.apptheta.com',     # Allow domain and subdomains
    '.scriber.io',     # Allow domain and subdomains
    '.54.67.108.236',  # AWS-assigned elastic IP
    '.ec2-54-67-108-236.us-west-1.compute.amazonaws.com',
]

# LOGGING SETTINGS
BASE_LOG_DIR = '/webapps/scuserhome/logs/'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': BASE_LOG_DIR + 'scriber_django.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # root logger
        '': {
            'handlers': ['file'],
            'level': 'INFO',
        },
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
        'ingestor': {
            'handlers': ['file'],
            'level': 'INFO',
        },
    }
}

# CELERY SETTINGS
# Since we use Redis as a cache backend, we connect to it over a socket file to avoid TCP overhead.
BROKER_URL = 'redis+socket:///var/run/redis/redis.sock'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 18000}  # 5 hours
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_RESULT_BACKEND = 'redis+socket:///var/run/redis/redis.sock'

CELERY_QUEUES = (
    Queue('default', Exchange('default', type='direct'), routing_key='default'),
    Queue('rankings', Exchange('rankings', type='direct'), routing_key='rankings'),
    Queue('realtime', Exchange('realtime', type='direct'), routing_key='realtime'),
)
CELERY_DEFAULT_QUEUE = 'default'
CELERY_DEFAULT_EXCHANGE = 'default'
CELERY_DEFAULT_ROUTING_KEY = 'default'

class ScriberRouter(object):
    def route_for_task(self, task, *args, **kwargs):
        if task.startswith('realtime.') or task.startswith('ml.'):
            return {'queue': 'realtime'}
        elif task.startswith('appstore.tasks.fetch_missing_ios_artists'):
            return {'queue': 'default'}
        elif task.startswith('appstore.') or task.startswith('deeprank.'):
            return {'queue': 'rankings'}

CELERY_ROUTES = (ScriberRouter(), )

# Note that crontab scheduling is in UTC time.
CELERYBEAT_SCHEDULE = {
    'daily-metrics-kickoff': {
        'task': 'metrics.tasks.tasks.daily_metrics_kickoff',
        'schedule': crontab(minute=0, hour=9),
        'args': (),
    },
    'daily-apple-metrics-kickoff-americas': {
        'task': 'metrics.tasks.reporttasks.daily_apple_metrics_kickoff',
        'schedule': crontab(minute=0, hour=10),
        'args': (),
        'kwargs': {'timezone_str': 'America/Los_Angeles'},
    },
    'daily-google-metrics-kickoff': {
        'task': 'metrics.tasks.reporttasks.daily_google_metrics_kickoff',
        'schedule': crontab(minute=0, hour=12),
        'args': (),
    },
    'daily-email-kickoff': {
        'task': 'emailer.tasks.daily_email_kickoff',
        'schedule': crontab(minute=0, hour=13),
        'args': (),
        'kwargs': {'date_str': str(datetime.utcnow().date())},
    },
    'followed-apps-cache-refresh': {
        'task': 'dashboard.tasks.followed_apps_cache_refresh_kickoff',
        'schedule': crontab(minute=0, hour='*'),
        'args': (),
    },
}

# CACHE SETTINGS
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '/var/run/redis/redis.sock',
        'OPTIONS': {
            'MAX_ENTRIES': 3000,
        },
    },
    'disk': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/appThetaCache',
        'TIMEOUT': 60 * 60 * 24 * 7,
        'OPTIONS': {
            'MAX_ENTRIES': 10000,
        },
    },
}

# DATABASE SETTINGS
PERSISTENT_DEFAULT_CONNECTION = 'persistent'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sdb',
        'USER': 'scriberdbmaster',
        'PASSWORD': 'fSS4Q5oBVTeVX9MwozzgMhMulTjXVZ0',
        'HOST': 'scriberdb2.cuhmo66njenl.us-west-1.rds.amazonaws.com',
        'PORT': '5432',
    },
    PERSISTENT_DEFAULT_CONNECTION: {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sdb',
        'USER': 'scriberdbmaster',
        'PASSWORD': 'fSS4Q5oBVTeVX9MwozzgMhMulTjXVZ0',
        'HOST': 'scriberdb2.cuhmo66njenl.us-west-1.rds.amazonaws.com',
        'PORT': '5432',
        'CONN_MAX_AGE': 60*20,
    },
}
DATABASE_ROUTERS = ['core.db.dynamic.DynamicDbRouter']

# MAILGUN SETTINGS
ENABLE_MAILGUN = False

# DEMO SETTINGS
DEFAULT_USER_ID = 125
DEMO_AUTH_USER_ID = 2

# ENVIRONMENT-SPECIFIC STRIPE SETTINGS
PAYMENTS_PLANS = {
  "small": {
    "stripe_plan_id": "smallMonthlyPlan",
    "short_plan_id": "small",
      "name": "Custom Metrics",
        "description": "The small monthly subscription plan",
        "features": [
                    "5 million events, plus $50/month for each additional 5M",
                    "Up to 10 apps",
                    "Track custom events in apps",
                    "Real-time and trends for all events",
                    "Email Support",
                    ],
        "price": 50,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    }
}

# CLOUDWATCH SETTINGS
ENABLE_CLOUDWATCH = False
CLOUDWATCH_NAMESPACE = 'ScriberProd'
CLOUDWATCH_REGION = 'us-west-1'

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',  # This must be first
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',  # This must be last
)