"""
  Scriber project Django settings for the production environment.
  """

from scriber_app.settings.common import *

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        'verbose': {
            'format': ('%(asctime)s [%(process)d] [%(levelname)s] ' +
                       '%(filename)s:%(lineno)s:%(funcName)s %(message)s'),
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}


DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
      'NAME': 'postgres',
      'USER': 'postgres',
      'HOST': 'db',
      'PORT': '5432',
  }
}

# CELERY SETTINGS
# Since we use Redis as a cache backend, we connect to it over a socket file to avoid TCP overhead.
#BROKER_URL = 'redis+socket:///tmp/redis.sock'
BROKER_URL = 'redis:///redis:6379/'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# CACHE SETTINGS
CACHES = {
  'default': {
    'BACKEND': 'redis_cache.RedisCache',
    'LOCATION': 'redis:6379',
    },
}

TIMEOUT=0

DEMO_AUTH_USER_ID = 2

# ENVIRONMENT-SPECIFIC STRIPE SETTINGS
PAYMENTS_PLANS = {
  "small": {
    "stripe_plan_id": "smallMonthlyTestPlan",
    "short_plan_id": "small",
      "name": "Small Monthly Test Plan",
        "description": "The small monthly subscription test plan",
        "features": ["iOS and Android stats",
                    "Subscription & IAP Analytics",
                    "Up to $7,500 per month earnings",
                    "Email Support"
                    ],
        "price": 79.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
    "medium": {
      "stripe_plan_id": "mediumMonthlyTestPlan",
      "short_plan_id": "medium",
        "name": "Medium Monthly Test Plan",
        "description": "The medium monthly subscription test plan",
        "features": ["iOS and Android stats",
                    "Subscription & IAP Analytics",
                    "Up to $25,000 per month earnings",
                    "Email Support"
                    ],
        "price": 129.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
    "large": {
      "stripe_plan_id": "largeMonthlyTestPlan",
      "short_plan_id": "large",
        "name": "Large Monthly Test Plan",
        "description": "The large monthly subscription test plan",
        "features": ["iOS and Android stats",
                    "Subscription & IAP Analytics",
                    "Up to $100,000 per month earnings",
                    "Email & Phone Support"
                    ],
        "price": 249.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
}

DEBUG = True

# CLOUDWATCH SETTINGS
ENABLE_CLOUDWATCH = False
CLOUDWATCH_NAMESPACE = 'ScriberDev'
CLOUDWATCH_REGION = 'us-west-1'
