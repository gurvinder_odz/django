"""
Scriber project Django settings for the testing environment.
"""

from .common import *
from celery.schedules import crontab
from datetime import timedelta
from kombu import Exchange, Queue

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# LOGGING SETTINGS
BASE_LOG_DIR = '/webapps/scuserhome/logs/'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': BASE_LOG_DIR + 'scriber_django.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        # root logger
        '': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'ingestor': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}

# CELERY SETTINGS
# Since we use Redis as a cache backend, we connect to it over a socket file to avoid TCP overhead.
BROKER_URL = 'redis+socket:///var/run/redis/redis.sock'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 18000}  # 5 hours
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_RESULT_BACKEND = 'redis+socket:///var/run/redis/redis.sock'

CELERY_QUEUES = (
    Queue('default', Exchange('default', type='direct'), routing_key='default'),
    Queue('rankings', Exchange('rankings', type='direct'), routing_key='rankings'),
    Queue('realtime', Exchange('realtime', type='direct'), routing_key='realtime'),
)
CELERY_DEFAULT_QUEUE = 'default'
CELERY_DEFAULT_EXCHANGE = 'default'
CELERY_DEFAULT_ROUTING_KEY = 'default'

class ScriberRouter(object):
    def route_for_task(self, task, *args, **kwargs):
        if task.startswith('realtime.') or task.startswith('ml.'):
            return {'queue': 'realtime'}
        elif task.startswith('appstore.') or task.startswith('deeprank.'):
            return {'queue': 'rankings'}

CELERY_ROUTES = (ScriberRouter(), )

# Note that crontab scheduling is in UTC time.
CELERYBEAT_SCHEDULE = {
    'daily-metrics-kickoff': {
        'task': 'metrics.tasks.tasks.daily_metrics_kickoff',
        'schedule': crontab(minute=0, hour=9),
        'args': (),
    },
    'daily-apple-metrics-kickoff-americas': {
        'task': 'metrics.tasks.reporttasks.daily_apple_metrics_kickoff',
        'schedule': crontab(minute=0, hour=10),
        'args': (),
        'kwargs': {'timezone_str': 'America/Los_Angeles'},
    },
    'daily-google-metrics-kickoff': {
        'task': 'metrics.tasks.reporttasks.daily_google_metrics_kickoff',
        'schedule': crontab(minute=0, hour=12),
        'args': (),
    },
    'daily-email-kickoff': {
        'task': 'emailer.tasks.daily_email_kickoff',
        'schedule': crontab(minute=0, hour=13),
        'args': (),
    },
    'realtime-batch-cache-update': {
        'task': 'realtime.batchtasks.realtime_batch_cache_update',
        'schedule': timedelta(seconds=60*30),
        'args': (),
    },
}

# CACHE SETTINGS
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '/var/run/redis/redis.sock',
    },
}

# DATABASE SETTINGS
PERSISTENT_DEFAULT_CONNECTION = 'persistent'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sdb',
        'USER': 'scuser',
        'PASSWORD': 'fSS4Q5oBVTeVX9MwozzgMhMulTjXVZ0',
        'HOST': 'localhost',
        'PORT': '',
    },
    PERSISTENT_DEFAULT_CONNECTION: {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sdb',
        'USER': 'scuser',
        'PASSWORD': 'fSS4Q5oBVTeVX9MwozzgMhMulTjXVZ0',
        'HOST': 'localhost',
        'PORT': '',
        'CONN_MAX_AGE': 60*20,
    },
}

# MAILGUN SETTINGS
ENABLE_MAILGUN = False

# DEMO SETTINGS
DEMO_AUTH_USER_ID = 2

# ENVIRONMENT-SPECIFIC STRIPE SETTINGS
PAYMENTS_PLANS = {
    "small": {
        "stripe_plan_id": "smallMonthlyTestPlan",
        "name": "Small Monthly Test Plan",
        "description": "The small monthly subscription test plan",
        "price": 79.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
    "medium": {
        "stripe_plan_id": "mediumMonthlyTestPlan",
        "name": "Medium Monthly Test Plan",
        "description": "The medium monthly subscription test plan",
        "price": 129.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
    "large": {
        "stripe_plan_id": "largeMonthlyTestPlan",
        "name": "Large Monthly Test Plan",
        "description": "The large monthly subscription test plan",
        "price": 249.99,
        "currency": "usd",
        "interval": "month",
        "trial_period_days": 60,
    },
}

# CLOUDWATCH SETTINGS
ENABLE_CLOUDWATCH = True
CLOUDWATCH_NAMESPACE = 'ScriberTest'
CLOUDWATCH_REGION = 'us-west-1'
