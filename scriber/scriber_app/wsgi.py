"""
WSGI config for scriber project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os

# Set the DJANGO_SETTINGS_MODULE environment variable to the production module unless it has already
# been set.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scriber_app.settings.prod")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
