from core.customfields import BigAutoField, BigForeignKey
from core.models import TimestampedModel
from django.db import models
from localization.models import Country


class AppStorePlatform(TimestampedModel):
  id = models.AutoField(primary_key=True)

  IOS = 'iOS'
  ANDROID = 'Android'
  NAME_CHOICES = (
      (IOS, 'iOS'),
      (ANDROID, 'Android'),
  )

  APP_STORE = 'App Store'
  GOOGLE_PLAY = 'Google Play'
  DISPLAY_NAME_CHOICES = (
      (APP_STORE, 'App Store'),
      (GOOGLE_PLAY, 'Google Play'),
  )

  name = models.CharField(choices=NAME_CHOICES, max_length=256)
  display_name = models.CharField(choices=DISPLAY_NAME_CHOICES, max_length=256)
  description = models.CharField(max_length=256)

  class Meta:
    db_table = 'app_store_platforms'


class AppStoreCategory(TimestampedModel):
  id = models.AutoField(primary_key=True)
  name = models.CharField(max_length=256)
  ios_genre_id = models.IntegerField(null=True, db_index=True)
  parent_category = models.ForeignKey('self', null=True, db_column='parent_category')
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')

  class Meta:
    db_table = 'app_store_categories'
    unique_together = ('ios_genre_id', 'platform')
    verbose_name = 'app_store_category'
    verbose_name_plural = 'app_store_categories'


class AppStoreArtist(TimestampedModel):
  id = models.AutoField(primary_key=True)
  artist_id = models.BigIntegerField(null=True, db_index=True)
  name = models.CharField(max_length=256)
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')

  class Meta:
    db_table = 'app_store_artists'
    unique_together = ('artist_id', 'platform')


class AppStoreApp(TimestampedModel):
  id = BigAutoField(primary_key=True)
  name = models.CharField(max_length=256)
  bundle_id = models.CharField(max_length=256, null=True, db_index=True)
  track_id = models.BigIntegerField(null=True, db_index=True)
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')
  sku = models.CharField(max_length=256, null=True)
  primary_category = models.ForeignKey(AppStoreCategory, null=True, db_column='primary_category')
  artist = models.ForeignKey(AppStoreArtist, null=True, db_column='artist')
  is_free = models.NullBooleanField(null=True)

  def to_json(self):
    json_dict = {
      'name':self.name, 
      'bundle_id':self.bundle_id, 
      'is_free':self.is_free, 
      'platform': self.platform.id
    }
    if self.primary_category:
      json_dict['category'] = self.primary_category.name
    return json_dict

  class Meta:
    db_table = 'app_store_apps'
    unique_together = ('bundle_id', 'platform')
    unique_together = ('track_id', 'platform')


class AppStoreRankType(TimestampedModel):
  id = models.AutoField(primary_key=True)
  display_name = models.CharField(max_length=256, unique=True)
  name = models.CharField(max_length=256, unique=True)
  description = models.CharField(max_length=256)
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')
  pop_id = models.IntegerField(null=True)

  class Meta:
    db_table = 'app_store_rank_types'


# TODO(d-felix): Handle country collisions.
class AppStoreRegion(TimestampedModel):
  id = models.AutoField(primary_key=True)
  country = models.ForeignKey(Country, db_column='country')
  has_ios_rankings = models.BooleanField(default=False)
  storefront_ios5 = models.CharField(
      max_length=20,
      null=True)   # storefront id used in ios5 request headers

  class Meta:
    db_table = 'app_store_regions'


class AppStoreRankRequest(TimestampedModel):
  id = BigAutoField(primary_key=True)
  category = models.ForeignKey(AppStoreCategory, null=True, db_column='category')
  rank_type = models.ForeignKey(AppStoreRankType, db_column='rank_type')
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')
  region = models.ForeignKey(AppStoreRegion, null=True, db_column='region')
  time = models.DateTimeField(db_index=True)
  rank_server_id = models.IntegerField(default=1)

  class Meta:
    db_table = 'app_store_rank_requests'


class AppStoreRank(models.Model):
  id = BigAutoField(primary_key=True)
  app = models.ForeignKey(AppStoreApp, db_column='app')
  rank = models.IntegerField(null=True)
  rank_request = models.ForeignKey(AppStoreRankRequest, db_column='rank_request')

  class Meta:
    db_table = 'app_store_ranks'
