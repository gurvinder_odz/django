import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scriber_app.settings.dev")
django.setup()


import codecs
import sys
import json
from datetime import datetime
from pytz import utc


import localization.models
from collections import namedtuple
from appstore.models import AppStoreRegion, AppStoreRankType, AppStoreCategory, AppStoreApp, AppStoreRank, AppStoreRankRequest, AppStorePlatform
from appinfo import fetcher
from django.db import transaction
from django.db.models import Q

from celery import group, task
from celery.utils.log import get_task_logger
from core.celeryutils import exponential_backoff
from core.utils import unpack
import requests

logger = get_task_logger(__name__)


store_url = 'http://itunes.apple.com/WebObjects/MZStore.woa/wa/topChartFragmentData'

itunes_api_lookup_url = "https://itunes.apple.com/lookup?id=%s"

def populateArtistInfo():
  try:
    appStoreFetcher = fetcher.AppStoreFetcher()
    while True:
      appStoreApps = AppStoreApp.objects.filter(artist=None)[:150]
      if len(appStoreApps) == 0:
        break;

      
      appStoreFetcher.updateAppStoreApps(appStoreApps)

    




  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


if __name__ == '__main__':
  populateArtistInfo()