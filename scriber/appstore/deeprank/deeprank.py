import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scriber_app.settings.dev")
django.setup()


import codecs
import sys
import json
from datetime import datetime
from pytz import utc


import localization.models
from collections import namedtuple
from appstore.models import AppStoreRegion, AppStoreRankType, AppStoreCategory, AppStoreApp, AppStoreRank, AppStoreRankRequest, AppStorePlatform
from django.conf import settings
from django.db import transaction
from django.db.models import Q

from celery import group, task
from celery.utils.log import get_task_logger
from core.cache import cache_results
from core.celeryutils import exponential_backoff
from core.utils import unpack
import requests

logger = get_task_logger(__name__)


store_url = 'http://itunes.apple.com/WebObjects/MZStore.woa/wa/topChartFragmentData'

_IOS_DEEP_RANK_URL_BASE = 'https://itunes.apple.com/WebObjects/MZStore.woa/wa/topChartFragmentData?genreId=%s&popId=%s&pageNumbers=%d&pageSize=%d'
_IOS_DEEP_RANK_USERAGENT =  'iTunes-iPad/5.1.1 (64GB; dt:28)'
_MAX_RETRIES = 1
_PAGESIZE = 500
_MAX_RANK = 1500

_IOS5_TOP_LISTS_ID = 2

PCONN = settings.PERSISTENT_DEFAULT_CONNECTION

@cache_results()
def ios_platform(timeout=60*20):
  iosPlatform = AppStorePlatform.objects.using(PCONN).get(name=AppStorePlatform.IOS)
  return iosPlatform

def fetch_deep_ranks(countriesAlpha2, platform, categories, force_sync=False):
  dbcountries = None
  appRegions = None
  if countriesAlpha2 is not None:
    dbcountries = localization.models.Country.objects.using(PCONN).filter(alpha2__in=countriesAlpha2)
  else:
    dbcountries = localization.models.Country.objects.using(PCONN).all()

  # Create lists to avoid multiple query executions.
  categories = [c for c in categories]
  appRegions = [r for r in AppStoreRegion.objects.using(PCONN).filter(country__in=dbcountries)]
  rankTypes = [t for t in AppStoreRankType.objects.using(PCONN).all()]
  taskList = []

  for rankType in rankTypes:
    for region in appRegions:
      for category in categories:
        taskList.append(fetch_top_list.s(
            categoryId=category.id, categoryIosGenreId=category.ios_genre_id,
            rankTypeId=rankType.id, rankTypePopId=rankType.pop_id,
            regionId=region.id, regionStorefront=region.storefront_ios5,
            platformId=platform.id))

  taskGroup = group(taskList)
  if force_sync:
    taskGroup.apply()
  else:
    taskGroup.apply_async()


def do_fetch(category, rank_type_pop_id, region_storefront_id, page, pagesize):
  url = _IOS_DEEP_RANK_URL_BASE % (category, rank_type_pop_id, page, pagesize)
  headers = {'User-Agent': _IOS_DEEP_RANK_USERAGENT, 'X-Apple-Store-Front': region_storefront_id }
  response = requests.get(url, headers=headers, timeout=60)
  return response


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True, name='deeprank.fetch_top_list')
def fetch_top_list(self, *args, **kwargs):
  try:
    (categoryId, categoryIosGenreId, rankTypeId, rankTypePopId, regionId, regionStorefront,
        platformId) = unpack(kwargs,
        'categoryId', 'categoryIosGenreId',
        'rankTypeId', 'rankTypePopId',
        'regionId', 'regionStorefront',
        'platformId')

    platform = ios_platform()
    rankRecords = []

    for page in range(0,_MAX_RANK/_PAGESIZE):
      execTime = datetime.utcnow().replace(tzinfo=utc)
      response = do_fetch(categoryIosGenreId, rankTypePopId, regionStorefront, page, _PAGESIZE)

      if response.status_code != requests.codes.ok:
        raise ValueError('Deep ranks fetch from %s failed with status %s' %
            (response.url, response.status_code))

      apparray = response.json()[0]['contentData']
      hasnextpage = response.json()[0]['hasNextPage']

      rank = 1 + page * _PAGESIZE
      requestRankRecords = []

      appStoreApps = AppStoreApp.objects.using(PCONN).filter(bundle_id__in=[x['buyData']['bundle-id'] for x in apparray])

      for app in apparray:
        item = {}
        item['rank'] = rank
        rank += 1
        item['name'] = app['buyData']['item-title']
        item['appid'] = app['id']
        item['bundleid'] = app['buyData']['bundle-id']

        appStoreApp = None
        for x in appStoreApps:
          if x.bundle_id == item['bundleid']:
            appStoreApp = x;
            break;

        created = False
        if not appStoreApp:
          appStoreApp, created = AppStoreApp.objects.using(PCONN).get_or_create(bundle_id=item['bundleid'],
            defaults={'name': item['name'], 'track_id': item['appid'], 'platform': platform})

        rankRecord = AppStoreRank(app=appStoreApp, rank=item['rank'])
        requestRankRecords.append(rankRecord)

      rankRequest = AppStoreRankRequest(category_id=categoryId, rank_type_id=rankTypeId,
          platform_id=platformId, region_id=regionId, time=execTime,
          rank_server_id=_IOS5_TOP_LISTS_ID)
      rankRequest.save(using=PCONN)
      for record in requestRankRecords:
        record.rank_request = rankRequest

      if not hasnextpage:
        break;

      rankRecords.extend(requestRankRecords)

    AppStoreRank.objects.using(PCONN).bulk_create(rankRecords)

  except Exception as e:
    print e
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


if __name__ == '__main__':
  categories = AppStoreCategory.objects.using(PCONN).filter(~Q(parent_category__ios_genre_id=6021))
  fetch_deep_ranks(['US', 'DE'], AppStorePlatform.objects.using(PCONN).get(name='iOS'), categories, True)
