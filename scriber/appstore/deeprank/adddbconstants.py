import os, django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scriber_app.seetings.dev")
django.setup()


import localization.models
from collections import namedtuple
from appstore.models import AppStoreRegion, AppStoreRankType, AppStoreCategory
from django.conf import settings


# Set the storefront_ios5 column for all AppRegions
def setRegionStorefronts():
    Country = namedtuple('Country', ('name', 'storefront'))
    countries = {
        'DZ': Country('Algeria', '143563,9'),
        'AO': Country('Angola',  '143564,9'),
        'AI': Country('Anguilla', '143538,9'),
        'AG': Country('Antigua and Barbuda', '143540,9'),
        'AR': Country('Argentina', '143505-2,9'),
        'AM': Country('Armenia', '143524,9'),
        'AU': Country('Australia', '143460,9'),
        'AZ': Country('Azerbaijan', '143568,9'),
        'BS': Country('Bahamas', '143539,9'),
        'BH': Country('Bahrain', '143559,9'),
        'BB': Country('Barbados', '143541,9'),
        'BY': Country('Belarus', '143565,9'),
        'BE': Country('Belgium', '143446-2,9'),
        'BZ': Country('Belize', '143555-2,9'),
        'BM': Country('Bermuda', '143542,9'),
        'BO': Country('Bolivia', '143556,9'),
        'BW': Country('Botswana', '143525,9'),
        'BR': Country('Brazil', '143503,9'),
        'VG': Country('British Virgin Islands', '143543,9'),
        'BN': Country('Brunei Darussalam', '143560,9'),
        'CA': Country('Canada', '143455-6,9'),
        'KY': Country('Cayman Islands', '143544,9'),
        'CZ': Country('Czech Republic', '143489,9'),
        'CL': Country('Chile', '143483-2,9'),
        'CO': Country('Colombia', '143501-2,9'),
        'CR': Country('Costa Rica', '143495-2,9'),
        'CY': Country('Cyprus', '143557-2,9'),
        'DK': Country('Denmark', '143458-2,9'),
        'DE': Country('Germany', '143443,9'),
        'DM': Country('Dominica', '143545,9'),
        'EC': Country('Ecuador', '143509-2,9'),
        'EE': Country('Estonia', '143518,9'),
        'EG': Country('Egypt', '143516,9'),
        'SV': Country('El Salvador', '143506-2,9'),
        'ES': Country('Spain', '143454-8,9'),
        'FR': Country('France', '143442,9'),
        'GH': Country('Ghana', '143573,9'),
        'GR': Country('Greece', '143448,9'),
        'GD': Country('Grenada', '143546,9'),
        'GT': Country('Guatemala', '143504-2,9'),
        'GY': Country('Guyana', '143553,9'),
        'HN': Country('Honduras', '143510-2,9'),
        'HK': Country('Hong Kong', '143463,9'),
        'HR': Country('Croatia', '143494,9'),
        'IS': Country('Iceland', '143558,9'),
        'IN': Country('India', '143467,9'),
        'ID': Country('Indonesia', '143476,9'),
        'IE': Country('Ireland', '143449,9'),
        'IL': Country('Israel', '143491,9'),
        'IT': Country('Italy', '143450,9'),
        'JM': Country('Jamaica', '143511,9'),
        'JO': Country('Jordan', '143528,9'),
        'KZ': Country('Kazakhstan', '143517,9'),
        'KE': Country('Kenya', '143529,9'),
        'KW': Country('Kuwait', '143493,9'),
        'LV': Country('Latvia', '143519,9'),
        'LB': Country('Lebanon', '143497,9'),
        'LT': Country('Lithunaia', '143520,9'),
        'LU': Country('Luxembourg', '143451-2,9'),
        'MO': Country('Macao', '143515,9'),
        'MK': Country('Macedonia', '143530,9'),
        'MG': Country('Madagascar', '143531,9'),
        'HU': Country('Hungary', '143482,9'),
        'MY': Country('Malaysia', '143473,9'),
        'ML': Country('Mali', '143532,9'),
        'MT': Country('Malta', '143521,9'),
        'MU': Country('Mauritius', '143533,9'),
        'MX': Country('Mexico', '143468,9'),
        'MD': Country('Moldova', '143523,9'),
        'MS': Country('Montserrat', '143547,9'),
        'NL': Country('Netherlands', '143452,9'),
        'NZ': Country('New Zealand', '143461,9'),
        'NI': Country('Nicaragua', '143512-2,9'),
        'NE': Country('Niger', '143534,9'),
        'NG': Country('Nigeria', '143561,9'),
        'NO': Country('Norway', '143457-2,9'),
        'OM': Country('Oman', '143562,9'),
        'AT': Country('Austria', '143445,9'),
        'PK': Country('Pakistan', '143477,9'),
        'PA': Country('Panama', '143485-2,9'),
        'PY': Country('Paraguay', '143513-2,9'),
        'PE': Country('Peru', '143507-2,9'),
        'PH': Country('Philippines', '143474,9'),
        'PL': Country('Poland', '143478,9'),
        'PT': Country('Portugal', '143453,9'),
        'QA': Country('Qatar', '143498,9'),
        'DO': Country('Dominican Republic', '143508-2,9'),
        'RO': Country('Romania', '143487,9'),
        'SA': Country('Saudi Arabia', '143479,9'),
        'CH': Country('Switzerland', '143459-2,9'),
        'SN': Country('Senegal', '143535,9'),
        'SG': Country('Singapore', '143464,9'),
        'SK': Country('Slovakia', '143496,9'),
        'SI': Country('Slovenia', '143499,9'),
        'ZA': Country('South Africa', '143472,9'),
        'LK': Country('Sri Lanka', '143486,9'),
        'KN': Country('Saint Kitts and Nevis', '143548,9'),
        'LC': Country('Saint Lucia', '143549,9'),
        'VC': Country('Saint Vincent and the Grenadines', '143550,9'),
        'FI': Country('Finland', '143447-2,9'),
        'SR': Country('Suriname', '143554-2,9'),
        'SE': Country('Sweden', '143456,9'),
        'TZ': Country('Tanzania', '143572,9'),
        'TH': Country('Thailand', '143475,9'),
        'TT': Country('Trinidad and Tobago', '143551,9'),
        'TN': Country('Tunisia', '143536,9'),
        'TR': Country('Turkey', '143480,9'),
        'TC': Country('Turks and Caicos Islands', '143552,9'),
        'UG': Country('Uganda', '143537,9'),
        'GB': Country('United Kingdom', '143444,9'),
        'AE': Country('United Arab Emirates', '143481,9'),
        'US': Country('United States', '143441-1,9'),
        'UY': Country('Uruguay', '143514-2,9'),
        'UZ': Country('Uzbekistan', '143566,9'),
        'VE': Country('Venezuela', '143502-2,9'),
        'VN': Country('Viet Nam', '143471,9'),
        'YE': Country('Yemen', '143571,9'),
        'BG': Country('Bulgaria', '143526,9'),
        'RU': Country('Russian Federation', '143469,9'),
        'CN': Country('China', '143465-2,9'),
        'TW': Country('Taiwan', '143470,9'),
        'JP': Country('Japan', '143462-1,9'),
        'KR': Country('South Korea', '143466,9'),
    }

    for countrycode in countries:
        country = countries[countrycode]
        dbcountry = localization.models.Country.objects.filter(alpha2=countrycode)[0]
        appRegion = AppStoreRegion.objects.filter(country=dbcountry)[0]
        appRegion.storefront_ios5 = country.storefront
        appRegion.save()

 
# set the numberic popularity ids on AppStoreRankType 
def setPopularityTypeIds():
    popularityIdsDict = {
        'toppaidapplications':30,
        'topfreeapplications':27,
        'topgrossingapplications':38,
        'toppaidipadapplications':47,
        'topfreeipadapplications':44,
        'topgrossingipadapplications':46 
    }
       
    for name in popularityIdsDict:
        appRankType = AppStoreRankType.objects.filter(name=name)[0]
        appRankType.pop_id = popularityIdsDict[name];
        appRankType.save()


def addOverallGenreId():
    overallCategory = AppStoreCategory.objects.filter(name='Overall').get()
    overallCategory.ios_genre_id = 36
    overallCategory.save()

if __name__ == '__main__':
    setRegionStorefronts()
    setPopularityTypeIds()
    addOverallGenreId()
