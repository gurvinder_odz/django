from __future__ import absolute_import

import re
import requests

from appinfo import fetcher
from appstore.models import AppStoreApp, AppStoreArtist, AppStoreCategory, AppStorePlatform, AppStoreRank, AppStoreRankRequest, AppStoreRankType, AppStoreRegion
from celery import group, task
from celery.utils.log import get_task_logger
from core.cache import cache_results
from core.celeryutils import exponential_backoff
from core.db.dynamic import in_database
from core.utils import unpack
from datetime import datetime
from django.conf import settings
from django.db import transaction
from django.db.models import Q
from pytz import utc
from random import random
from appstore.deeprank import deeprank


PCONN = settings.PERSISTENT_DEFAULT_CONNECTION
APP_ID_CURSOR = None
ARTIST_APP_ID_CURSOR = None

_FETCH_ARTIST_BATCH_SIZE = 150
_FETCH_IOS_APP_BATCH_SIZE = 150
_IOS_LIMIT = 200
_IOS_API_URL_BASE = 'http://itunes.apple.com/%s/rss/%s/limit=%s/%s'
_IOS_OVERALL_CATEGORY_GENRE_ID = 36
_MAX_RETRIES = 5
_NEWSSTAND_IOS_GENRE_ID = 6021
_TOP_COUNTRIES_ALPHA2 = ['AU', 'BR', 'CA', 'CN', 'FR', 'DE', 'IT', 'JP', 'KR', 'LA', 'MO', 'NL',
                         'RU', 'ES', 'SE', 'SG', 'CH', 'GB', 'US', 'VN']

_RSS_SERVER_TOP_LISTS_ID = 1

logger = get_task_logger(__name__)


@cache_results()
def ios_platform(timeout=60 * 20):
  iosPlatform = AppStorePlatform.objects.using(PCONN).get(name=AppStorePlatform.IOS)
  return iosPlatform


@task(max_retries=5, ignore_result=True, bind=True)
def fetch_deep_ios_ranks(self, *args, **kwargs):
  try:
    iosPlatform = ios_platform()
    categories = AppStoreCategory.objects.using(PCONN).filter(
        ~Q(parent_category__ios_genre_id=_NEWSSTAND_IOS_GENRE_ID))
    deeprank.fetch_deep_ranks(_TOP_COUNTRIES_ALPHA2, iosPlatform, categories)
    # For now, treat all exceptions as transient errors and retry.
  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=5, ignore_result=True, bind=True)
def fetch_top_country_ranks(self, *args, **kwargs):
  try:
    (platform_id, ) = unpack(kwargs, 'platform_id')
    categories = AppStoreCategory.objects.using(PCONN).filter(
        ~Q(parent_category__ios_genre_id=_NEWSSTAND_IOS_GENRE_ID))
    rankTypes = AppStoreRankType.objects.using(PCONN).all()
    taskList = []
    for rankType in rankTypes:
      for region_str in _TOP_COUNTRIES_ALPHA2:
        for category in categories:
          taskList.append(fetch_app_ranks.s(category=category.ios_genre_id,
                                            platform_id=platform_id, rank_type_str=rankType.name,
                                            region_str=region_str))

    taskGroup = group(taskList)
    taskGroup.apply_async()

  # For now, treat all exceptions as transient errors and retry.
  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def fetch_app_ranks(self, *args, **kwargs):
  try:
    category, platform_id, rank_type_str, region_str = unpack(kwargs, 'category', 'platform_id',
                                                              'rank_type_str', 'region_str')
    iosPlatform = ios_platform()
    if platform_id == iosPlatform.id:
      url = ios_api_url(region_str, rank_type_str, category=category)
      response = requests.get(url)
      process_ios_ranks(response.json(), category, rank_type_str, region_str)
    # only implement iOS handling for now.
    return

  # For now, treat all exceptions as transient errors and retry.
  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


def ios_api_url(region_str, rank_type_str, limit=_IOS_LIMIT, category=None, content_type='json'):
  if not (region_str and rank_type_str):
    raise ValueError('Missing arguments for ios_api_url')
  genre = '' if category in [None, _IOS_OVERALL_CATEGORY_GENRE_ID] else 'genre=%s/' % category
  return _IOS_API_URL_BASE % (region_str.lower(), rank_type_str, limit, genre + content_type)


def process_ios_ranks(data, category, rank_type_str, region_str):
  appCategory = AppStoreCategory.objects.using(PCONN).get(name='Overall') if category is None else \
      AppStoreCategory.objects.using(PCONN).get(ios_genre_id=category)
  iosPlatform = ios_platform()
  appRankType = AppStoreRankType.objects.using(PCONN).get(name=rank_type_str, platform=iosPlatform)
  appRegion = AppStoreRegion.objects.using(PCONN).get(country__alpha2=region_str.upper())
  execTime = datetime.utcnow().replace(tzinfo=utc)
  ranks = data.get('feed', {}).get('entry', [])

  # Apple has a bad habit of never returning lists of length 1, and instead
  # only returning the lone element itself in these cases.
  if not isinstance(ranks, list):
    ranks = [ranks]

  appsDict = {}
  artistDict = {}
  for idx, entry in enumerate(ranks):
    name = entry.get('im:name', {}).get('label', None)
    attributes = entry.get('id', {}).get('attributes', {})
    trackIdStr = attributes.get('im:id', None)
    bundleId = attributes.get('im:bundleId', None)
    trackId = long(trackIdStr) if trackIdStr else None
    rank = idx + 1

    artistName = entry.get('im:artist', {}).get('label', None)
    artistUrl = entry.get('im:artist', {}).get('attributes', {}).get('href', '')
    m = re.match(r'^.*\/id([\d]+)(\?.*)?$', artistUrl)
    artistId = long(m.group(1)) if m is not None else None

    if bundleId:
      appsDict[bundleId] = {'app': None}
      appsDict[bundleId]['name'] = name
      appsDict[bundleId]['track_id'] = trackId
      appsDict[bundleId]['rank'] = rank
      appsDict[bundleId]['artist_id'] = artistId

    if artistId and artistId not in artistDict:
      artistDict[artistId] = {'artist': None}
      artistDict[artistId]['name'] = artistName

  # Fetch pre-existing apps and artists since get_or_create is expensive.
  existingApps = AppStoreApp.objects.using(PCONN).filter(bundle_id__in=appsDict.keys())
  for app in existingApps:
    appsDict[app.bundle_id]['app'] = app

  existingArtists = AppStoreArtist.objects.using(PCONN).filter(artist_id__in=artistDict.keys())
  for artist in existingArtists:
    artistDict[artist.artist_id]['artist'] = artist

  # get_or_create records that were not found in the initial scan, and create
  # the AppStoreRank objects to be persisted.
  rankRecords = []
  for bundleId in appsDict:
    artistId = appsDict[bundleId]['artist_id']
    artist = artistDict[artistId]['artist'] if artistId in artistDict else None
    artistName = artistDict[artistId]['name'] if artistId in artistDict else None
    artistCreated = False
    if artistId and not artist:
      artist, artistCreated = AppStoreArtist.objects.using(PCONN).get_or_create(
          artist_id=artistId, defaults={
              'name': artistName, 'platform': iosPlatform})
      artistDict[artistId]['artist'] = artist

    app = appsDict[bundleId]['app']
    appName = appsDict[bundleId]['name']
    trackId = appsDict[bundleId]['track_id']
    rank = appsDict[bundleId]['rank']
    appCreated = False
    if not app:
      app, appCreated = AppStoreApp.objects.using(PCONN).get_or_create(
          bundle_id=bundleId, defaults={
              'name': appName, 'track_id': trackId, 'platform': iosPlatform, 'artist': artist})
      appsDict[bundleId]['app'] = app

    # Update app.name and app.artist fields if necessary.
    if not appCreated:
      if appName and not app.name:
        app.name = appName
        if artist and not app.artist:
          app.artist = artist
        app.save(using=PCONN)
      elif artist and not app.artist:
        app.artist = artist
        app.save(using=PCONN)

    rankRecord = AppStoreRank(app=app, rank=rank)
    rankRecords.append(rankRecord)

  # Persist the rankings data.
  rankRequest = AppStoreRankRequest(category=appCategory, rank_type=appRankType,
                                    platform=iosPlatform, region=appRegion, time=execTime)
  rankRequest.save(using=PCONN)
  for record in rankRecords:
    record.rank_request = rankRequest
  AppStoreRank.objects.using(PCONN).bulk_create(rankRecords)


@task(max_retries=5, ignore_result=True, bind=True)
def fetch_missing_ios_artists(self, *args, **kwargs):
  try:
    with in_database(PCONN, write=True):
      global ARTIST_APP_ID_CURSOR
      if ARTIST_APP_ID_CURSOR is None:
        ARTIST_APP_ID_CURSOR = int(random() * AppStoreApp.objects.all().count())

      apps = []
      while len(apps) < _FETCH_ARTIST_BATCH_SIZE:
        appStoreApps = [
            app for app in AppStoreApp.objects.filter(
                artist=None,
                id__gt=ARTIST_APP_ID_CURSOR).order_by('id')[
                :_FETCH_ARTIST_BATCH_SIZE -
                len(apps)]]
        if appStoreApps:
          ARTIST_APP_ID_CURSOR = appStoreApps[-1].id
          apps.extend(appStoreApps)
        elif ARTIST_APP_ID_CURSOR == 0:
          break
        else:
          ARTIST_APP_ID_CURSOR = 0

      if not apps:
        logger.info('No missing iOS artists found')
        return

      appStoreFetcher = fetcher.AppStoreFetcher()
      appStoreFetcher.updateAppStoreApps(appStoreApps)

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=5, ignore_result=True, bind=True)
def update_ios_apps(self, *args, **kwargs):
  try:
    with in_database(PCONN, write=True):
      global APP_ID_CURSOR
      if APP_ID_CURSOR is None:
        APP_ID_CURSOR = int(random() * AppStoreApp.objects.all().count())

      apps = [
          app for app in AppStoreApp.objects.filter(
              id__gt=APP_ID_CURSOR).order_by('id')[:_FETCH_IOS_APP_BATCH_SIZE]]
      APP_ID_CURSOR = apps[-1].id

      appStoreFetcher = fetcher.AppStoreFetcher()
      appStoreFetcher.updateAppStoreApps(apps)

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))
