# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0011_auto_20150429_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appstoreplatform',
            name='display_name',
            field=models.CharField(default='Invalid Platform Display Name', max_length=256, choices=[(b'App Store', b'App Store'), (b'Google Play', b'Google Play')]),
            preserve_default=False,
        ),
    ]
