# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0019_appstorerankrequest_rank_server_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appstorerankrequest',
            name='rank_source',
        ),
    ]
