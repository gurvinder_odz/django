# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0010_auto_20150424_2127'),
    ]

    operations = [
        migrations.AddField(
            model_name='appstoreplatform',
            name='display_name',
            field=models.CharField(max_length=256, null=True, choices=[(b'App Store', b'App Store'), (b'Google Play', b'Google Play')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='appstoreplatform',
            name='name',
            field=models.CharField(max_length=256, choices=[(b'iOS', b'iOS'), (b'Android', b'Android')]),
            preserve_default=True,
        ),
        migrations.RunSQL(
            sql="""
                UPDATE app_store_platforms
                SET display_name = 'App Store'
                WHERE name = 'iOS';

                UPDATE app_store_platforms
                SET display_name = 'Google Play'
                WHERE name = 'Android';
                """,
        ),
    ]
