# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0021_auto_20150527_0255'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appstorerank',
            name='creation_time',
        ),
        migrations.RemoveField(
            model_name='appstorerank',
            name='last_modified',
        ),
    ]
