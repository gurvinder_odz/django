# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

ALPHA2 = """('AE', 'AG', 'AI', 'AL', 'AM', 'AO', 'AR', 'AT', 'AU', 'AZ', 'BB', 'BE', 'BF', 'BG',
  'BH', 'BJ', 'BM', 'BN', 'BO', 'BR', 'BS', 'BT', 'BW', 'BY', 'BZ', 'CA', 'CG', 'CH', 'CL', 'CN',
  'CO', 'CR', 'CV', 'CY', 'CZ', 'DE', 'DK', 'DM', 'DO', 'DZ', 'EC', 'EE', 'EG', 'ES', 'FI', 'FJ',
  'FM', 'FR', 'GB', 'GD', 'GH', 'GM', 'GR', 'GT', 'GW', 'GY', 'HK', 'HN', 'HR', 'HU', 'ID', 'IE',
  'IL', 'IN', 'IS', 'IT', 'JM', 'JO', 'JP', 'KE', 'KG', 'KH', 'KN', 'KR', 'KW', 'KY', 'KZ', 'LA',
  'LB', 'LC', 'LK', 'LR', 'LT', 'LU', 'LV', 'MD', 'MG', 'MK', 'ML', 'MN', 'MO', 'MR', 'MS', 'MT',
  'MU', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NE', 'NG', 'NI', 'NL', 'NO', 'NP', 'NZ', 'OM', 'PA', 'PE',
  'PG', 'PH', 'PK', 'PL', 'PT', 'PW', 'PY', 'QA', 'RO', 'RU', 'SA', 'SB', 'SC', 'SE', 'SG', 'SI',
  'SK', 'SL', 'SN', 'SR', 'ST', 'SV', 'SZ', 'TC', 'TD', 'TH', 'TJ', 'TM', 'TN', 'TR', 'TT', 'TW',
  'TZ', 'UA', 'UG', 'US', 'UY', 'UZ', 'VC', 'VE', 'VG', 'VN', 'YE', 'ZA', 'ZW')"""

sqlQuery = """
  INSERT INTO app_store_regions
    (creation_time, last_modified, country, has_ios_rankings)
  SELECT
    now(),
    now(),
    id,
    true
  FROM
    countries
  WHERE
    alpha2 in %s
  ;
  """

class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppStoreRegion',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('has_ios_rankings', models.BooleanField(default=False)),
                ('country', models.ForeignKey(to='localization.Country', db_column=b'country')),
            ],
            options={
                'db_table': 'app_store_regions',
            },
            bases=(models.Model,),
        ),
        migrations.RunSQL(
            sql=sqlQuery % str(ALPHA2),
        ),
    ]
