# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppStoreApp',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('bundle_id', models.CharField(max_length=256, null=True, db_index=True)),
                ('track_id', models.BigIntegerField(null=True, db_index=True)),
                ('sku', models.CharField(max_length=256, null=True)),
            ],
            options={
                'db_table': 'app_store_apps',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AppStoreCategory',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('ios_genre_id', models.IntegerField(null=True)),
                ('parent_category', models.ForeignKey(db_column=b'parent_category', to='appstore.AppStoreCategory', null=True)),
            ],
            options={
                'db_table': 'app_store_categories',
                'verbose_name': 'app_store_category',
                'verbose_name_plural': 'app_store_categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AppStorePlatform',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=256)),
            ],
            options={
                'db_table': 'app_store_platforms',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AppStoreRank',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('rank', models.IntegerField(null=True)),
                ('time', models.DateTimeField(db_index=True)),
                ('app', models.ForeignKey(to='appstore.AppStoreApp', db_column=b'app')),
                ('category', models.ForeignKey(db_column=b'category', to='appstore.AppStoreCategory', null=True)),
                ('platform', models.ForeignKey(to='appstore.AppStorePlatform', db_column=b'platform')),
            ],
            options={
                'db_table': 'app_store_ranks',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AppStoreRankType',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('display_name', models.CharField(max_length=256)),
                ('name', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=256)),
                ('platform', models.ForeignKey(to='appstore.AppStorePlatform', db_column=b'platform')),
            ],
            options={
                'db_table': 'app_store_rank_types',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='appstorerank',
            name='rank_type',
            field=models.ForeignKey(to='appstore.AppStoreRankType', db_column=b'rank_type'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='appstorerank',
            name='region',
            field=models.ForeignKey(db_column=b'region', to='appstore.AppStoreRegion', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='appstorecategory',
            name='platform',
            field=models.ForeignKey(to='appstore.AppStorePlatform', db_column=b'platform'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='appstorecategory',
            unique_together=set([('ios_genre_id', 'platform')]),
        ),
        migrations.AddField(
            model_name='appstoreapp',
            name='platform',
            field=models.ForeignKey(to='appstore.AppStorePlatform', db_column=b'platform'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='appstoreapp',
            unique_together=set([('track_id', 'platform')]),
        ),
        migrations.RunSQL(
            sql="""
                INSERT INTO app_store_platforms
                    (creation_time, last_modified, name, description)
                VALUES
                    (now(), now(), 'iOS', 'Apple''s iOS platform'),
                    (now(), now(), 'Android', 'The Android platform')
                ;
            """,
        ),
    ]
