# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0004_auto_20150407_2132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appstorecategory',
            name='ios_genre_id',
            field=models.IntegerField(null=True, db_index=True),
            preserve_default=True,
        ),
    ]
