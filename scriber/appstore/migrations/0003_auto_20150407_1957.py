# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

IOS_CATEGORY_MAP = {
  'Business': 6000,
  'Weather': 6001,
  'Utilities': 6002,
  'Travel': 6003,
  'Sports': [6004, 7016],
  'Social Networking': 6005,
  'Reference': 6006,
  'Productivity': 6007,
  'Photo & Video': 6008,
  'News': 6009,
  'Navigation': 6010,
  'Music': [6011, 7011],
  'Lifestyle': 6012,
  'Health & Fitness': 6013,
  'Games': 6014,
  'Action': 7001,
  'Adventure': 7002,
  'Arcade': 7003,
  'Board': 7004,
  'Card': 7005,
  'Casino': 7006,
  'Dice': 7007,
  'Educational': 7008,
  'Family': 7009,
  'Kids': 7010,
  'Puzzle': 7012,
  'Racing': 7013,
  'Role Playing': 7014,
  'Simulation': 7015,
  'Strategy': 7017,
  'Trivia': 7018,
  'Word': 7019,
  'Finance': 6015,
  'Entertainment': [6016, 13015],
  'Education': 6017,
  'Books': 6018,
  'Medical': 6020,
  'Newsstand': 6021,
  'News & Politics': 13001,
  'Fashion & Style': 13002,
  'Home & Garden': 13003,
  'Outdoors & Nature': 13004,
  'Sports & Leisure': 13005,
  'Automotive': 13006,
  'Arts & Photography': 13007,
  'Brides & Weddings': 13008,
  'Business & Investing': 13009,
  'Children\'\'s Magazines': 13010,
  'Computers & Internet': 13011,
  'Cooking': 13012,
  'Crafts & Hobbies': 13013,
  'Electronics & Audio': 13014,
  'Health': 13017,
  'History': 13018,
  'Literary Magazines & Journals': 13019,
  'Men\'\'s Interest': 13020,
  'Movies & Music': 13021,
  'Parenting & Family': 13023,
  'Pets': 13024,
  'Professional & Trade': 13025,
  'Regional News': 13026,
  'Science': 13027,
  'Teens': 13028,
  'Travel & Regional': 13029,
  'Women\'\'s Interest': 13030,
  'Catalogs': 6022,
  'Food & Drink': 6023,
}

platformSubquery = "SELECT id FROM app_store_platforms WHERE name = 'iOS'"

categoryQuery = """
  INSERT INTO app_store_categories
    (creation_time, last_modified, name, ios_genre_id, parent_category, platform)
  VALUES %s
  ;
  """
categoryQueryComponents = []
for category in IOS_CATEGORY_MAP:
  if hasattr(IOS_CATEGORY_MAP[category], '__len__'):
    for code in IOS_CATEGORY_MAP[category]:
      categoryQueryComponents.append("(now(), now(), '%s', %s, NULL, (%s))" %
          (category, code, platformSubquery))
  else:
    categoryQueryComponents.append("(now(), now(), '%s', %s, NULL, (%s))" %
        (category, IOS_CATEGORY_MAP[category], platformSubquery))

class Migration(migrations.Migration):

  dependencies = [
    ('appstore', '0002_auto_20150407_1945'),
  ]

  operations = [
    migrations.RunSQL(
      sql=categoryQuery % ', '.join(categoryQueryComponents),
    ),
    migrations.RunSQL(
      sql="""
        INSERT INTO app_store_rank_types
          (creation_time, last_modified, display_name, name, description, platform)
        VALUES
          (now(), now(), 'Top Free', 'topfreeapplications', '', (%s)),
          (now(), now(), 'Top Free iPad', 'topfreeipadapplications', '', (%s)),
          (now(), now(), 'Top Grossing', 'topgrossingapplications', '', (%s)),
          (now(), now(), 'Top Grossing iPad', 'topgrossingipadapplications', '', (%s)),
          (now(), now(), 'Top Paid', 'toppaidapplications', '', (%s)),
          (now(), now(), 'Top Paid iPad', 'toppaidipadapplications', '', (%s))
        ;
      """ % (platformSubquery, platformSubquery, platformSubquery,
             platformSubquery, platformSubquery, platformSubquery)
    ),
  ]
