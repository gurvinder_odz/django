# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0016_auto_20150504_2127'),
    ]

    operations = [
        migrations.AddField(
            model_name='appstorerankrequest',
            name='rank_source',
            field=models.CharField(max_length=512, null=True, db_index=True),
            preserve_default=True,
        ),
    ]
