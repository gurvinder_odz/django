# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0012_auto_20150429_1647'),
    ]

    operations = [
         migrations.RunSQL(
            sql="""
                UPDATE
                  app_store_rank_requests
                SET category = subquery.id
                FROM (
                  SELECT
                    c.id,
                    c.platform
                  FROM app_store_categories c
                  JOIN app_store_platforms p
                  ON (c.platform = p.id)
                  WHERE c.name = 'Overall'
                ) subquery
                WHERE
                  category IS NULL AND
                  app_store_rank_requests.platform = subquery.platform
                ;
                """,
        ),
    ]
