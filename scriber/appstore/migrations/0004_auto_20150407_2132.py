# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

class Migration(migrations.Migration):

  dependencies = [
    ('appstore', '0003_auto_20150407_1957'),
  ]

  operations = [
    migrations.RunSQL(
      sql="""
        UPDATE app_store_categories
        SET parent_category = subquery.id
        FROM (
          SELECT
            id
          FROM
            app_store_categories
          WHERE
            ios_genre_id = 6014
        ) subquery
        WHERE ios_genre_id >= 7000 AND ios_genre_id < 8000
        ;
      """
    ),
    migrations.RunSQL(
      sql="""
        UPDATE app_store_categories
        SET parent_category = subquery.id
        FROM (
          SELECT
            id
          FROM
            app_store_categories
          WHERE
            ios_genre_id = 6021
        ) subquery
        WHERE ios_genre_id >= 13000 AND ios_genre_id < 14000
        ;
      """
    ),
  ]
