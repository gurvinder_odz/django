# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0020_remove_appstorerankrequest_rank_source'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appstorerank',
            name='creation_time',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='appstorerank',
            name='last_modified',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
