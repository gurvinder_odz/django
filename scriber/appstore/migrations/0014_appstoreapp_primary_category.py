# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0013_auto_20150430_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='appstoreapp',
            name='primary_category',
            field=models.ForeignKey(db_column=b'primary_category', to='appstore.AppStoreCategory', null=True),
            preserve_default=True,
        ),
    ]
