# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0007_auto_20150408_2116'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appstorerank',
            name='category',
        ),
        migrations.RemoveField(
            model_name='appstorerank',
            name='platform',
        ),
        migrations.RemoveField(
            model_name='appstorerank',
            name='rank_type',
        ),
        migrations.RemoveField(
            model_name='appstorerank',
            name='region',
        ),
        migrations.RemoveField(
            model_name='appstorerank',
            name='time',
        ),
    ]
