# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0005_auto_20150407_2143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appstoreranktype',
            name='display_name',
            field=models.CharField(unique=True, max_length=256),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='appstoreranktype',
            name='name',
            field=models.CharField(unique=True, max_length=256),
            preserve_default=True,
        ),
    ]
