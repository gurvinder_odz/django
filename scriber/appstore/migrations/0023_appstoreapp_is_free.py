# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0022_auto_20150527_0344'),
    ]

    operations = [
        migrations.AddField(
            model_name='appstoreapp',
            name='is_free',
            field=models.NullBooleanField(),
            preserve_default=True,
        ),
    ]
