# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

  dependencies = [
      ('appstore', '0009_auto_20150408_2200'),
  ]

  operations = [
    migrations.RunSQL(
      sql="""
        INSERT INTO app_store_categories
          (creation_time, last_modified, name, ios_genre_id, parent_category, platform)
        VALUES
          (now(), now(), 'Overall', NULL, NULL,
            (SELECT id FROM app_store_platforms WHERE name = 'iOS'))
        ;
      """
    ),
  ]