# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0017_appstorerankrequest_rank_source'),
    ]

    operations = [
        migrations.AddField(
            model_name='appstoreranktype',
            name='pop_id',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='appstoreregion',
            name='storefront_ios5',
            field=models.CharField(max_length=20, null=True),
            preserve_default=True,
        ),
    ]
