# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0006_auto_20150408_1810'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppStoreRankRequest',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('time', models.DateTimeField(db_index=True)),
                ('category', models.ForeignKey(db_column=b'category', to='appstore.AppStoreCategory', null=True)),
                ('platform', models.ForeignKey(to='appstore.AppStorePlatform', db_column=b'platform')),
                ('rank_type', models.ForeignKey(to='appstore.AppStoreRankType', db_column=b'rank_type')),
                ('region', models.ForeignKey(db_column=b'region', to='appstore.AppStoreRegion', null=True)),
            ],
            options={
                'db_table': 'app_store_rank_requests',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='appstorerank',
            name='rank_request',
            field=models.ForeignKey(db_column=b'rank_request', to='appstore.AppStoreRankRequest', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='appstorerank',
            name='platform',
            field=models.ForeignKey(db_column=b'platform', to='appstore.AppStorePlatform', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='appstorerank',
            name='rank_type',
            field=models.ForeignKey(db_column=b'rank_type', to='appstore.AppStoreRankType', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='appstorerank',
            name='time',
            field=models.DateTimeField(null=True, db_index=True),
            preserve_default=True,
        ),
        migrations.RunSQL(
            sql="""
                DELETE FROM app_store_ranks
                WHERE rank_request IS NULL
                ;
                """,
        ),
    ]
