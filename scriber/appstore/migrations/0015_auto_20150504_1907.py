# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0014_appstoreapp_primary_category'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppStoreArtist',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('artist_id', models.BigIntegerField(null=True, db_index=True)),
                ('name', models.CharField(max_length=256)),
                ('platform', models.ForeignKey(to='appstore.AppStorePlatform', db_column=b'platform')),
            ],
            options={
                'db_table': 'app_store_artists',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='appstoreapp',
            name='artist',
            field=models.ForeignKey(db_column=b'artist', to='appstore.AppStoreArtist', null=True),
            preserve_default=True,
        ),
    ]
