# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0008_auto_20150408_2141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appstorerank',
            name='rank_request',
            field=models.ForeignKey(db_column=b'rank_request', default=0, to='appstore.AppStoreRankRequest'),
            preserve_default=False,
        ),
    ]
