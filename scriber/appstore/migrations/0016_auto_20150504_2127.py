# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0015_auto_20150504_1907'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='appstoreartist',
            unique_together=set([('artist_id', 'platform')]),
        ),
    ]
