import logging

from appstore.models import AppStoreApp, AppStoreCategory, AppStorePlatform
from django.db.models import Q

logger = logging.getLogger(__name__)

def update_primary_category_ios(bundle_id, track_id, ios_genre_id, name):
  iosPlatform = AppStorePlatform.objects.get(name=AppStorePlatform.IOS)
  category = AppStoreCategory.objects.get(ios_genre_id=ios_genre_id, platform=iosPlatform)

  app = AppStoreApp.objects.filter(Q(bundle_id=bundle_id) | Q(track_id=track_id),
      platform=iosPlatform).first()
  if app is None:
    app = AppStoreApp(name=name, bundle_id=bundle_id, track_id=track_id, platform=iosPlatform,
        primary_category=category)
    app.save()
    return
  elif (category != app.primary_category or name):
    app.primary_category = category
    app.name = name
    app.save()

  # Log partial matches, i.e. AppStoreApp records matching exactly
  # one of bundle_id and track_id.
  if app.bundle_id != bundle_id or app.track_id != track_id:
    logger.warning('Encountered partial Appnfo to AppStoreApp match. App bundle_id: %s, ' +
        'Info bundle_id: %s, App track_id: %s, Info track_id: %s' %
        (app.bundle_id, bundle_id, app.track_id, track_id))
