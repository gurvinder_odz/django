from core.models import TimestampedModel
from django.db import models
from localization.redshift_models import Country


class AppStorePlatform(TimestampedModel):
  id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=1024)
  description = models.CharField(max_length=1024)

  class Meta:
    db_table = 'app_store_platforms'


class AppStoreApp(TimestampedModel):
  id = models.BigIntegerField(primary_key=True)
  name = models.CharField(max_length=1024)
  bundle_id = models.CharField(max_length=1024, null=True)
  track_id = models.BigIntegerField(null=True)
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')
  sku = models.CharField(max_length=1024, null=True)

  class Meta:
    db_table = 'app_store_apps'
    unique_together = ('bundle_id', 'platform')
    unique_together = ('track_id', 'platform')


class AppStoreCategory(TimestampedModel):
  id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=1024)
  ios_genre_id = models.IntegerField(null=True)
  parent_category = models.ForeignKey('self', null=True, db_column='parent_category')
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')

  class Meta:
    db_table = 'app_store_categories'
    unique_together = ('ios_genre_id', 'platform')
    verbose_name = 'app_store_category'
    verbose_name_plural = 'app_store_categories'


class AppStoreRankType(TimestampedModel):
  id = models.IntegerField(primary_key=True)
  display_name = models.CharField(max_length=1024, unique=True)
  name = models.CharField(max_length=1024, unique=True)
  description = models.CharField(max_length=1024)
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')

  class Meta:
    db_table = 'app_store_rank_types'


# TODO(d-felix): Handle country collisions.
class AppStoreRegion(TimestampedModel):
  id = models.IntegerField(primary_key=True)
  country = models.ForeignKey(Country, db_column='country')
  has_ios_rankings = models.BooleanField(default=False)

  class Meta:
    db_table = 'app_store_regions'


class AppStoreRankRequest(TimestampedModel):
  id = models.BigIntegerField(primary_key=True)
  category = models.ForeignKey(AppStoreCategory, null=True, db_column='category')
  rank_type = models.ForeignKey(AppStoreRankType, db_column='rank_type')
  platform = models.ForeignKey(AppStorePlatform, db_column='platform')
  region = models.ForeignKey(AppStoreRegion, null=True, db_column='region')
  time = models.DateTimeField()

  class Meta:
    db_table = 'app_store_rank_requests'


class AppStoreRank(TimestampedModel):
  id = models.BigIntegerField(primary_key=True)
  app = models.ForeignKey(AppStoreApp, db_column='app')
  rank = models.IntegerField(null=True)
  rank_request = models.ForeignKey(AppStoreRankRequest, db_column='rank_request')

  class Meta:
    db_table = 'app_store_ranks'
