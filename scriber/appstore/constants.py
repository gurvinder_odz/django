from appstore.models import AppStoreCategory, AppStorePlatform


IOS_NEWSSTAND_GENRE_ID = 6021
IOS_OVERALL_CATEGORY_ID = AppStoreCategory.objects.get(
    name='Overall',
    platform__name=AppStorePlatform.IOS).id
IOS_PLATFORM_ID = AppStorePlatform.objects.get(name=AppStorePlatform.IOS).id

MAX_IOS_RANK = 1500
