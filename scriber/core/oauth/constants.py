GOOGLE_OAUTH2_PROVIDER_AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/auth'
GOOGLE_OAUTH2_PROVIDER_TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
GOOGLE_OAUTH2_DEFAULT_FILE_PASSWORD = 'notasecret'

OOB_REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

GSUTIL_CLIENT_ID = '909320924072.apps.googleusercontent.com'
GSUTIL_CLIENT_NOTSOSECRET = 'p3RlpR10xMFh9ZXBS/ZNLYUu'
