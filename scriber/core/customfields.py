import binascii
import string
import warnings

from Crypto import Random
from Crypto.Random import random
from django import forms
from django.conf import settings
from django.core import exceptions
from django.db import models
from django.db.models import fields
from django.db.models.fields.related import OneToOneField
from django.utils.encoding import smart_str, force_unicode
from django.utils.translation import ugettext as _

"""
Adapted from module mydjangolib.bigint_patch

Create and link to auto-incrementing primary keys of type bigint without
having to reload the model instance after saving it to get the ID set in
the instance.

Logs:
- v1.0: Created by Florian
- v1.1: Updated by Thomas
  * Fixed missing param `connection`
  * Used endswith for engine type check 
    (for better compatibility with `dj_database_url` and heroku)  
  * Added support for sqlite3 (which uses BIGINT by default)
  * Added south's add_introspection_rules if south is defined
  * Added BigOneToOneField and a short description
  * Assumed file location: common/fields.py
- v1.2: Updated by d-felix
  * Updated ENGINE fetching and related control flow
  * Removed south support as this functionality has been folded into migrations starting with
    Django 1.7
"""

__version__ = "1.2"
__author__ = "Florian Leitner"
__author__ = "Thomas Yip @ BeeDesk"


class BigIntegerField(fields.IntegerField):
    def db_type(self, connection):
        if 'mysql' in settings.DATABASES['default']['ENGINE']:
            return "bigint"
        elif 'oracle' in settings.DATABASES['default']['ENGINE']:
            return "NUMBER(19)"
        elif 'postgres' in settings.DATABASES['default']['ENGINE']:
            return "bigint"
        elif 'sqlite3' in settings.DATABASES['default']['ENGINE']:
            return super(BigIntegerField, self).db_type(connection)
        else:
            raise NotImplemented
    
    def get_internal_type(self):
        return "BigIntegerField"
    
    def to_python(self, value):
        if value is None:
            return value
        try:
            return long(value)
        except (TypeError, ValueError):
            raise exceptions.ValidationError(
                _("This value must be a long integer."))


class BigAutoField(fields.AutoField):
    def db_type(self, connection):
        if 'mysql' in settings.DATABASES['default']['ENGINE']:
            return "bigint AUTO_INCREMENT"
        elif 'oracle' in settings.DATABASES['default']['ENGINE']:
            return "NUMBER(19)"
        elif 'postgres' in settings.DATABASES['default']['ENGINE']:
            return "bigserial"
        elif 'sqlite3' in settings.DATABASES['default']['ENGINE']:
            return super(BigAutoField, self).db_type(connection)
        else:
            raise NotImplemented
 
    def get_internal_type(self):
        return "BigAutoField"
    
    def to_python(self, value):
        if value is None:
            return value
        try:
            return long(value)
        except (TypeError, ValueError):
            raise exceptions.ValidationError(
                _("This value must be a long integer."))
 
class BigForeignKey(fields.related.ForeignKey):
    
    def db_type(self, connection):
        rel_field = self.rel.get_related_field()
        # next lines are the "bad tooth" in the original code:
        if (isinstance(rel_field, BigAutoField) or
                (not connection.features.related_fields_match_type and
                isinstance(rel_field, BigIntegerField))):
            # because it continues here in the django code:
            # return IntegerField().db_type()
            # thereby fixing any AutoField as IntegerField
            return BigIntegerField().db_type(connection)
        return rel_field.db_type(connection)


class BigOneToOneField(BigForeignKey, OneToOneField):
    """
    If you use subclass model, you might need to name 
    the `ptr` field explicitly. This is the field type you 
    might want to use. Here is an example: 
    
    class Base(models.Model):
        title = models.CharField(max_length=40, verbose_name='Title')

    class Concrete(Base):
        base_ptr = fields.BigOneToOneField(Base)
        ext = models.CharField(max_length=12, null=True, verbose_name='Ext')
    """
    pass


"""
Copyright (c) 2008, Alexander Artemenko
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY Alexander Artemenko ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

class BaseEncryptedField(models.Field):
    '''This code is based on the djangosnippet #1095
       You can find the original at http://www.djangosnippets.org/snippets/1095/'''

    def __init__(self, *args, **kwargs):
        self.cipher_type = kwargs.pop('cipher', 'AES')
        self.block_type = kwargs.pop('block_type', None)
        self.secret_key = kwargs.pop('secret_key', settings.ENCRYPTED_DB_FIELD_SECRET_KEY)
        self.secret_key = self.secret_key[:32]

        if self.block_type is None:
            warnings.warn(
                "Default usage of pycrypto's AES block type defaults has been "
                "deprecated and will be removed in 0.3.0 (default will become "
                "MODE_CBC). Please specify a secure block_type, such as CBC.",
                DeprecationWarning,
            )
        try:
            imp = __import__('Crypto.Cipher', globals(), locals(), [self.cipher_type], -1)
        except:
            imp = __import__('Crypto.Cipher', globals(), locals(), [self.cipher_type])
        self.cipher_object = getattr(imp, self.cipher_type)
        if self.block_type:
            self.prefix = '$%s$%s$' % (self.cipher_type, self.block_type)
            self.iv = Random.new().read(self.cipher_object.block_size)
            self.cipher = self.cipher_object.new(
                self.secret_key,
                getattr(self.cipher_object, self.block_type),
                self.iv)
        else:
            self.cipher = self.cipher_object.new(self.secret_key)
            self.prefix = '$%s$' % self.cipher_type

        self.original_max_length = max_length = kwargs.get('max_length', 40)
        self.unencrypted_length = max_length
        # always add at least 2 to the max_length:
        #     one for the null byte, one for padding
        max_length += 2
        mod = max_length % self.cipher.block_size
        if mod > 0:
            max_length += self.cipher.block_size - mod
        kwargs['max_length'] = max_length * 2 + len(self.prefix)

        super(BaseEncryptedField, self).__init__(*args, **kwargs)

    def _is_encrypted(self, value):
        return isinstance(value, basestring) and value.startswith(self.prefix)

    def _get_padding(self, value):
        # We always want at least 2 chars of padding (including zero byte),
        # so we could have up to block_size + 1 chars.
        mod = (len(value) + 2) % self.cipher.block_size
        return self.cipher.block_size - mod + 2

    def to_python(self, value):
        if self._is_encrypted(value):
            if self.block_type:
                self.iv = binascii.a2b_hex(value[len(self.prefix):])[:len(self.iv)]
                self.cipher = self.cipher_object.new(
                    self.secret_key,
                    getattr(self.cipher_object, self.block_type),
                    self.iv)
                decrypt_value = binascii.a2b_hex(value[len(self.prefix):])[len(self.iv):]
            else:
                decrypt_value = binascii.a2b_hex(value[len(self.prefix):])
            return force_unicode(
                self.cipher.decrypt(decrypt_value).split('\0')[0]
            )
        return value

    def get_db_prep_value(self, value, connection=None, prepared=False):
        if value is None:
            return None

        value = smart_str(value)

        if not self._is_encrypted(value):
            padding = self._get_padding(value)
            if padding > 0:
                value += "\0" + ''.join([
                    random.choice(string.printable)
                    for index in range(padding-1)
                ])
            if self.block_type:
                self.cipher = self.cipher_object.new(
                    self.secret_key,
                    getattr(self.cipher_object, self.block_type),
                    self.iv)
                value = self.prefix + binascii.b2a_hex(self.iv + self.cipher.encrypt(value))
            else:
                value = self.prefix + binascii.b2a_hex(self.cipher.encrypt(value))
        return value

    def deconstruct(self):
        original = super(BaseEncryptedField, self).deconstruct()
        kwargs = original[-1]
        if self.cipher_type != 'AES':
            kwargs['cipher'] = self.cipher_type
        if self.block_type is not None:
            kwargs['block_type'] = self.block_type
        if self.original_max_length != 40:
            kwargs['max_length'] = self.original_max_length
        return original[:-1] + (kwargs,)


class EncryptedTextField(BaseEncryptedField):
    __metaclass__ = models.SubfieldBase

    def get_internal_type(self):
        return 'TextField'

    def formfield(self, **kwargs):
        defaults = {'widget': forms.Textarea}
        defaults.update(kwargs)
        return super(EncryptedTextField, self).formfield(**defaults)


class EncryptedCharField(BaseEncryptedField):
    __metaclass__ = models.SubfieldBase

    def get_internal_type(self):
        return "CharField"

    def formfield(self, **kwargs):
        defaults = {'max_length': self.max_length}
        defaults.update(kwargs)
        return super(EncryptedCharField, self).formfield(**defaults)

    def get_db_prep_value(self, value, connection=None, prepared=False):
        if value is not None and not self._is_encrypted(value):
            if len(value) > self.unencrypted_length:
                raise ValueError(
                    "Field value longer than max allowed: " +
                    str(len(value)) + " > " + str(self.unencrypted_length)
                )
        return super(EncryptedCharField, self).get_db_prep_value(
            value,
            connection=connection,
            prepared=prepared,
        )
