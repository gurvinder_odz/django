#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    # Set the DJANGO_SETTINGS_MODULE environment variable to the production module unless it has
    # already been set.
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scriber_app.settings.prod")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
