from django.db import models
from core.customfields import BigAutoField
from core.models import TimestampedModel


class AppInfo(models.Model):
  app_info_id = BigAutoField(primary_key=True)
  app = models.CharField(max_length=256, db_index=True)
  platform = models.CharField(max_length=256)
  fetch_time = models.DateTimeField(auto_now_add=True, db_index=True)

  category = models.CharField(max_length=256)
  ios_genre_id = models.IntegerField(null=True)
  developer = models.CharField(max_length=256)
  file_size_bytes = models.BigIntegerField(null=True)
  icon_url = models.CharField(max_length=4096)
  identifier = models.CharField(max_length=256, null=True)
  name = models.CharField(max_length=256)
  release_date = models.DateTimeField(null=True)
  version = models.CharField(max_length=256, null=True)

  # For now, avoid using a foreign key to the currencies table.
  currency = models.CharField(max_length=32)
  amt_local = models.DecimalField(max_digits=30, decimal_places=10)
  amt_local_micros = models.BigIntegerField()
  amt_usd_micros = models.BigIntegerField()

  class Meta:
    db_table = 'app_info'
