# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appinfo', '0002_appinfo_identifier'),
    ]

    operations = [
        migrations.AddField(
            model_name='appinfo',
            name='ios_genre_id',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
