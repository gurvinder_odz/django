# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AppInfo',
            fields=[
                ('app_info_id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('app', models.CharField(max_length=256, db_index=True)),
                ('platform', models.CharField(max_length=256)),
                ('fetch_time', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('category', models.CharField(max_length=256)),
                ('developer', models.CharField(max_length=256)),
                ('file_size_bytes', models.BigIntegerField(null=True)),
                ('icon_url', models.CharField(max_length=4096)),
                ('name', models.CharField(max_length=256)),
                ('release_date', models.DateTimeField(null=True)),
                ('version', models.CharField(max_length=256, null=True)),
                ('currency', models.CharField(max_length=32)),
                ('amt_local', models.DecimalField(max_digits=30, decimal_places=10)),
                ('amt_local_micros', models.BigIntegerField()),
                ('amt_usd_micros', models.BigIntegerField()),
            ],
            options={
                'db_table': 'app_info',
            },
            bases=(models.Model,),
        ),
    ]
