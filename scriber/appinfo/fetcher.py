import logging
import requests

from appinfo.constants import ANDROID_PLATFORM_STRING, IOS_PLATFORM_STRING
from appinfo.models import AppInfo
from appstore import api
from core.currency import convert_decimal_to_usd, symbol_to_currency_code
from core.db.constants import TEN_PLACES
from datetime import datetime, timedelta
from dateutil import parser
from decimal import Decimal
from django.conf import settings
from django.db import transaction
from pytz import utc
from re import match
from scriber.api import Scriber
from appstore.models import AppStoreCategory, AppStoreApp, AppStoreArtist


logger = logging.getLogger(__name__)

_ANDROID_APP_INFO_API_KEY = '1aad42af66eb5c148d29e72adb932dc2'
_ANDROID_APP_INFO_URL = 'http://api.playstoreapi.com/v1.1/apps/%s?key=%s'
_IOS_BUNDLE_ID_INFO_URL = 'https://itunes.apple.com/lookup?bundleId=%s'
_IOS_ID_INFO_URL = 'https://itunes.apple.com/lookup?id=%s'

_FILE_SIZE_MULTIPLIER = {
    'k': 1000,
    'm': 1000000,
    'g': 1000000000,
}

_STALE_INFO_DELTA = timedelta(days=1)


# TODO(d-felix): This functionality is out of place here.
# Consider merging the appinfo and appstore django apps.
def _update_primary_category(appInfo):
  if appInfo.platform == IOS_PLATFORM_STRING:
    trackId = long(appInfo.identifier) if appInfo.identifier is not None else None
    api.update_primary_category_ios(bundle_id=appInfo.app, track_id=trackId,
                                    ios_genre_id=appInfo.ios_genre_id, name=appInfo.name)
  elif appInfo.platform == ANDROID_PLATFORM_STRING:
    logger.info('Primary category updates not yet implemented for Android apps')
    pass
  else:
    raise ValueError('Unrecognized platform %s' % appInfo.platform)


class AbstractAppInfoFetcher():

  """
  An abstract base class for app information fetchers.
  Subclasses are expected to override the fetch_direct method.
  """

  def __init__(self, platform):
    self.platform = platform

  def retrieve_from_db(self, app_str):
    stale_info_threshold = datetime.now().replace(tzinfo=utc) - _STALE_INFO_DELTA
    try:
      appInfo = AppInfo.objects.filter(app=app_str, platform=self.platform,
                                       fetch_time__gte=stale_info_threshold).latest('fetch_time')
      return appInfo
    except AppInfo.DoesNotExist:
      return None

  def fetch(self, app_str, direct=False):
    appInfo = None
    if not direct:
      appInfo = self.retrieve_from_db(app_str)
    if appInfo is None:
      appInfo = self.fetch_direct(app_str)
      # appInfo shouldn't be None at this point.
      # TODO(d-felix): Debug this.
      if appInfo is not None:
        if not settings.DEBUG:
          _update_primary_category(appInfo)
    return appInfo

  def fetch_direct(self, app_str):
    raise NotImplementedError('Implementation not defined for abstract base class.')


# Fetcher sets or updates AppStoreApp and Artist data from the iTunes API
class AppStoreFetcher():

  # itunes denies requests with more than 10 sometimes
  def updateAppStoreApps(self, app_store_apps):
    if len(app_store_apps) > 150:
      return

    app_id_csv = "0"
    for app in app_store_apps:
      app_id_csv += "," + str(app.track_id)

    url = _IOS_ID_INFO_URL % app_id_csv

    response = requests.get(url, timeout=60)

    itunes_app_array_json = response.json()['results']
    artist_id_array = []

    # bulk fetch artists. Create missing artists
    for app_json in itunes_app_array_json:
      artist_id_array.append(app_json['artistId'])

    app_store_artists = list(AppStoreArtist.objects.filter(artist_id__in=artist_id_array))
    app_store_artist_dict = {x.artist_id: x for x in app_store_artists}

    with transaction.atomic():
      for app_json in itunes_app_array_json:
        app_track_id = app_json['trackId']
        app_is_free = app_json['price'] == 0 or app_json['formattedPrice'] == 'Free'
        app_store_app = [x for x in app_store_apps if x.track_id == app_track_id][0]
        app_store_app.name = app_json['trackName']

        if not app_store_app.primary_category:
          app_store_category = AppStoreCategory.objects.filter(
              ios_genre_id=app_json['primaryGenreId'])[0]
          app_store_app.primary_category = app_store_category

        artist_id = app_json['artistId']
        app_store_artist = None

        if artist_id in app_store_artist_dict:
          app_store_artist = app_store_artist_dict[artist_id]

        if not app_store_artist:
          app_store_artist, created = AppStoreArtist.objects.get_or_create(
              artist_id=artist_id, defaults={
                  'name': app_json['artistName'], 'platform': app_store_app.platform})
          if created:
            app_store_artist_dict[artist_id] = app_store_artist
        app_store_app.artist = app_store_artist
        app_store_app.is_free = app_is_free
        app_store_app.save()


def parseItunesAppJson(appJson):
  price = appJson['price']
  currency = appJson['currency']
  amtLocalRaw = Decimal(str(price))
  amtLocal = amtLocalRaw.quantize(TEN_PLACES)
  amtLocalMicros = long(amtLocalRaw * 1000000)
  amtUsd = convert_decimal_to_usd(amtLocalRaw, currency)
  amtUsdMicros = long(amtUsd * 1000000) if amtUsd is not None else None
  if amtUsd is None:
    logger.info("Currency conversion failed due to unrecognized currency: %s" % currency)

  releaseDate = parser.parse(appJson['releaseDate'])

  appInfo = AppInfo(
      app=appJson['bundleId'],
      amt_local=amtLocal,
      amt_local_micros=amtLocalMicros,
      amt_usd_micros=amtUsdMicros,
      category=appJson['primaryGenreName'],
      ios_genre_id=appJson['primaryGenreId'],
      currency=currency,
      developer=appJson['sellerName'],
      file_size_bytes=appJson['fileSizeBytes'],
      icon_url=appJson['artworkUrl60'],
      identifier=str(appJson['trackId']),
      name=appJson['trackName'],
      platform=IOS_PLATFORM_STRING,
      release_date=releaseDate,
      version=appJson['version']
  )

  return appInfo


class ItunesBundleIdLookupAppInfoFetcher(AbstractAppInfoFetcher):

  def fetch_direct(self, app_str):
    if not app_str:
      return None

    url = _IOS_BUNDLE_ID_INFO_URL % app_str
    response = requests.get(url)

    # Retrieve the first entry of the results list.
    results = response.json().get('results', [{}])
    if not results or len(results) == 0:
      return None
    results = results[0]

    appInfo = parseItunesAppJson(results)
    appInfo.save()
    return appInfo


class ItunesIdLookupAppInfoFetcher(AbstractAppInfoFetcher):

  def retrieve_from_db(self, apple_identifier):
    stale_info_threshold = datetime.now().replace(tzinfo=utc) - _STALE_INFO_DELTA
    try:
      identifierStr = str(apple_identifier)
      appInfo = AppInfo.objects.filter(identifier=identifierStr, platform=self.platform,
                                       fetch_time__gte=stale_info_threshold).latest('fetch_time')
      return appInfo
    except AppInfo.DoesNotExist:
      return None

  def fetch_direct(self, apple_identifier):
    if not apple_identifier:
      return None

    identifierStr = str(apple_identifier)
    url = _IOS_ID_INFO_URL % identifierStr
    response = requests.get(url)

    # Retrieve the first entry of the results list.
    resultsList = response.json().get('results', [{}])
    if not resultsList:
      return None
    results = resultsList[0]

    appInfo = parseItunesAppJson(results)
    appInfo.save()

    return appInfo


class PlaystoreapiAppInfoFetcher(AbstractAppInfoFetcher):

  def fetch_direct(self, app_str):
    return self.fetch_direct_with_api_key(app_str, _ANDROID_APP_INFO_API_KEY)

  def fetch_direct_with_api_key(self, app_str, api_key):
    if not app_str:
      return None

    url = _ANDROID_APP_INFO_URL % (app_str, api_key)
    response = requests.get(url)
    results = response.json()

    # track playstore API calls with Scriber
    s = Scriber("OXTZhX5tBFKRdcj31o04zxuVd9gyQeGycT9sNvKOrKY", "scriber.io")
    username = 'unknown'
    if 'error' in results:
      s.record_event(username, 'Playstore API', {'success': False})
      return None
    else:
      s.record_event(username, 'Playstore API', {'success': True})

    fileSizeBytes = None
    releaseDate = None
    version = None

    for info in results['additionalInfo']:
      if 'fileSize' in info:
        fileSizeStr = info['fileSize']
        m = match(r'([\d]+)[,\.]([\d]+)([^\d]?)', fileSizeStr)
        if m:
          baseSize = m.group(1) + '.' + m.group(2)
          multiplier = _FILE_SIZE_MULTIPLIER.get(m.group(3).lower(), 1)
          fileSizeDecRaw = Decimal(baseSize)
          fileSizeDec = fileSizeDecRaw.quantize(TEN_PLACES)
          fileSizeBytes = long(fileSizeDec * multiplier)
        else:
          logger.info('Could not parse filesize string: %s' % fileSizeStr)
      if 'datePublished' in info:
        datePublished = info['datePublished']
        releaseDate = parser.parse(datePublished).replace(tzinfo=utc)
      version = info.get('softwareVersion', None)

    appInfo = AppInfo(
        app=app_str,
        category=results['category'],
        developer=results['developer'],
        file_size_bytes=fileSizeBytes,
        icon_url=results['logo'],
        name=results['appName'],
        platform=ANDROID_PLATFORM_STRING,
        release_date=releaseDate,
        version=version,
    )

    # Parse the price field into price and currency.
    priceInfo = results['price']
    if priceInfo.lower() == 'free':
      price = '0.00'
      currency = 'USD'
    else:
      m = match(r'([^\d]*)([\d]+)[,\.]([\d]+)([^\d]*)', priceInfo)
      if m is None:
        logger.info('Could not parse price information %s' % priceInfo)
        return appInfo
      else:
        symbol = m.group(1) if m.group(1) else m.group(4)
        currency = symbol_to_currency_code(symbol)
        price = m.group(2) + '.' + m.group(3)

    # Perform currency conversion.
    amtLocalRaw = Decimal(str(price))
    amtLocal = amtLocalRaw.quantize(TEN_PLACES)
    amtLocalMicros = long(amtLocalRaw * 1000000)
    amtUsd = convert_decimal_to_usd(amtLocalRaw, currency)
    amtUsdMicros = long(amtUsd * 1000000) if amtUsd is not None else None
    if amtUsd is None:
      logger.info("Currency conversion failed due to unrecognized currency: %s" % currency)

    appInfo.amt_local = amtLocal
    appInfo.amt_local_micros = amtLocalMicros
    appInfo.amt_usd_micros = amtUsdMicros
    appInfo.currency = currency
    appInfo.save()
    return appInfo

# Module scope fetchers
IOS_BUNDLE_ID_APP_INFO_FETCHER = ItunesBundleIdLookupAppInfoFetcher(IOS_PLATFORM_STRING)
IOS_ID_APP_INFO_FETCHER = ItunesIdLookupAppInfoFetcher(IOS_PLATFORM_STRING)
ANDROID_APP_INFO_FETCHER = PlaystoreapiAppInfoFetcher(ANDROID_PLATFORM_STRING)

# We use bundle ID as the default iOS identifier.
PLATFORM_TYPE_TO_FETCHER_MAP = {
    IOS_BUNDLE_ID_APP_INFO_FETCHER.platform: IOS_BUNDLE_ID_APP_INFO_FETCHER,
    ANDROID_APP_INFO_FETCHER.platform: ANDROID_APP_INFO_FETCHER,
}


def fetcher_for_platform_type(platform_type_str):
  """
  Returns the proper app info fetcher instance for the provided platform type
  string descriptor. None is returned for unrecognized platforms.
  """
  return PLATFORM_TYPE_TO_FETCHER_MAP.get(platform_type_str, None)


def fetch(app_str, platform_type_str, direct=False):
  """
  A convenience function for fetching app info for the provided app identifier
  and platform type. The app identifier is expected to be the bundle ID for
  iOS apps, and the package ID for Android apps.
  """
  fetcher = fetcher_for_platform_type(platform_type_str)
  if fetcher is None:
    return None
  return fetcher.fetch(app_str, direct)
