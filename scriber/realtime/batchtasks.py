from __future__ import absolute_import

from calendar import timegm
from celery import group, task
from celery.signals import worker_ready
from celery.utils.log import get_task_logger
from copy import copy
from core.celeryutils import exponential_backoff
from core.timeutils import display, from_timestamp, utc_epoch
from core.utils import unpack
from customers.models import Customer
from datetime import timedelta
from django.core.cache import cache
from django.db import connection
from ingestor.models import ChannelProduct
from pytz import utc
from realtime.api import minute_rounded_epoch, sessions_batch_cache_key

logger = get_task_logger(__name__)

# Session counts data is important and relatively small, so we endure a four
# hour timeout.
_DEFAULT_CACHE_TIMEOUT_SECS = 60 * 60 * 4

_MAX_RETRIES = 4
_RETRY_COUNTDOWN_SECS = 60 * 10

_SESSION_START = 0
_SESSION_STOP  = 1

# app_start and app_foreground
_SESSION_START_EVENT_IDS = [2, 3]

# app_terminate, app_background, logout
_SESSION_STOP_EVENT_IDS  = [1, 4, 8]

_MAX_LOOKBACK_MINUTES = 1440
_MAX_LOOKBACK_SECONDS = 60 * _MAX_LOOKBACK_MINUTES
_MAX_SESSION_LENGTH_MINUTES = 30
_MAX_SESSION_LENGTH_SECONDS = 60 * _MAX_SESSION_LENGTH_MINUTES
_MIN_SESSION_LENGTH_SECONDS = 1

_BATCH_SESSIONS_QUERY = """
  SELECT
    u.time event_time,
    u.event_type,
    c.channel_product_id,
    u.scriber_id
  FROM
    user_events u JOIN channel_products c ON u.platform_app_id = c.platform_app_id
  WHERE
    u.time >= %s
    AND u.time <= %s
    AND c.channel_product_id IN %s
    AND u.event_type IN %s
  ORDER BY
    channel_product_id,
    scriber_id,
    event_time
  ;
  """

@worker_ready.connect
def init_realtime_batch_cache(sender=None, **kwargs):
  if 'RealtimeWorker' in sender.hostname:
    logger.info('Received celeryd_init signal, kicking off realtime batch cache update')
    realtime_batch_cache_update.delay()

class Session(object):
  def __init__(self, startTime, endTime, endTimeIsFake, channelProductId, scriberId):
    self.startTime = startTime
    self.endTime = endTime
    self.endTimeIsFake = endTimeIsFake
    self.channelProductId = channelProductId
    self.scriberId = scriberId

class SessionEvent(object):
  def __init__(self, eventTime, eventType, channelProductId, scriberId):
    if not (eventTime and eventType and channelProductId and scriberId):
      raise ValueError('Null argument passed to session event constructor')
    if eventType not in _SESSION_START_EVENT_IDS + _SESSION_STOP_EVENT_IDS:
      raise ValueError('Cannot create session event with unrecognized user event type %s' % \
          eventType)
    self.eventTime = eventTime
    self.channelProductId = channelProductId
    self.scriberId = scriberId
    self.eventType = _SESSION_START if eventType in _SESSION_START_EVENT_IDS else _SESSION_STOP

  def compatible_with_session(self, session):
    return session is not None and \
        session.channelProductId == self.channelProductId and \
        session.scriberId == self.scriberId

  def apply_to_open_session(self, session):
    if self.eventType == _SESSION_START:
      remainderSession = Session(self.eventTime,
          self.eventTime + timedelta(seconds=_MAX_SESSION_LENGTH_SECONDS),
          True, self.channelProductId, self.scriberId)
      if self.compatible_with_session(session) and self.eventTime < session.endTime:
        # TODO(d-felix): Perhaps implement a runaway event time cap that is distinct from the
        # maximum session length.
        session.endTime = self.eventTime
      return remainderSession
    else:
      if self.compatible_with_session(session) and self.eventTime < session.endTime:
        # Correct the session end time and mark as legitimate.
        session.endTime = self.eventTime
        session.endTimeIsFake = False
      return None

class ActiveSessionCounts(object):
  def __init__(self, startEpoch, endEpoch, channelProductIdList):
    self.results = {'times': [], 'product_series': {}}
    self.timesToIndex = {}
    startTime = from_timestamp(startEpoch, utc)
    for delta in range(_MAX_LOOKBACK_MINUTES + _MAX_SESSION_LENGTH_MINUTES):
      roundedEpoch = minute_rounded_epoch(startTime + timedelta(minutes=delta))
      if roundedEpoch > endEpoch:
        break
      self.results['times'].append(roundedEpoch)
      self.timesToIndex[roundedEpoch] = delta
    for channelProductId in channelProductIdList:
      self.results['product_series'][channelProductId] = {'current': [
        {
          'id': 'Sessions',
          'name': 'Active Sessions',
          'data': [0] * len(self.results['times'])
        }
      ]}

  def build_times_index(self):
    self.timesToIndex = {}
    for idx, epoch in enumerate(self.results['times']):
      self.timesToIndex[epoch] = idx

  def truncate(self, startEpoch, endEpoch):
    newStartEpoch = min(max(startEpoch, self.results['times'][0]),
        self.results['times'][-1])
    newRoundedStartEpoch = minute_rounded_epoch(from_timestamp(newStartEpoch, utc))
    newEndEpoch = max(min(endEpoch, self.results['times'][-1]),
        self.results['times'][0])
    newRoundedEndEpoch = minute_rounded_epoch(from_timestamp(newEndEpoch, utc))
    newStartIndex = self.timesToIndex[newRoundedStartEpoch]
    newEndIndex = self.timesToIndex[newRoundedEndEpoch]
    self.results['times'] = copy(self.results['times'][newStartIndex: newEndIndex + 1])
    for channelProductId in self.results['product_series']:
      for countData in self.results['product_series'][channelProductId]['current']:
        countData['data'] = copy(countData['data'][newStartIndex: newEndIndex + 1])
    self.build_times_index()

  def process_session(self, session):
    if session is None:
      return
    elif (session.endTime - session.startTime) < timedelta(seconds=_MIN_SESSION_LENGTH_SECONDS):
      return
    startEpoch = min(max(minute_rounded_epoch(session.startTime), self.results['times'][0]),
        self.results['times'][-1])
    endEpoch = max(min(minute_rounded_epoch(session.endTime), self.results['times'][-1]),
        self.results['times'][0])
    startIndex = self.timesToIndex[startEpoch]
    endIndex = self.timesToIndex[endEpoch]
    if session.channelProductId in self.results['product_series']:
      for data in self.results['product_series'][session.channelProductId]['current']:
        if data['id'] == 'Sessions':
          for index in range(startIndex, endIndex + 1):
            data['data'][index] += 1

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def batch_sessions(self, *args, **kwargs):
  try:
    (customerId, ) = unpack(kwargs, 'customer_id')
    channelProductIdList = [cp.channel_product_id for cp in
        ChannelProduct.objects.filter(customer_id__customer_id=customerId)]
    channelProductIdTuple = tuple(channelProductIdList)
    eventTypeTuple = tuple(_SESSION_START_EVENT_IDS + _SESSION_STOP_EVENT_IDS)

    execEpoch = utc_epoch()
    dbStartEpoch = execEpoch - _MAX_LOOKBACK_SECONDS - _MAX_SESSION_LENGTH_SECONDS
    dbStartTime = from_timestamp(dbStartEpoch, utc)
    dbStartTimeStr = display(dbStartTime)
    dbEndTime = from_timestamp(execEpoch, utc)
    dbEndTimeStr = display(dbEndTime)

    counts = ActiveSessionCounts(dbStartEpoch, execEpoch, channelProductIdList)
    if channelProductIdTuple:
      cursor = connection.cursor()
      cursor.execute(_BATCH_SESSIONS_QUERY, [dbStartTimeStr, dbEndTimeStr, channelProductIdTuple,
          eventTypeTuple])
      bufferedSession = None
      for row in cursor.fetchall():
        sessionEvent = SessionEvent(row[0], row[1], row[2], row[3])
        remainderSession = sessionEvent.apply_to_open_session(bufferedSession)
        counts.process_session(bufferedSession)
        bufferedSession = remainderSession
      counts.process_session(bufferedSession)

    # Remove any old data that was used to generate accurate session counts at the start time.
    # TODO(d-felix): Is this necessary anymore?
    counts.truncate(dbStartEpoch, execEpoch)

    # Cache by channelProductId instead of customerId.
    # TODO(d-felix): Key this task execution by channelProductId and not customerId.
    for channelProductId in channelProductIdList:
      sessionData = {'results': {'product_series': {}}, 'exec_epoch': execEpoch}
      sessionData['results']['times'] = counts.results['times']
      sessionData['results']['product_series'][channelProductId] = \
          counts.results['product_series'][channelProductId]
      cacheKey = sessions_batch_cache_key(channelProductId)
      cache.set(cacheKey, sessionData, _DEFAULT_CACHE_TIMEOUT_SECS)

  except Exception as e:
    raise self.retry(exc=e, countdown=_RETRY_COUNTDOWN_SECS)

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def realtime_batch_cache_update(self, *args, **kwargs):
  try:
    taskList = []
    for customer in Customer.objects.all():
      taskList.append(batch_sessions.s(customer_id=customer.customer_id))
    updateGroup = group(taskList)
    updateGroup.apply_async()
  except Exception as e:
    raise self.retry(exc=e, countdown=30)
