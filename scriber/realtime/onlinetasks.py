from __future__ import absolute_import

import heapq

from calendar import timegm
from celery import group, task
from celery.signals import worker_ready
from celery.utils.log import get_task_logger
from core.celeryutils import exponential_backoff
from core.collections.doublylinkedlist import DoublyLinkedList
from core.timeutils import display, from_timestamp, utc_epoch
from core.utils import unpack
from datetime import timedelta
from django.core.cache import cache
from django.db import connection
from ingestor.models import ChannelProduct, UserEventType
from ingestor.requestutils import event_time
from pytz import utc
from realtime.api import minute_rounded_epoch, sessions_online_cache_key

_MAX_RETRIES = 5

_SESSION_START = 0
_SESSION_STOP  = 1

# app_start and app_foreground
_SESSION_START_EVENT_IDS = [2, 3]

# app_terminate, app_background, logout
_SESSION_STOP_EVENT_IDS  = [1, 4, 8]

_RECOGNIZED_SESSION_IDS = _SESSION_START_EVENT_IDS + _SESSION_STOP_EVENT_IDS
_RECOGNIZED_SESSION_IDS_TUPLE = tuple(_RECOGNIZED_SESSION_IDS)

_MAX_SESSION_LENGTH_MINUTES = 30
_MAX_SESSION_LENGTH_SECONDS = 60 * _MAX_SESSION_LENGTH_MINUTES

_EVENT_LIFESPAN_SECS = 60 * 60
_STALENESS_THRESHOLD_SECS = _EVENT_LIFESPAN_SECS + _MAX_SESSION_LENGTH_SECONDS
_DEFAULT_CACHE_TIMEOUT_SECS = 60 * 30

_BACKFILL_SESSIONS_QUERY = """
  SELECT
    u.time event_time,
    u.event_type,
    c.channel_product_id,
    u.scriber_id
  FROM
    user_events u JOIN channel_products c ON u.platform_app_id = c.platform_app_id
  WHERE
    u.time >= %s
    AND u.time <= %s
    AND c.channel_product_id = %s
    AND u.event_type IN %s
  ORDER BY
    channel_product_id,
    scriber_id,
    event_time
  ;
  """

logger = get_task_logger(__name__)

# @worker_ready.connect
# def init_realtime_online_backfill(sender=None, **kwargs):
#   if 'RealtimeWorker' in sender.hostname:
#     logger.info('Received celeryd_init signal, kicking off realtime online backfill')
#    realtime_online_backfill.delay()

class Session(object):
  def __init__(self, startTime, endTime, startTimeIsFake, endTimeIsFake):
    self.startTime = startTime
    self.endTime = endTime
    self.startTimeIsFake = startTimeIsFake
    self.endTimeIsFake = endTimeIsFake


class UserSessionTracker(object):
  def __init__(self):
    self.sessions = DoublyLinkedList()

  def cleanup(self, cleanupTime):
    if self.sessions.head:
      while self.sessions.head.data.endTime < cleanupTime:
        self.sessions.popleft()
    return self.sessions.length == 0

  def get_session_delta(self, session, lnode, rnode):
    delta = []
    rounded_lb = minute_rounded_epoch(lnode.data.endTime) if lnode else None
    rounded_ub = minute_rounded_epoch(rnode.data.startTime) if rnode else None
    rounded_start = minute_rounded_epoch(session.startTime)
    rounded_end = minute_rounded_epoch(session.endTime)
    while rounded_start <= rounded_end:
      if (rounded_lb is None or rounded_lb < rounded_start) and (rounded_ub is None or
          rounded_start < rounded_ub):
        delta.append(rounded_start)
      rounded_start += 60
    return delta

  def remove_fake_node(self, node):
    dec = self.get_session_delta(node.data, node.prev, node.next)
    self.sessions.delete(node)
    return dec

  def insert_disjoint_session(self, session, lnode, rnode):
    inc = self.get_session_delta(session, lnode, rnode)
    if lnode:
      self.sessions.insertright(session, lnode)
    elif rnode:
      self.sessions.insertleft(session, rnode)
    else:
      self.sessions.append(session)
    return inc

  def insert_session(self, session):
    inc = []
    dec = []
    if session.endTimeIsFake:
      # We know that session.startTime is real (i.e. not imputed). We first
      # find the existing node, if any, whose session serves as the greatest
      # lower bound for the incoming session when sorted by startTime.
      # We start the search from the right end of the DoublyLinkedList since we
      # generally expect events to arrive in time-sorted order.
      lnode = self.sessions.findright(start_time_ordered, session)

      # Ignore duplicates.
      if lnode and lnode.data.startTime == session.startTime:
        lnode.data.startTimeIsFake = False
        return inc, dec

      if (lnode is not None) and (lnode.data.endTime > session.startTime):
        if not lnode.data.endTimeIsFake:
          session.endTime = lnode.data.endTime
          session.endTimeIsFake = False
          lnode.data.endTime = session.startTime
          lnode.data.endTimeIsFake = True
          # The incoming session's range is contained entirely in the ranges
          # of existing nodes, so no incrementing is needed.
          # However, if our session boundary adjustments have left lnode with
          # imputed start and end times, then we may need to decrement some
          # buckets.
          if lnode.data.startTimeIsFake:
            dec = self.remove_fake_node(lnode)
          return inc, dec
        else:
          # This ensures that the incoming session's range becomes disjoint
          # from its predecessor's.
          lnode.data.endTime = session.startTime

      # Now process the case for which lnode.data's range is disjoint from the
      # incoming session's.
      rnode = lnode.next if lnode else self.sessions.head
      if (rnode is not None) and (rnode.data.startTime < session.endTime):
        if (not rnode.data.startTimeIsFake) or (session.endTime < rnode.data.endTime):
          session.endTime = rnode.data.startTime
        else:
          # For simplicity, alter the range of the existing session
          # First truncate the range of the incoming session.
          session.endTime = rnode.data.startTime

          # Then obtain the delta.
          inc = self.get_session_delta(session, lnode, rnode)

          # Expand the range of the existing session.
          rnode.data.startTime = session.startTime
          rnode.data.startTimeIsFake = False

          # Return the deltas.
          return inc, dec

      # Now process the case for which the incoming session's range is disjoint
      # from all other sessions'.
      inc = self.insert_disjoint_session(session, lnode, rnode)
      return inc, dec

    else:
      # We know session.endTime is real.
      lnode = self.sessions.findright(end_time_ordered, session)
      rnode = lnode.next if lnode else self.sessions.head

      # Ignore duplicates.
      if lnode and lnode.data.endTime == session.endTime:
        lnode.data.endTimeIsFake = False
        return inc, dec

      if (rnode is not None) and (rnode.data.startTime < session.endTime):
        if not rnode.data.startTimeIsFake:
          session.startTime = rnode.data.startTime
          session.startTimeIsFake = False
          rnode.data.startTime = session.endTime
          rnode.data.startTimeIsFake = True
          if rnode.data.endTimeIsFake:
            dec = self.remove_fake_node(rnode)
          return inc, dec
        else:
          rnode.data.startTime = session.endTime

      # rnode's data range is now disjoint from the incoming session's.
      if (lnode is not None) and (lnode.data.endTime > session.startTime):
        if (not lnode.data.endTimeIsFake) or (session.startTime > lnode.data.startTime):
          session.startTime = lnode.data.endTime
        else:
          # First truncate the incoming session.
          session.startTime = lnode.data.endTime

          # Then obtain the delta.
          inc = self.get_session_delta(session, lnode, rnode)

          # Expand the range of the existing session.
          lnode.data.endTime = session.endTime
          lnode.data.endTimeIsFake = False

          # Return the deltas
          return inc, dec

      # Now process the case for which the incoming session's range is disjoint
      # from all other sessions'.
      inc = self.insert_disjoint_session(session, lnode, rnode)
      return inc, dec


class ProductSessionTracker(object):
  def __init__(self):
    self.trackers = {}
    self.last_cleanup = None
    self.counts = {}
    self.eventheap = []

  def cleanup(self, cleanup_epoch):
    cleanupUsers = []
    bucketed_cleanup_epoch = minute_rounded_epoch(from_timestamp(cleanup_epoch, utc))
    while self.eventheap and (self.eventheap[0][0] < cleanup_epoch):
      (eventEpoch, scriberId) = heapq.heappop(self.eventheap)
      cleanupUsers.append(scriberId)
    trackersToPop = []
    for scriberId in cleanupUsers:
      if scriberId in self.trackers:
        user_tracker_is_empty = self.trackers[scriberId].cleanup(cleanup_epoch)
        if user_tracker_is_empty:
          trackersToPop.append(scriberId)
    for scriberId in trackersToPop:
      self.trackers.pop(scriberId, None)

    epochsToPop = []
    if self.last_cleanup is None:
      for epoch in self.counts:
        if epoch < bucketed_cleanup_epoch:
          epochsToPop.append(epoch)
    else:
      epoch = bucketed_cleanup_epoch - 60
      while epoch >= self.last_cleanup:
        if epoch in self.counts:
          epochsToPop.append(epoch)
          self.counts.pop(epoch, None)
        epoch -= 60
    for epoch in epochsToPop:
      self.counts.pop(epoch, None)
    self.last_cleanup = bucketed_cleanup_epoch

  def insert_session(self, session, scriberId, epoch):
    heapq.heappush(self.eventheap, (epoch, scriberId))
    if scriberId not in self.trackers:
        self.trackers[scriberId] = UserSessionTracker()
    inc, dec = self.trackers[scriberId].insert_session(session)
    for bucket in inc:
      self.counts[bucket] = 1 + self.counts.get(bucket, 0)
    for bucket in dec:
      if bucket in self.counts:
        self.counts[bucket] = max(0, self.counts[bucket] - 1)


class SessionTracker(object):
  def __init__(self):
    self.trackers = {}
    channelProducts = ChannelProduct.objects.filter(platform_app_id__isnull=False)
    for channelProduct in channelProducts:
      self.trackers[channelProduct.channel_product_id] = ProductSessionTracker()


TRACKER = SessionTracker()

def start_time_ordered(session1, session2):
  return session1.startTime <= session2.startTime

def end_time_ordered(session1, session2):
  return session1.endTime <= session2.endTime

def session_from_message_data(event_time, event_type_id):
  if event_type_id in _SESSION_START_EVENT_IDS:
    return Session(event_time, event_time + timedelta(seconds=_MAX_SESSION_LENGTH_SECONDS),
        False, True)
  elif event_type_id in _SESSION_STOP_EVENT_IDS:
    return Session(event_time - timedelta(seconds=_MAX_SESSION_LENGTH_SECONDS), event_time,
        True, False)
  else:
    raise ValueError('Event type %s does not represent a session boundary' % event_type_id)

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def update_online_sessions(self, *args, **kwargs):
  try:
    message, platformAppId, scriberId = unpack(kwargs, 'message', 'platform_app_id', 'scriber_id')

    eventType = UserEventType.objects.get(event_type_str=message['event_type'])
    if eventType.event_type_id not in _RECOGNIZED_SESSION_IDS:
      return

    channelProduct = ChannelProduct.objects.filter(
        platform_app_id__platform_app_id=platformAppId).first()
    if channelProduct is None:
      logger.warning('Could not identify product for platform_app_id %s' %
          platformApp.platform_app_id)
      return
    channelProductId = channelProduct.channel_product_id

    eventTime = event_time(message)
    eventEpoch = timegm(eventTime.utctimetuple())
    session = session_from_message_data(eventTime, eventType.event_type_id)

    if channelProductId not in TRACKER.trackers:
      TRACKER.trackers[channelProductId] = ProductSessionTracker()
    pTracker = TRACKER.trackers[channelProductId]

    # A timestamp for the current update.
    execEpoch = utc_epoch()
    stalenessEpoch = execEpoch - _STALENESS_THRESHOLD_SECS

    # Only update for relevant events
    if timegm(eventTime.utctimetuple()) >= stalenessEpoch:
      pTracker.insert_session(session, scriberId, eventEpoch)
    pTracker.cleanup(stalenessEpoch)

    # Cache results
    results = {'cache_time': execEpoch, 'counts': pTracker.counts}
    cacheKey = sessions_online_cache_key(channelProductId)
    cache.set(cacheKey, results, _DEFAULT_CACHE_TIMEOUT_SECS)

  # For now, treat all exceptions as transient errors and retry.
  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def realtime_online_backfill(self, *args, **kwargs):
  try:
    taskList = []
    for channelProduct in ChannelProduct.objects.filter(platform_app_id__isnull=False):
      taskList.append(realtime_online_backfill_for_product.s(
          channel_product_id=channelProduct.channel_product_id))
    backfillGroup = group(taskList)
    backfillGroup.apply_async()
  except Exception as e:
    raise self.retry(exc=e, countdown=30)


@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def realtime_online_backfill_for_product(self, *args, **kwargs):
  pass
  """
  try:
    (channelProductId, ) = unpack(kwargs, 'channel_product_id')

    # Wipe any existing data in the relevant ProductSessionTracker.
    if channelProductId not in TRACKER.trackers:
      TRACKER.trackers[channelProductId] = ProductSessionTracker()
    pTracker = TRACKER.trackers[channelProductId]
    pTracker.__init__()

    execEpoch = utc_epoch()
    endTime = from_timestamp(execEpoch, utc)
    endTimeStr = display(endTime)
    startTime = endTime - timedelta(seconds=_STALENESS_THRESHOLD_SECS)
    startTimeStr = display(startTime)
    cursor = connection.cursor()
    cursor.execute(_BACKFILL_SESSIONS_QUERY, [startTimeStr, endTimeStr, channelProductId,
        _RECOGNIZED_SESSION_IDS_TUPLE])
    for row in cursor.fetchall():
      eventTime, eventTypeId, channelProductId, scriberId = row[:4]
      eventEpoch = timegm(eventTime.utctimetuple())
      if scriberId not in pTracker.trackers:
        pTracker.trackers[scriberId] = UserSessionTracker()
      session = session_from_message_data(eventTime, eventTypeId)
      pTracker.insert_session(session, scriberId, eventEpoch)

    # Cache results
    results = {'cache_time': execEpoch, 'counts': pTracker.counts}
    cacheKey = sessions_online_cache_key(channelProductId)
    cache.set(cacheKey, results, _DEFAULT_CACHE_TIMEOUT_SECS)

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))
  """
