from calendar import timegm
from core.timeutils import from_timestamp, utc_epoch
from datetime import timedelta
from django.core.cache import cache
from pytz import utc

_KEY_DELIMITER = '/'
_REALTIME_MODULE_CACHE_KEY_PREFIX = 'qBD1R4'

MAX_LOOKBACK_MINS = 1440
MAX_LOOKBACK_SECS = 60 * MAX_LOOKBACK_MINS

ONLINE_RESULT_WINDOW_SECS = 60 * 60

def minute_rounded_epoch(dt):
  return timegm((dt - timedelta(seconds=dt.second, microseconds=dt.microsecond)).utctimetuple())

def sessions_batch_cache_key(channel_product_id):
  return _KEY_DELIMITER.join([_REALTIME_MODULE_CACHE_KEY_PREFIX, 'batch', sessions.__module__,
      sessions.__name__, str(channel_product_id)])

def sessions_online_cache_key(channel_product_id):
  return _KEY_DELIMITER.join([_REALTIME_MODULE_CACHE_KEY_PREFIX, 'online', sessions.__module__,
      sessions.__name__, str(channel_product_id)])

def sessions(channelProductIdList, startEpoch, endEpoch):
  requestTime = from_timestamp(utc_epoch(), utc)
  requestedStartTime = from_timestamp(startEpoch, utc)
  cappedStartTime = min(max(requestedStartTime, requestTime - timedelta(seconds=MAX_LOOKBACK_SECS)),
      requestTime)
  requestedEndTime = from_timestamp(endEpoch, utc)
  cappedEndTime = min(max(requestedEndTime, requestTime - timedelta(seconds=MAX_LOOKBACK_SECS)),
      requestTime)
  thresholdTime = requestTime - timedelta(seconds=ONLINE_RESULT_WINDOW_SECS)

  results = {'times': [], 'product_series': {}}
  rstart = minute_rounded_epoch(cappedStartTime)
  rend = minute_rounded_epoch(cappedEndTime)
  rThreshold = minute_rounded_epoch(thresholdTime)

  epoch = rstart
  while epoch <= rend:
    results['times'].append(epoch)
    epoch += 60

  for channelProductId in channelProductIdList:
    data = [0] * len(results['times'])
    results['product_series'][channelProductId] = {'current': [
      {
        'id': 'Sessions',
        'name': 'Active Sessions',
        'data': data,
      }
    ]}

    # TODO(d-felix): Implement fallback behavior on cache misses.
    batch_key = sessions_batch_cache_key(channelProductId)
    batch_results = cache.get(batch_key)
    if batch_results:
      # TODO(d-felix): Rewrite the batch_results structure since it's no longer
      # being consumed by the front-end.
      b_times = batch_results['results']['times']
      if channelProductId not in batch_results['results']['product_series']:
        continue
      b_data = batch_results['results']['product_series'][channelProductId]['current'][0]['data']
      for idx, minute in enumerate(b_times):
        if rstart <= minute <= rend:
          new_idx = (minute - rstart) / 60
          data[new_idx] = b_data[idx]

    online_key = sessions_online_cache_key(channelProductId)
    online_results = cache.get(online_key)
    if online_results:
      for minute in online_results.get('counts', {}):
          if minute >= rThreshold:
            val = online_results['counts'][minute]
            idx = (minute - rstart) / 60
            if minute <= rend:
              data[idx] = val

  return results
