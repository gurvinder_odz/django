from django.http import HttpResponse
from django.shortcuts import render

'''
  help page to make sales account for ITC
'''
def create_itunesconnect_sales_account(request):
  return render(request, 'partials/help-itunesconnect-partial.html', 
    {})

def androidsdk_license_key(request):
  return render(request, 'partials/help-androidsdk-license-key-partial.html', 
    {})