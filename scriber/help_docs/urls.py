from django.conf.urls import patterns, url
from help_docs import views

urlpatterns = patterns('',
  url(r'^create_itunesconnect_sales_account', views.create_itunesconnect_sales_account),
  url(r'^androidsdk_license_key', views.androidsdk_license_key),
)
