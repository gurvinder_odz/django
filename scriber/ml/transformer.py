import base64
import logging
import pickle

from appstore.constants import IOS_OVERALL_CATEGORY_ID
from core.timeutils import convert_tz
from core.utils import unpack
from datetime import datetime
from dateutil import parser
from django.db import connection
from math import exp, log, sqrt
from ml.dataset import Dataset
from pytz import timezone, utc
from sklearn.linear_model import Lasso

logger = logging.getLogger(__name__)


class Transformer(object):

  def __init__(self, name, *args, **kwargs):
    self.name = name
    self.configured = False

  def configure(self, *args, **kwargs):
    self.configured = True

  def transform(self, dataset, *args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')

  def deflate(self):
    raise NotImplementedError('Not implemented for abstract base class')

  @staticmethod
  def inflate(*args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')


class GenericTransformer(Transformer):

  def __init__(self, name, func, *args, **kwargs):
    super(GenericTransformer, self).__init__(name, *args, **kwargs)
    self.transform_func = func

  def configure(self, *args, **kwargs):
    self.conf_args = args
    self.conf_kwargs = kwargs
    self.configured = True

  # TODO(d-felix): Rethink this.
  def transform(self, dataset, *args, **kwargs):
    return self.transform_func(dataset, conf_args=self.conf_args, conf_kwargs=self.conf_kwargs,
                               *args, **kwargs)


class SimpleFeatureTransformer(Transformer):

  def __init__(self, name, func, features=None, outputs=None, *args, **kwargs):
    super(SimpleFeatureTransformer, self).__init__(name)
    self.transform_func = func
    self.features = [] if not features else features
    self.outputs = [] if not outputs else outputs

  def transform(self, dataset, *args, **kwargs):
    new_features = {}
    for idx, feature in enumerate(self.features):
      if feature not in dataset.features:
        logger.info('Cannot apply transformation to missing feature %s' % feature)
        continue
      feature_name = self.outputs[idx] if self.outputs else feature
      new_features[feature_name] = [self.transform_func(oldValue, *args, **kwargs)
                                    for oldValue in dataset.features[feature]]

    dataset.features.update(new_features)
    return dataset


class SimplePredictionTransformer(Transformer):

  def __init__(self, name, func, *args, **kwargs):
    super(SimplePredictionTransformer, self).__init__(name)
    self.transform_func = func

  def transform(self, dataset, *args, **kwargs):
    new_predictions = []
    for prediction in dataset.predictions:
      new_predictions.append(self.transform_func(prediction, *args, **kwargs))
    dataset.predictions = new_predictions
    return dataset


class SciKitModelTransformer(Transformer):

  def __init__(self, name, model, *args, **kwargs):
    super(SciKitModelTransformer, self).__init__(name)
    self.model = model

  def configure(self, dataset, *args, **kwargs):
    self.model.fit(dataset.data, dataset.labels)
    self.configured = True

  def transform(self, dataset, *args, **kwargs):
    # TODO(d-felix): Implement a custom LinearModelTransformer.
    if isinstance(self.model, Lasso) and dataset.data and \
            len(dataset.data[0]) > len(self.model.coef_):
      dataset.data = [record[:len(self.model.coef_)] for record in dataset.data]

    if dataset.data:
      dataset.predictions = self.model.predict(dataset.data)
    return dataset

  def deflate(self):
    deflatedModel = base64.b64encode(pickle.dumps(self.model))
    return {'name': self.name, 'model': deflatedModel}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    inflatedModel = pickle.loads(base64.b64decode(deflation['model']))
    transformer = SciKitModelTransformer(deflation['name'], inflatedModel)
    transformer.configured = True
    return transformer


# TODO(d-felix): Handle the transformer name constants below more cleanly.
class ZScaleTransformer(Transformer):

  def __init__(self, name, features=None, *args, **kwargs):
    super(ZScaleTransformer, self).__init__(name)
    self.means = {}
    self.stdevs = {}
    if not features:
      self.features = []
    else:
      self.features = features

  def configure(self, dataset, *args, **kwargs):
    n = len(dataset.ids)
    if n == 0:
      raise ValueError('Cannot create Z-scaling transformer signature from dataset without records')

    sums = {}
    sum_sqs = {}
    for feature in self.features:
      if feature not in dataset.features:
        continue
      sums[feature] = 0.0
      sum_sqs[feature] = 0.0
      for value in dataset.features[feature]:
        sums[feature] += float(value)
        sum_sqs[feature] += float(value) * float(value)

    for feature in sums:
      self.means[feature] = sums[feature] / n
      variance = (sum_sqs[feature] / n) - (self.means[feature] ** 2)
      self.stdevs[feature] = sqrt(variance) if variance > 0.0 else 0.0

  def transform(self, dataset, *args, **kwargs):
    if not self.means or not self.stdevs:
      raise ValueError('Means and standard deviations must be provided for z-scaling')

    new_features = {}
    for feature in self.means:
      new_features[feature] = []
      for value in dataset.features[feature]:
        new_features[feature].append((value - self.means[feature]) /
                                     (self.stdevs[feature] if self.stdevs[feature] > 0.0 else 1.0))

    # TODO(d-felix): How to handle feature updating vs. creating new features?
    dataset.features.update(new_features)
    return dataset

  def deflate(self):
    return {'name': self.name, 'features': self.features, 'means': self.means,
            'stdevs': self.stdevs}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = ZScaleTransformer(deflation['name'], deflation['features'])
    transformer.means = deflation['means']
    transformer.stdevs = deflation['stdevs']
    transformer.configured = True
    return transformer


class LogTransformer(SimpleFeatureTransformer):

  def __init__(self, name, base=None, features=None, *args, **kwargs):
    base = float(base) if base is not None else None
    func = ((lambda x, *args, **kwargs: log(x, base)) if base else
            (lambda x, *args, **kwargs: log(x)))
    super(LogTransformer, self).__init__(name, func, features, *args, **kwargs)
    self.base = base

  def deflate(self):
    return {'name': self.name, 'base': self.base, 'features': self.features}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = LogTransformer(deflation['name'], deflation['base'], deflation['features'])
    transformer.configured = True
    return transformer


class PowTransformer(SimpleFeatureTransformer):

  def __init__(self, name, power, features=None, outputs=None, *args, **kwargs):
    power = float(power)
    func = lambda x, *args, **kwargs: x ** power
    super(PowTransformer, self).__init__(name, func, features, outputs, *args, **kwargs)
    self.power = power

  def deflate(self):
    deflation = {'name': self.name, 'power': self.power, 'features': self.features}
    if self.outputs:
      deflation['outputs'] = self.outputs
    return deflation

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = PowTransformer(deflation['name'], deflation['power'], deflation['features'],
                                 deflation.get('outputs', None))
    transformer.configured = True
    return transformer


class ReplaceNullTransformer(SimpleFeatureTransformer):

  def __init__(self, name, value, features=None, *args, **kwargs):
    func = lambda x, *args, **kwargs: value if x is None else x
    super(ReplaceNullTransformer, self).__init__(name, func, features, *args, **kwargs)
    self.value = value

  def deflate(self):
    return {'name': self.name, 'value': self.value, 'features': self.features}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = ReplaceNullTransformer(deflation['name'], deflation['value'],
                                         deflation['features'])
    transformer.configured = True
    return transformer


class PredictionExpTransformer(SimplePredictionTransformer):

  def __init__(self, name, *args, **kwargs):
    func = lambda x, *args, **kwargs: exp(float(x)) if x is not None else None
    super(PredictionExpTransformer, self).__init__(name, func, *args, **kwargs)

  def deflate(self):
    return {'name': self.name}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = PredictionExpTransformer(deflation['name'])
    transformer.configured = True
    return transformer


class PredictionPowTransformer(SimplePredictionTransformer):

  def __init__(self, name, power, *args, **kwargs):
    func = lambda x, *args, **kwargs: float(x) ** power if x is not None else None
    super(PredictionPowTransformer, self).__init__(name, func, *args, **kwargs)
    self.power = power

  def deflate(self):
    return {'name': self.name, 'power': self.power}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = PredictionPowTransformer(deflation['name'], deflation['power'])
    transformer.configured = True
    return transformer


class FormatterTransformer(Transformer):

  def __init__(self, name, features=None, *args, **kwargs):
    super(FormatterTransformer, self).__init__(name)
    self.features = [] if not features else features

  def transform(self, dataset, *args, **kwargs):
    data = []
    for idx in range(len(dataset.ids)):
      featureValues = [dataset.features[feature][idx] for feature in self.features]
      data.append(featureValues)

    dataset.data = data
    return dataset

  def deflate(self):
    return {'name': self.name, 'features': self.features}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = FormatterTransformer(deflation['name'], deflation['features'])
    transformer.configured = True
    return transformer


class RemoveFeatureTransformer(Transformer):

  def __init__(self, name, features=None, *args, **kwargs):
    super(FormatterTransformer, self).__init__(name)
    self.features = [] if not features else features

  def transform(self, dataset, *args, **kwargs):
    for feature in self.features:
      dataset.features.pop(feature, None)
    return dataset

  def deflate(self):
    return {'name': self.name, 'features': self.features}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = RemoveFeatureTransformer(deflation['name'], deflation['features'])
    transformer.configured = True
    return transformer


# Forbidden values are expected to be JSON serializable.
# TODO(d-felix): Move discarded records to an unscored dataset?
class RemoveForbiddenValuesTransformer(Transformer):

  def __init__(self, name, forbidden_values=None, features=None, *args, **kwargs):
    super(RemoveForbiddenValuesTransformer, self).__init__(name)
    self.forbidden_values = [None] if forbidden_values is None else forbidden_values
    self.features = [] if not features else features

  def transform(self, dataset, *args, **kwargs):
    newFeatures = {}

    validIndices = [idx for idx in range(len(dataset.ids)) if not any(
        [dataset.features[feature][idx] in self.forbidden_values for feature in self.features])]

    for feature in dataset.features:
      newFeatures[feature] = [dataset.features[feature][idx] for idx in validIndices]
    newIds = [dataset.ids[idx] for idx in validIndices]
    if dataset.labels:
      newLabels = [dataset.labels[idx] for idx in validIndices]

    dataset.features = newFeatures
    dataset.ids = newIds
    if dataset.labels:
      dataset.labels = newLabels
    return dataset

  def deflate(self):
    return {'name': self.name, 'forbidden_values': self.forbidden_values, 'features': self.features}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = RemoveForbiddenValuesTransformer(deflation['name'], deflation['forbidden_values'],
                                                   deflation['features'])
    transformer.configured = True
    return transformer


class RemoveDateOutliersTransformer(Transformer):

  def __init__(self, name, features=None, lt=None, lte=None, gt=None, gte=None, *args, **kwargs):
    super(RemoveDateOutliersTransformer, self).__init__(name)
    self.lt = lt
    self.lte = lte
    self.gt = gt
    self.gte = gte
    self.features = [] if not features else features

  def transform(self, dataset, *args, **kwargs):
    lt = [parser.parse(x).date() for x in self.lt] if self.lt is not None else None
    lte = [parser.parse(x).date() for x in self.lte] if self.lte is not None else None
    gt = [parser.parse(x).date() for x in self.gt] if self.gt is not None else None
    gte = [parser.parse(x).date() for x in self.gte] if self.gte is not None else None

    newIds = []
    newLabels = []
    newFeatures = {}
    for feature in dataset.features:
      newFeatures[feature] = []

    for column_idx in range(len(dataset.ids)):
      for idx, feature in enumerate(self.features):
        if ((lt is not None and dataset.features[feature][column_idx] < lt[idx]) or
                (lte is not None and dataset.features[feature][column_idx] <= lte[idx]) or
                (gt is not None and dataset.features[feature][column_idx] > gt[idx]) or
                (gte is not None and dataset.features[feature][column_idx] >= gte[idx])):
          break
        elif idx == len(self.features) - 1:
          for f in dataset.features:
            newFeatures[f].append(dataset.features[f][column_idx])
          newIds.append(dataset.ids[column_idx])
          if dataset.labels:
            newLabels.append(dataset.labels[column_idx])

    dataset.ids = newIds
    dataset.features = newFeatures
    if dataset.labels:
      dataset.labels = newLabels
    return dataset

  def deflate(self):
    deflation = {'name': self.name, 'features': self.features, 'lt': self.lt, 'lte': self.lte,
                 'gt': self.gt, 'gte': self.gte}
    return deflation

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = RemoveDateOutliersTransformer(
        deflation['name'],
        deflation['features'],
        lt=deflation['lt'],
        lte=deflation['lte'],
        gt=deflation['gt'],
        gte=deflation['gte'])
    transformer.configured = True
    return transformer


# TODO(d-felix): Make this a versioned transformer, or allow the query to be
# specified as an argument.
#
# TODO(d-felix): This transformer is specific to the DailyRevenueModel. Allow
# Models to define their own transformers, and check these private sets for
# inflation/deflation instructions before falling back to the registry.
#
# TODO(d-felix): Query execution time is horrible. Fix this.
class IosRankTransformer(Transformer):

  def __init__(self, name, iphone_rank_type='topgrossingapplications',
               ipad_rank_type='topgrossingipadapplications', *args, **kwargs):
    super(IosRankTransformer, self).__init__(name)
    self.RANK_DATA_BIRTHDAY_STR = '2015-04-09'
    self.QUERY = """
      SELECT
        s.app_id app_id,
        s.record_date record_date,
        MAX(CASE WHEN(s.rank_age_index = 1 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_1,
        MAX(CASE WHEN(s.rank_age_index = 2 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_2,
        MAX(CASE WHEN(s.rank_age_index = 3 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_3,
        MAX(CASE WHEN(s.rank_age_index = 4 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_4,
        MAX(CASE WHEN(s.rank_age_index = 5 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_5,
        MAX(CASE WHEN(s.rank_age_index = 6 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_6,
        MAX(CASE WHEN(s.rank_age_index = 7 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_7,
        MAX(CASE WHEN(s.rank_age_index = 8 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_8,
        MAX(CASE WHEN(s.rank_age_index = 1 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_1,
        MAX(CASE WHEN(s.rank_age_index = 2 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_2,
        MAX(CASE WHEN(s.rank_age_index = 3 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_3,
        MAX(CASE WHEN(s.rank_age_index = 4 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_4,
        MAX(CASE WHEN(s.rank_age_index = 5 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_5,
        MAX(CASE WHEN(s.rank_age_index = 6 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_6,
        MAX(CASE WHEN(s.rank_age_index = 7 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_7,
        MAX(CASE WHEN(s.rank_age_index = 8 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_8,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 1) THEN 1 ELSE 0 END) is_monday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 2) THEN 1 ELSE 0 END) is_tuesday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 3) THEN 1 ELSE 0 END) is_wednesday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 4) THEN 1 ELSE 0 END) is_thursday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 5) THEN 1 ELSE 0 END) is_friday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 6) THEN 1 ELSE 0 END) is_saturday,
        MIN(CASE WHEN(s.rank_age_index <= 24 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) best_rank_3d,
        MIN(CASE WHEN(s.rank_age_index <= 24 AND s.device = 'iPad') THEN s.rank ELSE NULL END) best_rank_ipad_3d,
        MIN(CASE WHEN(s.rank_age_index <= 8 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) best_rank_1d,
        MIN(CASE WHEN(s.rank_age_index <= 8 AND s.device = 'iPad') THEN s.rank ELSE NULL END) best_rank_ipad_1d
      FROM (
        SELECT
          k.app app_id,
          date_range.date::date record_date,
          k.rank rank,
          ceil(extract('epoch' from ((date_range.date + '1 day') - (r.time AT TIME ZONE
              'America/Los_Angeles')))/10800) rank_age_index,
          CASE WHEN (t.name = %s) THEN 'iPhone' ELSE 'iPad' END device
        FROM app_store_rank_requests r
        JOIN app_store_rank_types t ON (r.rank_type = t.id)
        JOIN app_store_regions rg ON (r.region = rg.id)
        JOIN countries c ON (rg.country = c.id)
        JOIN app_store_categories cg ON (r.category = cg.id)
        JOIN app_store_ranks k ON (r.id = k.rank_request)
        JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
        ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
            date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
        WHERE
          c.alpha2 = %s AND
          t.name IN %s AND
          cg.id = %s AND
          (k.app, date_range.date::date) IN %s
      ) s
      GROUP BY 1, 2
      ;
    """
    self.IPHONE_RANK_TYPE = iphone_rank_type
    self.IPAD_RANK_TYPE = ipad_rank_type

  def transform(self, dataset, *args, **kwargs):
    (invocationContext, ) = unpack(kwargs, 'invocation_context')
    segmentKey = invocationContext['segment_key']
    country = segmentKey[0]
    category = segmentKey[1] if len(segmentKey) > 1 else IOS_OVERALL_CATEGORY_ID

    idTuple = tuple(dataset.ids)
    idLength = len(idTuple[0])
    yesterdayPacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
    rank_types_tuple = (self.IPHONE_RANK_TYPE, self.IPAD_RANK_TYPE)
    cursor = connection.cursor()
    cursor.execute(
        self.QUERY, [
            self.IPHONE_RANK_TYPE, self.RANK_DATA_BIRTHDAY_STR, str(
                yesterdayPacific.date()), country, rank_types_tuple, category, idTuple])

    nFeatures = len(cursor.cursor.description) - idLength
    if nFeatures <= 0:
      raise ValueError('No new features found by RankTransformer query')

    # The first idLength columns in a records constitute the id key.
    newFeatures = {c.name: [] for c in cursor.cursor.description[idLength:]}
    newFeatureNames = [c.name for c in cursor.cursor.description[idLength:]]
    newFeatureValues = {}
    for row in cursor.fetchall():
      key = row[:idLength]
      newFeatureValues[key] = row[idLength:]
    cursor.close()

    for idx, key in enumerate(dataset.ids):
      if key in newFeatureValues:
        # We enumerate over newFeatureNames to guarantee a consistent iteration
        # order with the query results columns.
        for col_idx, name in enumerate(newFeatureNames):
          newFeatures[name].append(newFeatureValues[key][col_idx])
      else:
        for name in newFeatures:
          newFeatures[name].append(None)

    dataset.features.update(newFeatures)
    return dataset

  def deflate(self):
    return {
        'name': self.name,
        'iphone_rank_type': self.IPHONE_RANK_TYPE,
        'ipad_rank_type': self.IPAD_RANK_TYPE}

  @staticmethod
  def inflate(deflation, *args, **kwargs):
    transformer = IosRankTransformer(
        deflation['name'],
        deflation.get('iphone_rank_type', 'topgrossingapplications'),
        deflation.get('ipad_rank_type', 'topgrossingipadapplications'))
    transformer.configured = True
    return transformer
