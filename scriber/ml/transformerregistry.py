import logging

from ml import transformer


TRANSFORMERS = [
  transformer.FormatterTransformer,
  transformer.IosRankTransformer,
  transformer.LogTransformer,
  transformer.PowTransformer,
  transformer.PredictionExpTransformer,
  transformer.PredictionPowTransformer,
  transformer.RemoveDateOutliersTransformer,
  transformer.RemoveForbiddenValuesTransformer,
  transformer.ReplaceNullTransformer,
  transformer.SciKitModelTransformer,
  transformer.ZScaleTransformer,
]

REGISTRY = {}
for cls in TRANSFORMERS:
  REGISTRY[cls.__name__] = cls

logger = logging.getLogger(__name__)

def deflate(transformer):
  if transformer.__class__.__name__ not in REGISTRY:
    logger.warning('Serializing unregistered transformer class %s' % transformer.__class__)
  deflation = transformer.deflate()
  data = {'class_name': transformer.__class__.__name__, 'deflation': deflation}
  return data

def inflate(data):
  className = data['class_name']
  if className not in REGISTRY:
    raise ValueError('Cannot deserialize unregistered transformer class name %s' % className)
  cls = REGISTRY[className]
  deflation = data['deflation']
  transformer = cls.inflate(deflation)
  return transformer
