from core.db.dynamic import connection


class Labeler(object):
  def label(self, dataset, invocation_context=None,
      *args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')


class DbLabeler(Labeler):
  def __init__(self, query, arg_list=None, processor=None):
    self.query = query
    self.arg_list = arg_list
    if processor is None:
      self.processor = lambda *args, **kwargs: None
    else:
      self.processor = processor

  def label(self, dataset, invocation_context=None, *args, **kwargs):
    # This function may modify the Labeler, and therefore accepts a reference
    # to its host as an argument.
    self.processor(self, invocation_context)

    labelMap = {}
    cursor = connection.cursor()
    cursor.execute(self.query, self.arg_list)

    for record in cursor.fetchall():
      # We expect the first column to contain the label.
      label = record[0]
      # We expect the remaining columns to be in 1-to-1 order-preserving
      # correspondance with the components of the id key.
      key = record[1:]
      labelMap[key] = label

    cursor.close()

    oldLabels = dataset.labels
    labels = []
    for idx, id in enumerate(dataset.ids):
      fallbackLabel = oldLabels[idx] if oldLabels else None
      labels.append(labelMap[id] if id in labelMap else fallbackLabel)

    dataset.labels = labels


class DictionaryLabeler(Labeler):
  def __init__(self, labels, *args, **kwargs):
    if not isinstance(labels, dict):
      raise ValueError('Must provide a labels dictionary to create a DictionaryLabeler. Found: %s'
          % labels)
    self.labels = labels

  def label(self, dataset, invocation_context=None, *args, **kwargs):
    segment_key = tuple(invocation_context['segment_key'])
    label_map = self.labels[segment_key]
    old_labels = dataset.labels
    new_labels = []
    for idx, id in enumerate(dataset.ids):
      fallback_label = old_labels[idx] if old_labels else None
      new_labels.append(label_map.get(id, fallback_label))

    dataset.labels = new_labels


class ModelPredictionLabeler(Labeler):
  def __init__(self, model, label_modifier=None, request_kwargs=None, processor=None):
    self.model = model
    self.label_modifier = label_modifier
    self.request_kwargs = request_kwargs
    if processor is None:
      self.processor = lambda *args, **kwargs: None
    else:
      self.processor = processor

  def label(self, dataset, invocation_context=None, *args, **kwargs):
    # This function may modify the Labeler, and therefore accepts a reference
    # to its host as an argument.
    self.processor(self, invocation_context)

    # Reconstruct the expected request_data from the passed dataset.
    # Note that this approach relies on the dataset.ids information being
    # duplicated in dataset.features.
    request_data = []
    for idx in range(len(dataset.ids)):
      entry = {}
      # Include any contributions from request_kwargs.
      for key in self.request_kwargs:
        entry[key] = self.request_kwargs[key]
      for feature in dataset.features:
        entry[feature] = dataset.features[feature][idx]
      request_data.append(entry)

    predictions = self.model.predict(request_data, *args, **kwargs)

    # TODO(d-felix): Remove the need for this.
    if len(predictions) != len(dataset.ids):
      raise ValueError('Mismatch in id length %s and prediction length %s' %
          (len(dataset.ids), len(predictions)))

    # Modify the new label if necessary, and fall back to a prior label if no
    # new label is found.
    oldLabels = dataset.labels
    labels = []
    for idx, entry in enumerate(predictions):
      # TODO(d-felix): Make this officially supported in the Model object API.
      prediction = entry['prediction']
      record_id = dataset.ids[idx]
      if prediction is not None:
        labels.append(self.label_modifier(prediction, record_id) if self.label_modifier
            else prediction)
      else:
        labels.append(oldLabels[idx] if oldLabels else None)

    dataset.labels = labels
