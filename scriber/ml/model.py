import json
import logging

from calendar import timegm
from copy import deepcopy
from core.db.dynamic import connection
from customers.models import Customer
from datetime import datetime
from django.core.cache import cache
from django.db.models import F, Max
from ml import transformerregistry
from ml.models import ModelSegment as SerializedModelSegment


_SEGMENT_CACHE_KEY_PREFIX = 'modelSegment'
_SEGMENT_CACHE_KEY_IDENTIFIER = '4e3TIIxVvBgJHcWy'
_KEY_DELIMITER = '/'

logger = logging.getLogger(__name__)

_LATEST_SEGMENT_QUERY = """
  SELECT
    ms.model_name,
    ms.segment_key,
    ms.owner,
    ms.content
  FROM model_segments ms
  JOIN (
    SELECT
      model_name,
      segment_key,
      owner,
      MAX(creation_time) max_creation_time
    FROM model_segments
    WHERE
      model_name = %s AND
      version = %s AND
      segment_key IN %s AND
      owner %s %s
    GROUP BY 1, 2, 3
  ) subquery
  ON (
    ms.model_name = subquery.model_name AND
    ms.segment_key = subquery.segment_key AND
    %s AND
    ms.creation_time = subquery.max_creation_time
  )
  ;
  """

def _latest_segment_query(owner_id):
  if owner_id is None:
    return _LATEST_SEGMENT_QUERY % ('%s', '%s', '%s', 'IS', '%s', 'ms.owner IS NULL')
  else:
    return _LATEST_SEGMENT_QUERY % ('%s', '%s', '%s', '=', '%s', 'ms.owner = subquery.owner')


def _segment_update_cache_key(model_name, version, owner_id, segment_key_json):
  return _KEY_DELIMITER.join([_SEGMENT_CACHE_KEY_PREFIX, _SEGMENT_CACHE_KEY_IDENTIFIER,
      model_name, str(version), str(owner_id), segment_key_json])


def _cache_segment_update(model_name, version, owner_id, segment_key_json):
  timestamp = timegm(datetime.utcnow().utctimetuple())
  key = _segment_update_cache_key(model_name, version, owner_id, segment_key_json)
  value = timestamp
  cache.set(key, value, None)


class Model(object):
  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    if name is None:
      raise ValueError('Model construction requires a name argument')
    self.name = name
    self.version = version
    self.trainer = None
    self.owner_id = owner_id
    self.fallback = fallback

    self.segment_map = {}
    self.segment_load_times = {}
    segment_keys_json = tuple([json.dumps(k) for k in self._segment_keys()])
    cursor = connection.cursor()
    cursor.execute(_latest_segment_query(owner_id), [name, version, segment_keys_json, owner_id])
    for row in cursor.fetchall():
      segmentKeyJson, contentJson = row[1], row[3]
      segmentObj = ModelSegment.from_json(segmentKeyJson, contentJson)
      self.segment_map[tuple(segmentObj.key)] = segmentObj
      self.segment_load_times[tuple(segmentObj.key)] = timegm(datetime.utcnow().utctimetuple())

  def predict(self, request_data, format_results=True, *args, **kwargs):
    # Partition the provided data by segment key, and construct an interleave
    # sequence in order to collate the predictions of each segment.
    collationData = {'records':{}, 'interleave':[], 'predictions':{}}
    noUpdateKeys = {}
    for record in request_data:
      # The key for segment_map must be hashable, so we cast the segment_key to a tuple.
      key = tuple(self._segment_key(record, *args, **kwargs))
      if key not in self.segment_map:
        # Check for and apply updates. Log if no update is found.
        segmentKey = list(key)
        if key not in noUpdateKeys and self._segment_update_available(segmentKey):
          self._reload_segment(segmentKey)
        else:
          # TODO(d-felix): Raise error or add an error message to the response.
          noUpdateKeys[key] = None
          # logger.info('Unavailable segment %s for model %s' % (key, self.name))
      if key not in collationData['records']:
        collationData['records'][key] = []
      collationData['records'][key].append(record)
      collationData['interleave'].append((key, len(collationData['records'][key]) - 1))

    # Generate predictions for each segment.
    for key in collationData['records']:

      # Before generating predictions, reload the segment if necessary
      segmentKey = list(key)
      if self._segment_update_available(segmentKey):
        self._reload_segment(segmentKey)

      dataset = self._dataset(collationData['records'][key], *args, **kwargs)
      ids = deepcopy(dataset.ids)

      if key in self.segment_map:
        # Returns a map from the id to the raw prediction
        predictions = self.segment_map[key].predict(dataset, *args, **kwargs)
        collationData['predictions'][key] = []
        for id in ids:
          prediction = predictions[id] if id in predictions else None
          collationData['predictions'][key].append(prediction)
      # If one exists, use fallback model for unavailable segments.
      elif self.fallback:
        # Returns an unformatted list of results.
        predictions = self.fallback.predict(collationData['records'][key], format_results=False,
            *args, **kwargs)
        collationData['predictions'][key] = predictions
      else:
        collationData['predictions'][key] = [None] * len(collationData['records'][key])
        continue

    # Collate and return predictions.
    predictions = []
    for entry in collationData['interleave']:
      # predict() may return fewer records than exist in the interleave data
      # TODO(d-felix): FIX THIS!
      predictions.append(collationData['predictions'][entry[0]][entry[1]])

    if format_results:
      return self._format_results(request_data, predictions, *args, **kwargs)
    else:
      return predictions

  def train_and_update(self, request_data, *args, **kwargs):
    segmentKeys = {}
    for record in request_data:
      key = self._segment_key(record, *args, **kwargs)
      if key is None:
        logger.info('Cannot train model: no segment key found for record %s' % record)
        continue
      # Dict key must be hashable.
      segmentKeys[tuple(key)] = key
    for keyTuple in segmentKeys:
      self._train_and_update(segmentKeys[keyTuple], *args, **kwargs)

  def _dataset(self, data, *args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')

  def _format_results(self, request_data, predictions, *args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')

  def _id(self, record, *args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')

  def _segment_key(self, record, *args, **kwargs):
    # Returns the segment key corresponding to the provided arguments.
    raise NotImplementedError('Not implemented for abstract base class')

  def _segment_keys(self):
    # Returns a list of all recognized segment keys for this model.
    raise NotImplementedError('Not implemented for abstract base class')

  def _reload_segment(self, segment_key):
    segment_keys_json = tuple(json.dumps(segment_key))
    cursor = connection.cursor()
    cursor.execute(_latest_segment_query(self.owner_id),
        [self.name, self.version, segment_keys_json, self.owner_id])
    for row in cursor.fetchall():
      segmentKeyJson, contentJson = row[1], row[3]
      segmentObj = ModelSegment.from_json(segmentKeyJson, contentJson)
      self.segment_map[tuple(segmentObj.key)] = segmentObj
      self.segment_load_times[tuple(segmentObj.key)] = timegm(datetime.utcnow().utctimetuple())

  def _segment_update_available(self, segment_key):
    segment_key_json = json.dumps(segment_key)
    key = _segment_update_cache_key(self.name, self.version, self.owner_id, segment_key_json)
    updateTimestamp = cache.get(key)
    if updateTimestamp and updateTimestamp > self.segment_load_times.get(tuple(segment_key), 0):
      return True
    return False

  def _train_and_update(self, segment_key, *args, **kwargs):
    if self.trainer is None:
      raise Exception('No training pipeline found for model %s' % self.name)
    if segment_key not in self._segment_keys():
      raise ValueError('Cannot train segment for model %s using invalid key %s' %
          (self.name, segment_key))

    invocation_context = {'segment_key': segment_key, 'owner_id': self.owner_id,
        'args': args, 'kwargs': kwargs}
    self.trainer.execute(invocation_context=invocation_context)
    pipeline = self.trainer

    if pipeline is None or not pipeline.configured_transformers:
      logger.warning('Training unsuccessful, not updating model for segment key %s' % segment_key)
      return

    # Update the live model.
    modelSegment = ModelSegment(segment_key, deepcopy(pipeline.configured_transformers))
    self.segment_map[tuple(segment_key)] = modelSegment

    # Persist the newly trained segment.
    deflations = [transformerregistry.deflate(s) for s in modelSegment.transformers]
    content = json.dumps({'transformers': deflations})
    segment_key_json = json.dumps(segment_key)
    owner = Customer.objects.get(pk=self.owner_id) if self.owner_id else None
    serializedModelSegment = SerializedModelSegment(content=content, content_type='JSON',
        model_name=self.name, segment_key=segment_key_json, owner=owner, version=self.version)
    serializedModelSegment.save()
    _cache_segment_update(self.name, self.version, self.owner_id, segment_key_json)


class ModelSegment(object):
  def __init__(self, key, transformers):
    self.key = key
    self.transformers = transformers

  @staticmethod
  def from_db_model(segment):
    transformers = []
    segmentDict = json.loads(segment.content)
    segmentKey = json.loads(segment.segment_key)
    for entry in segmentDict['transformers']:
      transformers.append(transformerregistry.inflate(entry))
    return ModelSegment(segmentKey, transformers)

  @staticmethod
  def from_json(segment_key_json, content_json):
    transformers = []
    segmentDict = json.loads(content_json)
    segmentKey = json.loads(segment_key_json)
    for entry in segmentDict['transformers']:
      transformers.append(transformerregistry.inflate(entry))
    return ModelSegment(segmentKey, transformers)

  def predict(self, dataset, *args, **kwargs):
    for transformer in self.transformers:
      dataset = transformer.transform(dataset=dataset, invocation_context={'segment_key': self.key})
    ret = {}
    for idx, prediction in enumerate(dataset.predictions):
      ret[dataset.ids[idx]] = prediction
    return ret
