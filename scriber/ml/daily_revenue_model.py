import logging

from appstore.constants import IOS_NEWSSTAND_GENRE_ID, IOS_OVERALL_CATEGORY_ID, IOS_PLATFORM_ID
from appstore.models import AppStoreCategory
from appstore.tasks import _TOP_COUNTRIES_ALPHA2
from core.timeutils import convert_tz
from core.utils import unpack
from customers.models import Customer
from datetime import date, datetime
from math import log
from ml.dataset import Dataset
from ml.extract import DbExtractor
from ml.label import DbLabeler, ModelPredictionLabeler
from ml.model import Model
from ml.overall_revenue_model import OverallRevenueModel
from ml.trainingpipeline import TrainingPipeline
from ml.transformer import (
    FormatterTransformer, IosRankTransformer, LogTransformer, PredictionExpTransformer,
    RemoveDateOutliersTransformer, RemoveForbiddenValuesTransformer, ReplaceNullTransformer,
    SciKitModelTransformer, ZScaleTransformer)
from pytz import timezone, utc
from sklearn import linear_model


logger = logging.getLogger(__name__)

MAX_RANK_AGE_INDEX = 8
RANK_DATA_BIRTHDAY = date(2015, 4, 9)

REGULARIZATION_STRENGTH = 0.01
AUGMENTED_REGULARIZATION_STRENGTH = 0.01

EXTRACTION_QUERY = """
  SELECT DISTINCT
    a.id app_id,
    date_range.date::date record_date
  FROM app_store_rank_requests r
  JOIN app_store_rank_types t ON (r.rank_type = t.id)
  JOIN app_store_regions rg ON (r.region = rg.id)
  JOIN countries c ON (rg.country = c.id)
  JOIN app_store_categories cg ON (r.category = cg.id)
  JOIN app_store_ranks k ON (r.id = k.rank_request)
  JOIN app_store_apps a ON (k.app = a.id)
  JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
  ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
      date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
  WHERE
    c.alpha2 = %s AND
    t.name = 'topgrossingapplications' AND
    cg.id = %s
  ;
  """

AUGMENTED_EXTRACTION_QUERY = """
  SELECT DISTINCT
    a.id app_id,
    date_range.date::date record_date
  FROM app_store_rank_requests r
  JOIN app_store_rank_types t ON (r.rank_type = t.id)
  JOIN app_store_regions rg ON (r.region = rg.id)
  JOIN countries c ON (rg.country = c.id)
  JOIN app_store_categories cg ON (r.category = cg.id)
  JOIN app_store_ranks k ON (r.id = k.rank_request)
  JOIN app_store_apps a ON (k.app = a.id)
  LEFT JOIN channel_products cp ON (a.track_id = cp.apple_identifier)
  JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
  ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
      date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
  WHERE
    c.alpha2 = %s AND
    t.name = 'topgrossingapplications' AND
    cg.id IN %s AND
    a.primary_category = %s AND
    (cp.apple_identifier IS NOT NULL OR cg.id = %s)
  ;
  """

# TODO(d-felix): Add support for an 'Overall' category model.
LABEL_QUERY = """
  SELECT
    LN(revenue.developer_proceeds_usd) log_revenue_usd,
    apps.id app_id,
    revenue.report_date
  FROM (
    SELECT
      rd.apple_identifier apple_identifier,
      rd.report_date report_date,
      SUM(arr.developer_proceeds_usd) developer_proceeds_usd
    FROM apple_report_records arr JOIN (
      SELECT
        li.apple_vendor_id vendor_id,
        cp.apple_identifier apple_identifier,
        cp.apple_sku sku,
        a.primary_category primary_category,
        ar.report_date report_date,
        MIN(ar.report_id) report_id
      FROM channel_products cp JOIN customer_external_login_info li ON (cp.customer_id = li.customer_id)
      JOIN customers c ON (cp.customer_id = c.customer_id)
      JOIN app_store_apps a ON cp.apple_identifier = a.track_id
      JOIN apple_reports ar ON li.apple_vendor_id = ar.vendor_id
      LEFT JOIN app_store_categories subcategories ON a.primary_category = subcategories.parent_category
      WHERE
        li.apple_vendor_id IS NOT NULL AND
        (a.primary_category = %s OR subcategories.id = %s) AND
        ar.report_date >= '2015-04-08' AND
        (c.training_privacy = %s OR c.customer_id = %s)
      GROUP BY 1, 2, 3, 4, 5
    ) rd
    ON (arr.report_id = rd.report_id AND
      (arr.apple_identifier = rd.apple_identifier OR arr.parent_identifier = rd.sku))
    WHERE arr.country_code = %s
    GROUP BY 1, 2
  ) revenue JOIN app_store_apps apps ON (revenue.apple_identifier = apps.track_id)
  WHERE revenue.developer_proceeds_usd > 0.0
  ;
  """

# TODO(d-felix): Document the expected request format.


class DailyRevenueModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(DailyRevenueModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country, category = segment_key[0], segment_key[1]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [str(RANK_DATA_BIRTHDAY), str(yesterday_pacific), country, category]

    self.trainer.extractor = DbExtractor(EXTRACTION_QUERY, processor=update_extraction_args)

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      owner_id = invocation_context.get('owner_id', None)

      # Default to an invalid customer_id if no owner_id is provided.
      # This allows us to use a single label query for both the public and
      # private model cases.
      owner_id = owner_id if owner_id is not None else -1
      country, category = segment_key[0], segment_key[1]
      labeler.arg_list = [category, category, Customer.PUBLIC, owner_id, country]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(IosRankTransformer('IosRankTransformer'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']

    self.trainer.append(ReplaceNullTransformer('ReplaceNull', value=250, features=rank_features))
    self.trainer.append(LogTransformer('Log', features=rank_features))
    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features))

    prediction_features = rank_features + ['is_monday', 'is_tuesday', 'is_wednesday', 'is_thursday',
                                           'is_friday', 'is_saturday']

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionExpTransformer('PredictionExp'))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for recordDict in data:
      record = self._id(recordDict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and predction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, recordDict in enumerate(request_data):
      recordDict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list consisting of the two-character
  # country code followed by the category ID. Example: ['US', 29]
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    category_id = record.get('category_id', 0)
    category = AppStoreCategory.objects.filter(id=category_id).first()
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2) or (category is None):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2, category_id]

  def _segment_keys(self):
    categories = AppStoreCategory.objects.filter(platform__name='iOS')
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      for category in categories:
        if category.parent_category != IOS_NEWSSTAND_GENRE_ID:
          keys.append([country, category.id])
    return keys


class AugmentedDailyRevenueModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(AugmentedDailyRevenueModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country, category = segment_key[0], segment_key[1]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [str(RANK_DATA_BIRTHDAY), str(yesterday_pacific), country,
                            (category, IOS_OVERALL_CATEGORY_ID), category, IOS_OVERALL_CATEGORY_ID]

    self.trainer.extractor = DbExtractor(
        AUGMENTED_EXTRACTION_QUERY,
        processor=update_extraction_args)

    def update_labeler_request_kwargs(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country, category = segment_key[0], segment_key[1]
      labeler.request_kwargs['country_alpha2'] = country
      labeler.request_kwargs['category_id'] = category

    def label_modifier_func(label, record_id):
      if label > 0.0:
        return log(float(label))
      else:
        return None

    self.trainer.labelers.append(ModelPredictionLabeler(
        # Use local name and version variables to prevent modelregistry changes
        # from affecting this labeler.
        model=OverallRevenueModel(name='OverallRevenue', version=1),
        label_modifier=label_modifier_func,
        request_kwargs={'platform_id': IOS_PLATFORM_ID},
        processor=update_labeler_request_kwargs))

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      owner_id = invocation_context.get('owner_id', None)

      # Default to an invalid customer_id if no owner_id is provided.
      # This allows us to use a single label query for both the public and
      # private model cases.
      owner_id = owner_id if owner_id is not None else -1
      country, category = segment_key[0], segment_key[1]
      labeler.arg_list = [category, category, Customer.PUBLIC, owner_id, country]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(IosRankTransformer('IosRankTransformer'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']

    self.trainer.append(ReplaceNullTransformer('ReplaceNull', value=250, features=rank_features))
    self.trainer.append(LogTransformer('Log', features=rank_features))
    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features))

    prediction_features = rank_features + ['is_monday', 'is_tuesday', 'is_wednesday', 'is_thursday',
                                           'is_friday', 'is_saturday']

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionExpTransformer('PredictionExp'))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for recordDict in data:
      record = self._id(recordDict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and prediction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, recordDict in enumerate(request_data):
      recordDict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list consisting of the two-character
  # country code followed by the category ID. Example: ['US', 29]
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    category_id = record.get('category_id', 0)
    category = AppStoreCategory.objects.filter(id=category_id).first()
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2) or (category is None):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2, category_id]

  def _segment_keys(self):
    categories = AppStoreCategory.objects.filter(platform__name='iOS')
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      for category in categories:
        if category.parent_category != IOS_NEWSSTAND_GENRE_ID:
          keys.append([country, category.id])
    return keys
