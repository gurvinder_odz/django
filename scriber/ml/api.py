from ml.modelregistry import MODELS

# TODO(d-felix): Deal with missing rank data, outlier removal, records with missing values,
# result set length, and target de-transformation.
# TODO(d-felix): train_model tasks for Google Play.
def predict(model_name=None, request_data=None, *args, **kwargs):
  if model_name not in MODELS:
    raise ValueError('Cannot make prediction for unrecognized model %s' % model_name)
  modelObj = MODELS[model_name]
  return modelObj.predict(request_data, *args, **kwargs)

def train_and_update(model_name=None, request_data=None, *args, **kwargs):
  if model_name not in MODELS:
    raise ValueError('Cannot train unrecognized model %s' % model_name)
  modelObj = MODELS[model_name]
  return modelObj.train_and_update(request_data, *args, **kwargs)
