from core.customfields import BigAutoField
from core.models import TimestampedModel
from customers.models import Customer
from django.db import models


class ModelSegment(TimestampedModel):
  id = BigAutoField(primary_key=True)
  content = models.TextField()
  content_type = models.CharField(max_length=64)
  model_name = models.CharField(max_length=256, db_index=True)
  segment_key = models.CharField(max_length=256, db_index=True)
  owner = models.ForeignKey(Customer, null=True, db_column='owner')
  version = models.IntegerField(null=True)

  class Meta:
    db_table = 'model_segments'
