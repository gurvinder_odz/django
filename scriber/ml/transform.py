import base64
import logging
import pickle

from core.db.dynamic import connection
from core.timeutils import convert_tz
from core.utils import unpack
from datetime import datetime
from dateutil import parser
from math import sqrt
from ml.dataset import Dataset
from pytz import timezone, utc
from sklearn.linear_model import Lasso

logger = logging.getLogger(__name__)


class Transformer(object):
  def __init__(self, name):
    self.name = name

  def transform(self, dataset, *args, **kwargs):
    raise NotImplementedError('transform not implemented for abstract base class')


class InflatableTransformer(Transformer):
  @staticmethod
  def inflate(*args, **kwargs):
    raise NotImplementedError('Not implemented for abstract base class')

  def deflate(self):
    raise NotImplementedError('Not implemented for abstract base class')


class TransformerSignature(object):
  def __init__(self, transformer, dataset=None, *args, **kwargs):
    self.transformer = transformer
    self.dataset = dataset
    self.t_args = args
    self.t_kwargs = kwargs

  # Allow the dataset to be overridden by an invocation argument.
  def invoke(self, dataset=None, invocation_context=None):
    d = self.dataset if dataset is None else dataset
    return self.transformer.transform(d, invocation_context=invocation_context,
        *self.t_args, **self.t_kwargs)


class GenericTransformer(Transformer):
  def __init__(self, name, func):
    super(GenericTransformer, self).__init__(name)
    self.transform_func = func

  def transform(self, dataset, *args, **kwargs):
    return self.transform_func(dataset, *args, **kwargs)


def _resolve_feature_index_list(dataset, features, feature_index_list):
  if features is None and feature_index_list is None:
    raise ValueError('Must provide a list of features or feature indices to transform')
  elif feature_index_list is None:
    feature_index_list = []
    for name in features:
      feature_index_list.append(dataset.feature_index(name))
  return feature_index_list


class SimpleFeatureTransformer(Transformer):
  def __init__(self, name, func):
    super(SimpleFeatureTransformer, self).__init__(name)
    self.transform_func = func

  def transform(self, dataset, features=None, feature_index_list=None, *args, **kwargs):
    feature_index_list = _resolve_feature_index_list(dataset, features, feature_index_list)
    new_dataset = Dataset.from_metadata(dataset.metadata)
    for row_idx, record in enumerate(dataset.data):
      new_record = record[:]
      for column_idx in feature_index_list:
        new_record[column_idx] = self.transform_func(record[column_idx], *args, **kwargs)
      if dataset.labels:
        new_dataset.append(new_record, id=dataset.ids[row_idx], label=dataset.labels[row_idx])
      else:
        new_dataset.append(new_record, id=dataset.ids[row_idx])
    return new_dataset


class SimplePredictionTransformer(Transformer):
  def __init__(self, name, func):
    super(SimplePredictionTransformer, self).__init__(name)
    self.transform_func = func

  def transform(self, dataset, *args, **kwargs):
    new_predictions = []
    for prediction in dataset.predictions:
      new_predictions.append(self.transform_func(prediction, *args, **kwargs))
    dataset.predictions = new_predictions
    return dataset


class SciKitModelTransformer(InflatableTransformer):
  def __init__(self, name, model):
    super(SciKitModelTransformer, self).__init__(name)
    self.model = model

  def transform(self, dataset, *args, **kwargs):
    # TODO(d-felix): Implement a custom LinearModelTransformer.
    if isinstance(self.model, Lasso) and dataset.data and \
        len(dataset.data[0]) > len(self.model.coef_):
      dataset.data = [record[:len(self.model.coef_)] for record in dataset.data]

    dataset.predictions = self.model.predict(dataset.data)
    return dataset

  def deflate(self):
    flatModel = base64.b64encode(pickle.dumps(self.model))
    return {'name': self.name, 'model': flatModel}

  @staticmethod
  def inflate(name, model, *args, **kwargs):
    inflatedModel = pickle.loads(base64.b64decode(model))
    return SciKitModelTransformer(name, inflatedModel)


def remove_features(dataset, features=None, feature_index_list=None, *args, **kwargs):
  feature_index_list = _resolve_feature_index_list(dataset, features, feature_index_list)
  dataset.metadata['feature_names'] = [entry for idx, entry in
      enumerate(dataset.metadata['feature_names']) if idx not in feature_index_list]
  new_dataset = Dataset.from_metadata(dataset.metadata)
  for row_idx, record in enumerate(dataset.data):
    new_record = [entry for idx, entry in enumerate(record) if idx not in feature_index_list]
    if dataset.labels:
      new_dataset.append(new_record, id=dataset.ids[row_idx], label=dataset.labels[row_idx])
    else:
      new_dataset.append(new_record, id=dataset.ids[row_idx])
  return new_dataset


# By default, removes any record having a null value for one of the provided features.
def remove_forbidden_values(dataset, features=None, feature_index_list=None,
    forbidden_values=[None], *args, **kwargs):
  feature_index_list = _resolve_feature_index_list(dataset, features, feature_index_list)
  new_dataset = Dataset.from_metadata(dataset.metadata)
  for row_idx, record in enumerate(dataset.data):
    for idx, column_idx in enumerate(feature_index_list):
      if record[column_idx] in forbidden_values:
        break
      elif idx == len(feature_index_list) - 1:
        if dataset.labels:
          new_dataset.append(record, id=dataset.ids[row_idx], label=dataset.labels[row_idx])
        else:
          new_dataset.append(record, id=dataset.ids[row_idx])
  return new_dataset


def remove_outliers(dataset, features=None, feature_index_list=None,
    lt=None, lte=None, gt=None, gte=None, *args, **kwargs):
  feature_index_list = _resolve_feature_index_list(dataset, features, feature_index_list)
  new_dataset = Dataset.from_metadata(dataset.metadata)
  for row_idx, record in enumerate(dataset.data):
    for idx, column_idx in enumerate(feature_index_list):
      if ((lt is not None and record[column_idx] < lt[idx]) or
          (lte is not None and record[column_idx] <= lte[idx]) or
          (gt is not None and record[column_idx] > gt[idx]) or
          (gte is not None and record[column_idx] >= gte[idx])):
        break
      elif idx == len(feature_index_list) - 1:
        if dataset.labels:
          new_dataset.append(record, id=dataset.ids[row_idx], label=dataset.labels[row_idx])
        else:
          new_dataset.append(record, id=dataset.ids[row_idx])
  return new_dataset


def remove_date_outliers(dataset, features=None, feature_index_list=None,
    lt=None, lte=None, gt=None, gte=None, *args, **kwargs):
  lt = [parser.parse(x).date() for x in lt] if lt is not None else None
  lte = [parser.parse(x).date() for x in lte] if lte is not None else None
  gt = [parser.parse(x).date() for x in gt] if gt is not None else None
  gte = [parser.parse(x).date() for x in gte] if gte is not None else None
  return remove_outliers(dataset, features, feature_index_list, lt, lte, gt, gte, *args, **kwargs)


def z_scale(dataset, features=None, feature_index_list=None, means=None, stdevs=None,
    *args, **kwargs):
  feature_index_list = _resolve_feature_index_list(dataset, features, feature_index_list)
  if means is None or stdevs is None:
    raise ValueError('Means and standard deviations must be provided for z-scaling')
  new_dataset = Dataset.from_metadata(dataset.metadata)
  for row_idx, record in enumerate(dataset.data):
    new_record = record[:]
    for idx, column_idx in enumerate(feature_index_list):
      new_record[column_idx] = ((record[column_idx] - means[idx]) /
          (stdevs[idx] if stdevs[idx] > 0.0 else 1.0))
    if dataset.labels:
      new_dataset.append(new_record, id=dataset.ids[row_idx], label=dataset.labels[row_idx])
    else:
      new_dataset.append(new_record, id=dataset.ids[row_idx])
  return new_dataset


class DeferredTransformerSignature(object):
  def __init__(self, *args, **kwargs):
    self.signature = None
    self.t_args = args
    self.t_kwargs = kwargs

  def s(self, dataset):
    raise NotImplementedError('Not implemented for abstract base class')

  def invoke(self, dataset, invocation_context=None, *args, **kwargs):
    self.signature = self.s(dataset)
    return self.signature.invoke(dataset=dataset, invocation_context=invocation_context)


class SimpleDeferredTransformerSignature(DeferredTransformerSignature):
  def __init__(self, signature):
    self.signature = signature

  def s(self, dataset):
    return self.signature


# TODO(d-felix): Handle the transformer name constants below more cleanly.
class ZScaleTransformerSignatureFactory(DeferredTransformerSignature):
  def s(self, dataset):
    features, feature_index_list = unpack(self.t_kwargs, 'features', 'feature_index_list')
    feature_index_list = _resolve_feature_index_list(dataset, features, feature_index_list)
    n = len(dataset.data)
    if n == 0:
      raise ValueError('Cannot create Z-scaling transformer signature from dataset without records')
    v = len(feature_index_list)
    sums = [0.0] * v
    sum_sqs = [0.0] * v

    for record in dataset.data:
      for idx, column_idx in enumerate(feature_index_list):
        sums[idx] += float(record[column_idx])
        sum_sqs[idx] += float(record[column_idx]) * float(record[column_idx])

    means = [s/n for s in sums]
    variances = [0.0] * v
    for idx in range(v):
      variances[idx] = (sum_sqs[idx] / n) - (means[idx] ** 2)
    stdevs = [sqrt(x) if x > 0.0 else 0.0 for x in variances]

    z_scaler_sig = TransformerSignature(GenericTransformer('ZScale', z_scale), features=features,
        feature_index_list=feature_index_list, means=means, stdevs=stdevs)

    return z_scaler_sig


class SciKitModelTrainer(DeferredTransformerSignature):
  def __init__(self, model, *args, **kwargs):
    self.model = model
    self.signature = None
    self.t_args = args
    self.t_kwargs = kwargs

  def s(self, dataset):
    self.model.fit(dataset.data, dataset.labels)
    model_sig = TransformerSignature(SciKitModelTransformer('SciKitModel', self.model))
    return model_sig


# TODO(d-felix): This transformer is specific to the DailyRevenueModel. Allow
# Models to define their own transformers, and check these private sets for
# inflation/deflation instructions before falling back to the registry.
#
# TODO(d-felix): Query execution time is horrible. Fix this.
class RankTransformer(Transformer):
  def __init__(self, name):
    super(RankTransformer, self).__init__(name)
    self.RANK_DATA_BIRTHDAY_STR = '2015-04-09'
    self.QUERY = """
      SELECT
        s.app_id app_id,
        s.record_date record_date,
        MAX(CASE WHEN(s.rank_age_index = 1) THEN s.rank ELSE NULL END) rank_1,
        MAX(CASE WHEN(s.rank_age_index = 2) THEN s.rank ELSE NULL END) rank_2,
        MAX(CASE WHEN(s.rank_age_index = 3) THEN s.rank ELSE NULL END) rank_3,
        MAX(CASE WHEN(s.rank_age_index = 4) THEN s.rank ELSE NULL END) rank_4,
        MAX(CASE WHEN(s.rank_age_index = 5) THEN s.rank ELSE NULL END) rank_5,
        MAX(CASE WHEN(s.rank_age_index = 6) THEN s.rank ELSE NULL END) rank_6,
        MAX(CASE WHEN(s.rank_age_index = 7) THEN s.rank ELSE NULL END) rank_7,
        MAX(CASE WHEN(s.rank_age_index = 8) THEN s.rank ELSE NULL END) rank_8,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 1) THEN 1 ELSE 0 END) is_monday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 2) THEN 1 ELSE 0 END) is_tuesday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 3) THEN 1 ELSE 0 END) is_wednesday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 4) THEN 1 ELSE 0 END) is_thursday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 5) THEN 1 ELSE 0 END) is_friday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 6) THEN 1 ELSE 0 END) is_saturday
      FROM (
        SELECT
          a.id app_id,
          date_range.date::date record_date,
          k.rank rank,
          ceil(extract('epoch' from ((date_range.date + '1 day') - (r.time AT TIME ZONE
              'America/Los_Angeles')))/10800) rank_age_index
        FROM app_store_rank_requests r
        JOIN app_store_rank_types t ON (r.rank_type = t.id)
        JOIN app_store_regions rg ON (r.region = rg.id)
        JOIN countries c ON (rg.country = c.id)
        JOIN app_store_categories cg ON (r.category = cg.id)
        JOIN app_store_ranks k ON (r.id = k.rank_request)
        JOIN app_store_apps a ON (k.app = a.id)
        JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
        ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
            date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
        WHERE
          c.alpha2 = %s AND
          t.name = 'topgrossingapplications' AND
          cg.id = %s AND
          (a.id, date_range.date::date) IN %s
      ) s
      GROUP BY 1, 2
      ;
    """

  def transform(self, dataset, *args, **kwargs):
    (invocationContext, ) = unpack(kwargs, 'invocation_context')
    segmentKey = invocationContext['segment_key']
    country, category = segmentKey[0], segmentKey[1]

    idTuple = tuple(dataset.ids)
    idLength = len(idTuple[0])
    yesterdayPacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
    cursor = connection.cursor()
    cursor.execute(self.QUERY,
        [self.RANK_DATA_BIRTHDAY_STR, str(yesterdayPacific.date()), country, category, idTuple])
    nFeatures = len(cursor.cursor.description) - idLength
    if nFeatures <= 0:
      raise ValueError('No new features found by RankTransformer query')

    newFeatureNames = [c.name for c in cursor.cursor.description][idLength:]
    dataset.metadata['feature_names'].extend(newFeatureNames)

    newFeatures = {}
    for row in cursor.fetchall():
      key = row[:idLength]
      newFeatures[key] = row[idLength:]
    for idx, key in enumerate(dataset.ids):
      if key in newFeatures:
        dataset.data[idx].extend(newFeatures[key])
      else:
        dataset.data[idx].extend([None] * nFeatures)

    return dataset


# TODO(d-felix): Make this a versioned transformer, or allow the query to be
# specified as an argument.
#
# TODO(d-felix): This transformer is specific to the DailyRevenueModel. Allow
# Models to define their own transformers, and check these private sets for
# inflation/deflation instructions before falling back to the registry.
#
# TODO(d-felix): Query execution time is horrible. Fix this.
class IosRankTransformer(Transformer):
  def __init__(self, name):
    super(IosRankTransformer, self).__init__(name)
    self.RANK_DATA_BIRTHDAY_STR = '2015-04-09'
    self.QUERY = """
      SELECT
        s.app_id app_id,
        s.record_date record_date,
        MAX(CASE WHEN(s.rank_age_index = 1 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_1,
        MAX(CASE WHEN(s.rank_age_index = 2 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_2,
        MAX(CASE WHEN(s.rank_age_index = 3 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_3,
        MAX(CASE WHEN(s.rank_age_index = 4 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_4,
        MAX(CASE WHEN(s.rank_age_index = 5 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_5,
        MAX(CASE WHEN(s.rank_age_index = 6 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_6,
        MAX(CASE WHEN(s.rank_age_index = 7 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_7,
        MAX(CASE WHEN(s.rank_age_index = 8 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) rank_8,
        MAX(CASE WHEN(s.rank_age_index = 1 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_1,
        MAX(CASE WHEN(s.rank_age_index = 2 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_2,
        MAX(CASE WHEN(s.rank_age_index = 3 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_3,
        MAX(CASE WHEN(s.rank_age_index = 4 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_4,
        MAX(CASE WHEN(s.rank_age_index = 5 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_5,
        MAX(CASE WHEN(s.rank_age_index = 6 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_6,
        MAX(CASE WHEN(s.rank_age_index = 7 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_7,
        MAX(CASE WHEN(s.rank_age_index = 8 AND s.device = 'iPad') THEN s.rank ELSE NULL END) rank_ipad_8,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 1) THEN 1 ELSE 0 END) is_monday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 2) THEN 1 ELSE 0 END) is_tuesday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 3) THEN 1 ELSE 0 END) is_wednesday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 4) THEN 1 ELSE 0 END) is_thursday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 5) THEN 1 ELSE 0 END) is_friday,
        MAX(CASE WHEN(EXTRACT(ISODOW FROM s.record_date) = 6) THEN 1 ELSE 0 END) is_saturday,
        MIN(CASE WHEN(s.rank_age_index <= 24 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) best_rank_3d,
        MIN(CASE WHEN(s.rank_age_index <= 24 AND s.device = 'iPad') THEN s.rank ELSE NULL END) best_rank_ipad_3d,
        MIN(CASE WHEN(s.rank_age_index <= 8 AND s.device = 'iPhone') THEN s.rank ELSE NULL END) best_rank_1d,
        MIN(CASE WHEN(s.rank_age_index <= 8 AND s.device = 'iPad') THEN s.rank ELSE NULL END) best_rank_ipad_1d
      FROM (
        SELECT
          k.app app_id,
          date_range.date::date record_date,
          k.rank rank,
          ceil(extract('epoch' from ((date_range.date + '1 day') - (r.time AT TIME ZONE
              'America/Los_Angeles')))/10800) rank_age_index,
          CASE WHEN (t.name = 'topgrossingapplications') THEN 'iPhone' ELSE 'iPad' END device
        FROM app_store_rank_requests r
        JOIN app_store_rank_types t ON (r.rank_type = t.id)
        JOIN app_store_regions rg ON (r.region = rg.id)
        JOIN countries c ON (rg.country = c.id)
        JOIN app_store_categories cg ON (r.category = cg.id)
        JOIN app_store_ranks k ON (r.id = k.rank_request)
        JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
        ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
            date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
        WHERE
          c.alpha2 = %s AND
          t.name IN ('topgrossingapplications', 'topgrossingipadapplications') AND
          cg.id = %s AND
          (k.app, date_range.date::date) IN %s
      ) s
      GROUP BY 1, 2
      ;
    """

  def transform(self, dataset, *args, **kwargs):
    (invocationContext, ) = unpack(kwargs, 'invocation_context')
    segmentKey = invocationContext['segment_key']
    country, category = segmentKey[0], segmentKey[1]

    idTuple = tuple(dataset.ids)
    idLength = len(idTuple[0])
    yesterdayPacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
    cursor = connection.cursor()
    cursor.execute(self.QUERY,
        [self.RANK_DATA_BIRTHDAY_STR, str(yesterdayPacific.date()), country, category, idTuple])
    nFeatures = len(cursor.cursor.description) - idLength
    if nFeatures <= 0:
      raise ValueError('No new features found by RankTransformer query')

    newFeatureNames = [c.name for c in cursor.cursor.description][idLength:]
    dataset.metadata['feature_names'].extend(newFeatureNames)

    newFeatures = {}
    for row in cursor.fetchall():
      key = row[:idLength]
      newFeatures[key] = row[idLength:]
    for idx, key in enumerate(dataset.ids):
      if key in newFeatures:
        dataset.data[idx].extend(newFeatures[key])
      else:
        dataset.data[idx].extend([None] * nFeatures)

    return dataset
