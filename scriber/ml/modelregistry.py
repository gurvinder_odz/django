from ml.daily_revenue_model import AugmentedDailyRevenueModel, DailyRevenueModel
from ml.download_model import AugmentedFreeDownloadModel, AugmentedPaidDownloadModel
from ml.download_model import OverallFreeDownloadModel, OverallPaidDownloadModel
from ml.overall_revenue_model import OverallRevenueModel

AUGMENTED_DAILY_REVENUE = 'AugmentedDailyRevenue'
AUGMENTED_FREE_DOWNLOAD = 'AugmentedFreeDownload'
AUGMENTED_PAID_DOWNLOAD = 'AugmentedPaidDownload'
DAILY_REVENUE = 'DailyRevenue'
FREE_DOWNLOAD = 'FreeDownload'
OVERALL_FREE_DOWNLOAD = 'OverallFreeDownload'
OVERALL_PAID_DOWNLOAD = 'OverallPaidDownload'
OVERALL_REVENUE = 'OverallRevenue'
PAID_DOWNLOAD = 'PaidDownload'

CLASS = 'class'
NAME = 'name'
VERSION = 'version'

REGISTRY = {
  AUGMENTED_DAILY_REVENUE: {
    CLASS: AugmentedDailyRevenueModel,
    NAME: AUGMENTED_DAILY_REVENUE,
    VERSION: 1,
  },
  DAILY_REVENUE: {
    CLASS: AugmentedDailyRevenueModel,
    NAME: AUGMENTED_DAILY_REVENUE,
    VERSION: 1,
  },
  FREE_DOWNLOAD: {
    CLASS: AugmentedFreeDownloadModel,
    NAME: AUGMENTED_FREE_DOWNLOAD,
    VERSION: 1,
  },
  OVERALL_FREE_DOWNLOAD: {
    CLASS: OverallFreeDownloadModel,
    NAME: OVERALL_FREE_DOWNLOAD,
    VERSION: 1,
  },
  OVERALL_PAID_DOWNLOAD: {
    CLASS: OverallPaidDownloadModel,
    NAME: OVERALL_PAID_DOWNLOAD,
    VERSION: 2,
  },
  OVERALL_REVENUE: {
    CLASS: OverallRevenueModel,
    NAME: OVERALL_REVENUE,
    VERSION: 1,
  },
  PAID_DOWNLOAD: {
    CLASS: AugmentedPaidDownloadModel,
    NAME: AUGMENTED_PAID_DOWNLOAD,
    VERSION: 1,
  },
}

MODELS = {}
for modelKey in REGISTRY:
  modelClass, modelName, modelVersion = (REGISTRY[modelKey][CLASS], REGISTRY[modelKey][NAME],
      REGISTRY[modelKey][VERSION])
  MODELS[modelKey] = modelClass(name=modelName, version=modelVersion)
