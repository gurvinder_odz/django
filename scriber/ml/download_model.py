import logging

from appstore.constants import (
    IOS_NEWSSTAND_GENRE_ID, IOS_OVERALL_CATEGORY_ID, IOS_PLATFORM_ID, MAX_IOS_RANK)
from appstore.models import AppStoreCategory
from appstore.tasks import _TOP_COUNTRIES_ALPHA2
from core.timeutils import convert_tz
from core.utils import unpack
from customers.models import Customer
from datetime import date, datetime
from math import log
from metrics.models import APP_BUNDLE_RECORDS, FREE_OR_PAID_APP_RECORDS, PAID_APP_RECORDS
from ml.overall_revenue_model import OverallRevenueModel
from ml.dataset import Dataset
from ml.extract import DbExtractor
from ml.label import DbLabeler, DictionaryLabeler, ModelPredictionLabeler
from ml.model import Model
from ml.trainingpipeline import TrainingPipeline
from ml.transformer import (
    FormatterTransformer, IosRankTransformer, LogTransformer, PowTransformer,
    PredictionExpTransformer, PredictionPowTransformer, RemoveDateOutliersTransformer,
    RemoveForbiddenValuesTransformer, ReplaceNullTransformer, SciKitModelTransformer,
    ZScaleTransformer)
from pytz import timezone, utc
from sklearn import linear_model


logger = logging.getLogger(__name__)

DOWNLOAD_IDENTIFIER_TUPLE = tuple(APP_BUNDLE_RECORDS + FREE_OR_PAID_APP_RECORDS + PAID_APP_RECORDS)
MAX_RANK_AGE_INDEX = 8
RANK_DATA_BIRTHDAY = date(2015, 4, 9)

AUGMENTED_FREE_REGULARIZATION_STRENGTH = 0.004
AUGMENTED_PAID_REGULARIZATION_STRENGTH = 0.004
OVERALL_FREE_REGULARIZATION_STRENGTH = 0.004
OVERALL_PAID_REGULARIZATION_STRENGTH = 0.004

# TODO(d-felix): Automate the identification and price determination of top
# IAP-less paid apps for each country. The usefulness of the following list may
# be limited to the US.
NO_IAP_APP_PRICES = {
    98400: 0.99,  # com.ea.tgolinc
    98402: 0.99,  # com.ea.monoclinc
    92514: 6.99,  # com.mojang.minecraftpe
    93293: 1.99,  # com.robtop.geometryjump
    94113: 0.99,  # com.flipline.papasfreezeriatogo
    93055: 2.99,  # com.scottgames.fivenights3
    93349: 2.99,  # com.3minutegames.lifeline
}

FREE_APP_DOWNLOADS_DATE = date(2015, 6, 17)
FREE_APP_DOWNLOADS = {
    15036: 66491,  # com.etermax.preguntados
    15037: 138615,  # com.miniclip.8ballpoolmult
    15040: 74816,  # com.wb.MK.Brawler2015
    15042: 62333,  # com.midasplayer.apps.candycrushsodasaga
    15046: 38829,  # com.blizzard.wtcg.hearthstone
    15047: 99784,  # com.supercell.magic
    15049: 78981,  # com.midasplayer.apps.candycrushsaga
    15050: 103949,  # com.hipsterwhale.crossy
    15053: 40529,  # com.mobilityware.SolitaireFree
    15054: 41093,  # com.kabam.ff7
    15055: 39109,  # com.machinezone.ody0
    15057: 190631,  # com.kiloo.subwaysurfers
    15059: 45695,  # com.prettysimple.criminalcase
    15061: 51926,  # com.gameloft.despicableme2
    15062: 39247,  # com.aagame.aagame
    15064: 43617,  # com.halfbrick.FruitNinjaLite
    15067: 40395,  # com.nordcurrent.canteenhd
    15069: 39526,  # com.topfreegames.bikeracefree
    15072: 40247,  # com.imangi.templerun2
    15075: 41245,  # com.supercell.reef
    15087: 39391,  # com.brighthouse.BlackTiles
    15089: 38960,  # com.imangi.templerun
    15096: 39823,  # com.halfbrick.jetpack
    15099: 38684,  # com.ketchapp.zigzaggame
    15101: 40673,  # com.bigduckgames.flow
    15122: 39677,  # com.gameloft.asphalt8
    15161: 238783,  # com.elex-tech.ClashOfKings
    15183: 39964,  # com.robtop.geometryjumplite
    16451: 83143,  # com.crustalli.ktbnmfree
    16726: 207963,  # com.ludia.jurassicworld
    16766: 87307,  # com.naturalmotion.j3n64
    18034: 41534,  # com.ea.easportsufc.bv
    18400: 155954,  # com.alegrium.billionaire
    22805: 91456,  # com.cocoplay.cocoiceprincess
    24510: 40813,  # com.ea.sims3deluxe.ipad.inc
    24598: 60249,  # com.miniclip.dudeperfect
    36384: 41389,  # com.crazylion.buddyman.kick2.ninja
    67251: 95623,  # com.rovio.angrybirdsfight
    93298: 58168,  # com.happymagenta.ff
    103825: 54009,  # com.FDGEntertainment.Slayin
    214170: 40963,  # com.5thplanetgames.pets
    355299: 220298,  # com.midasplayer.apps.alphabettysaga
    397332: 121278,  # com.happymagenta.ssp
    750635: 56098,  # com.changaming.dwtlw
    887144: 40107,  # mobi.gameguru.loopdrive
    888535: 70660,  # com.iaendi.goalhero
    933681: 173295,  # com.ketchapp.triplejump
    934025: 47777,  # com.turner.cnblamburger
    956829: 258427,  # com.bethsoft.falloutshelter
    964906: 49856,  # com.infinite2.hd
}

OVERALL_EXTRACTION_QUERY = """
  SELECT DISTINCT
    a.id app_id,
    date_range.date::date record_date
  FROM app_store_rank_requests r
  JOIN app_store_rank_types t ON (r.rank_type = t.id)
  JOIN app_store_regions rg ON (r.region = rg.id)
  JOIN countries c ON (rg.country = c.id)
  JOIN app_store_categories cg ON (r.category = cg.id)
  JOIN app_store_ranks k ON (r.id = k.rank_request)
  JOIN app_store_apps a ON (k.app = a.id)
  LEFT JOIN channel_products cp ON (a.track_id = cp.apple_identifier)
  JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
  ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
      date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
  WHERE
    c.alpha2 = %s AND
    t.name = %s AND
    cg.id = %s AND
    (cp.apple_identifier IS NOT NULL OR a.id IN %s)
  ;
  """

AUGMENTED_EXTRACTION_QUERY = """
  SELECT DISTINCT
    a.id app_id,
    date_range.date::date record_date
  FROM app_store_rank_requests r
  JOIN app_store_rank_types t ON (r.rank_type = t.id)
  JOIN app_store_regions rg ON (r.region = rg.id)
  JOIN countries c ON (rg.country = c.id)
  JOIN app_store_categories cg ON (r.category = cg.id)
  JOIN app_store_ranks k ON (r.id = k.rank_request)
  JOIN app_store_apps a ON (k.app = a.id)
  LEFT JOIN channel_products cp ON (a.track_id = cp.apple_identifier)
  JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
  ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
      date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
  WHERE
    c.alpha2 = %s AND
    t.name = %s AND
    cg.id IN %s AND
    a.primary_category = %s AND
    (cp.apple_identifier IS NOT NULL OR cg.id = %s)
  ;
  """

LABEL_QUERY = """
  SELECT
    LN(1 + dl.downloads) log_downloads,
    dl.app_id app_id,
    dl.report_date
  FROM (
    SELECT
      rd.app_id app_id,
      rd.apple_identifier apple_identifier,
      rd.report_date report_date,
      SUM(arr.units) downloads
    FROM apple_report_records arr JOIN (
      SELECT
        li.apple_vendor_id vendor_id,
        cp.apple_identifier apple_identifier,
        cp.apple_sku sku,
        a.id app_id,
        a.primary_category primary_category,
        ar.report_date report_date,
        MIN(ar.report_id) report_id
      FROM channel_products cp JOIN customer_external_login_info li ON (cp.customer_id = li.customer_id)
      JOIN customers c ON (cp.customer_id = c.customer_id)
      JOIN app_store_apps a ON cp.apple_identifier = a.track_id
      JOIN apple_reports ar ON li.apple_vendor_id = ar.vendor_id
      WHERE
        li.apple_vendor_id IS NOT NULL AND
        ar.report_date >= '2015-04-08' AND
        (c.training_privacy = %s OR c.customer_id = %s)
      GROUP BY 1, 2, 3, 4, 5, 6
    ) rd
    ON (arr.report_id = rd.report_id AND
      (arr.apple_identifier = rd.apple_identifier OR arr.parent_identifier = rd.sku))
    WHERE arr.country_code = %s AND
      arr.product_type_identifier IN %s
    GROUP BY 1, 2, 3
  ) dl
  WHERE dl.downloads >= 0
  ;
  """

# TODO(d-felix): Document the expected request format.


class OverallPaidDownloadModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(OverallPaidDownloadModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [
          str(RANK_DATA_BIRTHDAY),
          str(yesterday_pacific),
          country,
          'toppaidapplications',
          IOS_OVERALL_CATEGORY_ID,
          tuple(NO_IAP_APP_PRICES.keys())]

    self.trainer.extractor = DbExtractor(
        OVERALL_EXTRACTION_QUERY,
        processor=update_extraction_args)

    def update_labeler_request_kwargs(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      labeler.request_kwargs['country_alpha2'] = country

    def label_modifier(label, record_id):
      app_id = record_id[0]
      if app_id in NO_IAP_APP_PRICES:
        return log(1 + (label / NO_IAP_APP_PRICES[app_id]))
      else:
        return None

    self.trainer.labelers.append(ModelPredictionLabeler(
        # Use local name and version variables to prevent modelregistry changes
        # from affecting this labeler.
        model=OverallRevenueModel(name='OverallRevenue', version=1),
        label_modifier=label_modifier,
        request_kwargs={'platform_id': IOS_PLATFORM_ID},
        processor=update_labeler_request_kwargs))

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]

      # Default to an invalid owner_id. This allows us to use a single label
      # query for both the overall and augmented models.
      owner_id = -1
      labeler.arg_list = [Customer.PUBLIC, owner_id, country, DOWNLOAD_IDENTIFIER_TUPLE]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(
        IosRankTransformer(
            'IosRankTransformer',
            iphone_rank_type='toppaidapplications',
            ipad_rank_type='toppaidipadapplications'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    best_rank_features = ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']
    rank_features += best_rank_features

    self.trainer.append(
        ReplaceNullTransformer(
            'ReplaceNull',
            value=MAX_IOS_RANK,
            features=rank_features[:]))
    self.trainer.append(LogTransformer('Log', features=rank_features[:]))

    best_rank_sq_features = ['best_rank_3d_sq', 'best_rank_ipad_3d_sq', 'best_rank_1d_sq',
                             'best_rank_ipad_1d_sq']
    rank_features += best_rank_sq_features
    self.trainer.append(PowTransformer('Pow', power=2, features=best_rank_features[:],
                                       outputs=best_rank_sq_features[:]))

    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features[:]))

    prediction_features = rank_features + ['is_monday', 'is_tuesday', 'is_wednesday', 'is_thursday',
                                           'is_friday', 'is_saturday']

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features[:]))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=OVERALL_PAID_REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionExpTransformer('PredictionExp'))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for record_dict in data:
      record = self._id(record_dict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and predction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, record_dict in enumerate(request_data):
      record_dict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list containing only the two-character
  # country code. Example: ['US']
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2]

  def _segment_keys(self):
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      keys.append([country])
    return keys


class AugmentedPaidDownloadModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(AugmentedPaidDownloadModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country, category = segment_key[0], segment_key[1]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [
          str(RANK_DATA_BIRTHDAY),
          str(yesterday_pacific),
          country,
          'toppaidapplications',
          (category, IOS_OVERALL_CATEGORY_ID),
          category,
          IOS_OVERALL_CATEGORY_ID]

    self.trainer.extractor = DbExtractor(
        AUGMENTED_EXTRACTION_QUERY,
        processor=update_extraction_args)

    def update_labeler_request_kwargs(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      labeler.request_kwargs['country_alpha2'] = country

    def label_modifier(label, record_id):
      if label >= 0.0:
        return log(1 + float(label))
      else:
        return None

    self.trainer.labelers.append(ModelPredictionLabeler(
        # Use local name and version variables to prevent modelregistry changes
        # from affecting this labeler.
        model=OverallPaidDownloadModel(name='OverallPaidDownload', version=2),
        label_modifier=label_modifier,
        request_kwargs={'platform_id': IOS_PLATFORM_ID},
        processor=update_labeler_request_kwargs))

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      owner_id = invocation_context.get('owner_id', None)

      # Default to an invalid customer_id if no owner_id is provided.
      # This allows us to use a single label query for both the public and
      # private model cases.
      owner_id = owner_id if owner_id is not None else -1
      labeler.arg_list = [Customer.PUBLIC, owner_id, country, DOWNLOAD_IDENTIFIER_TUPLE]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(
        IosRankTransformer(
            'IosRankTransformer',
            iphone_rank_type='toppaidapplications',
            ipad_rank_type='toppaidipadapplications'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    best_rank_features = ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']
    rank_features += best_rank_features

    self.trainer.append(
        ReplaceNullTransformer(
            'ReplaceNull',
            value=MAX_IOS_RANK,
            features=rank_features[:]))
    self.trainer.append(LogTransformer('Log', features=rank_features[:]))

    best_rank_sq_features = ['best_rank_3d_sq', 'best_rank_ipad_3d_sq', 'best_rank_1d_sq',
                             'best_rank_ipad_1d_sq']
    rank_features += best_rank_sq_features
    self.trainer.append(PowTransformer('Pow', power=2, features=best_rank_features[:],
                                       outputs=best_rank_sq_features[:]))

    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features[:]))

    prediction_features = rank_features + ['is_monday', 'is_tuesday', 'is_wednesday', 'is_thursday',
                                           'is_friday', 'is_saturday']

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features[:]))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=AUGMENTED_PAID_REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionExpTransformer('PredictionExp'))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for record_dict in data:
      record = self._id(record_dict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and prediction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, record_dict in enumerate(request_data):
      record_dict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list consisting of the two-character
  # country code followed by the category ID. Example: ['US', 29]
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    category_id = record.get('category_id', 0)
    category = AppStoreCategory.objects.filter(id=category_id).first()
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2) or (category is None):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2, category_id]

  def _segment_keys(self):
    categories = AppStoreCategory.objects.filter(platform__name='iOS')
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      for category in categories:
        if category.parent_category != IOS_NEWSSTAND_GENRE_ID:
          keys.append([country, category.id])
    return keys


class OverallFreeDownloadModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(OverallFreeDownloadModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [
          str(RANK_DATA_BIRTHDAY),
          str(yesterday_pacific),
          country,
          'topfreeapplications',
          IOS_OVERALL_CATEGORY_ID,
          tuple(FREE_APP_DOWNLOADS.keys())]

    self.trainer.extractor = DbExtractor(
        OVERALL_EXTRACTION_QUERY,
        processor=update_extraction_args)

    # The hardcoded download estimates are for the US.
    free_downloads = {('US',): {}}
    for app_id in FREE_APP_DOWNLOADS:
      key = (app_id, FREE_APP_DOWNLOADS_DATE)
      free_downloads[('US',)][key] = log(1 + float(FREE_APP_DOWNLOADS[app_id]))
    self.trainer.labelers.append(DictionaryLabeler(free_downloads))

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]

      # Default to an invalid owner_id. This allows us to use a single label
      # query for both the overall and augmented models.
      owner_id = -1
      labeler.arg_list = [Customer.PUBLIC, owner_id, country, DOWNLOAD_IDENTIFIER_TUPLE]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(
        IosRankTransformer(
            'IosRankTransformer',
            iphone_rank_type='topfreeapplications',
            ipad_rank_type='topfreeipadapplications'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    best_rank_features = ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']
    rank_features += best_rank_features

    self.trainer.append(
        ReplaceNullTransformer(
            'ReplaceNull',
            value=MAX_IOS_RANK,
            features=rank_features[:]))
    self.trainer.append(LogTransformer('Log', features=rank_features[:]))

    best_rank_sq_features = ['best_rank_3d_sq', 'best_rank_ipad_3d_sq', 'best_rank_1d_sq',
                             'best_rank_ipad_1d_sq']
    rank_features += best_rank_sq_features
    self.trainer.append(PowTransformer('Pow', power=2, features=best_rank_features[:],
                                       outputs=best_rank_sq_features[:]))

    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features[:]))

    # Exclude day of week variables since the hardcoded free app download
    # estimates are biased.
    prediction_features = rank_features

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features[:]))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=OVERALL_FREE_REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionExpTransformer('PredictionExp'))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for record_dict in data:
      record = self._id(record_dict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and predction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, record_dict in enumerate(request_data):
      record_dict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list containing only the two-character
  # country code. Example: ['US']
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2]

  def _segment_keys(self):
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      keys.append([country])
    return keys


class AugmentedFreeDownloadModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(AugmentedFreeDownloadModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country, category = segment_key[0], segment_key[1]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [
          str(RANK_DATA_BIRTHDAY),
          str(yesterday_pacific),
          country,
          'topfreeapplications',
          (category, IOS_OVERALL_CATEGORY_ID),
          category,
          IOS_OVERALL_CATEGORY_ID]

    self.trainer.extractor = DbExtractor(
        AUGMENTED_EXTRACTION_QUERY,
        processor=update_extraction_args)

    def update_labeler_request_kwargs(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      labeler.request_kwargs['country_alpha2'] = country

    def label_modifier(label, record_id):
      if label >= 0.0:
        return log(1 + float(label))
      else:
        return None

    self.trainer.labelers.append(ModelPredictionLabeler(
        # Use local name and version variables to prevent modelregistry changes
        # from affecting this labeler.
        model=OverallFreeDownloadModel(name='OverallFreeDownload', version=1),
        label_modifier=label_modifier,
        request_kwargs={'platform_id': IOS_PLATFORM_ID},
        processor=update_labeler_request_kwargs))

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      owner_id = invocation_context.get('owner_id', None)

      # Default to an invalid customer_id if no owner_id is provided.
      # This allows us to use a single label query for both the public and
      # private model cases.
      owner_id = owner_id if owner_id is not None else -1
      labeler.arg_list = [Customer.PUBLIC, owner_id, country, DOWNLOAD_IDENTIFIER_TUPLE]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(
        IosRankTransformer(
            'IosRankTransformer',
            iphone_rank_type='topfreeapplications',
            ipad_rank_type='topfreeipadapplications'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    best_rank_features = ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']
    rank_features += best_rank_features

    self.trainer.append(
        ReplaceNullTransformer(
            'ReplaceNull',
            value=MAX_IOS_RANK,
            features=rank_features[:]))
    self.trainer.append(LogTransformer('Log', features=rank_features[:]))

    best_rank_sq_features = ['best_rank_3d_sq', 'best_rank_ipad_3d_sq', 'best_rank_1d_sq',
                             'best_rank_ipad_1d_sq']
    rank_features += best_rank_sq_features
    self.trainer.append(PowTransformer('Pow', power=2, features=best_rank_features[:],
                                       outputs=best_rank_sq_features[:]))

    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features[:]))

    prediction_features = rank_features + ['is_monday', 'is_tuesday', 'is_wednesday', 'is_thursday',
                                           'is_friday', 'is_saturday']

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features[:]))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=AUGMENTED_FREE_REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionExpTransformer('PredictionExp'))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for record_dict in data:
      record = self._id(record_dict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and prediction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, record_dict in enumerate(request_data):
      record_dict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list consisting of the two-character
  # country code followed by the category ID. Example: ['US', 29]
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    category_id = record.get('category_id', 0)
    category = AppStoreCategory.objects.filter(id=category_id).first()
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2) or (category is None):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2, category_id]

  def _segment_keys(self):
    categories = AppStoreCategory.objects.filter(platform__name='iOS')
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      for category in categories:
        if category.parent_category != IOS_NEWSSTAND_GENRE_ID:
          keys.append([country, category.id])
    return keys
