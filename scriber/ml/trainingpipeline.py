import logging

from copy import deepcopy
from ml.dataset import Dataset


logger = logging.getLogger(__name__)

class TrainingPipeline(object):
  def __init__(self, dataset=None, extractor=None, labelers=None, transformers=None, score=False):
    self.dataset = dataset
    self.holdout = Dataset()
    self.extractor = extractor
    self.labelers = [] if labelers is None else labelers
    self.transformers = [] if transformers is None else transformers
    self.configured_transformers = []
    self.score = score

  def extract(self, invocation_context=None):
    self.dataset = self.extractor.extract(invocation_context)

  def label(self, invocation_context=None):
    for labeler in self.labelers:
      labeler.label(self.dataset, invocation_context=invocation_context)

  def partition(self, invocation_context=None):
    trainingData = Dataset()
    for feature in self.dataset.features:
      self.holdout.features[feature] = []
      trainingData.features[feature] = []

    for idx, label in enumerate(self.dataset.labels):
      ds = self.holdout if label is None else trainingData
      ds.ids.append(self.dataset.ids[idx])
      ds.labels.append(self.dataset.labels[idx])
      for feature in self.dataset.features:
        ds.features[feature].append(self.dataset.features[feature][idx])

    self.dataset = trainingData

  def transform(self, invocation_context=None):
    for t in self.transformers:
      c = deepcopy(t)
      c.configure(self.dataset, invocation_context=invocation_context)
      c.transform(self.dataset, invocation_context=invocation_context)
      self.configured_transformers.append(c)

  def score_holdout(self, invocation_context=None):
    for t in self.configured_transformers:
      t.transform(self.holdout, invocation_context=invocation_context)

  def execute(self, invocation_context, skip_reinit=False):
    # By default, clear any residual data left over from previous training runs.
    # Preserve the extractor, labelers, and unconfigured transformers.
    if not skip_reinit:
      self.__init__(extractor=self.extractor, labelers=self.labelers,
          transformers=self.transformers)

    self.extract(invocation_context)

    if not self.dataset.features:
      logger.info('Abandoning training: extraction producted no training data')
      return None
    for feature in self.dataset.features:
      if not self.dataset.features[feature]:
        logger.info('Abandoning training: extraction producted no training data')
        return None

    self.label(invocation_context)
    self.partition(invocation_context)

    if not self.dataset.features:
      logger.info('Abandoning training: no labeled training data found')
      return None
    for feature in self.dataset.features:
      if not self.dataset.features[feature]:
        logger.info('Abandoning training: no labeled training data found')
        return None

    self.transform(invocation_context)

    if self.score:
      self.score_holdout(invocation_context)

    return
    # return self

  def append(self, transformer):
    self.transformers.append(transformer)
