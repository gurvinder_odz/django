from core.db.dynamic import connection
from ml.dataset import Dataset


class Extractor(object):

  def extract(self, invocation_context):
    raise NotImplementedError('Not implemented for abstract base class')


class DbExtractor(Extractor):

  def __init__(self, query, arg_list=None, processor=None):
    if not query:
      raise ValueError('DbExtractor creation requires a query argument')

    self.query = query
    self.arg_list = arg_list
    if processor is None:
      self.processor = lambda *args, **kwargs: None
    else:
      self.processor = processor

  def extract(self, invocation_context=None):
    # This function may modify the Extractor, and therefore accepts a reference
    # to its host as an argument.
    self.processor(self, invocation_context)

    dataset = Dataset()
    cursor = connection.cursor()
    cursor.execute(self.query, self.arg_list)
    column_names = [c.name for c in cursor.cursor.description]
    newFeatures = {}

    # For now, create features from all columns.
    # TODO(d-felix): Avoid creating features that are simply
    # duplicates of id columns.
    for row in cursor.fetchall():
      id = row[:]
      dataset.append(list(row), id, feature_names=column_names)

    return dataset
