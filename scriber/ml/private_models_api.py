from django.core.cache import cache
from ml import api
from ml.model import Model
from ml.modelregistry import CLASS, MODELS, REGISTRY, VERSION


_PRIVATE_MODEL_CACHE_TIMEOUT_SECS = 60 * 30
_PRIVATE_MODEL_CACHE_KEY_PREFIX = 'privateModel'
_PRIVATE_MODEL_CACHE_KEY_IDENTIFIER = 'jrJXNm2RNJ6yQtff'
_KEY_DELIMITER = '/'

def _private_model_cache_key(model_name, owner_id):
  return _KEY_DELIMITER.join([_PRIVATE_MODEL_CACHE_KEY_PREFIX, _PRIVATE_MODEL_CACHE_KEY_IDENTIFIER,
      model_name, str(owner_id)])

def _cache_private_model(model, owner):
  # pass for now since pickle can't handle function objects.
  pass
  # cacheKey = _private_model_cache_key(model.name, owner)
  # cache.set(cacheKey, model, _PRIVATE_MODEL_CACHE_TIMEOUT_SECS)

def _retrieve_private_model(model_name, owner_id):
  cacheKey = _private_model_cache_key(model_name, owner_id)
  return cache.get(cacheKey)

def predict(model_name=None, request_data=None, owner_id=None, *args, **kwargs):
  publicModel = MODELS[model_name] if model_name in MODELS else None
  if owner_id is None:
    return publicModel.predict(request_data, *args, **kwargs)
  privateModel = _retrieve_private_model(model_name, owner_id)
  if privateModel is None:
    if model_name not in REGISTRY:
      raise ValueError('Cannot train unrecognized model %s' % model_name)
    modelClass, modelVersion = (REGISTRY[model_name][CLASS], REGISTRY[model_name][VERSION])
    privateModel = modelClass(name=model_name, version=modelVersion, owner_id=owner_id,
        fallback=publicModel)
  return privateModel.predict(request_data, *args, **kwargs)

def train_and_update(model_name=None, request_data=None, owner_id=None, *args, **kwargs):
  publicModel = MODELS[model_name] if model_name in MODELS else None
  privateModel = _retrieve_private_model(model_name, owner_id)
  if privateModel is None:
    if model_name not in REGISTRY:
      raise ValueError('Cannot train unrecognized model %s' % model_name)
    modelClass, modelVersion = (REGISTRY[model_name][CLASS], REGISTRY[model_name][VERSION])
    privateModel = modelClass(name=model_name, version=modelVersion, owner_id=owner_id,
        fallback=publicModel)
  ret = privateModel.train_and_update(request_data, *args, **kwargs)
  _cache_private_model(privateModel, owner_id)
  return ret
