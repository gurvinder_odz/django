# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0010_customer_training_privacy'),
        ('ml', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelsegment',
            name='owner',
            field=models.ForeignKey(db_column=b'owner', to='customers.Customer', null=True),
            preserve_default=True,
        ),
    ]
