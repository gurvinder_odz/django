# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ml', '0002_modelsegment_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='modelsegment',
            name='version',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
