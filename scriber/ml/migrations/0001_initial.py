# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ModelSegment',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', core.customfields.BigAutoField(serialize=False, primary_key=True)),
                ('content', models.TextField()),
                ('content_type', models.CharField(max_length=64)),
                ('model_name', models.CharField(max_length=256, db_index=True)),
                ('segment_key', models.CharField(max_length=256, db_index=True)),
            ],
            options={
                'db_table': 'model_segments',
            },
            bases=(models.Model,),
        ),
    ]
