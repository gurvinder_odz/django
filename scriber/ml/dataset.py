_NO_LABEL = []


class Dataset(object):
  def __init__(self):
    self.data = []
    self.features = {}
    self.ids = []
    self.labels = []
    self.predictions = []

  def add_feature(self, name, values=None):
    if name in self.features:
      raise ValueError('Feature %s already exists')
    if values is not None and len(values) != len(self.ids):
      raise ValueError('Cannot initialize new feature. Value list length is incompatible with ids')
    elif values is None:
      values = [None] * len(self.ids)
    self.features[name] = values

  def extend_features(self):
    for feature in self.features:
      self.features[feature].append(None)

  def append(self, raw_record=None, id=None, label=_NO_LABEL, label_index=None, feature_names=None):
    if raw_record is None:
      self.ids.append(id)
      self.extend_features()
      if label is not _NO_LABEL:
        self.labels.append(label)

    elif isinstance(raw_record, dict):
      self.ids.append(id)
      self.extend_features()
      for feature in raw_record:
        if feature not in self.features:
          self.add_feature(feature)
        self.features[feature][-1] = raw_record[feature]
      if label is not _NO_LABEL:
        self.labels.append(label)

    elif hasattr(raw_record, '__iter__'):
      if not raw_record or not feature_names or len(raw_record) != len(feature_names):
        raise ValueError('Cannot append record with missing or incompatible feature names')

      self.ids.append(id)
      self.extend_features()

      # Note that by omitting both a label and a label_index argument, no value
      # will be appended to self.labels
      if label is not _NO_LABEL:
        self.labels.append(label)

      for idx, name in enumerate(feature_names):
        if idx == label_index:
          # Avoid double-labelling
          if label is _NO_LABEL:
            self.labels.append(raw_record[label_index])
          continue
        if name not in self.features:
          self.add_feature(name)
        self.features[name][-1] = raw_record[idx]

    else:
      raise ValueError('Cannot append unrecognized type %s' % type(raw_record))

"""
class Dataset(object):
  # TODO(d-felix): Labels must be floats?
  def __init__(self, feature_names, label_name='target'):
    self.data = []
    self.ids = []
    self.labels = []
    self.metadata = {'label_name': label_name, 'feature_names': list(feature_names)}
    self.predictions = []

  @classmethod
  def from_metadata(cls, metadata):
    dataset = cls(metadata['feature_names'], metadata['label_name'])
    return dataset

  def append(self, raw_record, id=None, label=_NO_LABEL, label_index=None):
    # If no label is provided, we assume it is the first entry of the raw_record argument.
    if len(raw_record) != len(self.metadata['feature_names']) + (0 if label_index is None else 1):
      raise ValueError('Cannot append record to dataset due to feature count mismatch')
    self.ids.append(id)
    if label is not _NO_LABEL:
      self.labels.append(label)
      self.data.append(list(raw_record))
    elif label_index is not None:
      self.labels.append(raw_record[label_index])
      self.data.append(list(raw_record[:label_index] + raw_record[label_index + 1:]))
    else:
      self.data.append(list(raw_record))

  def feature_index(self, name):
    if name not in self.metadata['feature_names']:
      raise ValueError('Feature %s not found in metadata %s' %
          (name, self.metadata['feature_names']))
    return self.metadata['feature_names'].index(name)
"""
