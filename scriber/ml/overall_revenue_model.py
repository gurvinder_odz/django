import logging

from appstore.constants import IOS_OVERALL_CATEGORY_ID, MAX_IOS_RANK
from appstore.models import AppStoreCategory
from appstore.tasks import _TOP_COUNTRIES_ALPHA2
from core.timeutils import convert_tz
from core.utils import unpack
from customers.models import Customer
from datetime import date, datetime
from ml.dataset import Dataset
from ml.extract import DbExtractor
from ml.label import DbLabeler
from ml.model import Model
from ml.trainingpipeline import TrainingPipeline
from ml.transformer import FormatterTransformer, IosRankTransformer, LogTransformer, PredictionExpTransformer, PredictionPowTransformer, RemoveDateOutliersTransformer, RemoveForbiddenValuesTransformer, ReplaceNullTransformer, SciKitModelTransformer, ZScaleTransformer
from pytz import timezone, utc
from sklearn import linear_model


logger = logging.getLogger(__name__)

MAX_RANK_AGE_INDEX = 8
OVERALL_REGULARIZATION_STRENGTH = 0.004
RANK_DATA_BIRTHDAY = date(2015, 4, 9)

EXTRACTION_QUERY = """
  SELECT DISTINCT
    a.id app_id,
    date_range.date::date record_date
  FROM app_store_rank_requests r
  JOIN app_store_rank_types t ON (r.rank_type = t.id)
  JOIN app_store_regions rg ON (r.region = rg.id)
  JOIN countries c ON (rg.country = c.id)
  JOIN app_store_categories cg ON (r.category = cg.id)
  JOIN app_store_ranks k ON (r.id = k.rank_request)
  JOIN app_store_apps a ON (k.app = a.id)
  JOIN generate_series(%s::date, %s::date, '1 day') date_range(date)
  ON (date_range.date >= (r.time AT TIME ZONE 'America/Los_Angeles')::date AND
      date_range.date - (r.time AT TIME ZONE 'America/Los_Angeles')::date <= '2 day')
  WHERE
    c.alpha2 = %s AND
    t.name = 'topgrossingapplications' AND
    cg.id = %s
  ;
  """

LABEL_QUERY = """
  SELECT
    POW(revenue.developer_proceeds_usd, 0.25) sqrt_revenue_usd,
    apps.id app_id,
    revenue.report_date
  FROM (
    SELECT
      rd.apple_identifier apple_identifier,
      rd.report_date report_date,
      SUM(arr.developer_proceeds_usd) developer_proceeds_usd
    FROM apple_report_records arr JOIN (
      SELECT
        li.apple_vendor_id vendor_id,
        cp.apple_identifier apple_identifier,
        cp.apple_sku sku,
        a.primary_category primary_category,
        ar.report_date report_date,
        MIN(ar.report_id) report_id
      FROM channel_products cp JOIN customer_external_login_info li ON (cp.customer_id = li.customer_id)
      JOIN customers c ON (cp.customer_id = c.customer_id)
      JOIN app_store_apps a ON cp.apple_identifier = a.track_id
      JOIN apple_reports ar ON li.apple_vendor_id = ar.vendor_id
      LEFT JOIN app_store_categories subcategories ON a.primary_category = subcategories.parent_category
      WHERE
        li.apple_vendor_id IS NOT NULL AND
        ar.report_date >= '2015-04-08' AND
        c.training_privacy = %s
      GROUP BY 1, 2, 3, 4, 5
    ) rd
    ON (arr.report_id = rd.report_id AND
      (arr.apple_identifier = rd.apple_identifier OR arr.parent_identifier = rd.sku))
    WHERE arr.country_code = %s
    GROUP BY 1, 2
  ) revenue JOIN app_store_apps apps ON (revenue.apple_identifier = apps.track_id)
  WHERE revenue.developer_proceeds_usd > 0.0
  ;
  """

# TODO(d-felix): Document the expected request format.


class OverallRevenueModel(Model):

  def __init__(self, name=None, version=None, owner_id=None, fallback=None):
    super(OverallRevenueModel, self).__init__(name, version, owner_id, fallback)

    # Create and configure the training pipeline.
    self.trainer = TrainingPipeline()

    def update_extraction_args(extractor, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      yesterday_pacific = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles'))
      extractor.arg_list = [
          str(RANK_DATA_BIRTHDAY),
          str(yesterday_pacific),
          country,
          IOS_OVERALL_CATEGORY_ID]

    self.trainer.extractor = DbExtractor(EXTRACTION_QUERY, processor=update_extraction_args)

    def update_labeling_args(labeler, invocation_context):
      segment_key = invocation_context['segment_key']
      country = segment_key[0]
      labeler.arg_list = [Customer.PUBLIC, country]

    self.trainer.labelers.append(DbLabeler(LABEL_QUERY, None, processor=update_labeling_args))

    self.trainer.append(IosRankTransformer('IosRankTransformer'))
    self.trainer.append(RemoveForbiddenValuesTransformer('RemoveForbidden', features=['is_monday']))
    self.trainer.append(RemoveDateOutliersTransformer(
        'RemoveDateOutliers', features=['record_date'], lt=[str(RANK_DATA_BIRTHDAY)]))

    rank_features = ['rank_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['rank_ipad_' + str(idx) for idx in range(1, MAX_RANK_AGE_INDEX + 1)]
    rank_features += ['best_rank_3d', 'best_rank_ipad_3d', 'best_rank_1d', 'best_rank_ipad_1d']

    self.trainer.append(
        ReplaceNullTransformer(
            'ReplaceNull',
            value=MAX_IOS_RANK,
            features=rank_features))
    self.trainer.append(LogTransformer('Log', features=rank_features))
    self.trainer.append(ZScaleTransformer('ZScale', features=rank_features))

    prediction_features = rank_features + ['is_monday', 'is_tuesday', 'is_wednesday', 'is_thursday',
                                           'is_friday', 'is_saturday']

    self.trainer.append(FormatterTransformer(name='FormatData', features=prediction_features))
    self.trainer.append(
        SciKitModelTransformer(
            name='LinearModel',
            model=linear_model.Lasso(
                alpha=OVERALL_REGULARIZATION_STRENGTH)))

    # Invert the target transformation applied during labeling.
    self.trainer.append(PredictionPowTransformer('PredictionPow', power=4))

  def _id(self, record, *args, **kwargs):
    if not ('app_id' in record and 'record_date' in record):
      raise ValueError('Unable to extract id from record %s' % record)
    return (record['app_id'], record['record_date'])

  def _dataset(self, data, *args, **kwargs):
    dataset = Dataset()

    # TODO(d-felix): Stop duplicating fields across dataset.ids and dataset.data.
    for recordDict in data:
      record = self._id(recordDict)
      dataset.append(list(record), record, feature_names=['app_id', 'record_date'])
    return dataset

  def _format_results(self, request_data, predictions, *args, **kwargs):
    # TODO(d-felix): Improve result matching (e.g. make use of record ids).
    if len(request_data) != len(predictions):
      raise ValueError('Unable to format prediction results; request_data and predction lengths ' +
                       'do not match: %s %s' % (len(request_data), len(predictions)))
    for idx, recordDict in enumerate(request_data):
      recordDict['prediction'] = predictions[idx]
    return request_data

  # The segment key for a record is a list containing only the two-character
  # country code. Example: ['US']
  def _segment_key(self, record, *args, **kwargs):
    country_alpha2 = record.get('country_alpha2', None)
    if (country_alpha2 not in _TOP_COUNTRIES_ALPHA2):
      logger.info('Could not create segment key from record %s' % record)
      return None
    return [country_alpha2]

  def _segment_keys(self):
    keys = []
    for country in _TOP_COUNTRIES_ALPHA2:
      keys.append([country])
    return keys
