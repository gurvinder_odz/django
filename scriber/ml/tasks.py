from __future__ import absolute_import

from celery import task
from celery.utils.log import get_task_logger
from core.celeryutils import exponential_backoff
from core.utils import unpack
from ml import api, private_models_api
from ml.modelregistry import MODELS


# The maximum number of times a task from this module may be retried.
_MAX_RETRIES = 5

logger = get_task_logger(__name__)

@task(max_retries=_MAX_RETRIES, ignore_result=True, bind=True)
def train_model(self, *args, **kwargs):
  try:
    modelName, ownerId = unpack(kwargs, 'model_name', 'owner_id')
    if modelName not in MODELS:
      logger.error('Cannot train unrecognized model %s' % modelName)
      return

    if ownerId is None:
      api.train_and_update(**kwargs)
    else:
      private_models_api.train_and_update(**kwargs)

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))
