import logging

from django.http import HttpResponse
from django.shortcuts import render
import django.conf

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from customers.views import UserCreationForm, products

import dashboard

from customers.models import CustomerApiKey
from django.core.urlresolvers import reverse
from django.contrib.auth.views import password_reset, password_reset_confirm


logger = logging.getLogger(__name__)

def test_homepage(request):
  developers = [{'name':'Justin Gary', 
                 'title':'CEO',
                 'headshot_url':'/static/img/justin_gary.jpg',
                 'url':'http://solforgegame.com/',
                 'quote':'The collectible card game market is very competitive. App Theta let me see exactly what my market competitors were earning and how fast they were growing.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.stoneblade.solforge', 
                             'title':'SolForge', 
                             'description':'App Theta let us understand both our own growth and other companies, in a very competitive market.',
                             },
                             'other_apps':[]
                },
                {'name':'Virgil Zetterlind', 
                 'title':'CEO',
                 'headshot_url':'/static/img/virgil_zetterlind.jpg',
                 'url':'https://www.apple.com/my/ipad-air-2/change/',
                 'quote':'We have several app titles on iOS and Android, and App Theta shows all the sales trends the best.',
                 'main_app':{'platform':'iOS', 
                             'id':'io.conserve.whalealert2', 
                             'title':'Whale Alert', 
                             'description':'Featured in Apple\'s "Change is in the Air" campaign'
                             },
                 'other_apps':[],
                },
                {'name':'Dr. Craig Hunter', 
                 'title':'Founder',
                 'headshot_url':'/static/img/craig_hunter.jpg',
                 'url':'https://itunes.apple.com/us/app/theodolite/id339393884?mt=8',
                 'quote':'App Theta hooked me right away. Once I saw the sales charts, I wanted to immediately start measuring usage on iOS.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.hunter.theodolite', 
                             'title':'Theodolite', 
                             'description':'Augmented reality for surveying and other real work.'
                             },
                 'other_apps':[],
                },
                {'name':'Andrew Johnson', 
                 'title':'Founder',
                 'headshot_url':'/static/img/andrew_johnson.jpg',
                 'url':'https://www.apple.com/your-verse/elevating-expedition/',
                 'quote':'I built App Theta to help manage Gaia GPS, and it will work great for freemium and subscription apps like Gaia.',
                 'main_app':{'platform':'iOS', 
                             'id':'5ZPN38G6GE.com.trailbehind.GaiaGPS', 
                             'title':'Gaia GPS', 
                             'description':'Featured in Apple\'s "Elevating the Expedition" campaign.'},
                 'other_apps':[],
                },
                {'name':'Ryan "Guthook" Linn', 
                 'title':'Founder',
                 'headshot_url':'/static/img/ryan_linn.jpg',
                 'url':'http://www.guthookhikes.com/apps',
                 'quote':'It\'s the only software I like to use for sales reports, really clear trends.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.guthookhikes.ATHiker', 
                             'title':'Appalachian Trail Guide', 
                             'description':'The goto guides for thru-hikers and section-hikers.'},
                 'other_apps':[],
                },
                {'name':'Matt Pease', 
                 'title':'Founder',
                 'headshot_url':'/static/img/matt_pease.jpg',
                 'url':'https://itunes.apple.com/us/app/scanner911-police-radio-pro/id333246187?mt=8 ',
                 'quote':'You get a lot of utility for free with App Theta. I like to check the dashboard first thing each day.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.scanner911app.scanner911', 
                             'title':'Scanner911', 
                             'description':'The hit police scanner app.'},
                 'other_apps':[],
                }

               ]
  return render(request, "test_homepage.html", {"theta_devs":developers, 
                                                "form": UserCreationForm(),
                                           })


def homepage(request):
  
  return homepage_for_template(request, "homepage.html")


def homepage_for_template(request, template):
  if request.user.is_authenticated():
    return dashboard.views.main_dashboard(request) 
  developers = [
                {'name':'Justin Gary', 
                 'title':'CEO, Stoneblade',
                 'headshot_url':'/static/img/justin_gary.jpg',
                 'url':'http://solforgegame.com/',
                 'quote':'App Theta gives us unique sales insights in a very competitive market.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.stoneblade.solforge', 
                             'title':'Sol Forge', 
                             'description':'The hit collectible card game, from the makers of Ascension.'},
                 'other_apps':[],
                },
                {'name':'Virgil Zetterlind', 
                 'title':'Founder, EarthNC',
                 'headshot_url':'/static/img/virgil_zetterlind.jpg',
                 'url':'https://www.apple.com/my/ipad-air-2/change/',
                 'quote':'App Theta has the best sales dashboard I\'ve used for our app business.',
                 'main_app':{'platform':'iOS', 
                             'id':'io.conserve.whalealert2', 
                             'title':'Whale Alert', 
                             'description':'Featured in Apple\'s "Change is in the Air" campaign for iPad.'},
                 'other_apps':[],
                },
                
                {'name':'Ryan Linn', 
                 'title':'Founder of Guthook\'s Guides',
                 'headshot_url':'/static/img/ryan_linn.jpg',
                 'url':'http://www.guthookhikes.com/apps',
                 'quote':'With all of our guide titles, App Theta is the only software that gave me a clear view of the business.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.guthookhikes.ATHiker', 
                             'title':'Appalachian Trail Guide', 
                             'description':'The goto guides for thru-hikers and section-hikers.'},
                 'other_apps':[],
                },
                {'name':'Matt Pease', 
                 'title':'Founder of Scanner911',
                 'headshot_url':'/static/img/matt_pease.jpg?a',
                 'url':'https://itunes.apple.com/us/app/scanner911-police-radio-pro/id333246187?mt=8',
                 'quote':'You get a lot of utility even for free with App Theta, I like to check the dashboard first thing each day.',
                 'main_app':{'platform':'iOS', 
                             'id':'com.scanner911app.scanner911', 
                             'title':'Scanner911', 
                             'description':'The hit police scanner app.'},
                 'other_apps':[],
                }

               ]
  theta_apps = [
                  {'id':'com.inkstonesoftware.inkstoneaudiobooks', 
                   'platform':"iOS",
                   'url':'https://itunes.apple.com/us/app/audiobooks-hq-9-150+-free/id573235547',
                  },
                  {'id':'com.inkstonesoftware.ebooksearch', 
                   'platform':"iOS",
                   'url':'https://itunes.apple.com/us/app/ebook-search-free-books-for/id416454511',
                  },
                  {'id':'5ZPN38G6GE.com.trailbehind.GaiaGPS', 
                   'platform':"iOS",
                   'url':'https://www.apple.com/your-verse/elevating-expedition/',
                  },
                  {'id':'com.hunter.theodolite', 
                   'platform':"iOS",
                   'url':'https://itunes.apple.com/us/app/theodolite/id339393884',
                   },
                  
                 ]

  return render(request, template, {"theta_devs":developers,
                                    "theta_apps":theta_apps, 
                                           "form": UserCreationForm(),
                                           }
                                           )

# todo replace with real API key
def docs(request):
  apiKey = None
  if request.user.is_authenticated():
    apiKey = CustomerApiKey.objects.filter(customer_id__auth_user=request.user).first()
  if apiKey and apiKey.api_key:
    return render(request, "docs.html", {'api_key':apiKey.api_key})
  return render(request, "docs.html", {'api_key':"LOG_IN_TO_INSERT_YOUR_KEY_HERE"})


def jobs(request):
  return render(request, "jobs.html")


def support(request):
  return render(request, "help.html")


def about(request):
  return render(request, "about-us.html")


def team(request):
  return render(request, "team.html")


def terms_of_use(request):
  return render(request, "terms-of-use.html") 

def faq(request):
  return render(request, "faq.html") 


def pricing(request):
  return render(request, "pricing.html", {"plans":products(), 
                                          "user_plan":False, 
                                          "STRIPE_PUBLIC_KEY":django.conf.settings.STRIPE_PUBLIC_KEY})


def reset_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(request, template_name='reset-confirm.html',
        uidb64=uidb64, token=token, post_reset_redirect=reverse('login'))


def reset(request):
    return password_reset(request, template_name='reset-password.html',
        email_template_name='reset_email.html',
        subject_template_name='reset-subject.txt',
        post_reset_redirect=reverse('login'))


