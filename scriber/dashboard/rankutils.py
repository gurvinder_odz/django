import re

from appstore.models import AppStoreApp, AppStoreCategory, AppStorePlatform, AppStoreRankType, AppStoreRegion
from appstore.tasks import _TOP_COUNTRIES_ALPHA2
from calendar import timegm
from copy import deepcopy
from core.cache import cache_key, cache_results
from core.db.dynamic import connection
from core.timeutils import convert_tz, from_timestamp, midnight_timestamp
from datetime import datetime, timedelta
from dateutil import parser
from django.core.cache import cache
from localization.models import Country
from ml import private_models_api
from ml.modelregistry import DAILY_REVENUE, FREE_DOWNLOAD, PAID_DOWNLOAD
from pytz import timezone, utc


OVERALL_CATEGORY_DISPLAY_NAME = 'All'

CATEGORY_NAME_SORT_ORDER = ('Top Grossing', 'Top Paid', 'Top Free')
CATEGORY_DEVICE_SORT_ORDER = (None, 'iPhone', 'iPad')
PLATFORM_SORT_ORDER = (AppStorePlatform.APP_STORE, AppStorePlatform.GOOGLE_PLAY)


def categories(platform_id):
  categories = AppStoreCategory.objects.filter(platform__id=long(platform_id))
  catDict = {}
  subDict = {}

  for category in categories:
    parentCategory = category.parent_category
    if parentCategory:
      if parentCategory.id not in subDict:
        subDict[parentCategory.id] = []
      subDict[parentCategory.id].append({'id': category.id, 'name': category.name})
    else:
      name = OVERALL_CATEGORY_DISPLAY_NAME if category.name == 'Overall' else category.name
      catDict[category.id] = {'id': category.id, 'name': name}

  for parentId in subDict:
    subcategories = sorted(subDict[parentId], key=lambda x: x['name'])
    catDict[parentId]['subcategories'] = subcategories

  ret = [catDict[key] for key in catDict]
  ret = sorted(ret, key=lambda x: '' if x['name'] == OVERALL_CATEGORY_DISPLAY_NAME else x['name'])
  return deepcopy(ret)


def rank_type_sort_key(rank_type):
  name = rank_type['name']
  nameIndex = CATEGORY_NAME_SORT_ORDER.index(name) if name in CATEGORY_NAME_SORT_ORDER else \
      len(CATEGORY_NAME_SORT_ORDER)
  key = nameIndex * (1 + len(CATEGORY_DEVICE_SORT_ORDER))

  device = rank_type['device_type']
  deviceIndex = CATEGORY_DEVICE_SORT_ORDER.index(device) if device in CATEGORY_DEVICE_SORT_ORDER \
      else len(CATEGORY_DEVICE_SORT_ORDER)
  key += deviceIndex

  return key


def rank_types(platform_id):
  is_ios = AppStorePlatform.objects.get(pk=long(platform_id)).name == 'iOS'
  rankTypes = AppStoreRankType.objects.filter(platform__id=long(platform_id))
  ret = []

  for rankType in rankTypes:
    displayName = rankType.display_name
    deviceType = None
    if is_ios:
      m = re.match(r'^(.*) iPad$', rankType.display_name)
      if m:
        deviceType = 'iPad'
        displayName = m.group(1)
      else:
        deviceType = 'iPhone'
    ret.append({'id': rankType.id, 'name': displayName, 'device_type': deviceType})

  ret = sorted(ret, key=rank_type_sort_key)
  return ret


def countries(platform_id=None):
  countries = Country.objects.filter(alpha2__in=_TOP_COUNTRIES_ALPHA2)
  ret = []
  for country in countries:
    ret.append({'id': country.alpha2, 'name': country.display_name})

  ret = sorted(ret, key=lambda x: x['name'])
  return ret


def platform_sort_key(platform):
  name = platform['name']
  nameIndex = PLATFORM_SORT_ORDER.index(name) if name in PLATFORM_SORT_ORDER else \
      len(PLATFORM_SORT_ORDER)
  return nameIndex


def platforms():
  platforms = AppStorePlatform.objects.all()
  ret = []
  for platform in platforms:
    ret.append({'id': platform.id, 'name': platform.display_name})

  ret = sorted(ret, key=platform_sort_key)
  return ret


def bundle_ids(app_id_list):
  ret = {app_id: None for app_id in app_id_list}
  apps = AppStoreApp.objects.filter(id__in=app_id_list)
  for app in apps:
    ret[app.id] = app.bundle_id
  return ret


# TODO(d-felix): Support Android.
def externalize_app_ids(app_id_list, platform_id):
  platform = AppStorePlatform.objects.get(pk=platform_id)
  if platform.name == AppStorePlatform.IOS:
    return bundle_ids(app_id_list)
  else:
    raise NotImplementedError('App ID externalization not supported for platform %s' % platform_id)


def app_estimates_data(app_id_list, category_id, country_alpha2, start_epoch, end_epoch, estimate_type,
    owner_id=None):
  # TODO(d-felix): Handle future endDates during prediction instead of applying a min below.
  startDate = from_timestamp(start_epoch, timezone('America/Los_Angeles')).date()
  endDate = from_timestamp(end_epoch, timezone('America/Los_Angeles')).date()
  yesterday = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles')).date() - \
      timedelta(days=1)
  endDate = min(endDate, yesterday)
 
  return cached_app_estimates(app_id_list, category_id, country_alpha2,
      str(startDate), str(endDate), estimate_type, owner_id)


#@cache_results(timeout=60*60)
def cached_app_estimates(app_id_list, category_id, country_alpha2, start_date,
    end_date, estimate_type, owner_id):
  startDate = parser.parse(start_date).date()
  endDate = parser.parse(end_date).date()
  dates = []
  for delta in range((endDate - startDate + timedelta(days=1)).days):
    dates.append(startDate + timedelta(days=delta))
  timestamps = [midnight_timestamp(d, timezone('America/Los_Angeles')) for d in dates]

  category = AppStoreCategory.objects.get(pk=category_id)
  categoryStr = 'All' if category.name == 'Overall' else category.name
  platform_id = category.platform.id
  platformStr = category.platform.display_name

  # Retrieve the app objects in order to determine their free/paid status.
  apps = AppStoreApp.objects.in_bulk(app_id_list)

  free_request_data = []
  paid_request_data = []
  for app_id in app_id_list:
    # If an app's free/paid status is unknown, then default to free since this is more common.
    data_to_update = free_request_data if not apps[app_id].is_free is False else paid_request_data
    for d in dates:
      data_to_update.append(
        {
          'app_id': app_id,
          'platform_id': platform_id,
          'category_id': category_id,
          'country_alpha2': country_alpha2,
          'record_date': d
        })

  request_data = free_request_data + paid_request_data

  if category.platform.name == 'iOS':
    if estimate_type == 'downloads':
      free_response = private_models_api.predict(
          model_name=FREE_DOWNLOAD, request_data=free_request_data, owner_id=owner_id)
      paid_response = private_models_api.predict(
          model_name=PAID_DOWNLOAD, request_data=paid_request_data, owner_id=owner_id)
      response = free_response + paid_response
    else:
      response = private_models_api.predict(model_name=DAILY_REVENUE, request_data=request_data,
        owner_id=owner_id)
  else:
    response = []

  retDict = {}
  for app_id in app_id_list:
    retDict[app_id] = {
      'app_id': app_id,
      'platform': platformStr,
      'series_data': [
        {
          'type': 'Earnings',
          'category': categoryStr,
          'times': timestamps,
          'current': [None] * len(dates),
          'previous': [None] * len(dates)
        }
      ]
    }

  for prediction in response:
    revenue = prediction['prediction']
    app_id = prediction['app_id']
    d = prediction['record_date']
    idx = (d - dates[0]).days
    if 0 <= idx < len(dates):
      retDict[app_id]['series_data'][0]['current'][idx] = revenue

  ret = [retDict[app_id] for app_id in app_id_list]
  return deepcopy(ret)


TOP_APPS_QUERY = """
  SELECT
    app,
    rank,
    rank_time
  FROM (
    SELECT
      k.app app,
      k.rank rank,
      s0.time rank_time,
      MAX(s0.time)
    OVER(PARTITION BY k.rank)
    FROM app_store_ranks k
    JOIN (
      SELECT
        r.id,
        r.time
      FROM app_store_rank_requests r
      JOIN app_store_regions rg on (r.region = rg.id)
      JOIN countries c ON (rg.country = c.id)
      WHERE
        r.category = %s AND
        r.rank_type = %s AND
        c.alpha2 = %s AND
        r.rank_server_id = 2
      ORDER BY r.time DESC limit 3
    ) s0
    ON (k.rank_request = s0.id)
    WHERE
      k.rank >= %s AND
      k.rank <= %s
  ) s1
  WHERE s1.rank_time = s1.max
  ORDER BY rank;
  ;
  """


def top_apps(category_id, rank_type_id, country_alpha2, start_rank, end_rank):
  apps = [None] * (end_rank - start_rank + 1)
  cursor = connection.cursor()
  cursor.execute(TOP_APPS_QUERY, [category_id, rank_type_id, country_alpha2, start_rank, end_rank])
  for row in cursor.fetchall():
    app_id, rank = row[:2]
    apps[rank - start_rank] = app_id
  return apps


# TODO(d-felix): Optimize this method by adding a category-specific prediction
# API function to the daily revenue model.
def category_revenue_data(category_id, country_alpha2, rank_type_id, start_rank, end_rank,
    start_epoch, end_epoch, owner_id=None):
  # TODO(d-felix): Handle future endDates during prediction instead of applying a min below.
  startDate = from_timestamp(start_epoch, timezone('America/Los_Angeles')).date()
  endDate = from_timestamp(end_epoch, timezone('America/Los_Angeles')).date()
  yesterday = convert_tz(datetime.utcnow(), utc, timezone('America/Los_Angeles')).date() - \
      timedelta(days=1)
  endDate = min(endDate, yesterday)

  return cached_category_revenue_data(category_id, country_alpha2, rank_type_id, start_rank,
    end_rank, str(startDate), str(endDate), owner_id)


#@cache_results(timeout=60*60)
def cached_category_revenue_data(category_id, country_alpha2, rank_type_id, start_rank, end_rank,
    start_date, end_date, owner_id):
  startDate = parser.parse(start_date).date()
  endDate = parser.parse(end_date).date()
  dates = []
  for delta in range((endDate - startDate + timedelta(days=1)).days):
    dates.append(startDate + timedelta(days=delta))
  timestamps = [midnight_timestamp(d, timezone('America/Los_Angeles')) for d in dates]

  category = AppStoreCategory.objects.get(pk=category_id)
  categoryStr = category.name
  categoryStr = 'All' if categoryStr == 'Overall' else categoryStr
  platform_id = category.platform.id
  platformStr = category.platform.display_name

  apps = [None] * (end_rank - start_rank + 1)
  appsDict = {}
  cursor = connection.cursor()
  cursor.execute(TOP_APPS_QUERY, [category_id, rank_type_id, country_alpha2, start_rank, end_rank])
  for row in cursor.fetchall():
    app_id, rank = row[:2]
    apps[rank - start_rank] = app_id
    appsDict[app_id] = {
      'app_id': app_id,
      'platform': platformStr,
      'series_data': [
        {
          'type': 'Earnings',
          'category': categoryStr,
          'times': timestamps,
          'current': [None] * len(dates),
          'previous': [None] * len(dates)
        }
      ]
    }

  request_data = []
  for app_id in appsDict:
    for d in dates:
      request_data.append(
        {
          'app_id': app_id,
          'platform_id': platform_id,
          'category_id': category_id,
          'country_alpha2': country_alpha2,
          'record_date': d
        })

  # TODO(d-felix): Add revenue models for Android.
  if category.platform.name == 'iOS':
    # The private_models_api falls back to the standard api when owner_id is None.
    response = private_models_api.predict(model_name=DAILY_REVENUE, request_data=request_data,
        owner_id=owner_id)
  else:
    response = []

  for prediction in response:
    app_id = prediction['app_id']
    revenue = prediction['prediction']
    d = prediction['record_date']
    idx = (d - dates[0]).days
    if 0 <= idx < len(dates):
      appsDict[app_id]['series_data'][0]['current'][idx] = revenue

  ret = [appsDict[app_id] for app_id in apps]
  return deepcopy(ret)


APP_RANK_QUERY = """
  SELECT
    k.app app_id,
    k.rank rank,
    s1.time rank_time
  FROM (
    SELECT
      r.id AS request_id,
      r.time AS time
    FROM app_store_rank_requests r
    JOIN app_store_regions rg ON (r.region = rg.id)
    JOIN countries c on (rg.country = c.id)
    WHERE
      r.category = %s AND
      r.rank_type = %s AND
      r.time >= %s AND
      r.time <= %s AND
      c.alpha2 = %s
  ) s1
  JOIN app_store_ranks k ON (s1.request_id = k.rank_request)
  WHERE
    k.app IN %s
  ORDER BY app_id, rank_time
;
"""


def app_rank_data(app_id_list, category_id, country_alpha2, rank_type_id, start_epoch, end_epoch):
  category = AppStoreCategory.objects.get(pk=category_id)
  categoryStr = 'All' if category.name == 'Overall' else category.name
  platform_id = category.platform.id
  platformStr = category.platform.display_name

  rankType = AppStoreRankType.objects.get(pk=rank_type_id)
  rankTypeDisplayName = rankType.display_name
  deviceType = None
  if AppStorePlatform.objects.get(pk=long(platform_id)).name == 'iOS':
    m = re.match(r'^(.*) iPad$', rankType.display_name)
    if m:
      deviceType = 'iPad'
      rankTypeDisplayName = m.group(1)
    else:
      deviceType = 'iPhone'

  startTime = from_timestamp(start_epoch, utc)
  endTime = from_timestamp(end_epoch, utc)

  retDict = {}
  for app_id in app_id_list:
    retDict[app_id] = {
      'app_id': app_id,
      'platform': platformStr,
      'series_data': [
        {
          'type': 'Ranks',
          'category': categoryStr,
          'category_type': rankTypeDisplayName,
          'device_type': deviceType,
          'times': [],
          'current': []
        }
      ]
    }

  cursor = connection.cursor()
  appIdTuple = tuple(app_id_list)
  cursor.execute(APP_RANK_QUERY,
      [category_id, rank_type_id, startTime, endTime, country_alpha2, appIdTuple])
  for row in cursor.fetchall():
    app_id, rank, rankTime = row[:3]
    rankTimestamp = timegm(rankTime.utctimetuple())
    if rank:
      retDict[app_id]['series_data'][0]['times'].append(rankTimestamp)
      retDict[app_id]['series_data'][0]['current'].append(rank)

  ret = [retDict[app_id] for app_id in app_id_list]
  return deepcopy(ret)


def latest_app_rank_data(app_id_list, category_id, country_alpha2, rank_type_id,
    start_epoch, end_epoch):
  latestRankData = latest_category_rank_data(category_id, country_alpha2, rank_type_id, 1, 1500,
      start_epoch, end_epoch)

  if latestRankData:
    platformStr = latestRankData[0]['platform']
    categoryStr = latestRankData[0]['series_data'][0]['category']
    rankTypeDisplayName = latestRankData[0]['series_data'][0]['category_type']
    deviceType = latestRankData[0]['series_data'][0]['device_type']
  else:
    # This branch should never be needed.
    category = AppStoreCategory.objects.get(pk=category_id)
    categoryStr = category.name
    categoryStr = 'All' if categoryStr == 'Overall' else categoryStr
    platformId = category.platform.id
    platformStr = category.platform.display_name

    rankType = AppStoreRankType.objects.get(pk=rank_type_id)
    rankTypeDisplayName = rankType.display_name
    deviceType = None
    if AppStorePlatform.objects.get(pk=long(platformId)).name == 'iOS':
      m = re.match(r'^(.*) iPad$', rankType.display_name)
      if m:
        deviceType = 'iPad'
        rankTypeDisplayName = m.group(1)
      else:
        deviceType = 'iPhone'

  latestDict = {}
  for entry in latestRankData:
    latestDict[entry['app_id']] = entry

  ret = []
  for appId in app_id_list:
    if appId in latestDict:
      ret.append(latestDict[appId])
    else:
      ret.append({
        'app_id': appId,
        'platform': platformStr,
        'series_data': [
          {
            'type': 'Ranks',
            'category': categoryStr,
            'category_type': rankTypeDisplayName,
            'device_type': deviceType,
            'times': [None],
            'current': [None]
          }
        ]
      })

  return ret


ALL_APP_RANK_QUERY = """
  SELECT
    k.app app_id,
    k.rank rank,
    s1.time rank_time,
    s1.rank_type rank_type
  FROM (
    SELECT
      r.id AS request_id,
      r.rank_type AS rank_type,
      r.time AS time
    FROM app_store_rank_requests r
    JOIN app_store_regions rg ON (r.region = rg.id)
    JOIN countries c on (rg.country = c.id)
    WHERE
      r.category = %s AND
      r.time >= %s AND
      r.time <= %s AND
      c.alpha2 = %s
  ) s1
  JOIN app_store_ranks k ON (s1.request_id = k.rank_request)
  WHERE
    k.app IN %s
  ORDER BY app_id, rank_type, rank_time
  ;
  """


# TODO(d-felix): Merge this implementation with the app_rank_data function.
def all_app_rank_data(app_id_list, category_id, country_alpha2, start_epoch, end_epoch):
  category = AppStoreCategory.objects.get(pk=category_id)
  categoryStr = 'All' if category.name == 'Overall' else category.name
  platform_id = category.platform.id
  platformStr = category.platform.display_name
  is_ios_platform = category.platform.name == 'iOS'

  rankTypes = AppStoreRankType.objects.filter(platform__id=platform_id)
  rankTypeInfo = {}
  for rankType in rankTypes:
    rankTypeInfo[rankType.id] = {}
    displayName = rankType.display_name
    deviceType = None
    if is_ios_platform:
      m = re.match(r'^(.*) iPad$', rankType.display_name)
      if m:
        deviceType = 'iPad'
        displayName = m.group(1)
      else:
        deviceType = 'iPhone'
    rankTypeInfo[rankType.id]['display_name'] = displayName
    rankTypeInfo[rankType.id]['device_type'] = deviceType

  startTime = from_timestamp(start_epoch, utc)
  endTime = from_timestamp(end_epoch, utc)

  retDict = {}
  for app_id in app_id_list:
    retDict[app_id] = {}
    for rankType in rankTypeInfo:
      retDict[app_id][rankType] = {
        'app_id': app_id,
        'platform': platformStr,
        'series_data': [
          {
            'type': 'Ranks',
            'category': categoryStr,
            'category_type': rankTypeInfo[rankType]['display_name'],
            'device_type': rankTypeInfo[rankType]['device_type'],
            'times': [],
            'current': []
          }
        ]
      }

  cursor = connection.cursor()
  appIdTuple = tuple(app_id_list)
  cursor.execute(ALL_APP_RANK_QUERY, [category_id, startTime, endTime, country_alpha2, appIdTuple])
  for row in cursor.fetchall():
    app_id, rank, rankTime, rankType = row[:4]
    rankTimestamp = timegm(rankTime.utctimetuple())
    if rank:
      retDict[app_id][rankType]['series_data'][0]['times'].append(rankTimestamp)
      retDict[app_id][rankType]['series_data'][0]['current'].append(rank)

  ret = []
  for app_id in app_id_list:
    entry = {}
    for rankType in rankTypeInfo:
      if 'app_id' not in entry:
        entry = retDict[app_id][rankType]
      else:
        entry['series_data'].extend(retDict[app_id][rankType]['series_data'])
    ret.append(entry)
  return deepcopy(ret)


def category_rank_data(category_id, country_alpha2, rank_type_id, start_rank, end_rank,
    start_epoch, end_epoch):
  category = AppStoreCategory.objects.get(pk=category_id)
  categoryStr = category.name
  categoryStr = 'All' if categoryStr == 'Overall' else categoryStr
  platform_id = category.platform.id
  platformStr = category.platform.display_name

  rankType = AppStoreRankType.objects.get(pk=rank_type_id)
  rankTypeDisplayName = rankType.display_name
  deviceType = None
  if AppStorePlatform.objects.get(pk=long(platform_id)).name == 'iOS':
    m = re.match(r'^(.*) iPad$', rankType.display_name)
    if m:
      deviceType = 'iPad'
      rankTypeDisplayName = m.group(1)
    else:
      deviceType = 'iPhone'

  startTime = from_timestamp(start_epoch, utc)
  endTime = from_timestamp(end_epoch, utc)

  apps = [None] * (end_rank - start_rank + 1)
  appsDict = {}
  cursor = connection.cursor()
  cursor.execute(TOP_APPS_QUERY, [category_id, rank_type_id, country_alpha2, start_rank, end_rank])
  for row in cursor.fetchall():
    app_id, rank = row[:2]
    apps[rank - start_rank] = app_id
    appsDict[app_id] = {
      'app_id': app_id,
      'platform': platformStr,
      'series_data': [
        {
          'type': 'Ranks',
          'category': categoryStr,
          'category_type': rankTypeDisplayName,
          'device_type': deviceType,
          'times': [],
          'current': []
        }
      ]
    }

  appsTuple = tuple(appsDict.keys())
  cursor.execute(APP_RANK_QUERY,
      [category_id, rank_type_id, startTime, endTime, country_alpha2, appsTuple])
  for row in cursor.fetchall():
    appId, rank, rankTime = row[:3]
    rankTimestamp = timegm(rankTime.utctimetuple())
    if rank:
      appsDict[appId]['series_data'][0]['times'].append(rankTimestamp)
      appsDict[appId]['series_data'][0]['current'].append(rank)
  cursor.close()

  ret = [appsDict[app_id] for app_id in apps]
  return deepcopy(ret)


def latest_category_rank_data(category_id, country_alpha2, rank_type_id, start_rank, end_rank,
    start_epoch, end_epoch):
  category = AppStoreCategory.objects.get(pk=category_id)
  categoryStr = category.name
  categoryStr = 'All' if categoryStr == 'Overall' else categoryStr
  platformId = category.platform.id
  platformStr = category.platform.display_name

  rankType = AppStoreRankType.objects.get(pk=rank_type_id)
  rankTypeDisplayName = rankType.display_name
  deviceType = None
  if AppStorePlatform.objects.get(pk=long(platformId)).name == 'iOS':
    m = re.match(r'^(.*) iPad$', rankType.display_name)
    if m:
      deviceType = 'iPad'
      rankTypeDisplayName = m.group(1)
    else:
      deviceType = 'iPhone'

  ret = []
  cursor = connection.cursor()
  cursor.execute(TOP_APPS_QUERY, [category_id, rank_type_id, country_alpha2, start_rank, end_rank])
  for row in cursor.fetchall():
    app_id, rank, rankTime = row[:3]
    rankTimestamp = timegm(rankTime.utctimetuple())
    ret.append({
      'app_id': app_id,
      'platform': platformStr,
      'series_data': [
        {
          'type': 'Ranks',
          'category': categoryStr,
          'category_type': rankTypeDisplayName,
          'device_type': deviceType,
          'times': [rankTimestamp],
          'current': [rank]
        }
      ]
    })

  cursor.close()
  return ret


COUNTRY_APP_RANK_QUERY = """
  SELECT
    ar.app app_id,
    ar.rank rank,
    s.category_id category_id,
    s.category_name category_name,
    s.rank_type_id rank_type_id,
    s.rank_time rank_time
  FROM (
    SELECT
      arr.id,
      arr.time rank_time,
      cg.id category_id,
      cg.name category_name,
      rt.id rank_type_id
    FROM app_store_rank_requests arr JOIN app_store_categories cg ON (arr.category = cg.id)
    JOIN app_store_rank_types rt ON (arr.rank_type = rt.id)
    WHERE
      arr.region = %s AND
      arr.time >= %s AND
      arr.time <= %s
  ) s
  JOIN app_store_ranks ar ON (s.id = ar.rank_request)
  WHERE
    ar.app IN %s
  ORDER BY rank_time
  ;
  """


def followed_apps_country_rank_data_cache_key(app_id, country_alpha2):
  return cache_key(country_app_rank_data, app_id, country_alpha2)


# Setting consult_cache=True will check the cache for available results.
# This cache check ignores the start_epoch and end_epoch values, and any cached
# results that are returned were generated using a fixed end_epoch - start_epoch
# value of seven days.
def followed_apps_country_rank_data(app_id_list, country_alpha2, start_epoch, end_epoch,
    consult_cache=True):
  cachedResults = []
  if consult_cache:
    paredAppIdList = []
    for appId in app_id_list:
      key = followed_apps_country_rank_data_cache_key(appId, country_alpha2)
      cachedResult = cache.get(key)
      if cachedResult:
        cachedResults.extend(cachedResult)
      else:
        paredAppIdList.append(appId)
    if cachedResults:
      app_id_list = paredAppIdList

  if not app_id_list:
    return cachedResults

  directResults = country_app_rank_data(app_id_list, country_alpha2, start_epoch, end_epoch)
  cachedResults.extend(directResults)
  return cachedResults


def country_app_rank_data(app_id_list, country_alpha2, start_epoch, end_epoch):
  if not app_id_list:
    return []

  appIdTuple = tuple(app_id_list)
  regionId = AppStoreRegion.objects.get(country__alpha2=country_alpha2).id
  startTime = from_timestamp(start_epoch, utc)
  endTime = from_timestamp(end_epoch, utc)

  rankTypes = AppStoreRankType.objects.all()
  rankTypeInfo = {}
  for rankType in rankTypes:
    rankTypeInfo[rankType.id] = {}
    displayName = rankType.display_name
    deviceType = None
    if rankType.platform.name == 'iOS':
      m = re.match(r'^(.*) iPad$', rankType.display_name)
      if m:
        deviceType = 'iPad'
        displayName = m.group(1)
      else:
        deviceType = 'iPhone'
    rankTypeInfo[rankType.id]['display_name'] = displayName
    rankTypeInfo[rankType.id]['device_type'] = deviceType
    rankTypeInfo[rankType.id]['platform_name'] = rankType.platform.display_name
    rankTypeInfo[rankType.id]['platform_id'] = rankType.platform.id

  cursor = connection.cursor()
  cursor.execute(COUNTRY_APP_RANK_QUERY, [regionId, startTime, endTime, appIdTuple])

  resultDict = {}
  for row in cursor.fetchall():
    appId, rank, categoryId, categoryName, rankTypeId, rankTime = row[:6]

    key = (appId, categoryId, rankTypeId)
    if key not in resultDict:
      resultDict[key] = {
        'app_id': appId,
        'platform_id': rankTypeInfo[rankTypeId]['platform_id'],
        'platform': rankTypeInfo[rankTypeId]['platform_name'],
        'series_data': [
          {
            'type': 'Ranks',
            'category': categoryName,
            'category_type': rankTypeInfo[rankTypeId]['display_name'],
            'device_type': rankTypeInfo[rankTypeId]['device_type'],
            'times': [],
            'current': []
          }
        ]
      }

    rankTimestamp = timegm(rankTime.utctimetuple())
    if rank:
      resultDict[key]['series_data'][0]['times'].append(rankTimestamp)
      resultDict[key]['series_data'][0]['current'].append(rank)

  mergedDict = {}
  for key in resultDict:
    appId = resultDict[key]['app_id']
    if appId not in mergedDict:
      mergedDict[appId] = {
        'app_id': resultDict[key]['app_id'],
        'platform_id': resultDict[key]['platform_id'],
        'platform': resultDict[key]['platform'],
        'series_data': [],
      }
    mergedDict[appId]['series_data'].append(resultDict[key]['series_data'][0])

  # Include a shell entry for apps without ranking data.
  for appId in app_id_list:
    if appId not in mergedDict:
      platform = AppStoreApp.objects.get(pk=appId).platform
      mergedDict[appId] = {
        'app_id': appId,
        'platform_id': platform.id,
        'platform': platform.display_name,
        'series_data': [],
      }

  ret = mergedDict.values()
  return ret
