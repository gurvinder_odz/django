from __future__ import absolute_import

from calendar import timegm
from celery import group, task
from celery.utils.log import get_task_logger
from core.celeryutils import exponential_backoff
from core.utils import unpack
from customers.models import CustomerAppFollow
from dashboard import rankutils
from datetime import datetime, timedelta
from django.conf import settings
from django.core.cache import cache


PCONN = settings.PERSISTENT_DEFAULT_CONNECTION

_FOLLOWED_APPS_RANK_WINDOW_DAYS = 7
_FOLLOWED_APPS_CACHE_TIMEOUT = 3 * 60 * 60

logger = get_task_logger(__name__)

@task(max_retries=5, ignore_result=True, bind=True)
def followed_apps_cache_refresh_kickoff(self, *args, **kwargs):
  try:
    taskList = []
    followedAppIdsDict = {record.app.id: True for record in
        CustomerAppFollow.objects.using(PCONN).all()}
    for appId in followedAppIdsDict:
      taskList.append(followed_apps_cache_refresh.s(app_id=appId))

    taskGroup = group(taskList)
    taskGroup.apply_async()

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))


@task(max_retries=5, ignore_result=True, bind=True)
def followed_apps_cache_refresh(self, *args, **kwargs):
  try:
    (appId, ) = unpack(kwargs, 'app_id')
    endEpoch = timegm(datetime.utcnow().utctimetuple())
    startEpoch = timegm((datetime.utcnow() - timedelta(
        days=_FOLLOWED_APPS_RANK_WINDOW_DAYS)).utctimetuple())

    data = rankutils.country_app_rank_data([appId], 'US', startEpoch, endEpoch)
    key = rankutils.followed_apps_country_rank_data_cache_key(appId, 'US')
    cache.set(key, data, _FOLLOWED_APPS_CACHE_TIMEOUT)

  except Exception as e:
    raise self.retry(exc=e, countdown=exponential_backoff(self.request.retries))