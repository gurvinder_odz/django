import logging

from calendar import timegm
from copy import copy
from core.timeutils import display, from_timestamp, utc_epoch
from customers.models import Customer
from datetime import timedelta
from django.db import connection
from ingestor.models import CustomerLabel
from pytz import utc

logger = logging.getLogger(__name__)

_SESSION_START = 0
_SESSION_STOP  = 1

# app_start and app_foreground
_SESSION_START_EVENT_IDS = [2, 3]

# app_terminate, app_background, logout
_SESSION_STOP_EVENT_IDS  = [1, 4, 8]

_MAX_LOOKBACK_MINUTES = 1440
_MAX_LOOKBACK_SECONDS = 60 * _MAX_LOOKBACK_MINUTES
_MAX_SESSION_LENGTH_MINUTES = 30
_MAX_SESSION_LENGTH_SECONDS = 60 * _MAX_SESSION_LENGTH_MINUTES
_MIN_SESSION_LENGTH_SECONDS = 1

_MIN_REVENUE_BUCKET_WIDTH_SECONDS = 5

_REALTIME_LABELS_QUERY = """
  SELECT
    cp.channel_product_id channel_product_id,
    cl.customer_label_id customer_label_id,
    cl.label label_str,
    (%s * floor(EXTRACT(EPOCH FROM ue.time) / %s)) bucketed_epoch,
    COUNT(*) label_count
  FROM
    user_events ue JOIN channel_products cp ON (ue.platform_app_id = cp.platform_app_id)
    JOIN customer_labels cl ON ue.customer_label_id = cl.customer_label_id
  WHERE ue.time >= %s
    AND ue.time <= %s
    AND cp.channel_product_id IN %s
    AND ue.customer_label_id IS NOT NULL
  GROUP BY 1, 2, 3, 4
  ;
  """

_REALTIME_REVENUE_QUERY = """
  SELECT
    channel_product_id,
    bucketed_epoch,
    SUM(amt_usd_micros) amt_usd_micros
  FROM (
    SELECT
      cp.channel_product_id channel_product_id,
      CASE WHEN (u.amt_usd_micros IS NOT NULL) THEN u.amt_usd_micros ELSE
        (CASE WHEN (cp.amt_usd_micros IS NOT NULL) THEN cp.amt_usd_micros ELSE 0 END) END amt_usd_micros,
      (%s * floor(EXTRACT(EPOCH FROM u.time) / %s)) bucketed_epoch
    FROM
      user_purchases u JOIN channel_products cp ON u.channel_product_id = cp.channel_product_id
    WHERE u.time >= %s
      AND u.time <= %s
      AND cp.channel_product_id IN %s
  ) subquery
  GROUP BY 1, 2
  ;
  """

_REALTIME_SESSIONS_QUERY = """
  SELECT
    u.time event_time,
    u.event_type,
    c.channel_product_id,
    u.scriber_id
  FROM
    user_events u JOIN channel_products c ON u.platform_app_id = c.platform_app_id
  WHERE
    u.time >= %s
    AND u.time <= %s
    AND c.channel_product_id IN %s
    AND u.event_type IN %s
  ORDER BY
    channel_product_id,
    scriber_id,
    event_time
  ;
  """

class Session():
  def __init__(self, startTime, endTime, endTimeIsFake, channelProductId, scriberId):
    self.startTime = startTime
    self.endTime = endTime
    self.endTimeIsFake = endTimeIsFake
    self.channelProductId = channelProductId
    self.scriberId = scriberId

class SessionEvent():
  def __init__(self, eventTime, eventType, channelProductId, scriberId):
    if not (eventTime and eventType and channelProductId and scriberId):
      raise ValueError('Null argument passed to session event constructor')
    if eventType not in _SESSION_START_EVENT_IDS + _SESSION_STOP_EVENT_IDS:
      raise ValueError('Cannot create session event with unrecognized user event type %s' % \
          eventType)
    self.eventTime = eventTime
    self.channelProductId = channelProductId
    self.scriberId = scriberId
    self.eventType = _SESSION_START if eventType in _SESSION_START_EVENT_IDS else _SESSION_STOP

  def compatible_with_session(self, session):
    return session is not None and \
        session.channelProductId == self.channelProductId and \
        session.scriberId == self.scriberId

  def apply_to_open_session(self, session):
    if self.eventType == _SESSION_START:
      remainderSession = Session(self.eventTime,
          self.eventTime + timedelta(seconds=_MAX_SESSION_LENGTH_SECONDS),
          True, self.channelProductId, self.scriberId)
      if self.compatible_with_session(session) and self.eventTime < session.endTime:
        # TODO(d-felix): Perhaps implement a runaway event time cap that is distinct from the
        # maximum session length.
        session.endTime = self.eventTime
      return remainderSession
    else:
      if self.compatible_with_session(session) and self.eventTime < session.endTime:
        # Correct the session end time and mark as legitimate.
        session.endTime = self.eventTime
        session.endTimeIsFake = False
      return None

class ActiveSessionCounts():
  def __init__(self, startEpoch, endEpoch, channelProductIdList):
    self.results = {'times': [], 'product_series': {}}
    self.timesToIndex = {}
    startTime = from_timestamp(startEpoch, utc)
    for delta in range(_MAX_LOOKBACK_MINUTES + _MAX_SESSION_LENGTH_MINUTES):
      roundedEpoch = _minute_rounded_epoch(startTime + timedelta(minutes=delta))
      if roundedEpoch > endEpoch:
        break
      self.results['times'].append(roundedEpoch)
      self.timesToIndex[roundedEpoch] = delta
    for channelProductId in channelProductIdList:
      self.results['product_series'][channelProductId] = {'current': [
        {
          'id': 'Sessions',
          'name': 'Active Sessions',
          'data': [0] * len(self.results['times'])
        }
      ]}

  def build_times_index(self):
    self.timesToIndex = {}
    for idx, epoch in enumerate(self.results['times']):
      self.timesToIndex[epoch] = idx

  def truncate(self, startEpoch, endEpoch):
    newStartEpoch = min(max(startEpoch, self.results['times'][0]),
        self.results['times'][-1])
    newRoundedStartEpoch = _minute_rounded_epoch(from_timestamp(newStartEpoch, utc))
    newEndEpoch = max(min(endEpoch, self.results['times'][-1]),
        self.results['times'][0])
    newRoundedEndEpoch = _minute_rounded_epoch(from_timestamp(newEndEpoch, utc))
    newStartIndex = self.timesToIndex[newRoundedStartEpoch]
    newEndIndex = self.timesToIndex[newRoundedEndEpoch]
    self.results['times'] = copy(self.results['times'][newStartIndex: newEndIndex + 1])
    for channelProductId in self.results['product_series']:
      for countData in self.results['product_series'][channelProductId]['current']:
        countData['data'] = copy(countData['data'][newStartIndex: newEndIndex + 1])
    self.build_times_index()

  def process_session(self, session):
    if session is None:
      return
    elif (session.endTime - session.startTime) < timedelta(seconds=_MIN_SESSION_LENGTH_SECONDS):
      return
    startEpoch = min(max(_minute_rounded_epoch(session.startTime), self.results['times'][0]),
        self.results['times'][-1])
    endEpoch = max(min(_minute_rounded_epoch(session.endTime), self.results['times'][-1]),
        self.results['times'][0])
    startIndex = self.timesToIndex[startEpoch]
    endIndex = self.timesToIndex[endEpoch]
    if session.channelProductId in self.results['product_series']:
      for data in self.results['product_series'][session.channelProductId]['current']:
        if data['id'] == 'Sessions':
          for index in range(startIndex, endIndex + 1):
            data['data'][index] += 1

def realtime_sessions(user, channelProductIdList, startEpoch, endEpoch):
  dbStartEpoch = max(startEpoch, utc_epoch() - _MAX_LOOKBACK_SECONDS) - _MAX_SESSION_LENGTH_SECONDS
  dbStartTime = from_timestamp(dbStartEpoch, utc)
  dbStartTimeStr = display(dbStartTime)
  dbEndTime = from_timestamp(endEpoch, utc)
  dbEndTimeStr = display(dbEndTime)
  channelProductIdTuple = tuple(channelProductIdList)
  eventTypeTuple = tuple(_SESSION_START_EVENT_IDS + _SESSION_STOP_EVENT_IDS)

  counts = ActiveSessionCounts(dbStartEpoch, endEpoch, channelProductIdList)
  if channelProductIdTuple:
    cursor = connection.cursor()
    cursor.execute(_REALTIME_SESSIONS_QUERY, [dbStartTimeStr, dbEndTimeStr, channelProductIdTuple,
        eventTypeTuple])
    bufferedSession = None
    for row in cursor.fetchall():
      sessionEvent = SessionEvent(row[0], row[1], row[2], row[3])
      remainderSession = sessionEvent.apply_to_open_session(bufferedSession)
      counts.process_session(bufferedSession)
      bufferedSession = remainderSession
    counts.process_session(bufferedSession)

  # Remove any old data that was used to generate accurate session counts at the start time.
  counts.truncate(startEpoch, endEpoch)
  return counts.results

def _minute_rounded_epoch(dt):
  return timegm((dt - timedelta(seconds=dt.second, microseconds=dt.microsecond)).utctimetuple())

# TODO(d-felix): Abstraction.  This is too similar to realtime_labels.
def realtime_revenue(user, channelProductIdList, bucketWidthSecs, startEpoch, endEpoch):
  channelProductIdTuple = tuple(channelProductIdList)
  bucketWidthSecs = max(int(bucketWidthSecs), _MIN_REVENUE_BUCKET_WIDTH_SECONDS)

  dbStartEpoch = max(startEpoch, utc_epoch() - _MAX_LOOKBACK_SECONDS)
  dbStartTime = from_timestamp(dbStartEpoch, utc)
  dbStartTimeStr = display(dbStartTime)
  dbEndEpoch =min(endEpoch, utc_epoch())
  dbEndTime = from_timestamp(dbEndEpoch, utc)
  dbEndTimeStr = display(dbEndTime)

  times = []
  roundedStartEpoch = bucketWidthSecs * (dbStartEpoch / bucketWidthSecs)
  bucketEpoch = roundedStartEpoch
  while bucketEpoch <= dbEndEpoch:
    times.append(bucketEpoch)
    bucketEpoch += bucketWidthSecs

  results = {'times': times, 'product_series': {}}
  for channelProductId in channelProductIdList:
    results['product_series'][channelProductId] = {
      'current': [
        {
          'id': 'revenue',
          'name': 'Revenue',
          'data': [0.0] * len(results['times'])
        }
      ]
    }

  if channelProductIdTuple:
    cursor = connection.cursor()
    cursor.execute(_REALTIME_REVENUE_QUERY, [bucketWidthSecs, bucketWidthSecs,
        dbStartTimeStr, dbEndTimeStr, channelProductIdTuple])
    for row in cursor.fetchall():
      channelProductId, bucketEpoch, amtUsdMicros = row
      bucketEpoch = int(bucketEpoch)
      # Protect against the database computing different bucket boundaries.
      epochIndex = (bucketEpoch - roundedStartEpoch) / bucketWidthSecs
      if 0 <= epochIndex < len(times):
        for metric in results['product_series'][channelProductId]['current']:
          if metric['id'] == 'revenue':
            metric['data'][epochIndex] += float(amtUsdMicros) / 1000000.0

  return results

def realtime_labels(user, channelProductIdList, bucketWidthSecs, startEpoch, endEpoch):
  # Return results for all customer labels, not just those represented in the analysis period.
  customerLabels = CustomerLabel.objects.filter(customer_id__auth_user=user)

  channelProductIdTuple = tuple(channelProductIdList)
  bucketWidthSecs = max(int(bucketWidthSecs), _MIN_REVENUE_BUCKET_WIDTH_SECONDS)

  dbStartEpoch = max(startEpoch, utc_epoch() - _MAX_LOOKBACK_SECONDS)
  dbStartTime = from_timestamp(dbStartEpoch, utc)
  dbStartTimeStr = display(dbStartTime)
  dbEndEpoch = min(endEpoch, utc_epoch())
  dbEndTime = from_timestamp(dbEndEpoch, utc)
  dbEndTimeStr = display(dbEndTime)

  times = []
  roundedStartEpoch = bucketWidthSecs * (dbStartEpoch / bucketWidthSecs)
  bucketEpoch = roundedStartEpoch
  while bucketEpoch <= dbEndEpoch:
    times.append(bucketEpoch)
    bucketEpoch += bucketWidthSecs

  results = {'times': times, 'product_series': {}}
  for channelProductId in channelProductIdList:
    results['product_series'][channelProductId] = {'current': []}
    currentList = results['product_series'][channelProductId]['current']
    for label in customerLabels:
      currentList.append(
        {
          'id': label.customer_label_id,
          'name': label.label,
          'data': [0] * len(results['times'])
        }
      )

  if channelProductIdTuple:
    cursor = connection.cursor()
    cursor.execute(_REALTIME_LABELS_QUERY, [bucketWidthSecs, bucketWidthSecs,
        dbStartTimeStr, dbEndTimeStr, channelProductIdTuple])
    for row in cursor.fetchall():
      channelProductId, labelId, label, bucketEpoch, count = row
      bucketEpoch = int(bucketEpoch)
      # Protect against the database computing different bucket boundaries.
      epochIndex = (bucketEpoch - roundedStartEpoch) / bucketWidthSecs
      if 0 <= epochIndex < len(times):
        for metric in results['product_series'][channelProductId]['current']:
          if metric['id'] == labelId:
            metric['data'][epochIndex] += int(count)

  return results
