from core.cache import cache_key, cache_results
from django.core.cache import cache
from django.db import connection

# An autocomplete results list will be cached if its length is less than this amount.
_AUTOCOMPLETE_CACHE_THRESHOLD = 1000

_AUTOCOMPLETE_USERS_QUERY = """
  SELECT
    u.customer_user_id customer_user_id,
    u.email email,
    u.username username,
    u.scriber_id
  FROM
    auth_user a JOIN customers c ON a.id = c.auth_user
    JOIN users u ON c.customer_id = u.customer_id
  WHERE
    a.id = %s AND (
      LOWER(LEFT(u.customer_user_id, %s)) = LOWER(%s) OR
      LOWER(LEFT(u.email, %s)) = LOWER(%s) OR
      LOWER(LEFT(u.username, %s)) = LOWER(%s)
    )
  LIMIT %s
  ;
  """

def _filter_prefix_matches(argList, prefix):
  if prefix is None:
    return []
  prefix = prefix.lower()
  ret = []
  for match in argList:
    # Matching is case-insensitive.
    if match['value'].lower().startswith(prefix):
      ret.append(match)
  return ret

def _update_autocomplete_cache(arg, *args, **kwargs):
  return len(arg) < _AUTOCOMPLETE_CACHE_THRESHOLD

def autocomplete_users(userId, prefixArg, maxResults=20):
  """
  A wrapper for the cached autocomplete_users_backend function.
  This function prevents the maxResults argument from being included in the
  autocomplete_users_backend cache key.
  """
  results = autocomplete_users_backend(userId, prefixArg)
  return results[:maxResults]

@cache_results(timeout=60, update_cache=_update_autocomplete_cache)
def autocomplete_users_backend(userId, prefixArg):
  """
  Returns a list of dicts containing information about users with customer-assigned IDs, usernames,
  or email addresses having prefixArg as a prefix.
  Returns an empty list if prefixArg is empty or None.
  For best results, provide only lowercase prefix arguments.
  """
  # Don't return results for an empty or missing prefix.
  if prefixArg is None or len(prefixArg) == 0:
    return []
  prefix = prefixArg.lower()

  # For a prefix of length 2 or greater, check its nonempty nontrivial prefixes for cached results.
  if len(prefix) > 1:
    for i in range(1, len(prefix)):
      # Construct the cache key from the potentially non-lowercased prefixArg argument.
      key = cache_key(autocomplete_users_backend, userId, prefixArg[:0-i])
      cachedResults = cache.get(key)
      if cachedResults is not None:
        return _filter_prefix_matches(cachedResults, prefix)

  # If no prior results are found, consult the database.
  results = []
  cursor = connection.cursor()
  pLen = len(prefix)
  execArgs = [userId, pLen, prefix, pLen, prefix, pLen, prefix, _AUTOCOMPLETE_CACHE_THRESHOLD]
  cursor.execute(_AUTOCOMPLETE_USERS_QUERY, execArgs)
  for row in cursor:
    for index in range(3):
      # Matching is case-insensitive.
      if row[index].lower().startswith(prefix):
        # TODO(d-felix): Obfuscate the returned scriber_id value.
        results.append({"value": row[index],
            "column": cursor.description[index].name, "id": row[3]})

  return results
