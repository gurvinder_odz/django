import logging
from django.contrib.auth.models import User

from calendar import timegm
from copy import deepcopy
from core.timeutils import convert_tz, display, from_timestamp, midnight_timestamp, timestamp, DATE_FMT
from customers.models import Customer, CustomerAppFollow, CustomerExternalLoginInfo
from datetime import datetime, timedelta
from django.db import connection
from ingestor.models import CustomerLabel
from metrics import queries
from metrics.constants import MRR_DURATION_DAYS, SESSION_BOUNDARY_EVENT_IDS
from metrics.models import DailyCustomerMetrics, DailyProductMetrics
from pytz import timezone
from dashboard import rankutils
from appstore.models import AppStoreCategory, AppStorePlatform

logger = logging.getLogger(__name__)

_RECENT_SESSION_THRESHOLD_DAYS = 28

# TODO(d-felix): Handle the customer_user_id case.
# Note that app purchases may be associated with a different scriber_id.
_USER_INFO_QUERY = """
  SELECT
    event_subquery.scriber_id scriber_id,
    u.username username,
    u.email email,
    u.customer_user_id customer_user_id,
    event_subquery.platform_app_id platform_app_id,
    pa.platform_app_str,
    pt.platform_type_str,
    event_subquery.session_count session_count,
    event_subquery.recent_session_count recent_session_count,
    event_subquery.first_active_time first_active_time,
    event_subquery.last_active_time last_active_time,
    event_subquery.in_app_purchase_names in_app_purchase_names,
    event_subquery.revenue_usd_micros revenue_usd_micros
  FROM (
    SELECT
      ue.scriber_id,
      (CASE WHEN (ue.platform_app_id IS NOT NULL) THEN ue.platform_app_id ELSE 0 END) platform_app_id,
      SUM(CASE WHEN (ue.event_type IN %s) THEN 1 ELSE 0 END) session_count,
      SUM(CASE WHEN (ue.event_type IN %s and ue.time > %s) THEN 1 ELSE 0 END) recent_session_count,
      MIN(CASE WHEN (ue.event_type IN %s) THEN ue.time ELSE NULL END) first_active_time,
      MAX(CASE WHEN (ue.event_type IN %s) THEN ue.time ELSE NULL END) last_active_time,
      ARRAY_AGG(DISTINCT CASE WHEN (cp.platform_app_id IS NULL) THEN cp.channel_product_str ELSE NULL END) in_app_purchase_names,
      SUM(CASE WHEN (up.amt_usd_micros IS NOT NULL) THEN up.amt_usd_micros ELSE 0 END) revenue_usd_micros
    FROM
      user_events ue LEFT JOIN user_purchases up ON ue.event_id = up.event_id
      LEFT JOIN channel_products cp ON up.channel_product_id = cp.channel_product_id
    WHERE
      ue.scriber_id = %s
    GROUP BY 1, 2
  ) event_subquery
  LEFT JOIN platform_apps pa ON event_subquery.platform_app_id = pa.platform_app_id
  LEFT JOIN platform_types pt ON pa.platform_type_id = pt.platform_type_id
  JOIN users u ON event_subquery.scriber_id = u.scriber_id
  ;
  """

_PRODUCT_INFO_QUERY = """
  SELECT
    cp.channel_product_id channel_product_id,
    (CASE WHEN (cp.display_name IS NOT NULL) THEN cp.display_name ELSE cp.channel_product_str END) channel_product_str,
    cp.platform_app_id IS NOT NULL is_app,
    (CASE WHEN (pt.platform_type_id IS NOT NULL) THEN pt.platform_type_str ELSE NULL END) platform,
    pa.platform_app_id parent_app_id,
    cp.platform_app_id app_id,
    (CASE WHEN (pt2.platform_type_id IS NOT NULL) THEN pt2.platform_type_str ELSE NULL END) channel_platform,
    (CASE WHEN (ios_apps.apple_identifier IS NOT NULL) THEN true ELSE false END) is_active_ios_sales_report_product,
    cp.apple_identifier apple_identifier
  FROM
    channel_products cp JOIN customers c ON cp.customer_id = c.customer_id
    JOIN channels ch ON cp.channel_id = ch.channel_id
    LEFT JOIN platform_apps pa ON ch.platform_app_id = pa.platform_app_id
    LEFT JOIN platform_apps pa2 ON cp.platform_app_id = pa2.platform_app_id
    LEFT JOIN platform_types pt ON pa2.platform_type_id = pt.platform_type_id
    LEFT JOIN platform_types pt2 ON ch.platform_type_id = pt2.platform_type_id
    LEFT JOIN (
      SELECT DISTINCT
        arr.apple_identifier apple_identifier
      FROM
        customers c JOIN customer_external_login_info li ON (c.customer_id = li.customer_id)
        JOIN apple_reports ar ON (li.apple_vendor_id = ar.vendor_id)
        JOIN apple_report_records arr ON (ar.report_id = arr.report_id)
      WHERE
        c.auth_user = %s AND
        li.is_active = true
    ) ios_apps ON (cp.apple_identifier = ios_apps.apple_identifier)
  WHERE
    c.auth_user = %s
  ORDER BY is_app DESC
  ;
  """

_PLATFORM_APP_DAILY_LABEL_METRICS_QUERY = """
  SELECT
    cp.channel_product_id channel_product_id,
    m.customer_label_id customer_label_id,
    m.date date,
    m.count count
  FROM
    channel_products cp JOIN daily_label_metrics m ON cp.platform_app_id = m.platform_app_id
  WHERE
    m.date >= %s
    AND m.date <= %s
    AND cp.channel_product_id IN %s
  ;
  """

_PLATFORM_APP_DAILY_METRICS_QUERY = """
  SELECT
    cp.channel_product_id channel_product_id,
    m.date date,
    m.dau dau,
    m.mau mau,
    m.sessions sessions
  FROM
    channel_products cp JOIN daily_platform_app_metrics m ON (cp.platform_app_id = m.platform_app_id)
  WHERE
    m.date >= %s
    AND m.date <= %s
    AND cp.channel_product_id IN %s
  ;
  """

_PRODUCT_DAILY_METRICS_QUERY = """
  SELECT
    CASE WHEN (sm.channel_product_id IS NOT NULL) THEN sm.channel_product_id ELSE pm.channel_product_id END channel_product_id,
    CASE WHEN (sm.date IS NOT NULL) THEN sm.date ELSE pm.date END date,
    CASE WHEN (sm.developer_revenue_usd_micros IS NOT NULL) THEN sm.developer_revenue_usd_micros
      ELSE pm.developer_revenue_usd_micros END revenue,
    CASE WHEN (sm.downloads IS NOT NULL) THEN sm.downloads ELSE 0 END downloads,
    CASE WHEN (sm.updates IS NOT NULL) THEN sm.updates ELSE 0 END updates,
    CASE WHEN (sm.refunds IS NOT NULL) THEN sm.refunds ELSE 0 END refunds
  FROM
    daily_product_metrics pm FULL OUTER JOIN daily_sales_report_metrics sm
      ON (pm.channel_product_id = sm.channel_product_id AND pm.date = sm.date)
  WHERE
    (pm.date >= %s OR pm.date IS NULL)
    AND (pm.date <= %s OR pm.date IS NULL)
    AND (pm.channel_product_id IN %s OR pm.channel_product_id IS NULL)
    AND (sm.date >= %s OR sm.date IS NULL)
    AND (sm.date <= %s OR sm.date IS NULL)
    AND (sm.channel_product_id IN %s or sm.channel_product_id IS NULL)
  ;
  """

_PRODUCT_MONTHLY_METRICS_QUERY = """
  SELECT
    CASE WHEN (sm.channel_product_id IS NOT NULL) THEN sm.channel_product_id ELSE pm.channel_product_id END channel_product_id,
    CASE WHEN (sm.date IS NOT NULL) THEN sm.date ELSE pm.date END date,
    CASE WHEN (sm.developer_revenue_usd_micros IS NOT NULL) THEN sm.developer_revenue_usd_micros
      ELSE pm.developer_revenue_usd_micros END revenue,
    CASE WHEN (sm.recurring_revenue_usd_micros IS NOT NULL) THEN sm.recurring_revenue_usd_micros
      ELSE pm.developer_recurring_revenue_usd_micros END recurring_revenue,
    CASE WHEN (sm.downloads IS NOT NULL) THEN sm.downloads ELSE 0 END downloads,
    CASE WHEN (sm.refunds IS NOT NULL) THEN sm.refunds ELSE 0 END refunds
  FROM
    daily_product_metrics pm FULL OUTER JOIN daily_sales_report_metrics sm
      ON (pm.channel_product_id = sm.channel_product_id AND pm.date = sm.date)
  WHERE
    (pm.date >= %s OR pm.date IS NULL)
    AND (pm.date <= %s OR pm.date IS NULL)
    AND (pm.channel_product_id IN %s OR pm.channel_product_id IS NULL)
    AND (sm.date >= %s OR sm.date IS NULL)
    AND (sm.date <= %s OR sm.date IS NULL)
    AND (sm.channel_product_id IN %s or sm.channel_product_id IS NULL)
  ;
  """

def product_info_for_auth_user(userId, **kwargs):
  cursor = connection.cursor()
  cursor.execute(_PRODUCT_INFO_QUERY, [userId, userId])
  appsToProducts = {}
  results = {}
  count = 0;
  for row in cursor.fetchall():
    (channelProductId, channelProductStr, isApp, platform, parentAppId, appId, channelPlatform,
        isActiveIosProduct, appleIdentifier) = row[:9]
    if channelProductStr == 'com.trailbehind.stopwatch-':
      continue
    #if channelProductStr == 'otm-gaia-bundle':
    #  continue

    if 'com.trailbehind.cutesky' == channelProductStr:
       continue
    if 'com.gaiagps' in channelProductStr:
       continue
    if 'GaiaPro' == channelProductStr:
       continue
    if 'GaiaGreen' in channelProductStr:
       continue
    if 'GaiaGreen' in channelProductStr:
       continue
    if 'GaiaProYearAutoRenew' in channelProductStr and platform == 'Google Play':
       continue
    if 'GaiaPro1YearAutoRenew' in channelProductStr:
       continue
    if 'gaiapro_1year_subscr' in channelProductStr:
       count += 1
       if count == 2:
         continue

    # Skip apple products corresponding to inactive CustomerExternalLoginInfo records.
    if appleIdentifier is not None and not isActiveIosProduct:
      continue

    # App records should be processed before those having isApp == False.
    if isApp:
      #
      # Skip unrecognized iOS apps. Note that unrecognize non-app products are not skipped (yet).
      if platform == 'iOS' and not isActiveIosProduct:
        continue
      if channelProductId not in results:
        results[channelProductId] = {}
        results[channelProductId]['in_app_products'] = []
      results[channelProductId]['platform'] = platform
      results[channelProductId]['product_id_string'] = channelProductStr
      appsToProducts[appId] = channelProductId
    elif parentAppId is not None:
      if parentAppId in appsToProducts:
        parentChannelProductId = appsToProducts[parentAppId]
        results[parentChannelProductId]['in_app_products'].append({
            'product_id': channelProductId,
            'product_id_string': channelProductStr})
      else:
        # This is unexpected
        logger.error("Could not find channel_product for platform_app_id %s" % parentAppId)
    else:
      # Unexpected for now, but this could include webstore products.
      # Treat as top-level product.
      # This also includes app bundles.
      results[channelProductId] = {}
      results[channelProductId]['in_app_products'] = []
      results[channelProductId]['product_id_string'] = channelProductStr
      results[channelProductId]['platform'] = platform if platform else channelPlatform

  return results


def time_info_from_bounds(userId, start_in_seconds, end_in_seconds):
  customer = Customer.objects.get(auth_user__id=userId)
  customerTz = timezone(customer.timezone)
  startDate = from_timestamp(float(start_in_seconds), customerTz).date()
  endDate = from_timestamp(float(end_in_seconds), customerTz).date()
  previousEndDate = startDate - timedelta(days=1)
  previousStartDate = previousEndDate - (endDate - startDate)

  times = []
  previousTimes = []

  # Construct the list of times corresponding to midnight on the appropriate day.
  for delta in range((endDate - startDate + timedelta(days=1)).days):
    date = startDate + timedelta(days=delta)
    previousDate = previousStartDate + timedelta(days=delta)
    times.append(midnight_timestamp(date, customerTz))
    previousTimes.append(midnight_timestamp(previousDate, customerTz))

  return {
      'times': times,
      'start_date': startDate,
      'end_date': endDate,
      'previous_times': previousTimes,
      'previous_start_date': previousStartDate,
      'previous_end_date': previousEndDate
  }


# TODO(d-felix): Revisit handling for products which are not apps.
def daily_label_metrics_for_products(user, product_id_list, time_info):
  startDate = time_info['start_date']
  endDate = time_info['end_date']
  customerLabels = CustomerLabel.objects.filter(customer_id__auth_user=user)
  idToLabels = {label.customer_label_id: label for label in customerLabels}

  tempResults = {}
  for channelProductId in product_id_list:
    tempResults[channelProductId] = {}
    for label in customerLabels:
      tempResults[channelProductId][label.customer_label_id] = {'count': {}, 'count_series': []}

  productIdTuple = tuple(product_id_list)

  cursor = connection.cursor()
  cursor.execute(_PLATFORM_APP_DAILY_LABEL_METRICS_QUERY, [startDate.strftime(DATE_FMT),
      endDate.strftime(DATE_FMT), productIdTuple])
  for row in cursor.fetchall():
    channelProductId, labelId, date, count = row[0], row[1], row[2], row[3]
    tempResults[channelProductId][labelId]['count'][date] = count

  daysInAnalysisPeriod = (endDate - startDate + timedelta(days=1)).days
  for delta in range(daysInAnalysisPeriod):
    date = startDate + timedelta(days=delta)
    for channelProductId in tempResults:
      for labelId in tempResults[channelProductId]:
        tempResults[channelProductId][labelId]['count_series'].append(
            tempResults[channelProductId][labelId].get('count', {}).get(date, 0))

  results = {}
  for channelProductId in tempResults:
    results[channelProductId] = {'current': []}
    for labelId in tempResults[channelProductId]:
      results[channelProductId]['current'].append(
        {
          'id': labelId,
          'name': idToLabels[labelId].label,
          'data': tempResults[channelProductId][labelId]['count_series'],
        }
      )

  # Ensure that unwanted tempResults data is garbage collected.
  return deepcopy(results)


def daily_metrics_for_products(product_id_list, start_date, end_date):
  tempResults = {}
  for channelProductId in product_id_list:
    tempResults[channelProductId] = {
      'dau': {}, 'dau_series': [],
      'mau': {}, 'mau_series': [],
      'mrr': {}, 'mrr_series': [],
      'revenue': {}, 'revenue_series': [],
      'sessions': {}, 'sessions_series': [],
      'revenue28': {}, 'revenue28_series': [],
      'downloads': {}, 'downloads_series': [],
      'updates': {}, 'updates_series': [],
      'refunds': {}, 'refunds_series': [],
      'downloads28': {}, 'downloads28_series': [],
      'refunds28': {}, 'refunds28_series': [],
    }

  # A tuple is required for the queries' IN arguments.
  productIdTuple = tuple(product_id_list)

  cursor = connection.cursor()
  cursor.execute(_PLATFORM_APP_DAILY_METRICS_QUERY, [start_date.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple])
  for row in cursor.fetchall():
    channelProductId, date, dau, mau, sessions = row[0], row[1], row[2], row[3], row[4]
    tempResults[channelProductId]['dau'][date] = dau
    tempResults[channelProductId]['mau'][date] = mau
    tempResults[channelProductId]['sessions'][date] = sessions

  mrrStartDate = start_date - timedelta(days=MRR_DURATION_DAYS-1)
  cursor.execute(_PRODUCT_MONTHLY_METRICS_QUERY, [mrrStartDate.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple, mrrStartDate.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple])
  for row in cursor.fetchall():
    channelProductId, date, revenue, recRevenue, downloads, refunds = row[:6]
    dateToStartUpdate = max(date, start_date)
    dateToStopUpdate = min(date + timedelta(days=MRR_DURATION_DAYS-1), end_date)
    numberOfDatesToUpdate = 1 + (dateToStopUpdate - dateToStartUpdate).days
    for delta in range(numberOfDatesToUpdate):
      dateToUpdate = dateToStartUpdate + timedelta(days=delta)
      tempResults[channelProductId]['mrr'][dateToUpdate] = (recRevenue / 1000000.0) + \
          tempResults[channelProductId]['mrr'].get(dateToUpdate, 0.0)
      tempResults[channelProductId]['revenue28'][dateToUpdate] = (revenue / 1000000.0) + \
          tempResults[channelProductId]['revenue28'].get(dateToUpdate, 0.0)
      tempResults[channelProductId]['downloads28'][dateToUpdate] = downloads + \
          tempResults[channelProductId]['downloads28'].get(dateToUpdate, 0.0)
      tempResults[channelProductId]['refunds28'][dateToUpdate] = refunds + \
          tempResults[channelProductId]['refunds28'].get(dateToUpdate, 0.0)

  cursor.execute(_PRODUCT_DAILY_METRICS_QUERY, [start_date.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple, start_date.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple,])
  for row in cursor.fetchall():
    channelProductId, date, revenue, downloads, updates, refunds = row[:6]
    tempResults[channelProductId]['revenue'][date] = revenue / 1000000.0
    tempResults[channelProductId]['downloads'][date] = downloads
    tempResults[channelProductId]['updates'][date] = updates
    tempResults[channelProductId]['refunds'][date] = refunds

  daysInAnalysisPeriod = (end_date - start_date + timedelta(days=1)).days
  for delta in range(daysInAnalysisPeriod):
    date = start_date + timedelta(days=delta)
    for channelProductId in tempResults:
      tempResults[channelProductId]['dau_series'].append(
          tempResults[channelProductId].get('dau', {}).get(date, 0))
      tempResults[channelProductId]['mau_series'].append(
          tempResults[channelProductId].get('mau', {}).get(date, 0))
      tempResults[channelProductId]['mrr_series'].append(
          tempResults[channelProductId].get('mrr', {}).get(date, float(0)))
      tempResults[channelProductId]['revenue_series'].append(
          tempResults[channelProductId].get('revenue', {}).get(date, float(0)))
      tempResults[channelProductId]['revenue28_series'].append(
          tempResults[channelProductId].get('revenue28', {}).get(date, float(0)))
      tempResults[channelProductId]['sessions_series'].append(
          tempResults[channelProductId].get('sessions', {}).get(date, 0))
      tempResults[channelProductId]['downloads_series'].append(
          tempResults[channelProductId].get('downloads', {}).get(date, 0))
      tempResults[channelProductId]['updates_series'].append(
          tempResults[channelProductId].get('updates', {}).get(date, 0))
      tempResults[channelProductId]['refunds_series'].append(
          tempResults[channelProductId].get('refunds', {}).get(date, 0))
      tempResults[channelProductId]['downloads28_series'].append(
          tempResults[channelProductId].get('downloads28', {}).get(date, 0))
      tempResults[channelProductId]['refunds28_series'].append(
          tempResults[channelProductId].get('refunds28', {}).get(date, 0))

  # put sessions and other realtime graphs last if not data present
  has_sessions = False
  for channelProductId in tempResults:
    if sum(tempResults[channelProductId]['sessions_series']) > 0:
      has_sessions = True

  results = {}
  for channelProductId in tempResults:
    revenue28Series = tempResults[channelProductId]['revenue28_series']
    mauSeries = tempResults[channelProductId]['mau_series']
    results[channelProductId] = {}
    results[channelProductId]['data'] = [
      {
        'id': 'revenue',
        'name': 'Daily Revenue',
        'format': 'money',
        'help_text': 'The total money made each day, excluding fees paid to Apple and Google.',
        'data': tempResults[channelProductId]['revenue_series']
      },
      {
        'id': 'revenue28',
        'name': '28-Day Revenue',
        'format': 'money',
        'help_text': 'A 28-day rolling average of revenue.\n\nFour weeks are used instead of a month to remove weekly trend fluctuations.',
        'data': tempResults[channelProductId]['revenue28_series']
      },
      {
        'id': 'downloads',
        'name': 'Downloads',
        'help_text': 'The number of downloads, according to Apple and Google sales reports.',
        'data': tempResults[channelProductId]['downloads_series']
      },
      {
        'id': 'mrr',
        'name': 'Monthly Recurring',
        'format': 'money',
        'help_text': 'Revenue from subscriptions for the last 28 days.\n\nRecurring revenue is calculated by totalling new subscription revenue, renewals, and subtracting churned revenue.',
        'data': tempResults[channelProductId]['mrr_series']
      },
      {
        'id': 'updates',
        'name': 'Updates',
        'help_text': 'The number of app updates according to Apple and Google sales reports.',
        'data': tempResults[channelProductId]['updates_series']
      },
      {
        'id': 'refunds',
        'name': 'Refunds',
        'help_text': 'The number of refunds according to Apple and Google sales reports.\n\nNote that when a user purchases a bundle through Apple, they are refunded apps they purchased in the bundle previously.',
        'data': tempResults[channelProductId]['refunds_series']
      },
      {
        'id': 'downloads28',
        'name': '28-Day Downloads',
        'help_text': 'A 28-day rolling average of the number of downloads, according to Apple and Google sales reports.',
        'data': tempResults[channelProductId]['downloads28_series']
      },
      {
        'id': 'refunds28',
        'name': '28-Day Refunds',
        'help_text': 'A 28-day rolling average of the number of refunds, according to Apple and Google sales reports.',
        'data': tempResults[channelProductId]['refunds28_series']
      },
    ]

    # put mau 3rd if it's active, last otherwise
    data = tempResults[channelProductId]['mau_series']
    info = {
        'id': 'mau',
        'name': '28-Day Users',
        'help_text': 'Unique users over a 28-day period.\n\nUsers are de-duped across apps and days.',
        'data': data
      }
    if has_sessions:
      results[channelProductId]['data'].insert(0,info)
    else:
      results[channelProductId]['data'].append(info)

    # put dau 2nd if it's active, last otherwise
    data = tempResults[channelProductId]['dau_series']
    info = {
        'id': 'dau',
        'name': 'Daily Users',
        'help_text': 'Unique users today.\n\nUsers are de-duped across apps.',
        'data': data
      }
    if has_sessions:
      results[channelProductId]['data'].insert(0,info)
    else:
      results[channelProductId]['data'].append(info)

    # put sessions 1st if it's active, last otherwise
    data = tempResults[channelProductId]['sessions_series']
    info = {
        'id': 'sessions',
        'name': 'Sessions',
        'help_text': 'The number of app sessions today.',
        'data': data
      }
    if has_sessions:
      results[channelProductId]['data'].insert(0,info)
    else:
      results[channelProductId]['data'].append(info)

    '''
    {
        'id': 'arpu',
        'name': 'ARPU',
        'format': 'money',
        'data': [(revenue28Series[i] / mauSeries[i]) if mauSeries[i] != 0 else 0.0
                for i in range(daysInAnalysisPeriod)]
      },
    ,
    {
      'id': 'cancels',
      'name': 'Cancels',
      'data': [0] * daysInAnalysisPeriod
    },
    '''

  # Ensure that unwanted tempResults data is garbage collected.
  return deepcopy(results)


def aggregated_daily_metrics_for_products(userId, product_id_list, start_date, end_date):
  tempResults = {
    'dau': {}, 'dau_series': [],
    'mau': {}, 'mau_series': [],
    'mrr': {}, 'mrr_series': [],
    'revenue': {}, 'revenue_series': [],
    'revenue28': {}, 'revenue28_series': [],
    'downloads': {}, 'downloads_series': [],
    'updates': {}, 'updates_series': [],
    'refunds': {}, 'refunds_series': [],
    'downloads28': {}, 'downloads28_series': [],
    'refunds28': {}, 'refunds28_series': [],
  }

  customer = Customer.objects.filter(auth_user__id=userId).first()
  customerTz = timezone(customer.timezone)

  # A tuple is required for the queries' IN arguments.
  productIdTuple = tuple(product_id_list)

  # DAU calculation process raw (not pre-aggregated) records with timestamps rather than dates.
  startDateAtMidnight = datetime.combine(start_date, datetime.min.time())
  startTime = convert_tz(startDateAtMidnight, customerTz, timezone('UTC'))
  startTimeStr = display(startTime)
  endDateAtMidnight = datetime.combine(end_date + timedelta(days=1), datetime.min.time())
  endTime = convert_tz(endDateAtMidnight, customerTz, timezone('UTC'))
  endTimeStr = display(endTime)

  cursor = connection.cursor()
  cursor.execute(queries.MULTIPLE_CHANNEL_PRODUCT_USER_ID_DAU_QUERY,
      [customer.timezone, productIdTuple, startTimeStr, endTimeStr])
  for row in cursor.fetchall():
    date, dau = row[0], row[1]
    tempResults['dau'][date] = dau

  # Compute approximate MAU additively
  # TODO(d-felix): Fix this! Look into approximation algorithms.
  cursor.execute(_PLATFORM_APP_DAILY_METRICS_QUERY, [start_date.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple])
  for row in cursor.fetchall():
    date, mau = row[1], row[3]
    tempResults['mau'][date] = mau + tempResults['mau'].get(date, 0)

  mrrStartDate = start_date - timedelta(days=MRR_DURATION_DAYS-1)
  cursor.execute(_PRODUCT_MONTHLY_METRICS_QUERY, [mrrStartDate.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple, mrrStartDate.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple])
  for row in cursor.fetchall():
    date, revenue, recRevenue, downloads, refunds = row[1:6]
    dateToStartUpdate = max(date, start_date)
    dateToStopUpdate = min(date + timedelta(days=MRR_DURATION_DAYS-1), end_date)
    numberOfDatesToUpdate = 1 + (dateToStopUpdate - dateToStartUpdate).days
    for delta in range(numberOfDatesToUpdate):
      dateToUpdate = dateToStartUpdate + timedelta(days=delta)
      tempResults['mrr'][dateToUpdate] = (recRevenue / 1000000.0) + \
          tempResults['mrr'].get(dateToUpdate, 0.0)
      tempResults['revenue28'][dateToUpdate] = (revenue / 1000000.0) + \
          tempResults['revenue28'].get(dateToUpdate, 0.0)
      tempResults['downloads28'][dateToUpdate] = downloads + \
          tempResults['downloads28'].get(dateToUpdate, 0.0)
      tempResults['refunds28'][dateToUpdate] = refunds + \
          tempResults['refunds28'].get(dateToUpdate, 0.0)

  cursor.execute(_PRODUCT_DAILY_METRICS_QUERY, [start_date.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple, start_date.strftime(DATE_FMT),
      end_date.strftime(DATE_FMT), productIdTuple])
  for row in cursor.fetchall():
    date, revenue = row[1], row[2]
    tempResults['revenue'][date] = (revenue / 1000000.0) + \
        tempResults['revenue'].get(date, float(0))
    tempResults['downloads'][date] = downloads + tempResults['downloads'].get(date, 0)
    tempResults['updates'][date] = updates + tempResults['updates'].get(date, 0)
    tempResults['refunds'][date] = refunds + tempResults['refunds'].get(date, 0)

  daysInAnalysisPeriod = (end_date - start_date + timedelta(days=1)).days
  for delta in range(daysInAnalysisPeriod):
    date = start_date + timedelta(days=delta)
    tempResults['dau_series'].append(tempResults['dau'].get(date, 0))
    tempResults['mau_series'].append(tempResults['mau'].get(date, 0))
    tempResults['mrr_series'].append(tempResults['mrr'].get(date, float(0)))
    tempResults['revenue_series'].append(tempResults['revenue'].get(date, float(0)))
    tempResults['revenue28_series'].append(tempResults['revenue28'].get(date, float(0)))
    tempResults['downloads_series'].append(tempResults['downloads'].get(date, 0))
    tempResults['updates_series'].append(tempResults['updates'].get(date, 0))
    tempResults['refunds_series'].append(tempResults['refunds'].get(date, 0))
    tempResults['downloads28_series'].append(tempResults['downloads28'].get(date, 0))
    tempResults['refunds28_series'].append(tempResults['refunds28'].get(date, 0))

  revenue28Series = tempResults['revenue28_series']
  mauSeries = tempResults['mau_series']
  results = {}
  results['data'] = [
    {
      'id': 'dau',
      'name': 'Daily Users',
      'data': tempResults['dau_series']
    },
    {
      'id': 'mau',
      'name': '28-Day Users',
      'data': tempResults['mau_series']
    },
    {
      'id': 'mrr',
      'name': 'MRR',
      'format': 'money',
      'data': tempResults['mrr_series']
    },
    {
      'id': 'revenue',
      'name': 'Daily Rev.',
      'format': 'money',
      'data': tempResults['revenue_series']
    },
    {
      'id': 'revenue28',
      'name': '28-Day Rev.',
      'data': tempResults['revenue28_series']
    },
    {
      'id': 'downloads',
      'name': 'Downloads',
      'data': tempResults['downloads_series']
    },
    {
      'id': 'updates',
      'name': 'Updates',
      'data': tempResults['updates_series']
    },
    {
      'id': 'refunds',
      'name': 'Refunds',
      'data': tempResults['refunds_series']
    },
    {
      'id': 'downloads28',
      'name': '28-Day Downloads',
      'data': tempResults['downloads28_series']
    },
    {
      'id': 'refunds28',
      'name': '28-Day Refunds',
      'data': tempResults['refunds28_series']
    },
    {
      'id': 'arpu',
      'name': 'ARPU',
      'format': 'money',
      'data': [(revenue28Series[i] / mauSeries[i]) if mauSeries[i] != 0 else 0.0
              for i in range(daysInAnalysisPeriod)]
    }
  ]
  '''
      ,
      {
        'id': 'cancels',
        'name': 'Cancels',
        'data': [0] * daysInAnalysisPeriod
      },
  '''

  # Ensure that unwanted tempResults data is garbage collected.
  return deepcopy(results)


def daily_metrics_for_customer(userId, start_date, end_date):
  tempResults = {'dau': {}, 'mau': {}, 'mrr': {}, 'revenue': {}, 'revenue28': {},
      'dau_series': [], 'mau_series': [], 'mrr_series': [], 'revenue_series': [],
      'revenue28_series': []}

  dailyCustomerMetrics = DailyCustomerMetrics.objects.filter(customer_id__auth_user__id=userId,
      date__gte=start_date, date__lte=end_date)
  for record in dailyCustomerMetrics:
    tempResults['dau'][record.date] = record.dau
    tempResults['mau'][record.date] = record.mau

  mrrStartDate = start_date - timedelta(days=MRR_DURATION_DAYS-1)
  dailyProductMetrics = DailyProductMetrics.objects.filter(customer_id__auth_user__id=userId,
      date__gte=mrrStartDate, date__lte=end_date)
  for record in dailyProductMetrics:
    if start_date <= record.date <= end_date:
      tempResults['revenue'][record.date] = (record.revenue / 1000000.0) + \
          tempResults['revenue'].get(record.date, float(0))
    dateToStartUpdate = max(record.date, start_date)
    dateToStopUpdate = min(record.date + timedelta(days=MRR_DURATION_DAYS-1), end_date)
    numberOfDatesToUpdate = 1 + (dateToStopUpdate - dateToStartUpdate).days
    for delta in range(numberOfDatesToUpdate):
      dateToUpdate = dateToStartUpdate + timedelta(days=delta)
      tempResults['mrr'][dateToUpdate] = (record.recurring_revenue / 1000000.0) + \
          tempResults['mrr'].get(dateToUpdate, 0.0)
      tempResults['revenue28'][dateToUpdate] = (record.revenue / 1000000.0) + \
          tempResults['revenue28'].get(dateToUpdate, 0.0)

  daysInAnalysisPeriod = (end_date - start_date + timedelta(days=1)).days
  for delta in range(daysInAnalysisPeriod):
    date = start_date + timedelta(days=delta)
    tempResults['dau_series'].append(tempResults['dau'].get(date, 0))
    tempResults['mau_series'].append(tempResults['mau'].get(date, 0))
    tempResults['mrr_series'].append(tempResults['mrr'].get(date, float(0)))
    tempResults['revenue_series'].append(tempResults['revenue'].get(date, float(0)))
    tempResults['revenue28_series'].append(tempResults['revenue28'].get(date, float(0)))

  revenue28Series = tempResults['revenue28_series']
  mauSeries = tempResults['mau_series']
  results = {}
  results['data'] = [
    {
      'id': 'dau',
      'name': 'Daily Users',
      'data': tempResults['dau_series']
    },
    {
      'id': 'mau',
      'name': '28-Day Users',
      'data': tempResults['mau_series']
    },
    {
      'id': 'mrr',
      'name': 'MRR',
      'format': 'money',
      'data': tempResults['mrr_series']
    },
    {
      'id': 'revenue',
      'name': 'Daily Rev.',
      'format': 'money',
      'data': tempResults['revenue_series']
    },
    {
      'id': 'revenue28',
      'name': '28-Day Rev.',
      'data': tempResults['revenue28_series'],
    },
    {
      'id': 'arpu',
      'name': 'ARPU',
      'format': 'money',
      'data': [(revenue28Series[i] / mauSeries[i]) if mauSeries[i] != 0 else 0.0
              for i in range(daysInAnalysisPeriod)]
    },
    {
      'id': 'cancels',
      'name': 'Cancels',
      'data': [0] * daysInAnalysisPeriod
    },
  ]

  # Ensure that unwanted tempResults data is garbage collected.
  return deepcopy(results)


def user_info(scriber_id):
  thresholdTime = datetime.now() - timedelta(days=_RECENT_SESSION_THRESHOLD_DAYS)
  sessionEvents = tuple(SESSION_BOUNDARY_EVENT_IDS)

  userInfo = {
    'customer': {
      'apps': {},
      'events': [],
      'last_active': None,
      'lifetime_revenue': 0.0,
      'sessions': 0,
      'sessions_last_28_days': 0
    }
  }

  cursor = connection.cursor()
  cursor.execute(_USER_INFO_QUERY, [sessionEvents, sessionEvents, thresholdTime, sessionEvents,
      sessionEvents, scriber_id])
  for row in cursor.fetchall():
    (scriberId, username, email, customerUserId, platformAppId, platformAppStr,
        platformTypeStr, sessionCount, recentSessionCount, firstActiveTime,
        lastActiveTime, purchaseNames, revenueUsdMicros) = (row[0], row[1],
        row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9],
        row[10], row[11], row[12])

    if purchaseNames is None:
      purchaseNames = []
    elif None in purchaseNames:
      purchaseNames.remove(None)

    userInfo['customer']['sessions'] += sessionCount
    userInfo['customer']['sessions_last_28_days'] += recentSessionCount
    userInfo['customer']['lifetime_revenue'] += float(revenueUsdMicros) / 1000000.0
    lastActiveTimestamp = timestamp(lastActiveTime) if lastActiveTime else None
    if lastActiveTimestamp:
      if (userInfo['customer']['last_active'] is None or
          lastActiveTimestamp > userInfo['customer']['last_active']):
        userInfo['customer']['last_active'] = lastActiveTimestamp

    if platformAppId > 0:
      # TODO(d-felix): Add version and devices.
      userInfo['customer']['apps'][platformAppStr] = {
          'devices': [],
          'in_app_purchase_names': purchaseNames,
          'last_active': lastActiveTimestamp,
          'platform': platformTypeStr,
          'version': None
      }

    # username, email, and customerUserId are the same for all rows.
    if userInfo['customer'].get('username', None) is None:
      userInfo['customer']['username'] = username
    if userInfo['customer'].get('email', None) is None:
      userInfo['customer']['email'] = email
    if userInfo['customer'].get('id', None) is None:
      userInfo['customer']['id'] = customerUserId

  return userInfo


def graph_followed_apps_data_for_user_id(user_id, country_alpha2, start_in_seconds, end_in_seconds):
  followedAppRecords = None
  followedAppRecords = CustomerAppFollow.objects.filter(customer__auth_user__id=user_id)
  followedApps = [record.app for record in followedAppRecords]
  followedAppIds = [app.id for app in followedApps]

  rankData = rankutils.followed_apps_country_rank_data(followedAppIds, country_alpha2,
      start_in_seconds, end_in_seconds)

  # Replace internal app_id values with bundle_ids and package names.
  # TODO(d-felix): Support Android.
  id_map = rankutils.externalize_app_ids(followedAppIds, 1)

  for entry in rankData:
    for follow in followedAppRecords:
      if follow.app.id == entry['app_id']:
        if follow.country:
          entry['country'] = {"id":follow.country.alpha2, "name":follow.country.display_name}
    entry['app_id'] = id_map[entry['app_id']]
  return rankData


def visible_categories(user):
  iosNavCategory = AppStoreCategory.objects.get(name='Navigation',
      platform__name=AppStorePlatform.IOS)
  category_ids = {iosNavCategory.id: True}

  if user.is_authenticated():
    userHasActiveLogin = CustomerExternalLoginInfo.objects.filter(customer_id__auth_user=user,
        is_active=True).exists()
    if userHasActiveLogin or user.username in ('andrew', 'dan@scriber.io',
        'rckclmbr+scriber@gmail.com', 'yamane@open-associates.com'):
      category_ids = {c.id: True for c in AppStoreCategory.objects.all()}

  return category_ids.keys()


def graph_followed_apps_estimates_for_user_id(user_id, country_alpha2, start_in_seconds, end_in_seconds, estimate_type):
  start_in_seconds, end_in_seconds = int(float(start_in_seconds)), int(float(end_in_seconds))
  customer = Customer.objects.filter(auth_user__id=user_id).first()
  user = User.objects.get(id=user_id)
  ownerId = None
  if customer.training_privacy == Customer.PRIVATE:
    ownerId = customer.customer_id
  followedAppRecords = CustomerAppFollow.objects.filter(customer=customer)

  appData = {}
  visibleCategories = visible_categories(user)
  for record in followedAppRecords:
    app = record.app
    appId = app.id
    appCategoryId = app.primary_category.id
    if appCategoryId not in visibleCategories:
      # Shell record instead?
      continue
    elif appCategoryId not in appData:
      appData[appCategoryId] = []
    appData[appCategoryId].append(appId)

  data = []
  for categoryId in appData:
    categoryData = rankutils.app_estimates_data(appData[categoryId], categoryId,
        country_alpha2, start_in_seconds, end_in_seconds, estimate_type, owner_id=ownerId)
    data.extend(categoryData)

  # Replace internal app_id values with bundle_ids and package names.
  # TODO(d-felix): Support Android.
  id_map = rankutils.externalize_app_ids([record.app.id for record in followedAppRecords], 1)

  for entry in data:
    entry['app_id'] = id_map[entry['app_id']]
  return data