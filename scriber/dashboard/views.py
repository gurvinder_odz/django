import logging
import random
import os
import json
import requests
from copy import copy
from datetime import date, timedelta, datetime
from operator import itemgetter
from random import randint
from string import split
from appinfo import fetcher
from appinfo.models import AppInfo
from appstore.models import AppStoreApp, AppStoreCategory, AppStoreArtist
from appstore.models import AppStorePlatform, AppStoreRankType
from core.cache import cache_key
from core.db.dynamic import connection
from customers.models import Customer, CustomerExternalLoginInfo, CustomerAppFollow
from customers.views import itunes_and_google_account_lists
from dashboard import rankutils
from dashboard.autocomplete import autocomplete_users
from dashboard.realtimeutils import realtime_labels, realtime_revenue, realtime_sessions
from dashboard.utils import visible_categories, graph_followed_apps_estimates_for_user_id, graph_followed_apps_data_for_user_id, aggregated_daily_metrics_for_products, daily_label_metrics_for_products, daily_metrics_for_products, product_info_for_auth_user, time_info_from_bounds, user_info
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core import serializers
from django.core.cache import cache
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.decorators.cache import never_cache
from django.templatetags.static import static
from django.conf import settings
from emailer import tasks
from ingestor.models import ChannelProduct, User as EndUser
from ingestor.utils import sdk_versions_for_auth_user
from localization.models import Country
from realtime import api as realtime_api


logger = logging.getLogger(__name__)

DEMO_FOLLOWED_APPS = [app for app in AppStoreApp.objects.filter(
    bundle_id__in=['5ZPN38G6GE.com.trailbehind.GaiaGPS', 'com.trailbehind.gaiamarine'])]
_PRODUCT_INFO_CACHE_TIMEOUT = 10 * 60

MAX_RANK_FOR_DISPLAY = 200

USER_VISIBLE_CATEGORY_QUERY = """
  SELECT DISTINCT
    a.primary_category
  FROM customers c JOIN channel_products cp ON (c.customer_id = cp.customer_id)
  JOIN app_store_apps a ON (cp.apple_identifier = a.track_id)
  WHERE
    c.auth_user = %s AND
    a.primary_category IS NOT NULL
  ;
  """

USER_VISIBLE_CATEGORY_MAP = {
    'patrick@inkstonesoftware.com': [26, 31, 33, 43],
    'guthookhikes@gmail.com': [8],
    'vzetterlind@gmail.com': [8, 68],
    'mpease@gmail.com': [31],
    'Justin@stoneblade.com': [16, 37, 48],
    'sohail@ultimatefanlive.com': [11],
    'jared@scribd.com': [33],
    'suzanne@depict.com': [40],
    'mike@knert.com': [5, 48],
}

'''
  a guest demo of the main dashboard
'''


def demo(request):
  installed_sdks = []
  return render(request, 'dashboard/dashboard-main.html',
                {'view_type': 'real_dashboard',
                 "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLIC_KEY,
                 'itunes_accounts_json': json.dumps([]),
                 'google_play_accounts_json': json.dumps([]),
                 'developer_mode': False,
                 'sdk_versions_json': json.dumps(['iOS', 'Android'])})

'''
  the main logged in view
'''


def main_dashboard(request):
  if not request.user.is_authenticated():
    return redirect('/')
  has_subscription = False
  if hasattr(request.user, 'customer'):
    if request.user.customer.has_active_subscription():
      has_subscription = True

  installed_sdks = sdk_versions_for_auth_user(request.user)
  itunes_accounts, google_play_accounts = itunes_and_google_account_lists(request.user)
  customer = Customer.objects.filter(auth_user=request.user).first()
  return render(request, 'dashboard/dashboard-main.html',
                {'view_type': 'real_dashboard',
                 "STRIPE_PUBLIC_KEY": settings.STRIPE_PUBLIC_KEY,
                 'itunes_accounts': itunes_accounts,
                 'itunes_accounts_json': json.dumps(itunes_accounts),
                 'google_play_accounts': google_play_accounts,
                 'google_play_accounts_json': json.dumps(google_play_accounts),
                 'developer_mode': not has_subscription,
                 'hide_setup': customer.has_hidden_setup,
                 'sdk_versions': installed_sdks,
                 'sdk_versions_json': json.dumps(installed_sdks),
                 })


def rankings(request):
  installed_sdks = []
  return render(request, 'rankings.html',
                {})


def follow_app(request, platform, platform_app_id):
  customer = customer_or_gaia(request)
  app = app_for_id_and_platform(platform, platform_app_id)
  customerAppFollow = CustomerAppFollow.objects.filter(app=app, customer=customer)
  if len(customerAppFollow) > 0:
    return HttpResponse(
        json.dumps({"success": False, "error": "Already following"}), content_type='application/json')
  customerAppFollow = CustomerAppFollow.objects.create(app=app[0], customer=customer)
  customerAppFollow.save()
  return HttpResponse(json.dumps({"success": True}), content_type='application/json')


def unfollow_app(request, platform, platform_app_id):
  customer = customer_or_gaia(request)
  app = app_for_id_and_platform(platform, platform_app_id)

  customerAppFollow = CustomerAppFollow.objects.filter(app=app, customer=customer)
  if len(customerAppFollow) > 0:
    customerAppFollow[0].delete()
    return HttpResponse(json.dumps({"success": True}), content_type='application/json')
  return HttpResponse(
      json.dumps({"success": False, "error": "Not following"}), content_type='application/json')


def update_follow(request, platform, platform_app_id, country_id):
  customer = customer_or_gaia(request)
  app = app_for_id_and_platform(platform, platform_app_id)

  customerAppFollow = CustomerAppFollow.objects.filter(app=app, customer=customer)
  if len(customerAppFollow) > 0:
    customerAppFollow[0].country = Country.objects.filter(alpha2=country_id)[0]
    customerAppFollow[0].save()
    return HttpResponse(json.dumps({"success": True}), content_type='application/json')
  return HttpResponse(
      json.dumps({"success": False, "error": "Not following"}), content_type='application/json')


def app_for_id_and_platform(platform, platform_app_id):
  app = None
  if int(platform) == IOS_PLATFORM_ID:
    app = AppStoreApp.objects.filter(platform=platform, bundle_id=platform_app_id)
  else:
    # android
    app = AppStoreApp.objects.filter(platform=platform, track_id=platform_app_id)
  return app


def apps(request, platform, country, app_id):
  platform = int(platform)
  info = None
  app_store_app = None
  # 1 for iOS App Store
  if platform == 1:
    info = fetcher.fetch(app_id, "App Store")
    app_store_app = AppStoreApp.objects.filter(bundle_id=app_id, platform__id=platform).first()
  elif platform == 0:
    info = fetcher.fetch(app_id, "Google Play")
  apps = []
  if app_store_app:
    apps = AppStoreApp.objects.filter(artist=app_store_app.artist, platform__id=platform)
  else:
    # could this ever happen?
    logging.error("you should always be able to look up an AppStoreApp for bundle + platform")
  publisher_apps = [app.to_json() for app in apps]
  url = fetcher._IOS_ID_INFO_URL % app_store_app.track_id
  app_metadata = requests.get(url, timeout=60).json()['results'][0]
  return render(
    request, 
    'app-details.html', 
    {'app_id': app_id, 
    'app_metadata': app_metadata, 
    'app_info': info, 
    'publisher_json':json.dumps(publisher_apps), # for use by Angular
    'publisher_apps':publisher_apps} # for use by Django
    )


'''
    the view for a single publisher,
    aggregates data by countries and apps
'''
def publisher(request, artist_id):
  info = AppStoreArtist.objects.filter(artist_id=artist_id).first()
  if info:
    apps = AppStoreApp.objects.filter(artist=info)

    app_metadata = raw_apple_info(apps)

    artist = {
        'name': info.name,
        'artist_id': info.artist_id,
        'platform': info.platform.display_name,
    }
    return render(request, 
      'publisher.html', 
      {'publisher_apps':app_metadata, 
      'artist': artist,        
      })
  return render(request, 'publisher.html', {'error': "Artist not found."})

def raw_apple_info(apps):
  app_id_csv = "0"
  for app in apps:
    app_id_csv += "," + str(app.track_id)
  url = fetcher._IOS_ID_INFO_URL % app_id_csv
  app_metadata = requests.get(url, timeout=60).json()['results']
  return app_metadata


'''
  proxy app info URLs like https://itunes.apple.com/lookup?bundleId=com.trailbehind.GaiaGPS
'''


@never_cache
def app_info(request, bundle_id):
  if bundle_id == 'otm-gaia-bundle':
    info = {
        'fields': {
            'name': "Gaia Topo Bundle",
            'icon_url': 'http://is4.mzstatic.com/image/pf/us/r30/Purple5/v4/1d/14/4e/1d144ec7-6645-1fa2-3e76-c8f0e7153501/AppIcon57x57.png',
            'app': 'otm-gaia-bundle',
        }}
    return HttpResponse(json.dumps([info]), content_type='application/json')
  info = fetcher.fetch(bundle_id, "App Store")
  if info:
    info = serializers.serialize("json", [info])
    return HttpResponse(info, content_type='application/json')
  return HttpResponse(json.dumps({'error': "App not found."}), content_type='application/json')


def app_info_android(request, app_id):
  info = fetcher.fetch(app_id, "Google Play")
  if info:
    info = serializers.serialize("json", [info])
    return HttpResponse(info, content_type='application/json')
  return HttpResponse(json.dumps({'error': "App not found."}), content_type='application/json')


def app_info_for_app_id(request, app_id):
  info = AppInfo.objects.filter(app_info_id=app_id)
  if len(info):
    info = serializers.serialize("json", info)
    return HttpResponse(info, content_type='application/json')
  return HttpResponse(json.dumps({'error': "App not found."}), content_type='application/json')


def app_info_for_track_id(request, track_id):
  info = fetcher.IOS_ID_APP_INFO_FETCHER.fetch(track_id)
  if info:
    info = serializers.serialize("json", [info])
    return HttpResponse(info, content_type='application/json')
  return HttpResponse(json.dumps({'error': "App not found."}), content_type='application/json')


def customer_info(request, end_user_id):
  authUserId = _user_id(request.user)
  endUser = None
  # Don't allow lookup of other customers' users.
  if request.user.is_authenticated():
    endUser = EndUser.objects.filter(scriber_id=end_user_id,
                                     customer_id__auth_user__id=authUserId).first()

  t = datetime(2015, 10, 21, 0, 0)
  fakeUserInfo = {'customer': {'username': 'dave_user',
                               'email': 'dave_user@gmail.com',
                               'sessions': 297,
                               'last_active': (t - datetime(1970, 1, 1)).total_seconds(),
                               'lifetime_revenue': 56.43,
                               'sessions_last_28_days': 13,
                               'id': 3,
                               'apps': {
                                   '5ZPN38G6GE.com.trailbehind.GaiaGPS': {
                                       'last_active': (t - datetime(1970, 1, 1)).total_seconds(),
                                       'platform': 'App Store',
                                       'version': 'v1.4',
                                       'devices': ['iPad2', 'iPhone5'],
                                       'in_app_purchase_names': ['Gaia GPS 1Year Pro'],
                                   },
                                   'trailbehind.gaiagps.android': {
                                       'last_active': (t - datetime(1970, 1, 1)).total_seconds(),
                                       'platform': 'Android',
                                       'version': 'v2.1',
                                       'devices': ['Nexus 5'],
                                   }
                               },
                               'events': [
                                   {
                                       'event_time': (t - datetime(1972, 1, 1)).total_seconds(),
                                       'event_title': 'Renewed Gaia GPS 1Year Pro',
                                       'event_app_id': '5ZPN38G6GE.com.trailbehind.GaiaGPS',
                                   },
                                   {
                                       'event_time': (t - datetime(1971, 1, 1)).total_seconds(),
                                       'event_title': 'Bought Gaia GPS 1Year Pro',
                                       'event_app_id': '5ZPN38G6GE.com.trailbehind.GaiaGPS',
                                   },
                                   {
                                       'event_time': (t - datetime(1970, 1, 1)).total_seconds(),
                                       'event_title': 'First Active',
                                       'event_app_id': 'trailbehind.gaiagps.android',
                                   },
                               ]
                               }
                  }

  userInfo = fakeUserInfo
  return HttpResponse(json.dumps(userInfo), content_type='application/json')
  if not endUser and request.user.is_authenticated():
    return None
  if not endUser and not request.user.is_authenticated():
    userInfo = fakeUserInfo
  else:
    userInfo = user_info(endUser.scriber_id)

  return HttpResponse(json.dumps(userInfo), content_type='application/json')


def demo_drilldown(request, series_name):
  return render(request, 'dashboard/dashboard-drilldown-product.html',
                {'series_name': series_name})


def demo_drilldown_product(request, series_name, product_id):
  return render(request, 'demo-series-product.html',
                {'series_name': series_name, 'product_id': product_id})


def save_settings(request):
  response_dict = {}
  for key in request.POST:
    if (key == 'config'):
      response_dict[key] = request.POST[key]
  # todo save product settings in database
  return HttpResponse(json.dumps(response_dict), content_type='application/json')


def _user_id(user):
  """
  Returns the auth_user.id of an authenticated user.
  If the user is not authenticated, returns a default value for demonstration purposes.
  """
  return user.id if user.is_authenticated() else settings.DEMO_AUTH_USER_ID


def _filter_product_id_list_for_user(user, product_id_list):
  """
  For a given user and a list of product IDs, returns those IDs for which the product belongs to
  the user.
  """
  userId = _user_id(user)
  intersection = []
  channelProducts = ChannelProduct.objects.filter(product_id__customer_id__auth_user__id=userId)
  for channelProduct in channelProducts:
    if channelProduct.channel_product_id in product_id_list:
      intersection.append(channelProduct.channel_product_id)
  return intersection


def _product_info(user, consult_cache=True):
  userId = _user_id(user)
  cacheKey = cache_key(product_info_for_auth_user, userId)
  if consult_cache:
    info = cache.get(cacheKey)
    if info is not None:
      return info

  info = product_info_for_auth_user(userId)
  cache.set(cacheKey, info, _PRODUCT_INFO_CACHE_TIMEOUT)
  return info


def _product_series(user, product_id_list, time_info, series_name):
  currentSeries = daily_metrics_for_products(product_id_list, time_info['start_date'],
                                             time_info['end_date'])
  previousSeries = daily_metrics_for_products(product_id_list, time_info['previous_start_date'],
                                              time_info['previous_end_date'])
  productSeries = {}
  for channelProductId in product_id_list:
    productSeries[channelProductId] = {}
    productSeries[channelProductId]['current'] = currentSeries[channelProductId]['data']
    productSeries[channelProductId]['previous'] = previousSeries[channelProductId]['data']

  # TODO(d-felix): Push this filtering into the dashboard.utils functions.
  if series_name:
    for channelProductId in productSeries:
      for seriesType in ['current', 'previous']:
        entryList = []
        for entry in productSeries[channelProductId][seriesType]:
          if entry['name'] == series_name:
            entryList.append(entry)
        productSeries[channelProductId][seriesType] = entryList

  return productSeries


def _overall_series(user, product_id_list, time_info, series_name):
  userId = _user_id(user)
  currentSeries = aggregated_daily_metrics_for_products(
      userId,
      product_id_list,
      time_info['start_date'],
      time_info['end_date'])
  previousSeries = aggregated_daily_metrics_for_products(
      userId,
      product_id_list,
      time_info['previous_start_date'],
      time_info['previous_end_date'])
  overallSeries = {
      'current': currentSeries['data'],
      'previous': previousSeries['data'],
  }

  # TODO(d-felix): Push this filtering into the dashboard.utils functions.
  if series_name:
    for seriesType in ['current', 'previous']:
      entryList = []
      for entry in overallSeries[seriesType]:
        if entry['name'] == series_name:
          entryList.append(entry)
      overallSeries[seriesType] = entryList

  return overallSeries


def _time_info(user, start_in_seconds, end_in_seconds):
  return time_info_from_bounds(_user_id(user), start_in_seconds, end_in_seconds)


@never_cache
def products(request):
  # todo return enabled info for products, which is saved in save_settings()
  # TODO(d-felix): Return list instead of CSV.
  user = request.user
  consultCache = False
  if not request.user.is_authenticated():
    user = User.objects.get(pk=settings.DEMO_AUTH_USER_ID)
    consultCache = True
  productInfo = _product_info(user, consult_cache=consultCache)
  productCsv = ''
  for key, value in productInfo.iteritems():
    # \todo skipping Gaia Beta Android here, maybe a customer
    # will have a ghost app and we'll fix better
    if key == 57:
      continue
    productCsv += '%s' % key
    productCsv += ','
    for iap in value['in_app_products']:
      productCsv += '%s' % iap['product_id']
      productCsv += ','
  if len(productCsv) >= 1 and productCsv[len(productCsv) - 1] == ",":
    productCsv = productCsv[:-1]

  return HttpResponse(json.dumps({"product_string": productCsv}), content_type='application/json')


'''
  async call for data to graph on main dashboard
'''


@never_cache
def graph_data_with_range(request, product_ids, start_in_seconds, end_in_seconds):
  return graph_data_with_range_and_series(request, product_ids, None, start_in_seconds,
                                          end_in_seconds)

'''
  #todo get dan to help me feed the data
'''


@never_cache
def graph_data_app_with_range(request, platform, bundle_id, start_in_seconds, end_in_seconds):

  return graph_data_with_range_and_series(request, '57', None, start_in_seconds,
                                          end_in_seconds)


@never_cache
def graph_data_with_range_and_series(
    request,
    product_ids,
    series_name,
    start_in_seconds,
        end_in_seconds):
  user = request.user
  if not request.user.is_authenticated():
    user = User.objects.get(pk=settings.DEMO_AUTH_USER_ID)

  timeInfo = _time_info(user, start_in_seconds, end_in_seconds)
  product_id_list = [long(id) for id in split(product_ids, ',')]

  # Remove product_ids not associated with the current user.
  safe_product_id_list = _filter_product_id_list_for_user(user, product_id_list)

  graphData = {}
  graphData['times'] = timeInfo['times']
  graphData['product_series'] = _product_series(user, safe_product_id_list, timeInfo,
                                                series_name)
  graphData['product_info'] = _product_info(user)
  # TODO(d-felix): Reintroduce overall_series after implementing DAU and MAU approximation.
  # graphData['overall_series'] = _overall_series(user, safe_product_id_list, timeInfo,
  #     series_name)

  return HttpResponse(json.dumps(graphData), content_type='application/json')


'''
  custom events
'''


@never_cache
def graph_label_data_with_range(request, product_ids, start_in_seconds, end_in_seconds):
  user = (request.user if request.user.is_authenticated() else
          User.objects.get(pk=settings.DEMO_AUTH_USER_ID))
  timeInfo = _time_info(user, start_in_seconds, end_in_seconds)
  product_id_list = [long(id) for id in split(product_ids, ',')]

  # Remove product_ids not associated with the current user.
  safe_product_id_list = _filter_product_id_list_for_user(user, product_id_list)

  graphData = daily_label_metrics_for_products(user, safe_product_id_list, timeInfo)
  return HttpResponse(json.dumps(graphData), content_type='application/json')


@never_cache
def graph_realtime_label_data_with_range(request, product_ids, bucket_width_in_seconds,
                                         start_in_seconds, end_in_seconds):
  user = (request.user if request.user.is_authenticated() else
          User.objects.get(pk=settings.DEMO_AUTH_USER_ID))
  product_id_list = [long(id) for id in split(product_ids, ',')]

  # Remove product_ids not associated with the current user.
  safe_product_id_list = _filter_product_id_list_for_user(user, product_id_list)
  bucketWidthSecs = int(float(bucket_width_in_seconds))
  startEpoch = long(float(start_in_seconds))
  endEpoch = long(float(end_in_seconds))

  graphData = realtime_labels(user, safe_product_id_list, bucketWidthSecs, startEpoch, endEpoch)
  return HttpResponse(json.dumps(graphData), content_type='application/json')


@never_cache
def graph_realtime_revenue_data_with_range(request, product_ids, bucket_width_in_seconds,
                                           start_in_seconds, end_in_seconds):
  user = (request.user if request.user.is_authenticated() else
          User.objects.get(pk=settings.DEMO_AUTH_USER_ID))
  product_id_list = [long(id) for id in split(product_ids, ',')]

  # Remove product_ids not associated with the current user.
  safe_product_id_list = _filter_product_id_list_for_user(user, product_id_list)
  bucketWidthSecs = int(float(bucket_width_in_seconds))
  startEpoch = long(float(start_in_seconds))
  endEpoch = long(float(end_in_seconds))

  graphData = realtime_revenue(user, safe_product_id_list, bucketWidthSecs, startEpoch, endEpoch)
  return HttpResponse(json.dumps(graphData), content_type='application/json')


@never_cache
def graph_realtime_data_with_range_and_series(request, series_name, product_ids,
                                              start_in_seconds, end_in_seconds):
  if(series_name == 'Daily Revenue'):
    bucketWidthSecs = 3600
    return graph_realtime_revenue_data_with_range(
        request,
        product_ids,
        bucketWidthSecs,
        start_in_seconds,
        end_in_seconds)

  if series_name == 'Sessions' or series_name == 'sessions':
    user = (request.user if request.user.is_authenticated() else
            User.objects.get(pk=settings.DEMO_AUTH_USER_ID))
    product_id_list = [long(id) for id in split(product_ids, ',')] if product_ids else []
    safe_product_id_list = _filter_product_id_list_for_user(user, product_id_list)

    # Time arguments are string representations of floats.
    startEpoch = long(float(start_in_seconds))
    endEpoch = long(float(end_in_seconds))
    graphData = None
    graphData = realtime_sessions(user, safe_product_id_list, startEpoch, endEpoch)
    # graphData = realtime_api.sessions(safe_product_id_list, startEpoch, endEpoch)
    return HttpResponse(json.dumps(graphData), content_type='application/json')

  # anything else is a custom label
  bucketWidthSecs = 3600
  return graph_realtime_label_data_with_range(
      request,
      product_ids,
      bucketWidthSecs,
      start_in_seconds,
      end_in_seconds)


# return the team@gaiagps.com user when not logged
def user_id_for_request_user(user):
  if user.is_authenticated():
    return user.id
  else:
    return settings.DEFAULT_USER_ID


# return the team@gaiagps.com customer when not logged
def customer_or_gaia(request):
  user_id = user_id_for_request_user(request.user)
  return Customer.objects.get(auth_user_id=user_id)


@never_cache
def graph_followed_apps_data(request, country_alpha2, start_in_seconds, end_in_seconds):
  user_id = user_id_for_request_user(request.user)
  start_in_seconds = long(float(start_in_seconds))
  end_in_seconds = long(float(end_in_seconds))
  rankData = graph_followed_apps_data_for_user_id(
      user_id,
      country_alpha2,
      start_in_seconds,
      end_in_seconds)
  return HttpResponse(json.dumps(rankData), content_type='application/json')


@never_cache
def graph_followed_apps_estimates(request, country_alpha2, start_in_seconds, end_in_seconds, estimate_type):
  user_id = user_id_for_request_user(request.user)
  data = graph_followed_apps_estimates_for_user_id(
    user_id, 
    country_alpha2, 
    start_in_seconds, 
    end_in_seconds, 
    estimate_type)
  return HttpResponse(json.dumps(data), content_type='application/json')


'''
 ajax search autocomplete endpoint for customer search
'''


def autocomplete(request, prefix):
  if not request.user.is_authenticated():
    # for demo dashboard
    wordlist = open(os.path.join(settings.BASE_DIR, 'static/words')).readlines()
    new_words = []
    for x in wordlist:
      if x.startswith(prefix):
        new_words.append({'value': x, 'id': x, 'name': x})
    return HttpResponse(json.dumps(new_words), content_type='application/json')
    # return HttpResponse(json.dumps({'message': 'Unauthorized'}), status=401,
    # content_type='application/json')

  userId = request.user.id
  lowercasePrefix = prefix.lower() if prefix is not None else None
  results = autocomplete_users(userId, lowercasePrefix)
  return HttpResponse(json.dumps(results), content_type='application/json')


'''
 ajax search autocomplete endpoint for app search
 todo dan
'''


def search(request, prefix):
  lowercasePrefix = prefix.lower() if prefix is not None else None
  results = AppStoreApp.objects.filter(name__istartswith=lowercasePrefix)
  resultJson = []
  for result in results:
    app = {}
    app['value'] = result.name
    app['platform'] = result.platform.id
    app['name'] = result.name
    app['id'] = result.bundle_id
    resultJson.append(app)
  if resultJson == []:
    wordlist = open(os.path.join(settings.BASE_DIR, 'static/words')).readlines()
    new_words = []
    for x in wordlist:
      if x.startswith(prefix):
        new_words.append({'value': x, 'id': x, 'name': x})
    return HttpResponse(json.dumps(new_words), content_type='application/json')
  return HttpResponse(json.dumps(resultJson), content_type='application/json')


TOP_COUNTRIES_FOR_APP_QUERY = """
  SELECT DISTINCT
    c.alpha2
  FROM
    app_store_apps a JOIN app_store_ranks k ON (a.id = k.app)
    JOIN app_store_rank_requests r ON (k.rank_request = r.id)
    JOIN app_store_regions rg ON (r.region = rg.id)
    JOIN countries c ON (rg.country = c.id)
  WHERE a.id = %s
  ORDER BY 1
  ;
  """


def top_countries_for_app(request, market, app_id):
  # No authentication needed since rankings data is open to the public.
  # TODO(d-felix): Implement Android support.
  platform = AppStorePlatform.objects.get(id=market)
  is_ios_platform = platform.name == AppStorePlatform.IOS
  ret = []
  if is_ios_platform:
    app = AppStoreApp.objects.filter(bundle_id=app_id, platform=platform).first()
    if app is not None:
      cursor = connection.cursor()
      cursor.execute(TOP_COUNTRIES_FOR_APP_QUERY, [app.id])
      for row in cursor.fetchall():
        ret.append(row[0])
  return ret


def user_data_is_private(user):
  if user.is_authenticated():
    return Customer.objects.filter(auth_user=user, training_privacy=Customer.PRIVATE).exists()
  else:
    return False


@never_cache
# app_id is bundle_id or package depending on platform
# market is a numeric platform_id
# country is 2 char country code
def graph_app_ranks_data_with_range(request, market, country, app_id, start_in_seconds,
                                    end_in_seconds):
  # Arguments orginating from query parameters are passed as strings.
  market, start_in_seconds, end_in_seconds = (
      int(market), int(float(start_in_seconds)), int(float(end_in_seconds)))

  app = AppStoreApp.objects.get(bundle_id=app_id, platform__id=market)
  category = app.primary_category
  if category is None:
    # TODO(d-felix): Support Android.
    if AppStorePlatform.objects.get(pk=market).name == AppStorePlatform.IOS:
      # We fetch with direct=True to bypass persisted records which don't contain
      # category information.
      # Note that fetcher.fetch() may fail, so we must still handle the
      # category = None case.
      appInfo = fetcher.fetch(app_id, 'App Store', direct=True)
      if appInfo.ios_genre_id:
        category = AppStoreCategory.objects.get(ios_genre_id=appInfo.ios_genre_id,
                                                platform__id=market)

  category_id = category.id if category is not None else None

  rankData = rankutils.country_app_rank_data([app.id], country, start_in_seconds, end_in_seconds)

  userVisibleCategories = visible_categories(request.user)
  if category_id in userVisibleCategories:
    # Add revenue trends.
    ownerId = None
    if request.user.is_authenticated():
      privateCustomer = Customer.objects.filter(auth_user=request.user,
                                                training_privacy=Customer.PRIVATE).first()
      ownerId = privateCustomer.customer_id if privateCustomer else None
    revenueData = rankutils.app_estimates_data([app.id], category_id, country,
                                             start_in_seconds, end_in_seconds, 'revenue', owner_id=ownerId)
    rankData[0]['series_data'].extend(revenueData[0]['series_data'])

  # Externalize the app_id value.
  rankData[0]['app_id'] = app_id

  # TODO(d-felix): Do we want to return a list of length 1?

  return HttpResponse(json.dumps(rankData), content_type='application/json')


@never_cache
# category_type and category are numeric IDs
# country is 2 char country code
def graph_rankings(request, market, categoryType, category, subcategory, country, page,
                   numResults, start_in_seconds, end_in_seconds):
  # Arguments orginating from query parameters are passed as strings.
  if subcategory != "-999":
    category = subcategory
  market, categoryType, category, page, numResults, start_in_seconds, end_in_seconds = (
      int(market), int(categoryType), int(category), int(page), int(numResults),
      int(float(start_in_seconds)), int(float(end_in_seconds)))

  # Enforce sensible bounds on page and numResults to discourage URL manipulation.
  page = min(max(1, page), 100)
  numResults = min(max(1, numResults), 30)

  # page is 1-indexed
  startRank = 1 + (page - 1) * numResults
  endRank = startRank + numResults - 1
  startRank = min(MAX_RANK_FOR_DISPLAY, startRank)
  endRank = min(MAX_RANK_FOR_DISPLAY, endRank)

  ownerId = None
  if request.user.is_authenticated():
    privateCustomer = Customer.objects.filter(auth_user=request.user,
                                              training_privacy=Customer.PRIVATE).first()
    ownerId = privateCustomer.customer_id if privateCustomer else None

  rankData = rankutils.latest_category_rank_data(category, country, categoryType, startRank,
                                                 endRank, start_in_seconds, end_in_seconds)
  appIdList = [entry['app_id'] for entry in rankData]
  paredAppIdList = [app_id for app_id in appIdList if app_id is not None]
  revenueData = rankutils.app_estimates_data(
      paredAppIdList, category, country, start_in_seconds, end_in_seconds, 'revenue',
      owner_id=ownerId)
  overallCategory = AppStoreCategory.objects.get(name='Overall', platform__id=market)
  overallRankData = rankutils.latest_app_rank_data(paredAppIdList, overallCategory.id, country,
                                                   categoryType, start_in_seconds, end_in_seconds)

  merge = {entry['app_id']: entry for entry in rankData}
  for entry in overallRankData:
    app_id = entry['app_id']
    merge[app_id]['series_data'].append(entry['series_data'][0])

  userVisibleCategories = visible_categories(request.user)
  if category in userVisibleCategories:
    for entry in revenueData:
      app_id = entry['app_id']
      merge[app_id]['series_data'].append(entry['series_data'][0])
  else:
    for entry in revenueData:
      app_id = entry['app_id']
      values = []
      for val in entry['series_data'][0]['current']:
        values.append(None)
      entry['series_data'][0]['current'] = values
      merge[app_id]['series_data'].append(entry['series_data'][0])

  ret = [merge[app_id] for app_id in appIdList]

  # Replace internal app_id values with bundle_ids and package names.
  id_map = rankutils.externalize_app_ids(appIdList, market)
  for entry in ret:
    entry['app_id'] = id_map[entry['app_id']]
  return HttpResponse(json.dumps(ret), content_type='application/json')


@never_cache
# category_type and category are numeric IDs
# country is 2 char country code
def graph_ranks_data_with_range_prod(
    request,
    market,
    categoryType,
    category,
    subcategory,
    country,
    page,
    numResults,
    start_in_seconds,
        end_in_seconds):
  # Arguments orginating from query parameters are passed as strings.
  if subcategory != "-999":
    category = subcategory
  market, categoryType, category, page, numResults, start_in_seconds, end_in_seconds = (
      int(market), int(categoryType), int(category), int(page), int(numResults),
      int(float(start_in_seconds)), int(float(end_in_seconds)))

  # Enforce sensible bounds on page and numResults to discourage URL manipulation.
  page = min(max(1, page), 100)
  numResults = min(max(1, numResults), 30)

  # page is 1-indexed
  startRank = 1 + (page - 1) * numResults
  endRank = startRank + numResults - 1
  startRank = min(MAX_RANK_FOR_DISPLAY, startRank)
  endRank = min(MAX_RANK_FOR_DISPLAY, endRank)

  ownerId = None
  if request.user.is_authenticated():
    privateCustomer = Customer.objects.filter(auth_user=request.user,
                                              training_privacy=Customer.PRIVATE).first()
    ownerId = privateCustomer.customer_id if privateCustomer else None

  rankData = rankutils.category_rank_data(category, country, categoryType, startRank, endRank,
                                          start_in_seconds, end_in_seconds)
  appIdList = [entry['app_id'] for entry in rankData]
  paredAppIdList = [app_id for app_id in appIdList if app_id is not None]
  revenueData = rankutils.app_estimates_data(
      paredAppIdList, category, country, start_in_seconds, end_in_seconds, 'revenue',
      owner_id=ownerId)
  overallCategory = AppStoreCategory.objects.get(name='Overall', platform__id=market)
  overallRankData = rankutils.app_rank_data(paredAppIdList, overallCategory.id, country,
                                            categoryType, start_in_seconds, end_in_seconds)

  merge = {entry['app_id']: entry for entry in rankData}
  for entry in overallRankData:
    app_id = entry['app_id']
    merge[app_id]['series_data'].append(entry['series_data'][0])

  userVisibleCategories = visible_categories(request.user)
  if category in userVisibleCategories:
    for entry in revenueData:
      app_id = entry['app_id']
      merge[app_id]['series_data'].append(entry['series_data'][0])
  else:
    for entry in revenueData:
      app_id = entry['app_id']
      values = []
      for val in entry['series_data'][0]['current']:
        values.append(None)
      entry['series_data'][0]['current'] = values
      merge[app_id]['series_data'].append(entry['series_data'][0])

  ret = [merge[app_id] for app_id in appIdList]

  # Replace internal app_id values with bundle_ids and package names.
  id_map = rankutils.externalize_app_ids(appIdList, market)
  for entry in ret:
    entry['app_id'] = id_map[entry['app_id']]
  return HttpResponse(json.dumps(ret), content_type='application/json')


def competitors_menu_info(request):
  platforms = rankutils.platforms()
  new_platforms = []
  for platform in platforms:
    p = {}
    p['id'] = platform['id']
    p['name'] = platform['name']
    p['countries'] = rankutils.countries(platform['id'])
    p['categories'] = rankutils.categories(platform['id'])
    p['rank_types'] = rankutils.rank_types(platform['id'])
    new_platforms.append(p)
  return HttpResponse(json.dumps(new_platforms), content_type='application/json')
