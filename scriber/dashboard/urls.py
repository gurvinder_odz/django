from django.conf.urls import patterns, url
from dashboard import views
from emailer import mailgun, snowball

urlpatterns = patterns('',
  url(r'^$', views.main_dashboard),
  url(r'^drilldown/(?P<series_name>.+)/(?P<product_id>.+)/$', views.demo_drilldown_product),
  url(r'^drilldown/(?P<series_name>.+)/$', views.demo_drilldown),
  url(r'^autocomplete/(?P<prefix>.+)/$', views.autocomplete),
  url(r'^customer_info/(?P<end_user_id>.+)/$', views.customer_info),
  url(r'^app_info_for_app_id/(?P<app_id>.+)/$', views.app_info_for_app_id),
  url(r'^app_info/(?P<bundle_id>.+)/$', views.app_info),
  url(r'^app_info_android/(?P<app_id>.+)/$', views.app_info_android),
  url(r'^get_products/$', views.products),
  url(r'^save_settings/$', views.save_settings),
  url(r'^competitors_menu_info/$', views.competitors_menu_info),
  url(r'^graph_data/(?P<product_ids>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_data_with_range),
  url(r'^graph_data_app/(?P<market>.+)/(?P<country>.+)/(?P<app_id>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_app_ranks_data_with_range),
  url(r'^graph_data_followed_apps/(?P<country_alpha2>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_followed_apps_data),
  url(r'^graph_data_followed_apps_estimates/(?P<country_alpha2>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/(?P<estimate_type>.+)/$', views.graph_followed_apps_estimates),
  url(r'^graph_data_realtime/(?P<series_name>.+)/(?P<product_ids>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_realtime_data_with_range_and_series),
  url(r'^series_data/competitors/(?P<market>.+)/(?P<categoryType>.+)/(?P<category>.+)/(?P<subcategory>.+)/(?P<country>.+)/(?P<page>.+)/(?P<numResults>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_rankings),
  url(r'^series_data/custom_events/(?P<product_ids>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_label_data_with_range),
  url(r'^series_data/(?P<product_ids>.+)/(?P<series_name>.+)/(?P<start_in_seconds>.+)/(?P<end_in_seconds>.+)/$', views.graph_data_with_range_and_series),
  url(r'^test_daily_email/(?P<email_for_report>.+)/$', mailgun.test_daily_email),
  url(r'^daily_snowball/$', snowball.snowball),
)
