#!/bin/bash

DJANGODIR=/webapps/scuserhome/scriber
DJANGO_SETTINGS_MODULE=scriber_app.settings.test     # which settings file should Django use

# Activate the virtual environment
cd $DJANGODIR
source ../bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Run unit tests
# TODO(d-felix): Investigate running unit tests with an in-memory database.
coverage run manage.py test --settings=${DJANGO_SETTINGS_MODULE}
