#!/bin/bash
 
NAME="scriber_app"                                  # Name of the application
DJANGODIR=/webapps/scuserhome/scriber           # Django project directory
SOCKFILE=/webapps/scuserhome/run/gunicorn.sock  # We will communicate using this unix socket
USER=scuser                                     # The user to run as
GROUP=webapps                                   # The group to run as
NUM_WORKERS=3                                   # How many worker processes should Gunicorn spawn
TIMEOUT=90                                      # The woker process timeout when serving a request
DJANGO_SETTINGS_MODULE=scriber_app.settings.test    # Which settings file should Django use
DJANGO_WSGI_MODULE=scriber_app.wsgi                 # WSGI module name
NEW_RELIC_CONFIG_FILE=/webapps/scuserhome/conf/newrelic.ini  # New Relic configuration file
 
echo "Starting $NAME as `whoami`"
 
# Activate the virtual environment
cd $DJANGODIR
source ../bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
export NEW_RELIC_CONFIG_FILE=$NEW_RELIC_CONFIG_FILE

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
 
# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /webapps/scuserhome/bin/newrelic-admin run-program ../bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --timeout $TIMEOUT \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=-
