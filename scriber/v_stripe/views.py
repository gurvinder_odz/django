import json
import logging
import urllib
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.decorators.http import require_POST
import requests
import stripe

_log = logging.getLogger(__name__)


# See https://stripe.com/docs/connect/getting-started

AUTHORIZE_URL = "https://connect.stripe.com/oauth/authorize"
TOKEN_URL = "https://connect.stripe.com/oauth/token"

# TODO(dfelix): Add to settings
# development ca_5rLh1E3etzToK95KJ1xDMmwSM12VK14t
CLIENT_ID = "ca_5rLh1E3etzToK95KJ1xDMmwSM12VK14t"  # Stripe Connect client id

# REDIRECT_URI Must also be in Connect settings
REDIRECT_URI = "http://scriber.io/stripe/connected/"
# REDIRECT_URI = "http://localhost/stripe/connected/"


def connect(request):
    """
    Issues an redirect to stripe for their oauth
    :param request:
    :return:
    """
    params = dict(
        client_id=CLIENT_ID,
        response_type="code",
        scope="read_only",
        redirect_uri=REDIRECT_URI,
    )
    return redirect("{0}?{1}".format(AUTHORIZE_URL, urllib.urlencode(params)))


def connected(request):
    """
    Saves stripe credentials and redirects to another page.

    Callback url after stripe oauth
    :param request:
    :return:
    """

    code = request.GET.get("code", "")
    scope = request.GET.get("scope", "")

    res = requests.post(TOKEN_URL, data={
        "client_id": CLIENT_ID,
        "client_secret": settings.STRIPE_SECRET_KEY,
        "code": code,
        "grant_type": "authorization_code",
    })

    data = res.json()
    print data
    if "error" in data:
        _log.error("stripe oauth2 error: {0}, {1}"
                   .format(data["error"], data["error_description"]))
    else:
        # res contains:
        # {
        #   "token_type": "bearer",
        #   "stripe_publishable_key": PUBLISHABLE_KEY,
        #   "scope": "read_write",
        #   "livemode": false,
        #   "stripe_user_id": USER_ID,
        #   "refresh_token": REFRESH_TOKEN,
        #   "access_token": ACCESS_TOKEN
        # }

        stripe_user_id = data.get("stripe_user_id", None)
        refresh_token = data["refresh_token"]
        access_token = data["access_token"]
        stripe_publishable_key = data["stripe_publishable_key"]

    # TODO(dfelix): UserStripe.save(scriber_user_id, stripe_user_id, access_token, refresh_token, stripe_publishable_key, scope)

    return redirect("/") 

@require_POST
def webhook(request):
    """
    Called by stripe on specific events.
    https://stripe.com/docs/webhooks
    https://stripe.com/docs/api#event_types
    """

    event = json.loads(request.body)

    stripe_user_id = event["user_id"]
    scriber_user_id = user_id_for_stripe_user_id(stripe_user_id)
    access_token = stripe_access_token_for_stripe_user(scriber_user_id)

    scoped_stripe = ScopedStripe(access_token)

    if not settings.DEBUG:
        # Test events are sent to production (as per docs).
        # Just ignore them.
        if not event["livemode"]:
            return HttpResponse()

        # Retrieve the event from stripe, as a form of validation (ensures
        # this request is for this user, and the request wasn't hijacked)

        event_test = scoped_stripe._retrieve_event(event["id"])
        assert event_test.id == event["id"]

    _log.debug("stripe webhook: event type: %s" % event['type'])
    event_type = event['type'].replace(".", "_")
    meth = getattr(scoped_stripe, event_type, None)

    if meth:
        return meth(event)
    return HttpResponse()


# TODO(dfelix):  This is where all the events are received, need to save them
class ScopedStripe(object):
    """
    A stripe wrapper to:

    1.  Isolate stripe calls (to ensure they are always called with the users
        api key
    2.  Handler for stripe events
    """

    def __init__(self, api_key):
        self.api_key = api_key

    def _retrieve_event(self, event_id):
        return stripe.Event.retrieve(event_id, api_key=self.api_key)

    def invoice_payment_failed(self, event):
        """
        Occurs whenever an invoice attempts to be paid, and the payment fails.
        This can occur either due to a declined payment, or because the customer
        has no active card. A particular case of note is that if a customer with
        no active card reaches the end of its free trial, an
        invoice.payment_failed notification will occur.
        """
        # https://stripe.com/docs/api#invoice_object
        invoice = event["data"]["object"]
        return HttpResponse()

    def invoice_payment_succeeded(self, event):
        """
        Occurs whenever an invoice attempts to be paid, and the payment succeeds
        """
        # https://stripe.com/docs/api#invoice_object
        invoice = event["data"]["object"]
        return HttpResponse()

    def customer_subscription_deleted(self, event):
        """
        Occurs whenever a customer ends their subscription.
        """
        return HttpResponse()

    def customer_subscription_updated(self, event):
        """
        Occurs whenever a subscription changes. Examples would include
        switching from one plan to another, switching status from trial to
        active, and when a subscription is renewed
        """
        stripe_sub = event["data"]["object"]
        status = stripe_sub["status"]

        # status can be trialing, active, past_due, canceled, unpaid
        if status == "active":
            pass
        elif status in ["canceled"]:
            pass
        elif status in ["unpaid", "past_due"]:
            pass
        return HttpResponse()

    def account_application_deauthorized(self, event):
        """
        Occurs when a user deauthorizes the application
        """
        return HttpResponse()

    def ping(self, data):
        """
        May be sent by Stripe at any time to see if a provided webhook
        URL is working.
        """
        return HttpResponse()


def stripe_access_token_for_stripe_user(user_id):
    # TODO(dfelix): Return an api key for the given user_id
    return "stripe_access_token"


def user_id_for_stripe_user_id(stripe_user_id):
    # TODO(dfelix): Return a customer id
    return "scriber_user_id"