from django.conf.urls import patterns, url
from v_stripe import views

urlpatterns = patterns('',
  url(r'^webhook/$', views.webhook),
  url(r'^connect/$', views.connect),
  url(r"^connected/$", views.connected)
)