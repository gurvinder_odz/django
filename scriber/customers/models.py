import logging
import re

from core.customfields import EncryptedCharField
from core.models import TimestampedModel
from core.seleniumutils import click_through_to_new_page, submit_to_new_page
from customers import webdriverfactory
from django.conf import settings
from django.db import models
from appstore.models import AppStoreApp
from localization.models import Country

GOOGLE_ACCOUNTS_DOMAIN = 'accounts.google.com'
GOOGLE_DEV_CONSOLE_URL = 'https://console.developers.google.com/'

ITC_LOGIN_PAGE = 'https://itunesconnect.apple.com'

logger = logging.getLogger(__name__)

class Customer(TimestampedModel):
  customer_id = models.AutoField(primary_key=True)
  auth_user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, db_column="auth_user",
      related_name="scriber_customer_auth_user")
  name = models.CharField(max_length=256, null=True)
  has_hidden_setup = models.BooleanField(default=False)
  send_daily_report = models.BooleanField(default=True)
  send_marketing_email = models.BooleanField(default=True)
  timezone = models.CharField(max_length=256, default='America/Los_Angeles')

  PUBLIC = 'Public'
  PRIVATE = 'Private'
  PRIVACY_CHOICES = (
    (PUBLIC, 'Public'),
    (PRIVATE, 'Private'),
  )
  # TODO(d-felix): Move to app-level privacy settings. Keep this customer-level
  # setting to use as a default when a customer releases a new app.
  training_privacy = models.CharField(choices=PRIVACY_CHOICES, max_length=64, default='Public')

  class Meta:
    db_table = 'customers'


class CustomerApiKey(TimestampedModel):
  api_key_id = models.AutoField(primary_key=True)
  api_key = models.CharField(max_length=256, unique=True, db_index=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id")

  class Meta:
    db_table = 'customer_api_keys'


class CustomerExternalLoginInfo(TimestampedModel):
  login_info_id = models.AutoField(primary_key=True)
  customer_id = models.ForeignKey(Customer, db_column="customer_id", null=True)
  username = EncryptedCharField(max_length=256)
  password = EncryptedCharField(max_length=256)
  apple_vendor_id = models.BigIntegerField(null=True, db_index=True)
  refresh_token = EncryptedCharField(max_length=256, null=True)
  gc_bucket_id = models.CharField(max_length=256, null=True)
  is_active = models.BooleanField(default=True)

  GOOGLE_CLOUD = 'Google Cloud'
  ITUNES_CONNECT = 'iTunes Connect'
  EXTERNAL_SERVICE_CHOICES = (
    (GOOGLE_CLOUD, 'Google Cloud'),
    (ITUNES_CONNECT, 'iTunes Connect'),
  )
  external_service = models.CharField(choices=EXTERNAL_SERVICE_CHOICES, max_length=256,
      db_index=True)

  def is_valid(self):
    # TODO(d-felix): Establish a high-priority celery queue for login validation
    # so that we don't have too many browsers in memory.
    # TODO(d-felix): Perform the lazy PhantomJS initialization at system startup.
    # Note that this webdriver is not threadsafe.
    driver = webdriverfactory.phantom_js()
    driver.delete_all_cookies()

    if self.external_service == self.GOOGLE_CLOUD:
      driver.get(GOOGLE_DEV_CONSOLE_URL)

      # The Email field may be prepopulated and read-only.
      # TODO(d-felix): This undesirable behavior is caused by our PhantomJS
      # singleton maintaining some state between uses despite cookie deletion.
      # Investigate the possibility of a faster workaround.
      changeAccountLinks = driver.find_elements_by_id("account-chooser-link")
      if changeAccountLinks:
        click_through_to_new_page(changeAccountLinks[0], timeout_secs=10)
        addAccountLink = driver.find_element_by_id("account-chooser-add-account")
        click_through_to_new_page(addAccountLink, timeout_secs=10)

      loginForm = driver.find_element_by_id("gaia_loginform")
      loginForm.find_element_by_id("Email").send_keys(self.username)
      loginForm.find_element_by_id("Passwd").send_keys(self.password)
      signInButton = loginForm.find_element_by_id("signIn")
      click_through_to_new_page(signInButton, timeout_secs=10)
      currentUrl = driver.current_url
      domain = re.match(r'^https?://([^/]+)/.*', currentUrl).group(1)
      if domain == GOOGLE_ACCOUNTS_DOMAIN:
        logger.warning('Failed to verify Google Cloud login for customer %s. Page source: %s' %
            (self.customer_id.customer_id, driver.page_source))
        return False
      logger.info('Treating google login as valid since form submission redirected to %s' % domain)
      return True

    elif self.external_service == self.ITUNES_CONNECT:
      driver.get(ITC_LOGIN_PAGE)
      loginForm = driver.find_element_by_name('appleConnectForm')
      loginForm.find_element_by_id('accountname').send_keys(self.username)
      loginForm.find_element_by_id('accountpassword').send_keys(self.password)
      submit_to_new_page(loginForm)

      # Apple will redisplay the login form upon a failed attempt.
      # Note that the use of core.seleniumutils.submit_to_new_page() is
      # essential here due to the existence of the appleConnectForm element
      # on the previous page.
      newLoginForms = driver.find_elements_by_name('appleConnectForm')
      if len(newLoginForms) > 0:
        logger.warning('Failed to verify iTunes Connect login for customer %s. Page source: %s' %
            (self.customer_id.customer_id, driver.page_source))
        return False
      return True

  class Meta:
    db_table = 'customer_external_login_info'


class CustomerAppFollow(TimestampedModel):
  id = models.AutoField(primary_key=True)
  customer = models.ForeignKey(Customer, db_column="customer_id")
  app = models.ForeignKey(AppStoreApp, db_column="app_id")
  country = models.ForeignKey(Country, db_column='country', null=True)

  class Meta:
    db_table = 'customer_app_follow'

