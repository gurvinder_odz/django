from django.conf.urls import patterns, url
from customers import views

urlpatterns = patterns('',
  url(r'^$', views.settings),
  url(r'^hide_setup/$', views.hide_setup),
  url(r'^check_report_sync_status/$', views.check_report_sync_status),
  url(r'^update_sales_reports_account/$', views.update_sales_reports_account),
  url(r'^delete_sales_reports_account/$', views.delete_sales_reports_account),
  url(r'^signup/', views.signup),
  url(r'^purchase_plan/(?P<product_id>.+)/$', views.purchase_product),
  url(r'^mailchimp_webhook/', views.mailchimp_webhook),
  url(r'^toggle_newsletter_subscription/', views.toggle_newsletter_subscription),
  url(r'^toggle_daily_report/', views.toggle_daily_report),
  url(r'^get_user_settings/', views.get_user_settings),
  url(r'^subscribe_ajax/$', views.subscribe_ajax),
  url(r'^change_card_ajax/$', views.change_card_ajax),
  url(r'^change_plan_ajax/$', views.change_plan_ajax),
  url(r'^cancel_ajax/$', views.cancel_ajax),
)
