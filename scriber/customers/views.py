import copy
import logging, json
import mailchimp
from mailchimp import ListNotSubscribedError
import django.conf
from base64 import b64encode
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import logout, login, authenticate
from django.contrib import messages
from django.core.cache import cache
from django import forms
from os import urandom
import re
import dashboard
import payments 
import stripe
from celery import chain
from ingestor.utils import sdk_versions_for_auth_user
from core.apikey import new_api_key
from core.timeutils import utc_epoch
from customers import ephemera
from customers.models import Customer, CustomerApiKey, CustomerExternalLoginInfo
from customers.tasks import fetch_external_service_login_info
from metrics.tasks.reporttasks import backfill_daily_sales_report_metrics
from scriber.api import Scriber
from traceback import format_exc
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt


logger = logging.getLogger(__name__)


from payments.models import Customer as PaymentsCustomer
from django.views.decorators.http import require_POST
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required



def wrap_stripe_error(ajax_view):
    """Return a JSONResponse with status 500 if any stripe calls fail for some reason."""
    def wrapper(request):
        try:
            return ajax_view(request)
        except stripe.StripeError as e:
            logging.error('Stripe Error')
            logging.error(e)
            if request.method == "POST":
                logging.error(request.POST)
            return JSONResponse(content=u"%s" % e, status=500)
    return wrapper


@require_POST
@login_required
@wrap_stripe_error
def subscribe_ajax(request):
    """"""
    stripe_token = request.POST.get("stripe_token")
    plan_id = request.POST.get("plan_id", None)

    try:
        customer = request.user.customer
    except ObjectDoesNotExist:
        customer = PaymentsCustomer.create(request.user)

    customer.update_card(stripe_token)
    customer.subscribe(plan_id)
    messages.success(request, 'Your subscription is now active.')
    logging.info('Your subscription is now active.')
    return HttpResponse(json.dumps({"success":True}), content_type='application/json')


@require_POST
@login_required
@wrap_stripe_error
def change_plan_ajax(request):
    """"""
    plan_id = request.POST.get("plan_id", None)
    request.user.customer.subscribe(plan_id)
    messages.info(request, 'Subscription plan changed to: %s' % settings.PAYMENTS_PLANS[plan_id]['name'])
    return HttpResponse(json.dumps({'message': 'Subscription changed.'}), content_type='application/json')


@require_POST
@login_required
@wrap_stripe_error
def change_card_ajax(request):
    """"""
    if request.POST.get("stripe_token", None):
        customer = request.user.customer
        send_invoice = customer.card_fingerprint == ""
        customer.update_card(
            request.POST.get("stripe_token")
        )
        if send_invoice:
            customer.send_invoice()
        customer.retry_unpaid_invoices()
        messages.success(request, 'Card information was changed successfully.')
        return HttpResponse(json.dumps({'message': 'Card was updated.'}), content_type='application/json')
    else:
        raise stripe.StripeError('Invalid Stripe token')


@require_POST
@login_required
@wrap_stripe_error
def cancel_ajax(request):
    """"""
    request.user.customer.cancel(at_period_end=False)
    messages.info(request, "You have unsubscribed successfully.")
    return HttpResponse(json.dumps({'message': 'Cancellation successful.'}), content_type='application/json')


def get_mailchimp_api():
    return mailchimp.Mailchimp('02887fb3f4fe9f47aad7155eb533f55b-us10') #your api key here


def itunes_and_google_account_lists(user):
  itunes_accounts = []
  google_play_accounts = []
  customer = Customer.objects.get(auth_user=user)
  connected_accounts = CustomerExternalLoginInfo.objects.filter(customer_id=customer.customer_id,
      is_active=True)
  for c in connected_accounts:
    copied_c = {}
    copied_c['username'] = c.username
    copied_c['external_service'] = c.external_service
    if c.external_service == 'iTunes Connect':
      itunes_accounts.append(copied_c)
    if c.external_service == 'Google Cloud':
      google_play_accounts.append(copied_c)
  return itunes_accounts, google_play_accounts


@login_required
def settings(request):
  has_subscription = False
  if hasattr(request.user, 'customer'):
    if request.user.customer.has_active_subscription():
      has_subscription = True
  apiKey = CustomerApiKey.objects.filter(customer_id__auth_user=request.user).first().api_key

  itunes_accounts, google_play_accounts = itunes_and_google_account_lists(request.user)

  return render(
    request, 
    'account.html', 
    {'has_subscription': has_subscription,
     'sdk_versions': sdk_versions_for_auth_user(request.user),
     'itunes_accounts': itunes_accounts,
     'google_play_accounts': google_play_accounts,
     'apiKey': apiKey, 
     "STRIPE_PUBLIC_KEY":django.conf.settings.STRIPE_PUBLIC_KEY
    } 
  )


@require_POST
def signup(request):
  # track signups with Scriber
  s = Scriber("OXTZhX5tBFKRdcj31o04zxuVd9gyQeGycT9sNvKOrKY", "scriber.io")

  form_info = json.loads(request.body)
  if not re.match("[^@]+@[^@]+\.[^@]+", form_info['username']):
    s.record_event(form_info['username'], 'Signup', {'success':False, 'error':'bad email'})
    return HttpResponse(json.dumps({'error':"That's not a valid email", 'error_type':'email'}), 
                            content_type='application/json')
  elif not form_info['password']: 
    s.record_event(form_info['username'], 'Signup', {'success':False, 'error':'no password'})
    return HttpResponse(json.dumps({'error':"Enter a password", 'error_type':'password'}), 
                            content_type='application/json')
  else:
      try:
        user = User.objects.create_user(form_info['username'], form_info['username'], form_info['password'])
        s.record_event(form_info['username'], 'Signup', {'success':True})
      except:
        s.record_event(form_info['username'], 'Signup', {'success':False, 'error':'already registered'})
        return HttpResponse(json.dumps({'error':"That email is already registered."}), 
                            content_type='application/json')

      i = 0
      for word in form_info['name'].split(' '):
        if i == 0:
          user.first_name = word
        if i == len(form_info['name'].split(' ')) - 1 and len(form_info['name'].split(' ')) > 1:
          user.last_name = word
        i += 1
      user.save()
      user = authenticate(username=form_info['username'], password=form_info['password'])
      login(request, user)
      customer = Customer(auth_user=user)
      customer.save()
      customerApiKey = CustomerApiKey(api_key=new_api_key(), customer_id=customer)
      customerApiKey.save()
      try:
        m = get_mailchimp_api()
        m.lists.subscribe('10ac77810b', {'email':form_info['username']}, double_optin=False)
      except mailchimp.ListAlreadySubscribedError:
        s.record_event(form_info['username'], 'Mailchimp Error', {'success':False, 'error':'email already subscribed'})
        return HttpResponse(json.dumps({'error':"That email is already subscribed to the list"}), 
                            content_type='application/json')
      except mailchimp.Error, e:
        return HttpResponse(json.dumps({'error':'%s' % e,
                                        'error_class':'%s' % e.__class__}), 
                            content_type='application/json')
      return HttpResponse(json.dumps({}), content_type='application/json')


@require_POST
@login_required
def update_sales_reports_account(request):
  s = Scriber("OXTZhX5tBFKRdcj31o04zxuVd9gyQeGycT9sNvKOrKY", "scriber.io")
  username = request.user.username

  form_info = json.loads(request.body)
  if 'platform' in form_info and 'username' in form_info and 'password' in form_info:
    customer = Customer.objects.get(auth_user=request.user)
    connected_accounts = CustomerExternalLoginInfo.objects.filter(customer_id=customer,
        is_active=True)
    found = False
    for account in connected_accounts:
      if (account.username == form_info['username'] and
          account.external_service == form_info['platform']):
        # TODO(d-felix): Validate the new password before updating.
        account.password = form_info['password']
        account.save()
        found = True;
        kickoff_backfill(account, customer)
        s.record_event(username, 'Sales Account Update', {'success':True, 'platform':form_info['platform']})
        return HttpResponse(json.dumps({'message':'Password updated.'}), content_type='application/json')        
    if not found:
      new_account = CustomerExternalLoginInfo(customer_id=customer, 
                                              username=form_info['username'], 
                                              password=form_info['password'],
                                              external_service=form_info['platform'],
                                              is_active=False)
      new_account.save()
      # Note that account validation is slow and blocking.
      try:
        if new_account.is_valid():
          new_account.is_active = True
          s.record_event(username, 'Sales Account Sync', {'success':True, 'platform':form_info['platform']})
          new_account.save()
          kickoff_backfill(new_account, customer)
          return HttpResponse(json.dumps({}),
              content_type='application/json')
        else:
          s.record_event(username, 'Sales Account Sync', {'error':'Wrong username or password', 'success':False, 'platform':form_info['platform']})
          return HttpResponse(json.dumps({"error": "Wrong username or password."}), content_type='application/json')
      except Exception:
        logger.warning('Attempted sales account sync failed for customer %s. Traceback: %s, ' %
            (customer.customer_id, format_exc()))
        s.record_event(username, 'Sales Account Sync', {'error':'BadStatusLine', 'success':False, 'platform':form_info['platform']})
        return HttpResponse(json.dumps({"error": "Something went wrong."}),
            content_type='application/json')

  return HttpResponse(json.dumps({"error":"NOT SET UP YET"}), content_type='application/json')


def kickoff_backfill(account, customer):
  # Generate a cache key that can be used to check on the progress of the backfill.
  doneToken = b64encode(urandom(20)).rstrip('==')
  ephemera.set_key(customer.customer_id, doneToken, account.external_service)
  backfill_tasks = chain(
      fetch_external_service_login_info.s(login_info_id=account.login_info_id),
      backfill_daily_sales_report_metrics.s(login_info_id=account.login_info_id,
        done_token=doneToken))
  backfill_tasks.apply_async()


@login_required
def hide_setup(request):
  customer = Customer.objects.filter(auth_user=request.user).first()
  customer.has_hidden_setup = True
  customer.save();
  return HttpResponse(json.dumps({"status": "success"}),
            content_type='application/json')


@never_cache
@login_required
def check_report_sync_status(request):
  GOOGLE_CLOUD = CustomerExternalLoginInfo.GOOGLE_CLOUD
  ITUNES_CONNECT = CustomerExternalLoginInfo.ITUNES_CONNECT
  status = {}
  customer = Customer.objects.filter(auth_user=request.user).first()
  items = ephemera.get_keys(customer.customer_id)
  if items:
    for key in items:
      if items[key]['topic'] == GOOGLE_CLOUD:
        val = cache.get(key)
        age = utc_epoch() - items[key]['creation_time']
        # Allow for completion times of up to 1 hour.
        # Note that this assumes a cache timeout of at least one hour for the
        # doneToken.
        if not val and age <= 60*60:
          status[GOOGLE_CLOUD] = 'pending'
        # Don't overwrite existing GOOGLE_CLOUD status in order to give higher
        # precedence to 'pending' statuses.
        elif (val == True) and (GOOGLE_CLOUD not in status):
          status[GOOGLE_CLOUD] = 'finished'
      elif items[key]['topic'] == ITUNES_CONNECT:
        val = cache.get(key)
        age = utc_epoch() - items[key]['creation_time']
        if not val and age <= 60*60:
          status[ITUNES_CONNECT] = 'pending'
        elif (val == True) and (ITUNES_CONNECT not in status):
          status[ITUNES_CONNECT] = 'finished'

  return HttpResponse(json.dumps({'status': status}), content_type='application/json')


@require_POST
@login_required
def delete_sales_reports_account(request):
  # \todo remove saved reports?
  form_info = json.loads(request.body)
  if 'platform' in form_info and 'username' in form_info:
    customer = Customer.objects.get(auth_user=request.user)
    connected_accounts = CustomerExternalLoginInfo.objects.filter(customer_id=customer)
    foundAccount = None
    for account in connected_accounts:
      if account.username == form_info['username']:
        foundAccount = account
        break
    if foundAccount:
      foundAccount.is_active = False
      foundAccount.save()
      return HttpResponse(json.dumps({"success":True}), content_type='application/json')
    return HttpResponse(json.dumps({"error":"Account not found"}), content_type='application/json')


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given name, email, and password.
    """
    error_messages = {
        'duplicate_email': ("A user with that email already exists."),
    }


    class Meta:
        model = User
        fields = ("first_name","last_name","email")

    full_name = forms.CharField(label=("Full Name"))

    username = forms.CharField(label=("Username"),
        widget=forms.EmailInput)

    password = forms.CharField(label=("Password"),
        widget=forms.PasswordInput)

    def clean_password(self):
        password = self.cleaned_data["password"]
        return password

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_email'],
            code='duplicate_email',
        )

def logout(request):
    logout(request)
    return HttpResponseRedirect('/')


# listen for unsubscribes from mailchimp
@csrf_exempt
def mailchimp_webhook(request):
  if request.POST.get("type") == "unsubscribe":
    email = request.POST.get("data[email]")
    customers = Customer.objects.filter(auth_user__email=email)
    if len(customers):
      customer = customers[0]
      customer.send_marketing_email = False;
      customer.save()

  return HttpResponse(json.dumps({"success":True}), content_type='application/json')


# toggle newsletter subscribe from account screen
@never_cache
def toggle_daily_report(request):
  customer = Customer.objects.get(auth_user=request.user)
  if customer.send_daily_report:
    customer.send_daily_report = False
  else:
    customer.send_daily_report = True
  customer.save()
  return HttpResponse(json.dumps({"current_value":customer.send_daily_report}), content_type='application/json')



# toggle newsletter subscribe from account screen
@never_cache
def toggle_newsletter_subscription(request):
  customer = Customer.objects.get(auth_user=request.user)
  if customer.send_marketing_email:
    customer.send_marketing_email = False
    m = get_mailchimp_api()
    try:
      m.lists.unsubscribe('10ac77810b', {'email':request.user.email})
    except ListNotSubscribedError:
      logging.error("user removed sub via MailChimp, and we didn't get the web hook - should only be first few users ever")
  else:
    customer.send_marketing_email = True
    m = get_mailchimp_api()
    m.lists.subscribe('10ac77810b', {'email':request.user.email}, double_optin=False)
  customer.save()
  return HttpResponse(json.dumps({"current_value":customer.send_marketing_email}), content_type='application/json')


@never_cache
def get_user_settings(request):
  customer = Customer.objects.get(auth_user=request.user)
  customer.save()
  return HttpResponse(json.dumps({"send_marketing_email":customer.send_marketing_email,
                                  "send_daily_report":customer.send_daily_report}), content_type='application/json')


def purchase_product(request, product_id):
  return render(request, "purchase_product.html", {"plans":products(), 
                                                   "plan_to_purchase":product_id})

#todo real data for purchase plans
def products():
  ''' free_in_dev_mode_string = "Use the service free in Developer mode."
     'sdk_verions': sdk_versions_for_auth_user(request.user),
  refund_string = "Activate your account risk free. Get a full refund within 60 days of starting."
  stats_string = "iOS and Android stats"
  subs_string = "Subscription & IAP Analytics"
  up_to_7500_string = "Up to $7,500per month earnings"
  up_to_25000_string = "Up to $25,000 per month earnings"
  up_to_100000_string = "Up to $100,000 per month earnings"
  email_support_string = "Email Support"
  email_phone_support_string = "Email & Phone Support"
  '''
  return [ django.conf.settings.PAYMENTS_PLANS["small"],
           
         ]

