# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='timezone',
            field=models.CharField(default=b'America/Los_Angeles', max_length=256),
            preserve_default=True,
        ),
    ]
