# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0007_auto_20150311_0043'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerapikey',
            name='api_key',
            field=models.CharField(unique=True, max_length=256, db_index=True),
            preserve_default=True,
        ),
    ]
