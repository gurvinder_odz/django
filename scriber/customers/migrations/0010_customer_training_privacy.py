# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0009_customer_has_hidden_setup'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='training_privacy',
            field=models.CharField(default=b'Public', max_length=64, choices=[(b'Public', b'Public'), (b'Private', b'Private')]),
            preserve_default=True,
        ),
        migrations.RunSQL(
          sql="""
            UPDATE
              customers
            SET training_privacy = 'Private'
            WHERE
              customer_id IN (
                SELECT
                  c.customer_id customer_id
                FROM customers c JOIN auth_user au ON (c.auth_user = au.id)
                WHERE
                 au.username IN ('jared@scribd.com', 'Justin@stoneblade.com') 
              )
            ;
            """,
        ),
    ]
