# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0005_auto_20150305_0017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerexternallogininfo',
            name='external_service',
            field=models.CharField(db_index=True, max_length=256, choices=[(b'iTunes Connect', b'iTunes Connect')]),
            preserve_default=True,
        ),
    ]
