# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0006_auto_20150306_0144'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerexternallogininfo',
            name='gc_bucket_id',
            field=models.CharField(max_length=256, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerexternallogininfo',
            name='refresh_token',
            field=core.customfields.EncryptedCharField(max_length=256, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='customerexternallogininfo',
            name='external_service',
            field=models.CharField(db_index=True, max_length=256, choices=[(b'Google Cloud', b'Google Cloud'), (b'iTunes Connect', b'iTunes Connect')]),
            preserve_default=True,
        ),
    ]
