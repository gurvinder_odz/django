# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0008_auto_20150317_0118'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='has_hidden_setup',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
