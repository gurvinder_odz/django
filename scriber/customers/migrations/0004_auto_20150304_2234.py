# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20150130_2045'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerExternalLoginInfo',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('login_info_id', models.AutoField(serialize=False, primary_key=True)),
                ('apple_vendor_id', models.BigIntegerField(null=True)),
                ('customer_id', models.ForeignKey(db_column=b'customer_id', to='customers.Customer', null=True)),
            ],
            options={
                'db_table': 'customer_external_login_info',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='customer',
            name='name',
            field=models.CharField(max_length=256, null=True),
            preserve_default=True,
        ),
    ]
