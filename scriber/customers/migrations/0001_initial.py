# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('customer_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('auth_user', models.ForeignKey(db_column=b'auth_user', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'customers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CustomerApiKey',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('api_key_id', models.AutoField(serialize=False, primary_key=True)),
                ('api_key', models.CharField(unique=True, max_length=256)),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'customer_api_keys',
            },
            bases=(models.Model,),
        ),
    ]
