# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import core.customfields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20150304_2234'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerexternallogininfo',
            name='external_service',
            field=models.CharField(default='iTunes Connect', max_length=256, choices=[(b'iTunes Connect', b'iTunes Connect')]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customerexternallogininfo',
            name='is_active',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerexternallogininfo',
            name='password',
            field=core.customfields.EncryptedCharField(default='', max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customerexternallogininfo',
            name='username',
            field=core.customfields.EncryptedCharField(default='', max_length=256),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customerexternallogininfo',
            name='apple_vendor_id',
            field=models.BigIntegerField(null=True, db_index=True),
            preserve_default=True,
        ),
    ]
