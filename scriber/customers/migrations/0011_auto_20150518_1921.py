# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0010_customer_training_privacy'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='send_daily_report',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='send_marketing_email',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
