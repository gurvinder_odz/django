# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('localization', '0004_auto_20150429_1647'),
        ('customers', '0012_customerappfollow'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerappfollow',
            name='country',
            field=models.ForeignKey(db_column=b'country', to='localization.Country', null=True),
            preserve_default=True,
        ),
    ]
