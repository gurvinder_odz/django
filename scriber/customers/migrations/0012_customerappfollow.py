# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appstore', '0020_remove_appstorerankrequest_rank_source'),
        ('customers', '0011_auto_20150518_1921'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerAppFollow',
            fields=[
                ('creation_time', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('app_id', models.ForeignKey(to='appstore.AppStoreApp', db_column=b'app_id')),
                ('customer_id', models.ForeignKey(to='customers.Customer', db_column=b'customer_id')),
            ],
            options={
                'db_table': 'customer_app_follow',
            },
            bases=(models.Model,),
        ),
    ]
