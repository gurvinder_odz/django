# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_customer_timezone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='auth_user',
            field=models.ForeignKey(related_name='scriber_customer_auth_user', db_column=b'auth_user', to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
