CREATE DATABASE sdb;

DO
$body$
BEGIN
   IF NOT EXISTS (
      SELECT *
      FROM   pg_catalog.pg_user
      WHERE  usename = 'sdb') THEN
	  CREATE USER sdb WITH PASSWORD 'sdb7H4D2AL8J9F4E2';
   END IF;
END
$body$;

GRANT ALL PRIVILEGES ON DATABASE sdb to sdb;